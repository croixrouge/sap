function handleAnnSelector(selector) {
    if (selector) {
        if (selector.value == "autre") show(document.querySelector('#id_annonceur_txt'));
        else hide(document.querySelector('#id_annonceur_txt'));
    }
}

document.addEventListener("DOMContentLoaded", () => {
    attachHandlerSelector(document, '#id_annonceur', 'change', (ev) => {handleAnnSelector(ev.target)});
    handleAnnSelector(document.querySelector('#id_annonceur'));
});
