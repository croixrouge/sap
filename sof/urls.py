from django.urls import path

from croixrouge import views as cr_views

from . import views
from .forms import BilanForm
from .models import Bilan

urlpatterns = [
    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='sof-famille-list'),
    path('famille/attente/', views.FamilleListView.as_view(mode='attente'), name='sof-famille-attente'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='sof-famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='sof-famille-edit'),

    # Prestations
    path('prestation/menu/', views.PrestationMenu.as_view(), name='sof-prestation-menu'),
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(), name='sof-journal-list'),
    path('famille/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(), name='sof-prestation-famille-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='sof-prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='sof-prestation-delete'),
    path('prestation_gen/list/', views.PrestationListView.as_view(), name='sof-prestation-gen-list'),
    path('prestation_gen/add/', views.PrestationCreateView.as_view(), name='sof-prestation-gen-add'),

    # Doc. à imprimer
    path('famille/<int:pk>/print-info/', views.CoordonneesPDFView.as_view(), name='sof-print-coord-famille'),
    path('famille/<int:pk>/print-journal', views.JournalPDFView.as_view(), name='sof-print-journal'),
    path('bilan/<int:pk>/print/', views.BilanPDFView.as_view(), name='sof-print-bilan'),

    # Suivi, agenda, suivis terminés
    path('famille/<int:pk>/suivi/', views.SOFSuiviView.as_view(), name='sof-famille-suivi'),
    path('famille/<int:pk>/intervenant/add/', views.SuiviIntervenantCreate.as_view(), name='sof-intervenant-add'),
    path('famille/<int:pk>/intervenant/<int:obj_pk>/edit/', views.SuiviIntervenantUpdateView.as_view(),
         name='sof-intervenant-edit'),
    path('famille/<int:pk>/agenda/', views.AgendaSuiviView.as_view(), name='sof-famille-agenda'),
    path('famille/<int:pk>/bilan/add/',
         cr_views.BilanEditView.as_view(model=Bilan, form_class=BilanForm, is_create=True),
         name='famille-bilansof-add'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/', cr_views.BilanDetailView.as_view(model=Bilan),
         name='famille-bilansof-view'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/edit/',
         cr_views.BilanEditView.as_view(model=Bilan, form_class=BilanForm),
         name='famille-bilansof-edit'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/delete/', cr_views.BilanDeleteView.as_view(model=Bilan),
         name='famille-bilansof-delete'),
    path('famille/archivable/list/', views.FamilleArchivableListe.as_view(), name='sof-famille-archivable'),

    path('suivis_termines/', views.SuivisTerminesListView.as_view(), name='sof-suivis-termines'),
]
