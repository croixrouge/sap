from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famillesof',
            options={'permissions': {('can_archivesof', 'Archiver les dossiers SOF')}},
        ),
    ]
