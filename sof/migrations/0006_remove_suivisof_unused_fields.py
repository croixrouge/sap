from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0005_suivisof_annonceur'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivisof',
            name='collaboration',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='crise',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='remarque',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='ressource',
        ),
    ]
