from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0002_remove_cantransfer_perm'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivisof',
            name='aides',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='autres_contacts',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='autres_pers_presentes',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='competences',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='dates_demande',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='difficultes',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='disponibilites',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='pers_famille_presentes',
        ),
        migrations.RemoveField(
            model_name='suivisof',
            name='ref_presents',
        ),
    ]
