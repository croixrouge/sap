from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0003_remove_suivisof_demande_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationsof',
            name='avec_famille',
            field=models.BooleanField(default=True, verbose_name='Rendez-vous avec famille'),
        ),
    ]
