from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0004_prestationsof_avec_famille'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivisof',
            name='annonceur',
            field=models.CharField(blank=True, choices=[('contact', 'Formulaire de contact'), ('collab_crne', 'Collaborateurice CRNE'), ('serv_soc', 'Service social'), ('sagefemme', 'Sage-femme'), ('connaiss', 'Connaissance'), ('autre', 'Autre (préciser)')], max_length=15, verbose_name='Service annonceur'),
        ),
    ]
