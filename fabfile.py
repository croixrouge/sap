import getpass

from fabric import task
from invoke import Context, Exit

APP_DIR = '/var/www/croixrouge'
VIRTUALENV_DIR = '/var/venv/croixrouge/bin/activate'
default_db_owner = getpass.getuser()

MAIN_HOST = 'sap.croix-rouge-ne.ch'
DEPLOY_USER = 'deploy'
DEPLOY_HOST_DSN = f"{DEPLOY_USER}@{MAIN_HOST}"


@task(hosts=[DEPLOY_HOST_DSN])
def deploy(conn):
    """
    Deploy project with latest GitLab code
    """
    with conn.cd(APP_DIR):
        conn.run("git pull")
        with conn.prefix('source %s' % VIRTUALENV_DIR):
            conn.run("python manage.py migrate")
            conn.run("python manage.py collectstatic --noinput")
        conn.run("touch common/wsgi.py")


def restore_db_from_file(dbname, source_file):
    local = Context()

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    def exist_username(user):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return user in res.stdout.split()

    if source_file.endswith('.gz'):
        local.run(f'gunzip {source_file}')
        source_file = source_file[:-3]

    if exist_local_db(dbname):
        rep = input('A local database named "{}" already exists. Overwrite? (y/n)'.format(dbname))
        if rep == 'y':
            local.run('psql -d postgres -c "DROP DATABASE {};"'.format(dbname))
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    if source_file.endswith('.sql'):
        local.run(f'psql {dbname} < {source_file}')
    else:
        local.run(f'pg_restore --no-owner --no-privileges -d {dbname} {source_file}')
    local.run(f'psql -d {dbname} -c "DELETE FROM otp_totp_totpdevice;"')
    local.run(f'psql -d {dbname} -c "DELETE FROM otp_static_statictoken;"')
    local.run(f'psql -d {dbname} -c "DELETE FROM otp_static_staticdevice;"')
    local.run(f'rm {source_file}')


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='croixrouge'):
    """ Dump a remote database and load it locally
        !!!! Call the function with: fab clone-remote-db !!!!
     """
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    tmp_path = '/tmp/{db}.dump'.format(db=dbname)
    conn.sudo('pg_dump --no-owner --no-privileges -Fc {db} > {tmp}'.format(db=dbname, tmp=tmp_path), user='postgres')
    conn.get(tmp_path, None)
    conn.run('rm {}'.format(tmp_path))
    restore_db_from_file(dbname, f'{dbname}.dump')


@task(hosts=[MAIN_HOST])
def clone_backup_db(conn, dbname='croixrouge'):
    res = conn.run(f'ls /var/lib/autopostgresqlbackup/daily/{dbname}/', hide='stdout')
    paths = {}
    for idx, fpath in enumerate(res.stdout.split(), start=1):
        print(f'{idx}: {fpath}')
        paths[idx] = fpath
    rep = input('What file do you want to restore? ')
    path = paths[int(rep)]
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    remote_path = f'/var/lib/autopostgresqlbackup/daily/{dbname}/{path}'
    conn.sudo(f'chmod o+r {remote_path}')
    conn.get(remote_path, None)
    restore_db_from_file(dbname, path)
