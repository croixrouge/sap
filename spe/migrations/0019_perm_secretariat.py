from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0018_Permission_transfert_spe'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famillespe',
            options={'permissions': {
                ('can_manage_waiting_list', "Gérer la liste d'attente"),
                ('can_transfer', 'Transférer la famille au SIFP')
            }},
        ),
    ]
