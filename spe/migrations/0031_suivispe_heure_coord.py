from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0030_interv_freq2niveau'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivispe',
            name='heure_coord',
            field=models.BooleanField(default=False, verbose_name='Heure de coordination'),
        ),
    ]
