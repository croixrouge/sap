from django.db import migrations


def migrate_freq_interv(apps, schema_editor):
    SuiviSPE = apps.get_model('spe', 'SuiviSPE')
    SuiviSPE.objects.filter(freq_interv__gt=0, freq_interv__lte=0.5).update(niveau_interv=1)
    SuiviSPE.objects.filter(freq_interv__gt=0.5).update(niveau_interv=2)


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0029_medlink_date_rapport_evaluation'),
    ]

    operations = [
        migrations.RunPython(migrate_freq_interv)
    ]
