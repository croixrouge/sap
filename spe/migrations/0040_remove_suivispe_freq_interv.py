from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0039_migrate_NiveauSPE'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivispe',
            name='freq_interv',
        ),
    ]
