from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('spe', '0019_perm_secretariat'),
    ]

    operations = [
        migrations.AddField(
            model_name='bilan',
            name='sig_famille',
            field=models.BooleanField(default=True, verbose_name='Apposer signature de la famille'),
        ),
        migrations.AddField(
            model_name='bilan',
            name='sig_interv',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL, verbose_name='Signature des intervenants'),
        ),
    ]
