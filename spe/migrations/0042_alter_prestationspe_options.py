from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0041_remove_suivispe_niveau_interv'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='prestationspe',
            options={
                'ordering': ('-date_prestation',),
                'permissions': (('edit_prest_prev_month', 'Modifier prestations du mois précédent'),)
            },
        ),
    ]
