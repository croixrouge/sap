from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0028_suivispe_niveau_interv'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationspe',
            name='medlink_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Date import. Medlink'),
        ),
        migrations.AddField(
            model_name='rapportspe',
            name='evaluation',
            field=models.TextField(blank=True, verbose_name='Évaluation / Hypothèses'),
        ),
    ]
