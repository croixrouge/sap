from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0006_Auteur_bilan_nullable'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivispe',
            name='referent_1',
        ),
        migrations.RemoveField(
            model_name='suivispe',
            name='referent_2',
        ),
    ]
