from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0021_add_rapport_signature_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suivispe',
            name='equipe',
            field=models.CharField(choices=[('montagnes', 'SPE - Montagnes'), ('neuch_ville', 'SPE - Neuchâtel-ville'), ('litt_est', 'SPE - Littoral Est'), ('litt_ouest', 'SPE - Littoral Ouest')], max_length=15, verbose_name='Équipe'),
        ),
    ]
