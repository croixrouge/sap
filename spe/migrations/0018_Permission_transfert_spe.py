from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0017_Ajouter_RapportSPE'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famillespe',
            options={'permissions': {('can_transfer', 'Transférer la famille au SIFP')}},
        ),
    ]
