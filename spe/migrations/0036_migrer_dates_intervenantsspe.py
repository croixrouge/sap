from django.db import migrations


def migrate_intervenantspe(apps, schema_editor):
    IntervenantSpe = apps.get_model('spe', 'IntervenantSPE')
    Role = apps.get_model('croixrouge', 'Role')
    role_educ = Role.objects.filter(nom='Educ').first()
    role_psy = Role.objects.filter(nom='Psy').first()
    for interv in IntervenantSpe.objects.all().select_related('suivi'):
        interv.date_debut = (
            next(date for date in [
                interv.suivi.date_debut_evaluation, interv.suivi.date_debut_suivi, interv.suivi.date_demande
            ] if date is not None)
            if interv.role in [role_educ, role_psy] else next(date for date in [
                interv.suivi.date_debut_suivi, interv.suivi.date_debut_evaluation, interv.suivi.date_demande
            ] if date is not None)
        )
        interv.save()


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0035_Add_dates_intervenantspe'),
    ]

    operations = [
        migrations.RunPython(migrate_intervenantspe),
    ]
