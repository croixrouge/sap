from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0033_rapportspe_observ_ipe_sof'),
    ]

    operations = [
        migrations.AddField(
            model_name='rapportspe',
            name='observ_apa',
            field=models.TextField(blank=True, verbose_name='Observations APA'),
        ),
    ]
