from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0044_unaccent_extension'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famillespe',
            options={'permissions': {('can_archive', 'Archiver les dossiers SPE'), ('can_manage_waiting_list', "Gérer la liste d'attente")}},
        ),
    ]
