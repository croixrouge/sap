from django.db import migrations


def migrate_equipes(apps, schema_editor):
    SuiviSPE = apps.get_model('spe', 'SuiviSPE')
    SuiviSPE.objects.filter(
        date_fin_suivi__isnull=True, equipe__in=['neuch_ville', 'litt_est', 'litt_ouest']
    ).update(equipe='littoral')


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0026_alter_suivispe_ref_presents'),
    ]

    operations = [
        migrations.RunPython(migrate_equipes),
    ]
