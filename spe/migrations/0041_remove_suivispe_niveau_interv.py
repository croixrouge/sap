from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0040_remove_suivispe_freq_interv'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivispe',
            name='niveau_interv',
        ),
    ]
