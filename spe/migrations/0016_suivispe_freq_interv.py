from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0015_Ajout_SuiviSpe_competences'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivispe',
            name='freq_interv',
            field=models.DecimalField(max_digits=3, decimal_places=2, default=0, verbose_name='Fréquence d’intervention'),
        ),
    ]
