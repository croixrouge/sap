from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0027_migrate_equipes'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivispe',
            name='niveau_interv',
            field=models.PositiveSmallIntegerField(
                blank=True, choices=[(1, '1'), (2, '2'), (3, '3')], null=True,
                verbose_name='Niveau d’intervention'
            ),
        ),
        migrations.AlterField(
            model_name='suivispe',
            name='equipe',
            field=models.CharField(choices=[
                ('montagnes', 'Montagnes et V-d-T'), ('littoral', 'Littoral et V-d-R'),
                ('neuch_ville', 'SPE - Neuchâtel-ville (archives)'), ('litt_est', 'SPE - Littoral Est (archives)'),
                ('litt_ouest', 'SPE - Littoral Ouest (archives)')
            ], max_length=15, verbose_name='Équipe'),
        ),
    ]
