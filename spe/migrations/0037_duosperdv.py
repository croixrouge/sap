# Generated by Django 4.2.2 on 2023-07-17 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0036_migrer_dates_intervenantsspe'),
    ]

    operations = [
        migrations.CreateModel(
            name='DuoSPERdv',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duo_abr', models.CharField(max_length=15)),
                ('rendez_vous', models.DateField(verbose_name='Rendez-vous')),
            ],
        ),
    ]
