from django.urls import path

from croixrouge import views as cr_views

from . import views, views_stats
from .forms import BilanForm
from .models import Bilan

urlpatterns = [
    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='spe-famille-list'),
    path('famille/attente/', views.FamilleListView.as_view(mode='attente'), name='spe-famille-attente'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='spe-famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='spe-famille-edit'),
    path('famille/<int:obj_pk>/niveau/add/', views.NiveauSPECreateUpdateView.as_view(), name='spe-niveau-add'),
    path('famille/<int:obj_pk>/niveau/<int:pk>/edit/', views.NiveauSPECreateUpdateView.as_view(),
         name='spe-niveau-edit'
         ),
    path('famille/<int:obj_pk>/niveau/<int:pk>/delete/', views.NiveauSPEDeleteView.as_view(),
         name='spe-niveau-delete'),

    # Prestations
    path('prestation/menu/', views.PrestationMenu.as_view(), name='spe-prestation-menu'),
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(), name='spe-journal-list'),
    path('famille/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(), name='spe-prestation-famille-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='spe-prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='spe-prestation-delete'),
    path('prestation_gen/list/', views.PrestationListView.as_view(), name='spe-prestation-gen-list'),
    path('prestation_gen/add/', views.PrestationCreateView.as_view(), name='spe-prestation-gen-add'),

    # Doc. à imprimer
    path('famille/<int:pk>/print-evaluation/', views.EvaluationPDFView.as_view(), name='spe-print-evaluation'),
    path('famille/<int:pk>/print-info/', views.CoordonneesPDFView.as_view(), name='spe-print-coord-famille'),
    path('famille/<int:pk>/print-journal', views.JournalPDFView.as_view(), name='spe-print-journal'),
    path('bilan/<int:pk>/print/', views.BilanPDFView.as_view(), name='spe-print-bilan'),

    # Demande, suivi, agenda, suivis terminés
    path('famille/<int:pk>/demande/', views.DemandeView.as_view(), name='spe-demande'),
    path('famille/<int:pk>/suivi/', views.SPESuiviView.as_view(), name='spe-famille-suivi'),
    path('famille/<int:pk>/intervenant/add/', views.SuiviIntervenantCreate.as_view(), name='spe-intervenant-add'),
    path('famille/<int:pk>/intervenant/<int:obj_pk>/edit/', views.SuiviIntervenantUpdateView.as_view(),
         name='spe-intervenant-edit'),
    path('famille/<int:pk>/agenda/', views.AgendaSuiviView.as_view(), name='spe-famille-agenda'),
    path('famille/<int:pk>/bilan/add/',
         cr_views.BilanEditView.as_view(model=Bilan, form_class=BilanForm, is_create=True),
         name='famille-bilanspe-add'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/', cr_views.BilanDetailView.as_view(model=Bilan),
         name='famille-bilanspe-view'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/edit/',
         cr_views.BilanEditView.as_view(model=Bilan, form_class=BilanForm),
         name='famille-bilanspe-edit'),
    path('famille/<int:pk>/bilan/<int:obj_pk>/delete/', cr_views.BilanDeleteView.as_view(model=Bilan),
         name='famille-bilanspe-delete'),
    path('famille/archivable/list/', views.FamilleArchivableListe.as_view(), name='spe-famille-archivable'),

    path('suivis_termines/', views.SuivisTerminesListView.as_view(), name='spe-suivis-termines'),
    path('charge_utilisateurs/', views.UtilisateurChargeDossierView.as_view(), name='charge-utilisateurs'),
    path('duo/edit/<path:sigles>/', views.DuoSPERdvEditView.as_view(), name='duo-date-change'),

    # Statistiques
    path('statistiques/', views_stats.StatistiquesView.as_view(), name='stats'),
    path('statistiques/localite/', views_stats.StatistiquesParLocaliteView.as_view(), name='stats-localite'),
    path('statistiques/region/', views_stats.StatistiquesParRegionView.as_view(), name='stats-region'),
    path('statistiques/duree/', views_stats.StatistiquesParDureeView.as_view(), name='stats-duree'),
    path('statistiques/age/', views_stats.StatistiquesParAgeView.as_view(), name='stats-age'),
    path('statistiques/niveaux/', views_stats.StatistiquesParNiveauView.as_view(), name='stats-niveaux'),
    path('statistiques/motifs/', views_stats.StatistiquesMotifsView.as_view(), name='stats-motifs'),
    path('statistiques/prestations/spe/', views_stats.StatistiquesPrestationView.as_view(),
         {'unite': 'spe'}, name='stats-prestation-spe'),
    path('statistiques/ressources/', views_stats.StatistiquesRessourcesView.as_view(), name='stats-ressources'),

    path('export/prestation/', views.ExportPrestationView.as_view(), name='export-prestation'),
]
