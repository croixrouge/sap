from datetime import date

from city_ch_autocomplete.forms import CityChField, CityChMixin
from django import forms
from django.db.models import Count, Q

from croixrouge.forms import (
    AgendaFormBase,
    BootstrapMixin,
    BSCheckboxSelectMultiple,
    FamilleFormBase,
    PickDateWidget,
    PrestationFormBase,
    ReadOnlyableMixin,
    RichTextField,
)
from croixrouge.models import Contact, EquipeChoices, Role, Utilisateur

from .models import Bilan, DuoSPERdv, FamilleSPE, IntervenantSPE, NiveauSPE, PrestationSPE, SuiviSPE


class EquipeFilterForm(forms.Form):
    equipe = forms.ChoiceField(
        label="Équipe", choices=[('', 'Toutes')] + EquipeChoices.choices, required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'}),
    )

    def filter(self, familles):
        if self.cleaned_data['equipe']:
            familles = familles.filter(suivispe__equipe=self.cleaned_data['equipe'])
        return familles


class FamilleFilterForm(EquipeFilterForm):
    ressource = forms.ChoiceField(
        label='Ressources',
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    niveau = forms.ChoiceField(
        label="Niv. d'interv.",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    interv = forms.ChoiceField(
        label="Intervenant-e",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom de famille…', 'autocomplete': 'off',
                   'class': 'form-control form-control-sm inline'}),
        required=False
    )
    duos = forms.ChoiceField(
        label="Duos Educ/Psy",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        intervenants = Utilisateur.objects.filter(is_active=True, groups__name='spe')
        self.fields['interv'].choices = [
            ('', 'Tout-e-s'), ('0', 'Aucun')] + [
            (user.id, user.nom_prenom) for user in intervenants
        ]
        self.fields['niveau'].choices = [
            ('', 'Tous')
        ] + [(key, val) for key, val in NiveauSPE.INTERV_CHOICES]

        self.fields['ressource'].choices = [
            ('', 'Toutes')
        ] + [(el.pk, el.nom) for el in Role.objects.filter(
                nom__in=['ASE', 'IPE', 'Coach APA', 'Assistant-e social-e']
            )]
        self.fields['duos'].choices = [
            ('', 'Tous')
        ] + [
            (duo, duo) for duo in FamilleSPE.objects.exclude(
                suivispe__date_fin_suivi__isnull=False
            ).with_duos().exclude(duo='').values_list('duo', flat=True).distinct().order_by('duo')
        ]

    def filter(self, familles):
        if self.cleaned_data['interv']:
            if self.cleaned_data['interv'] == '0':
                familles = familles.annotate(
                    num_interv=Count('suivispe__intervenants', filter=(
                        Q(suivispe__intervenantspe__date_fin__isnull=True) |
                        Q(suivispe__intervenantspe__date_fin__gt=date.today())
                    ))
                ).filter(num_interv=0)
            else:
                familles = familles.filter(
                    Q(suivispe__intervenantspe__intervenant=self.cleaned_data['interv']) & (
                        Q(suivispe__intervenantspe__date_fin__isnull=True) |
                        Q(suivispe__intervenantspe__date_fin__gt=date.today())
                    )
                )

        if self.cleaned_data['nom']:
            familles = familles.filter(nom__istartswith=self.cleaned_data['nom'])

        familles = super().filter(familles)

        if self.cleaned_data['niveau']:
            familles = familles.with_niveau_interv().filter(niveau_interv=self.cleaned_data['niveau'])

        if self.cleaned_data['ressource']:
            ress = IntervenantSPE.objects.actifs().filter(
                role=self.cleaned_data['ressource'],
            ).values_list('intervenant', flat=True)
            familles = familles.filter(suivispe__intervenants__in=ress).distinct()

        if self.cleaned_data['duos']:
            familles = familles.with_duos().filter(duo=self.cleaned_data['duos'])
        return familles


class FamilleForm(FamilleFormBase):
    equipe = forms.ChoiceField(label="Équipe", choices=[('', '------')] + SuiviSPE.EQUIPES_CHOICES[:2])

    # Concernant 'npa' & 'location', ils sont en lecture seule ici => pas besoin de faire de l'autocomplete

    class Meta:
        model = FamilleSPE
        fields = ['nom', 'rue', 'npa', 'localite', 'telephone', 'region', 'autorite_parentale',
                  'monoparentale', 'statut_marital', 'connue', 'accueil', 'garde',
                  'provenance', 'statut_financier']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance and self.instance.pk:
            self.initial['equipe'] = kwargs['instance'].suivi.equipe
        self.fields['provenance'].choices = [
            ch for ch in self.fields['provenance'].choices
            if (ch[0] != 'autre' or self.instance.provenance == 'autre')
        ]

    def save(self, **kwargs):
        famille = super().save(**kwargs)
        if famille.suivi.equipe != self.cleaned_data['equipe']:
            famille.suivi.equipe = self.cleaned_data['equipe']
            famille.suivi.save()
        return famille


class FamilleCreateForm(CityChMixin, FamilleForm):
    motif_detail = forms.CharField(
        label="Motif de la demande", widget=forms.Textarea(), required=False
    )
    typ = 'spe'
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'
    field_order = ['nom', 'rue', 'city_auto']

    class Meta(FamilleForm.Meta):
        exclude = ['typ', 'destination']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['provenance'].choices = [
            ch for ch in self.fields['provenance'].choices if ch[0] != 'autre'
        ]

    def save(self, **kwargs):
        famille = self._meta.model.objects.create_famille(
            equipe=self.cleaned_data['equipe'], **self.instance.__dict__
        )
        famille.suivi.motif_detail = self.cleaned_data['motif_detail']
        famille.suivi.save()
        return famille


class SuiviSPEForm(ReadOnlyableMixin, forms.ModelForm):
    equipe = forms.TypedChoiceField(
        choices=[('', '-------')] + SuiviSPE.EQUIPES_CHOICES[:2], required=False
    )

    class Meta:
        model = SuiviSPE
        fields = [
            'equipe', 'heure_coord', 'ope_referent', 'ope_referent_2',
            'mandat_ope', 'service_orienteur', 'service_annonceur', 'motif_demande',
            'motif_detail', 'demande_prioritaire', 'demarche', 'collaboration',
            'ressource', 'crise', 'remarque', 'remarque_privee',
        ]
        widgets = {
            'mandat_ope': BSCheckboxSelectMultiple,
            'motif_demande': BSCheckboxSelectMultiple,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['ope_referent'].queryset = Contact.membres_ope()
        self.fields['ope_referent_2'].queryset = Contact.membres_ope()


class IntervenantSPEForm(BootstrapMixin, forms.ModelForm):
    intervenant = forms.ModelChoiceField(
        queryset=Utilisateur.objects.filter(groups__name__startswith='spe').distinct().order_by('nom')
    )
    date_debut = forms.DateField(
        label='Date de début', initial=date.today(), widget=PickDateWidget()
    )

    class Meta:
        model = IntervenantSPE
        fields = ['intervenant', 'role', 'date_debut']

    def __init__(self, suivi=None, **kwargs):
        self.suivi = suivi
        super().__init__(**kwargs)
        self.fields['role'].queryset = Role.objects.filter(intervenant=True)

    def clean(self):
        if self.suivi.intervenantspe_set.filter(
            intervenant=self.cleaned_data['intervenant'], date_fin__isnull=True
        ).exists():
            self.add_error(None, "Cet intervenant est déjà actif sur ce suivi")
        return super().clean()

    def save(self, **kwargs):
        self.instance.suivi = self.suivi
        return super().save(**kwargs)


class IntervenantSPEEditForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = IntervenantSPE
        fields = ['intervenant', 'role', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': PickDateWidget(),
            'date_fin': PickDateWidget(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['intervenant'].disabled = True
        self.fields['role'].disabled = True


class DemandeSPEForm(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):
    class Meta:
        model = SuiviSPE
        fields = (
            'dates_demande', 'ref_presents', 'pers_famille_presentes', 'autres_pers_presentes',
            'difficultes', 'aides', 'competences', 'disponibilites', 'autres_contacts', 'remarque'
        )
        field_classes = {
            'difficultes': RichTextField,
            'aides': RichTextField,
            'disponibilites': RichTextField,
            'competences': RichTextField,
        }
        widgets = {
            'autres_contacts': forms.Textarea(attrs={'cols': 120, 'rows': 4}),
            'remarque': forms.Textarea(attrs={'cols': 120, 'rows': 4}),
        }


class AgendaForm(ReadOnlyableMixin, AgendaFormBase):
    ope_referent = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False
    )
    change_famille_perm = 'spe.change_famillespe'

    class Meta(AgendaFormBase.Meta):
        model = SuiviSPE


class PrestationSPEForm(PrestationFormBase):
    class Meta(PrestationFormBase.Meta):
        model = PrestationSPE
        fields = ['date_prestation', 'duree', 'texte', 'manque', 'fichier', 'intervenants']

    def defaults_intervenants(self):
        return Utilisateur.intervenants_spe()


class BilanForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Bilan
        exclude = ['famille', 'auteur']
        field_classes = {
            'objectifs': RichTextField,
            'rythme': RichTextField,
        }
        widgets = {
            'famille': forms.HiddenInput,
            'date': PickDateWidget,
            'sig_interv': BSCheckboxSelectMultiple,
        }
        labels = {'fichier': ''}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['sig_interv'].queryset = Utilisateur.objects.filter(
            pk__in=kwargs['initial']['famille'].suivispe.intervenantspe_set.actifs(
                self.instance.date or date.today()
            ).values_list('intervenant', flat=True)
        )


class DuoDateEditForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = DuoSPERdv
        fields = ['rendez_vous']
        widgets = {
            'rendez_vous': PickDateWidget,
        }


class NiveauSPEForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = NiveauSPE
        fields = ['niveau_interv', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': PickDateWidget(),
            'date_fin': PickDateWidget(),
        }

    def __init__(self, *args, **kwargs):
        self.famille = kwargs.pop('famille')
        super().__init__(*args, **kwargs)
        if self.instance.pk is None:
            self.fields.pop('date_fin')
            if self.famille.niveaux_spe.count() > 0:
                self.fields['date_debut'].required = True

    def clean_niveau_interv(self):
        niv = self.cleaned_data['niveau_interv']
        if self.instance.pk is None:
            der_niv = self.famille.niveaux_spe.last() if self.famille.niveaux_spe.count() > 0 else None
            if der_niv and der_niv.niveau_interv == niv:
                raise forms.ValidationError(f"Le niveau {niv} est déjà actif.")
        return niv
