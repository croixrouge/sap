from datetime import date, datetime, time, timedelta

from city_ch_autocomplete.forms import CityChField, CityChMixin
from django import forms
from django.db.models import Count, Q
from django.utils.html import escape
from django.utils.safestring import mark_safe

from croixrouge.forms import (
    AgendaFormBase,
    BootstrapMixin,
    BSCheckboxSelectMultiple,
    FamilleFormBase,
    PickDateWidget,
    PrestationFormBase,
    ReadOnlyableMixin,
    RichTextField,
)
from croixrouge.models import Contact, EquipeChoices, LibellePrestation, Personne, Role, Utilisateur
from croixrouge.utils import CSVFile, XLSXFile, unaccent

from .models import Bilan, DuoSPERdv, FamilleSPE, IntervenantSPE, NiveauSPE, PrestationSPE, SuiviSPE


class EquipeFilterForm(forms.Form):
    equipe = forms.ChoiceField(
        label="Équipe", choices=[('', 'Toutes')] + EquipeChoices.choices, required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'}),
    )

    def filter(self, familles):
        if self.cleaned_data['equipe']:
            familles = familles.filter(suivispe__equipe=self.cleaned_data['equipe'])
        return familles


class FamilleFilterForm(EquipeFilterForm):
    ressource = forms.ChoiceField(
        label='Ressources',
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    niveau = forms.ChoiceField(
        label="Niv. d'interv.",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    interv = forms.ChoiceField(
        label="Intervenant-e",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom de famille…', 'autocomplete': 'off',
                   'class': 'form-control form-control-sm inline'}),
        required=False
    )
    duos = forms.ChoiceField(
        label="Duos Educ/Psy",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        intervenants = Utilisateur.objects.filter(is_active=True, groups__name='spe')
        self.fields['interv'].choices = [
            ('', 'Tout-e-s'), ('0', 'Aucun')] + [
            (user.id, user.nom_prenom) for user in intervenants
        ]
        self.fields['niveau'].choices = [
            ('', 'Tous')
        ] + [(key, val) for key, val in NiveauSPE.INTERV_CHOICES]

        self.fields['ressource'].choices = [
            ('', 'Toutes')
        ] + [(el.pk, el.nom) for el in Role.objects.filter(
                nom__in=['ASE', 'IPE', 'Coach APA', 'Assistant-e social-e']
            )]
        self.fields['duos'].choices = [
            ('', 'Tous')
        ] + [
            (duo, duo) for duo in FamilleSPE.objects.exclude(
                suivispe__date_fin_suivi__isnull=False
            ).with_duos().exclude(duo='').values_list('duo', flat=True).distinct().order_by('duo')
        ]

    def filter(self, familles):
        if self.cleaned_data['interv']:
            if self.cleaned_data['interv'] == '0':
                familles = familles.annotate(
                    num_interv=Count('suivispe__intervenants', filter=(
                        Q(suivispe__intervenantspe__date_fin__isnull=True) |
                        Q(suivispe__intervenantspe__date_fin__gt=date.today())
                    ))
                ).filter(num_interv=0)
            else:
                familles = familles.filter(
                    Q(suivispe__intervenantspe__intervenant=self.cleaned_data['interv']) & (
                        Q(suivispe__intervenantspe__date_fin__isnull=True) |
                        Q(suivispe__intervenantspe__date_fin__gt=date.today())
                    )
                )

        if self.cleaned_data['nom']:
            familles = familles.filter(nom__istartswith=self.cleaned_data['nom'])

        familles = super().filter(familles)

        if self.cleaned_data['niveau']:
            familles = familles.with_niveau_interv().filter(niveau_interv=self.cleaned_data['niveau'])

        if self.cleaned_data['ressource']:
            ress = IntervenantSPE.objects.actifs().filter(
                role=self.cleaned_data['ressource'],
            ).values_list('intervenant', flat=True)
            familles = familles.filter(suivispe__intervenants__in=ress).distinct()

        if self.cleaned_data['duos']:
            familles = familles.with_duos().filter(duo=self.cleaned_data['duos'])
        return familles


class FamilleForm(FamilleFormBase):
    equipe = forms.ChoiceField(label="Équipe", choices=[('', '------')] + SuiviSPE.EQUIPES_CHOICES[:2])

    # Concernant 'npa' & 'location', ils sont en lecture seule ici => pas besoin de faire de l'autocomplete

    class Meta:
        model = FamilleSPE
        fields = ['nom', 'rue', 'npa', 'localite', 'telephone', 'region', 'autorite_parentale',
                  'monoparentale', 'statut_marital', 'connue', 'accueil', 'garde',
                  'provenance', 'statut_financier']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance and self.instance.pk:
            self.initial['equipe'] = kwargs['instance'].suivi.equipe
        self.fields['provenance'].choices = [
            ch for ch in self.fields['provenance'].choices
            if (ch[0] != 'autre' or self.instance.provenance == 'autre')
        ]

    def save(self, **kwargs):
        famille = super().save(**kwargs)
        if famille.suivi.equipe != self.cleaned_data['equipe']:
            famille.suivi.equipe = self.cleaned_data['equipe']
            famille.suivi.save()
        return famille


class FamilleCreateForm(CityChMixin, FamilleForm):
    motif_detail = forms.CharField(
        label="Motif de la demande", widget=forms.Textarea(), required=False
    )
    typ = 'spe'
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'
    field_order = ['nom', 'rue', 'city_auto']

    class Meta(FamilleForm.Meta):
        exclude = ['typ', 'destination']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['provenance'].choices = [
            ch for ch in self.fields['provenance'].choices if ch[0] != 'autre'
        ]

    def save(self, **kwargs):
        famille = self._meta.model.objects.create_famille(
            equipe=self.cleaned_data['equipe'], **self.instance.__dict__
        )
        famille.suivi.motif_detail = self.cleaned_data['motif_detail']
        famille.suivi.save()
        return famille


class SuiviSPEForm(ReadOnlyableMixin, forms.ModelForm):
    equipe = forms.TypedChoiceField(
        choices=[('', '-------')] + SuiviSPE.EQUIPES_CHOICES[:2], required=False
    )

    class Meta:
        model = SuiviSPE
        fields = [
            'equipe', 'heure_coord', 'ope_referent', 'ope_referent_2',
            'mandat_ope', 'service_orienteur', 'service_annonceur', 'motif_demande',
            'motif_detail', 'demande_prioritaire', 'demarche', 'collaboration',
            'ressource', 'crise', 'remarque', 'remarque_privee',
        ]
        widgets = {
            'mandat_ope': BSCheckboxSelectMultiple,
            'motif_demande': BSCheckboxSelectMultiple,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['ope_referent'].queryset = Contact.membres_ope()
        self.fields['ope_referent_2'].queryset = Contact.membres_ope()


class IntervenantSPEForm(BootstrapMixin, forms.ModelForm):
    intervenant = forms.ModelChoiceField(
        queryset=Utilisateur.objects.filter(groups__name__startswith='spe').distinct().order_by('nom')
    )
    date_debut = forms.DateField(
        label='Date de début', initial=date.today(), widget=PickDateWidget()
    )

    class Meta:
        model = IntervenantSPE
        fields = ['intervenant', 'role', 'date_debut']

    def __init__(self, suivi=None, **kwargs):
        self.suivi = suivi
        super().__init__(**kwargs)
        self.fields['role'].queryset = Role.objects.filter(intervenant=True)

    def clean(self):
        if self.suivi.intervenantspe_set.filter(
            intervenant=self.cleaned_data['intervenant'], date_fin__isnull=True
        ).exists():
            self.add_error(None, "Cet intervenant est déjà actif sur ce suivi")
        return super().clean()

    def save(self, **kwargs):
        self.instance.suivi = self.suivi
        return super().save(**kwargs)


class IntervenantSPEEditForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = IntervenantSPE
        fields = ['intervenant', 'role', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': PickDateWidget(),
            'date_fin': PickDateWidget(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['intervenant'].disabled = True
        self.fields['role'].disabled = True


class DemandeSPEForm(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):
    class Meta:
        model = SuiviSPE
        fields = (
            'dates_demande', 'ref_presents', 'pers_famille_presentes', 'autres_pers_presentes',
            'difficultes', 'aides', 'competences', 'disponibilites', 'autres_contacts', 'remarque'
        )
        field_classes = {
            'difficultes': RichTextField,
            'aides': RichTextField,
            'disponibilites': RichTextField,
            'competences': RichTextField,
        }
        widgets = {
            'autres_contacts': forms.Textarea(attrs={'cols': 120, 'rows': 4}),
            'remarque': forms.Textarea(attrs={'cols': 120, 'rows': 4}),
        }


class AgendaForm(ReadOnlyableMixin, AgendaFormBase):
    ope_referent = forms.ModelChoiceField(
        queryset=Contact.membres_ope(), required=False
    )
    change_famille_perm = 'spe.change_famillespe'

    class Meta(AgendaFormBase.Meta):
        model = SuiviSPE


class PrestationSPEForm(PrestationFormBase):
    class Meta(PrestationFormBase.Meta):
        model = PrestationSPE
        fields = ['date_prestation', 'duree', 'texte', 'manque', 'fichier', 'intervenants']

    def defaults_intervenants(self):
        return Utilisateur.intervenants_spe()


class BilanForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Bilan
        exclude = ['famille', 'auteur']
        field_classes = {
            'objectifs': RichTextField,
            'rythme': RichTextField,
        }
        widgets = {
            'famille': forms.HiddenInput,
            'date': PickDateWidget,
            'sig_interv': BSCheckboxSelectMultiple,
        }
        labels = {'fichier': ''}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['sig_interv'].queryset = Utilisateur.objects.filter(
            pk__in=kwargs['initial']['famille'].suivispe.intervenantspe_set.actifs(
                self.instance.date or date.today()
            ).values_list('intervenant', flat=True)
        )


class DuoDateEditForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = DuoSPERdv
        fields = ['rendez_vous']
        widgets = {
            'rendez_vous': PickDateWidget,
        }


class MedlinkImportForm(forms.Form):
    fichier = forms.FileField(
        label='Fichier',
        widget=forms.FileInput(attrs={'class': 'form-control'})
    )

    def _search_user(self, user_str):
        if user_str.startswith(('ASE-', 'GED-')):
            user_str = user_str[4:]
        user_name = user_str.replace('-ASE ', ' ').replace('-GED ', ' ')
        user_name = ' '.join([part for part in user_name.split(' ') if part.isupper()])
        user_name = unaccent(user_name)
        if user_name in self.users_db:
            return self.users_db[user_name]
        else:
            self.users_non_trouves.add(user_str)
            return None

    def _search_client(self, prest, client_str):
        if client_str in (None, ''):
            return None
        nom = unaccent(client_str)
        if nom in self.clients_db[prest]:
            return self.clients_db[prest][nom].famille
        else:
            self.clients_non_trouves[prest].add(client_str)
            return None

    def clean(self):
        cleaned_data = super().clean()
        filename = cleaned_data['fichier']
        if filename.name[-5:].lower() == '.xlsx':
            imp_file = XLSXFile(filename)
        elif filename.name[-4:].lower() == '.csv':
            imp_file = CSVFile(filename)
        else:
            raise forms.ValidationError("L’extension du fichier est incorrecte.")

        self.users_non_trouves = set()
        self.clients_non_trouves = {"spe": set(), "sof": set()}
        self.ignore_fields = []
        user_qs = Utilisateur.objects.filter(date_desactivation__isnull=True, roles__nom='ASE')
        self.users_db = {unaccent(u.nom): u for u in user_qs}
        assert len(user_qs) == len(self.users_db)  # Pas de doublons qui s'écrasent
        mois_passe = (date.today() - timedelta(days=60)).replace(day=1)
        self.clients_db = {
            "spe": {
                unaccent([p.nom, p.prenom]): p
                for p in Personne.objects.filter(famille__suivispe__isnull=False).filter(
                    Q(famille__suivispe__date_fin_suivi__isnull=True) |
                    Q(famille__suivispe__date_fin_suivi__gte=mois_passe)
                )
            },
            "sof": {
                unaccent([p.nom, p.prenom]): p
                for p in Personne.objects.filter(famille__suivisof__isnull=False).filter(
                    Q(famille__suivisof__date_fin_suivi__isnull=True) |
                    Q(famille__suivisof__date_fin_suivi__gte=mois_passe)
                )
            },
        }
        self.prestations = {"spe": [], "sof": []}
        prest_gen_spe = LibellePrestation.objects.get(code='spe03')
        prest_fam_spe = LibellePrestation.objects.get(code='spe04')  # Activités ASE
        prest_gen_sof = LibellePrestation.objects.get(code='sof03')
        prest_fam_sof = LibellePrestation.objects.get(code='sof04')  # Activités ASE

        # expected_headers = [
        #    'Object Id', 'Equipe', 'Collaborateur', 'Id externe', 'Date Releve',
        #    'Début', 'Fin', 'Client', 'Prestation', 'Durée'
        #]
        USER = 2
        DATE = 4
        CLIENT = 7
        PREST = 8
        DUREE = 9
        for idx, row in enumerate(imp_file.lines):
            if idx == 0 and row[0] == 'Mois':
                continue
            if idx < 2:
                try:
                    # if the A1 cell is a number, we infer that no headers are present.
                    int(row[0])
                except (TypeError, ValueError):
                    has_headers = True
                else:
                    has_headers = False
                if has_headers:
                    try:
                        USER = row.index('Collaborateur')
                        DATE = row.index('Date Releve')
                        CLIENT = row.index('Client')
                        PREST = row.index('Prestation')
                        DUREE = row.index('Durée')
                    except ValueError:
                        raise forms.ValidationError(
                            "Les en-têtes du fichier ne correspondent pas à ce qui est attendu. "
                            "Êtes-vous certain-e qu’il s'agit d’un fichier Medlink ?"
                        )
                    continue

            if not any(row):
                # Skip completely empty rows
                continue
            if "SPE" in row[PREST]:
                prest = "spe"
            elif "SOF" in row[PREST]:
                prest = "sof"
            else:
                # Skip non-SPE lines
                continue
            famille = self._search_client(prest, row[CLIENT])
            t = row[DUREE]
            if isinstance(t, str):
                t = time(*[int(p) for p in t.split(':')])
            duree = timedelta(hours=t.hour, minutes=t.minute)
            if not famille and not duree:
                # Par ex déplacements km
                continue

            if isinstance(row[DATE], str):
                match row[DATE].count(':'):
                    case 2:
                        pattern = '%d.%m.%Y %H:%M:%S'
                    case 1:
                        pattern = '%d.%m.%Y %H:%M'
                    case _:
                        pattern = '%d.%m.%Y'
                dt = datetime.strptime(row[DATE], pattern).date()
            else:
                dt = row[DATE]
            if prest == "spe":
                prestation = prest_fam_spe if famille else prest_gen_spe
            elif prest == "sof":
                prestation = prest_fam_sof if famille else prest_gen_sof
            self.prestations[prest].append(
                dict(
                    date_prestation=dt,
                    duree=duree,
                    famille=famille,
                    lib_prestation=prestation,
                    texte=row[PREST],
                    intervenant=self._search_user(row[USER])
                )
            )

        imp_file.close()
        error = ''
        if len(self.users_non_trouves) > 0:
            error += "Impossible de trouver les intervenant-e-s suivant-e-s dans la base: <br>"
            error += '<br>'.join([escape(f' - {u}') for u in self.users_non_trouves])
            error += '<br>'
        clients_ignores = set([val for key, val in self.data.items() if key.startswith('non_trouve_')])
        non_trouves = self.clients_non_trouves["spe"] | self.clients_non_trouves["sof"]
        if len(non_trouves - clients_ignores) > 0:
            error += "Impossible de trouver les personnes suivantes dans la base:"
            idx = 0
            for cl in self.clients_non_trouves["spe"]:
                self.fields[f'non_trouve_{idx}'] = forms.BooleanField(
                    label=f"[SPE] {cl}", required=False, widget=forms.CheckboxInput(attrs={'value': cl})
                )
                self.ignore_fields.append(f'non_trouve_{idx}')
                idx += 1
            for cl in self.clients_non_trouves["sof"]:
                self.fields[f'non_trouve_{idx}'] = forms.BooleanField(
                    label=f"[SOF] {cl}", required=False, widget=forms.CheckboxInput(attrs={'value': cl})
                )
                self.ignore_fields.append(f'non_trouve_{idx}')
                idx += 1
        if error:
            raise forms.ValidationError(mark_safe(error))


class NiveauSPEForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = NiveauSPE
        fields = ['niveau_interv', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': PickDateWidget(),
            'date_fin': PickDateWidget(),
        }

    def __init__(self, *args, **kwargs):
        self.famille = kwargs.pop('famille')
        super().__init__(*args, **kwargs)
        if self.instance.pk is None:
            self.fields.pop('date_fin')
            if self.famille.niveaux_spe.count() > 0:
                self.fields['date_debut'].required = True

    def clean_niveau_interv(self):
        niv = self.cleaned_data['niveau_interv']
        if self.instance.pk is None:
            der_niv = self.famille.niveaux_spe.last() if self.famille.niveaux_spe.count() > 0 else None
            if der_niv and der_niv.niveau_interv == niv:
                raise forms.ValidationError(f"Le niveau {niv} est déjà actif.")
        return niv
