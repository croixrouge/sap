from django.contrib import admin

from croixrouge.admin import FamilleAdminBase, TypePrestationFilter

from .models import Bilan, DuoSPERdv, FamilleSPE, IntervenantSPE, PrestationSPE, RapportSPE, SuiviSPE


@admin.register(FamilleSPE)
class FamilleSPEAdmin(FamilleAdminBase):
    def get_queryset(self, request):
        return FamilleSPE.objects.filter(typ=['spe'])


@admin.register(SuiviSPE)
class SuiviSPEAdmin(admin.ModelAdmin):
    list_display = ('famille', 'equipe', 'etape')
    list_filter = ('equipe',)
    ordering = ('famille__nom',)
    search_fields = ('famille__nom',)


@admin.register(IntervenantSPE)
class IntervenantSPEAdmin(admin.ModelAdmin):
    list_display = ['intervenant', 'famille', 'role', 'date_debut', 'date_fin', 'fin_suivi']
    list_filter = ['role']
    search_fields = ['suivi__famille__nom', 'intervenant__nom']
    raw_id_fields = ['suivi']

    def famille(self, obj):
        return obj.suivi.famille

    def fin_suivi(self, obj):
        return obj.suivi.date_fin_suivi


@admin.register(PrestationSPE)
class PrestationSPEAdmin(admin.ModelAdmin):
    list_display = ('lib_prestation', 'date_prestation', 'duree', 'auteur')
    list_filter = (TypePrestationFilter,)
    search_fields = ('famille__nom', 'texte')


@admin.register(DuoSPERdv)
class DuoSPERdvAdmin(admin.ModelAdmin):
    list_display = ('duo_abr', 'rendez_vous')


admin.site.register(Bilan)
admin.site.register(RapportSPE)
