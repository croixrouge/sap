from collections import OrderedDict, namedtuple
from datetime import date, timedelta

from django.contrib.postgres.aggregates import StringAgg
from django.db import models
from django.db.models import F, OuterRef, Q, Subquery
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property

from common import choices
from common.fields import ChoiceArrayField
from croixrouge.models import (
    Contact,
    EtapeBase,
    Famille,
    Personne,
    PrestationBase,
    Rapport,
    Role,
    Utilisateur,
)
from croixrouge.utils import format_d_m_Y, random_string_generator

Equipe = namedtuple("Equipe", ['code', 'nom', 'perm'])

EQUIPES_SPE = [
    Equipe(code='montagnes', nom='Montagnes et V-d-T', perm=None),
    Equipe(code='littoral', nom='Littoral et V-d-R', perm=None),
    # Anciennes équipes, conservées pour anciens dossiers
    Equipe(code='neuch_ville', nom='SPE - Neuchâtel-ville (archives)', perm=None),
    Equipe(code='litt_est', nom='SPE - Littoral Est (archives)', perm=None),
    Equipe(code='litt_ouest', nom='SPE - Littoral Ouest (archives)', perm=None),
]


class FamilleSPEQuerySet(models.QuerySet):
    def with_duos(self):
        """
        Renvoie une liste des groupes Psy/Educ différents, sous forme de chaînes
        de sigles séparées par "/".
        Peut aussi renvoyer 1 ou 3 intervenants, le cas échéant.
        """
        return self.annotate(
            duo=StringAgg(
                F('suivispe__intervenantspe__intervenant__sigle'),
                delimiter='/',
                filter=(
                    Q(suivispe__intervenantspe__date_fin__isnull=True) &
                    Q(suivispe__intervenantspe__role__nom__in=['Educ', 'Psy'])
                ),
                ordering='suivispe__intervenantspe__intervenant__sigle',
            )
        )

    def with_niveau_interv(self):
        """
        Annote les familles avec le niveau d’intervention le plus récent.
        """
        return self.annotate(
            niveau_interv=Subquery(
                NiveauSPE.objects.filter(
                    famille=OuterRef('pk')
                ).order_by('-date_debut').values('niveau_interv')[:1]
            )
        )


class FamilleSPEManager(models.Manager):
    def get_queryset(self):
        return FamilleSPEQuerySet(self.model, using=self._db).filter(
            suivispe__isnull=False
        ).prefetch_related(models.Prefetch('membres', queryset=Personne.objects.select_related('role')))

    def create_famille(self, equipe='', **kwargs):
        kwargs.pop('_state', None)
        kwargs['typ'] = ['spe']
        famille = self.create(**kwargs)
        SuiviSPE.objects.create(
            famille=famille,
            equipe=equipe,
            date_demande=date.today(),
        )
        return famille


class FamilleSPE(Famille):
    class Meta:
        proxy = True
        permissions = {
            ("can_manage_waiting_list", "Gérer la liste d'attente"),
            ("can_archive", "Archiver les dossiers SPE"),
        }

    default_typ = 'spe'
    suivi_name = 'suivispe'
    objects = FamilleSPEManager()

    @classmethod
    def actives(cls, date=None):
        qs = FamilleSPE.objects.filter(suivispe__isnull=False).order_by('nom', 'npa')
        if date is not None:
            return qs.filter(suivispe__date_demande__lte=date).filter(
                models.Q(suivispe__date_fin_suivi__isnull=True) |
                models.Q(suivispe__date_fin_suivi__gte=date)
            )
        else:
            return qs.filter(suivispe__date_fin_suivi__isnull=True)

    @property
    def prestations(self):
        return self.prestations_spe

    def interventions_actives(self, dt=None, aussi_futures=False):
        dt = dt or date.today()
        filters = Q(date_fin__isnull=True) | Q(date_fin__gte=dt)
        if not aussi_futures:
            filters &= Q(date_debut__lte=dt)
        return self.suivi.intervenantspe_set.filter(filters).select_related("intervenant", "role")

    def niveaux(self):
        return sorted(self.niveaux_spe.all(), key=lambda niv: niv.date_debut or date(2099, 1, 1))

    def niveau_actuel(self):
        try:
            return [niv.niveau_interv for niv in self.niveaux()][-1]
        except IndexError:
            return None

    @property
    def print_coords_url(self):
        # May be removed once we implement a 'spe-print-coord-famille' url
        return reverse('spe-print-coord-famille', args=[self.pk])

    @property
    def suivi_url(self):
        return reverse('spe-famille-suivi', args=[self.pk])

    @property
    def edit_url(self):
        return reverse('spe-famille-edit', args=[self.pk])

    def can_be_archived(self, user):
        if self.archived_at:
            return False
        closed_since = date.today() - self.suivi.date_fin_suivi
        return (
            user.has_perm('spe.can_archive') and
            self.suivi.date_fin_suivi is not None and
            closed_since.days > 180 and
            self.suivi.date_fin_suivi.year < date.today().year and
            (date.today().month > 1 or closed_since.days > 400)
        )

    def total_mensuel_suivi(self, date_debut_mois):
        # Accompagnement + ASE/IPE/SOF/APA
        return self.total_mensuel_par_prestation(['spe02', 'spe04', 'spe05', 'spe06', 'spe07'], date_debut_mois)

    def anonymiser(self):
        # Famille
        self.nom = random_string_generator()
        self.rue = ''
        self.telephone = ''
        self.remarques = ''
        self.archived_at = timezone.now()
        self.save()

        # Personne
        for personne in self.membres.all().select_related('role'):
            if personne.role.nom == 'Enfant suivi':
                personne.nom = random_string_generator()
                personne.prenom = random_string_generator()
                personne.contractant = False
                fields = ['rue', 'npa', 'localite', 'telephone', 'email', 'remarque', 'remarque_privee',
                          'profession', 'filiation', 'allergies', 'permis']
                [setattr(personne, field, '') for field in fields]
                personne.save()
                personne.reseaux.clear()
                if hasattr(personne, 'formation'):
                    personne.formation.cercle_scolaire = None
                    fields = ['college', 'classe', 'enseignant', 'creche', 'creche_resp', 'entreprise',
                              'maitre_apprentissage', 'remarque']
                    [setattr(personne.formation, field, '') for field in fields]
                    personne.formation.save()
            else:
                personne.delete()
        # Suivi
        fields = ['difficultes', 'aides', 'competences', 'autres_contacts', 'disponibilites', 'remarque',
                  'remarque_privee', 'motif_detail', 'referent_note', 'collaboration', 'ressources', 'crise',
                  'pers_famille_presentes', 'ref_presents', 'autres_pers_presentes']
        [setattr(self.suivi, field, '') for field in fields]
        self.suivi.save()

        # Related
        for doc in self.documents.all():
            doc.delete()
        for bilan in self.bilans_spe.all():
            bilan.delete()
        for rapport in self.rapports_spe.all():
            rapport.delete()
        self.prestations_spe.all().update(texte='')
        for prestation in self.prestations_spe.exclude(fichier=''):
            prestation.fichier.delete()
            prestation.save()


class _FamilleSPE:
    def __init__(self, famille):
        self.famille = famille
        self.famille.__class__ = FamilleSPE  # Maybe removed at some point?
        self.suivi = famille.suivispe

    def access_ok(self, user):
        if user.has_perm('spe.change_famillespe') or user.is_responsable:
            return True
        intervs = self.famille.interventions_actives(aussi_futures=True)
        return not intervs or user in [interv.intervenant for interv in intervs]

    def can_view(self, user):
        return user.has_perm('spe.view_famillespe')

    def can_edit(self, user):
        if user.has_perm('spe.change_famillespe') or user.is_responsable:
            return True
        if not self.suivi.date_fin_suivi or self.suivi.date_fin_suivi >= date.today():
            intervs = self.famille.interventions_actives()
            if (
                self.can_view(user) and
                (not intervs or user in [
                    interv.intervenant for interv in intervs if interv.role.editeur
                ])
            ):
                return True
        return False

    def can_be_deleted(self, user):
        if not user.has_perm('spe.change_famillespe'):
            return False
        if self.suivi.motif_fin_suivi == 'erreur':
            return True
        return False

Famille.typ_register['spe'] = _FamilleSPE


class Bilan(models.Model):
    famille = models.ForeignKey(FamilleSPE, related_name='bilans_spe', on_delete=models.CASCADE)
    date = models.DateField("Date du bilan")
    auteur = models.ForeignKey(
        Utilisateur, related_name='bilans_spe', on_delete=models.PROTECT, null=True,
    )
    objectifs = models.TextField("Objectifs")
    rythme = models.TextField("Rythme et fréquence")
    sig_famille = models.BooleanField("Apposer signature de la famille", default=True)
    sig_interv = models.ManyToManyField(Utilisateur, blank=True, verbose_name="Signature des intervenants")
    fichier = models.FileField("Fichier/image", blank=True, upload_to='bilans')

    delete_urlname = 'famille-bilanspe-delete'

    def __str__(self):
        return "Bilan du {} pour la famille {}".format(self.date, self.famille)

    def get_absolute_url(self):
        return reverse('famille-bilanspe-view', args=[self.famille_id, self.pk])

    def get_print_url(self):
        return reverse('spe-print-bilan', args=[self.pk])

    def edit_url(self):
        return reverse('famille-bilanspe-edit', args=[self.famille_id, self.pk])

    def can_edit(self, user):
        return self.famille.can_edit(user) or user in self.famille.suivi.intervenants.all()

    def title(self):
        return f"Bilan du {format_d_m_Y(self.date)}"


class Etape(EtapeBase):
    pass


class EtapeDebutSuivi(Etape):
    def date(self, suivi):
        return suivi.debut_suivi_selon_niveau


class EtapeBilan(Etape):
    delai_standard = 3 * 30  # 3 mois
    delai_niveau3 = 30  # 1 mois

    def date(self, suivi):
        """Date du dernier bilan"""
        if self.etape_suivante(suivi).code == 'bilan_suivant':
            return None
        return suivi.date_dernier_bilan()

    def etape_suivante(self, suivi):
        # Soit bilan suivant, soit rapport
        if suivi.date_dernier_rapport():
            return self.suivi_model.WORKFLOW['resume']
        date_bilan = suivi.date_dernier_bilan()
        prochain_rapport = suivi.date_prochain_rapport()
        if date_bilan and prochain_rapport and (prochain_rapport - date_bilan) < timedelta(days=4 * 30):
            return self.suivi_model.WORKFLOW['resume']
        else:
            return self.suivi_model.WORKFLOW['bilan_suivant']

    def delai_depuis(self, suivi, date_base):
        """Délai de cette étape à partir de la date date_base (en général date précédente étape)."""
        if not date_base:
            return None
        base = suivi.date_dernier_bilan() or date_base
        delai = self.delai_niveau3 if suivi.famille.niveau_actuel() == 3 else self.delai_standard
        return base + timedelta(days=delai)


class EtapeResume(Etape):
    def date(self, suivi):
        """Date du dernier rapport"""
        return suivi.date_dernier_rapport()


class EtapeFin(Etape):
    delai_standard = 365 + 180  # 18 mois
    delai_niveau3 = 270  # 9 mois

    def delai_depuis(self, suivi, *args):
        if not suivi.date_debut_suivi:
            return None
        debut_suivi = suivi.debut_suivi_selon_niveau
        delai = self.delai_niveau3 if suivi.famille.niveau_actuel() == 3 else self.delai_standard
        return debut_suivi + timedelta(days=delai)


class SuiviSPE(models.Model):
    WORKFLOW = OrderedDict([
        ('demande', Etape(
            1, 'demande', 'dem', 'Demande déposée', 'debut_evaluation', 0, 'demande', 'demande', True
        )),
        ('debut_evaluation', Etape(
            2, 'debut_evaluation', 'deb_eva', "Début de l’évaluation", 'fin_evaluation', 40, 'demande', 'demande', True
        )),
        ('fin_evaluation', Etape(
            3, 'fin_evaluation', "fin_eva", "Fin de l’évaluation", 'debut_suivi', 40,
            'debut_evaluation', 'debut_evaluation', True
        )),
        ('debut_suivi', EtapeDebutSuivi(
            4, 'debut_suivi', "dsuiv", 'Début du suivi', 'bilan_suivant', 40, 'fin_evaluation', 'fin_evaluation', False
        )),
        ('bilan_suivant',  EtapeBilan(
            5, 'bilan_suivant', "bil", 'Bilan suivant', 'resume', 90, 'debut_suivi', 'fin_evaluation', False
        )),
        ('resume', EtapeResume(
            6, 'resume', 'rés', 'Résumé', 'fin_suivi', 90, 'bilan_suivant', 'fin_evaluation', False
        )),
        ('fin_suivi', EtapeFin(
            7, 'fin_suivi', "fsuiv", 'Fin du suivi', 'archivage', 365 + 180, 'resume', 'fin_evaluation', True
        )),
        ('archivage', Etape(
            8, 'arch_dossier', 'arch', 'Archivage du dossier', None, 0, 'fin_suivi', 'fin_suivi', False
        )),
    ])

    EQUIPES_CHOICES = [(equ.code, equ.nom) for equ in EQUIPES_SPE]

    famille = models.OneToOneField(FamilleSPE, on_delete=models.CASCADE)
    equipe = models.CharField('Équipe', max_length=15, choices=EQUIPES_CHOICES)
    heure_coord = models.BooleanField("Heure de coordination", default=False)
    difficultes = models.TextField("Difficultés", blank=True)
    aides = models.TextField("Aides souhaitées", blank=True)
    competences = models.TextField('Ressources/Compétences', blank=True)
    dates_demande = models.CharField('Dates', max_length=128, blank=True)
    autres_contacts = models.TextField("Autres services contactés", blank=True)
    disponibilites = models.TextField('Disponibilités', blank=True)
    remarque = models.TextField(blank=True)
    remarque_privee = models.TextField('Remarque privée', blank=True)
    service_orienteur = models.CharField(
        "Orienté vers le SPE par", max_length=15, choices=choices.SERVICE_ORIENTEUR_CHOICES, blank=True
    )
    service_annonceur = models.CharField('Service annonceur', max_length=60, blank=True)
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=60, choices=choices.MOTIF_DEMANDE_CHOICES),
        verbose_name="Motif de la demande", blank=True, null=True)
    motif_detail = models.TextField('Motif', blank=True)

    # Référents
    intervenants = models.ManyToManyField(
        Utilisateur, through='IntervenantSPE', related_name='interventions_spe', blank=True
    )
    ope_referent = models.ForeignKey(Contact, blank=True, null=True, related_name='+',
                                     on_delete=models.SET_NULL, verbose_name='as. OPE')
    ope_referent_2 = models.ForeignKey(
        Contact, blank=True, null=True, related_name='+', on_delete=models.SET_NULL,
        verbose_name='as. OPE 2'
    )
    mandat_ope = ChoiceArrayField(
        models.CharField(max_length=65, choices=choices.MANDATS_OPE_CHOICES, blank=True),
        verbose_name="Mandat OPE", blank=True, null=True,
    )
    sse_referent = models.ForeignKey(Contact, blank=True, null=True, related_name='+',
                                     on_delete=models.SET_NULL, verbose_name='SSE')
    referent_note = models.TextField('Autres contacts', blank=True)
    collaboration = models.TextField('Collaboration', blank=True)
    ressource = models.TextField('Ressource', blank=True)
    crise = models.TextField('Gestion de crise', blank=True)

    date_demande = models.DateField("Demande déposée le", blank=True, null=True, default=None)
    date_debut_evaluation = models.DateField("Début de l’évaluation le", blank=True, null=True, default=None)
    date_fin_evaluation = models.DateField("Fin de l’évaluation le", blank=True, null=True, default=None)
    date_debut_suivi = models.DateField("Début du suivi le", blank=True, null=True, default=None)
    date_fin_suivi = models.DateField("Fin du suivi le", blank=True, null=True, default=None)

    demande_prioritaire = models.BooleanField("Demande prioritaire", default=False)
    demarche = ChoiceArrayField(models.CharField(max_length=60, choices=choices.DEMARCHE_CHOICES, blank=True),
                                verbose_name="Démarche", blank=True, null=True)

    pers_famille_presentes = models.CharField('Membres famille présents', max_length=200, blank=True)
    ref_presents = models.CharField('Intervenants présents', max_length=250, blank=True)
    autres_pers_presentes = models.CharField('Autres pers. présentes', max_length=100, blank=True)
    motif_fin_suivi = models.CharField('Motif de fin de suivi', max_length=20,
                                       choices=choices.MOTIFS_FIN_SUIVI_CHOICES, blank=True)
    unite = 'spe'

    class Meta:
        verbose_name = "Suivi SPE"
        verbose_name_plural = "Suivis SPE"

    def __str__(self):
        return 'Suivi SPE pour la famille {} '.format(self.famille)

    @property
    def date_fin_theorique(self):
        if self.date_fin_suivi:
            return self.date_fin_suivi
        if self.date_debut_suivi is None:
            return None
        return self.debut_suivi_selon_niveau + timedelta(days=548)  # env. 18 mois

    @cached_property
    def debut_suivi_selon_niveau(self):
        """
        Date de début de suivi tenant compte d'éventuel changement de niveau entre 2 et 3.
        """
        debut = self.date_debut_suivi
        if debut is None:
            return debut
        niv_prec = None
        for niv in self.famille.niveaux():
            if (
                (niv_prec in [1, 2] and niv.niveau_interv == 3) or
                (niv_prec == 3 and niv.niveau_interv in [1, 2])
            ):
                debut = niv.date_debut
            niv_prec = niv.niveau_interv
        return debut

    def date_prochain_rapport(self):
        if self.date_debut_suivi is None or self.date_fin_suivi is not None:
            return None
        date_dernier_rapport = self.date_dernier_rapport()
        niveau = self.famille.niveau_actuel()
        if date_dernier_rapport is None:
            # Premier à 9 ou 18 mois, selon niveau
            delai = 30 * 9 if niveau == 3 else 365 + 182
            return self.date_debut_suivi + timedelta(days=delai)
        else:
            # Suivants tous les 9 ou 12 mois, selon niveau
            delai = 30 * 9 if niveau == 3 else 365
            return date_dernier_rapport + timedelta(days=delai)

    def date_dernier_rapport(self):
        # Using rapports_spe.all() to leverage prefetchs
        debut_suivi = self.debut_suivi_selon_niveau
        return max([
            rapp.date for rapp in self.famille.rapports_spe.all() if rapp.date > debut_suivi
        ], default=None)

    def date_dernier_bilan(self):
        debut_suivi = self.debut_suivi_selon_niveau
        # Using bilans_spe.all() to leverage prefetchs
        return max([
            bilan.date for bilan in self.famille.bilans_spe.all() if bilan.date > debut_suivi
        ], default=None)

    @property
    def ope_referent_display(self):
        return self.ope_referent.nom_prenom if self.ope_referent else '-'

    @cached_property
    def etape(self):
        """L’étape *terminée* du suivi."""
        for key, etape in reversed(self.WORKFLOW.items()):
            if key != 'archivage':
                if etape.date(self):
                    return etape

    @cached_property
    def etape_suivante(self):
        return self.etape.etape_suivante(self) if self.etape else None

    def date_suivante(self):
        etape_date = self.etape.date(self)
        if not self.etape_suivante or not etape_date:
            return None
        return self.etape_suivante.delai_depuis(self, etape_date)

    def get_mandat_ope_display(self):
        dic = dict(choices.MANDATS_OPE_CHOICES)
        return '; '.join([dic[value] for value in self.mandat_ope]) if self.mandat_ope else '-'

    def get_motif_demande_display(self):
        dic = dict(choices.MOTIF_DEMANDE_CHOICES)
        return '; '.join([dic[value] for value in self.motif_demande]) if self.motif_demande else ''

    @property
    def ope_referents(self):
        return [ope for ope in [self.ope_referent, self.ope_referent_2] if ope]


# Avoid circular import
Etape.suivi_model = SuiviSPE


class PrestationSPE(PrestationBase):
    CURRENT_APP = "spe"

    famille = models.ForeignKey(FamilleSPE, related_name='prestations_spe', null=True, blank=True,
                                on_delete=models.SET_NULL, verbose_name="Famille")
    intervenants = models.ManyToManyField(Utilisateur, related_name='prestations_spe')
    medlink_date = models.DateTimeField('Date import. Medlink', blank=True, null=True)
    unite = 'spe'

    def can_edit(self, user):
        return (
            (user == self.auteur or user in self.intervenants.all() or
             user.has_perm(f'{self.CURRENT_APP}.edit_prest_prev_month')
            ) and self.check_date_allowed(user, self.date_prestation)
        )

    def save(self, *args, **kwargs):
        if self.famille is None:
            self.familles_actives = FamilleSPE.actives(self.date_prestation).count()
        super().save(*args, **kwargs)

    def edit_url(self):
        return reverse('spe-prestation-edit', args=[self.famille.pk if self.famille else 0, self.pk])


class IntervenantSPEManager(models.Manager):
    def actifs(self, _date=None):
        return self.filter(Q(date_fin__isnull=True) | Q(date_fin__gt=_date or date.today()))


class IntervenantSPE(models.Model):
    """
    Modèle M2M entre SuiviSPE et Utilisateur (utilisé par through de SuiviSPE.intervenants).
    """
    suivi = models.ForeignKey(SuiviSPE, on_delete=models.CASCADE)
    intervenant = models.ForeignKey(Utilisateur, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    date_debut = models.DateField('Date début', default=timezone.now)
    # date_fin est utilisé pour les interventions s'arrêtant avant la fin du suivi.
    date_fin = models.DateField('Date fin', null=True, blank=True)

    objects = IntervenantSPEManager()

    def __str__(self):
        return '{}, {} pour {}'.format(self.intervenant, self.role, self.suivi)

    def can_edit(self, user):
        return self.suivi.famille.can_edit(user)


class RapportSPE(Rapport):
    famille = models.ForeignKey(FamilleSPE, related_name='rapports_spe', on_delete=models.CASCADE)
    pres_interv = models.ManyToManyField(
        Utilisateur, blank=True, verbose_name="Intervenants cités dans le résumé", related_name='+'
    )
    sig_interv = models.ManyToManyField(
        Utilisateur, blank=True, verbose_name="Signature des intervenants", related_name='+'
    )
    # Remplace les anciens champs evolutions et evaluation (ces derniers pourront être
    # supprimés lorsque tous les rapports ayant du contenu pour ces champs auront été archivés).
    observations = models.TextField("Observations, évolution et hypothèses", blank=True)

    def is_observations_enable(self):
        return self.evolutions == '' and self.evaluation == ''

    def intervenants(self):
        if self.pres_interv.all():
            return list(self.pres_interv.all())
        return super().intervenants()


class NiveauSPE(models.Model):
    INTERV_CHOICES = [(0, '0'), (1, '1'), (2, '2'), (3, '3')]
    famille = models.ForeignKey(
        FamilleSPE, related_name='niveaux_spe', on_delete=models.CASCADE, verbose_name="Famille"
    )
    niveau_interv = models.PositiveSmallIntegerField("Niveau d’intervention", choices=INTERV_CHOICES)
    date_debut = models.DateField('Date début', blank=True, null=True)
    date_fin = models.DateField('Date fin', blank=True, null=True)

    def __str__(self):
        return (
            f"{self.famille.nom} - {self.niveau_interv} - du {format_d_m_Y(self.date_debut)} au "
            f"{format_d_m_Y(self.date_fin)}"
        )


class DuoSPERdv(models.Model):
    duo_abr = models.CharField(max_length=15)
    rendez_vous = models.DateField('Rendez-vous')

    def __str__(self):
        return f"Rendez-vous le {self.rendez_vous.strftime('%d.%m.%Y')} pour le duo {self.duo_abr}"
