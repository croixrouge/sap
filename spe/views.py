import calendar
import logging
from datetime import date, timedelta
from operator import attrgetter

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.postgres.aggregates import ArrayAgg
from django.core.exceptions import PermissionDenied
from django.db.models import Case, Count, DurationField, F, OuterRef, Prefetch, Q, Subquery, Sum, Value, When
from django.db.models.functions import Coalesce
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, FormView, ListView, TemplateView, UpdateView, View

from croixrouge.export import ExportReporting
from croixrouge.forms import JournalFilterForm, MonthSelectionForm
from croixrouge.models import LibellePrestation, Utilisateur
from croixrouge.pdf import JournalPdf
from croixrouge.utils import format_d_m_Y
from croixrouge.views import BasePDFView, BaseSuiviView, CreateUpdateView, FamilleUpdateViewBase, JournalAccesMixin

from .forms import (
    AgendaForm,
    DemandeSPEForm,
    DuoDateEditForm,
    EquipeFilterForm,
    FamilleCreateForm,
    FamilleFilterForm,
    FamilleForm,
    IntervenantSPEEditForm,
    IntervenantSPEForm,
    NiveauSPEForm,
    PrestationSPEForm,
    SuiviSPEForm,
)
from .models import Bilan, DuoSPERdv, FamilleSPE, IntervenantSPE, NiveauSPE, PrestationSPE, SuiviSPE
from .pdf import BilanPdf, CoordonneesFamillePdf, EvaluationPdf

logger = logging.getLogger('django')

MSG_READ_ONLY = "Vous n'avez pas les droits nécessaires pour modifier cette page"
MSG_ACCESS_DENIED = "Vous n’avez pas la permission d’accéder à cette page."


class EquipeRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleSPE, pk=kwargs['pk'])
        self.check_access(request)
        return super().dispatch(request, *args, **kwargs)

    def check_access(self, request):
        if not self.famille.can_view(request.user):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        self.readonly = not self.famille.can_edit(request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.readonly:
            messages.info(self.request, MSG_READ_ONLY)
        context['can_edit'] = not self.readonly
        context['famille'] = self.famille
        return context


class CheckCanEditMixin:
    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)


class FamilleCreateView(CreateView):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleSPE
    form_class = FamilleCreateForm
    action = 'Nouvelle famille'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('spe.add_famillespe'):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        famille = form.save()
        return HttpResponseRedirect(reverse('spe-famille-edit', args=[famille.pk]))


class FamilleUpdateView(EquipeRequiredMixin, SuccessMessageMixin, FamilleUpdateViewBase):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleSPE
    famille_model = FamilleSPE
    form_class = FamilleForm
    success_message = 'La famille %(nom)s a bien été modifiée.'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}


class FamilleListView(ListView):
    template_name = 'spe/famille_list.html'
    model = FamilleSPE
    paginate_by = 20
    mode = 'normal'
    normal_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Réf. SPE', 'referents'),
        ('Réf. OPE', 'referents_ope'), ('Statut', 'suivi'), ('Prior.', 'prioritaire'),
        ('Niveau', 'niveau_interv'),
    ]
    attente_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Région', 'region'),
        ('Réf. SPE', 'referents'), ('Réf. OPE', 'referents_ope'), ('Prior.', 'prioritaire'),
        ('Demande', 'date_demande'), ('Évaluation', 'evaluation'),
    ]

    def get(self, request, *args, **kwargs):
        request.session['current_app'] = 'spe'
        if self.mode == 'attente':
            self.paginate_by = None
            form_class = EquipeFilterForm
        else:
            form_class = FamilleFilterForm
        self.filter_form = form_class(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        familles = super().get_queryset().with_niveau_interv().filter(
            suivispe__isnull=False, suivispe__date_fin_suivi__isnull=True
        ).select_related(
            'suivispe__ope_referent', 'suivispe__ope_referent_2'
        ).prefetch_related(
            Prefetch(
                'suivispe__intervenantspe_set',
                queryset=IntervenantSPE.objects.actifs().select_related('intervenant', 'role')
            ),
            Prefetch(
                'niveaux_spe', queryset=NiveauSPE.objects.order_by('-date_debut')
            ),
            'suivispe__intervenants', 'bilans_spe', 'rapports_spe'
        ).order_by('nom', 'npa')

        if self.mode == 'attente':
            familles = familles.filter(
                suivispe__date_debut_suivi__isnull=True
            ).order_by(
                '-suivispe__demande_prioritaire', 'suivispe__date_demande',
            )
        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.mode == 'attente':
            cols = self.attente_cols
        else:
            cols = self.normal_cols
        # cf requete similaire dans UtilisateurChargeDossierView
        current_user = self.request.user
        ma_charge = current_user.interventions_spe.filter(
            Q(intervenantspe__date_fin__isnull=True) | Q(intervenantspe__date_fin__gt=date.today())
        ).annotate(
            niveau_interv=Subquery(
                NiveauSPE.objects.filter(
                    famille=OuterRef('famille_id')
                ).order_by('-date_debut').values('niveau_interv')[:1]
            ),
        ).aggregate(
            charge=Sum(Case(
                When(niveau_interv__lt=3, then='niveau_interv'),
                When(niveau_interv=3, then=5),
            ), filter=Q(date_fin_suivi__isnull=True)),
            nbre_coord=Count('heure_coord', filter=Q(heure_coord=True, date_fin_suivi__isnull=True)),
            nbre_eval=Count('id', filter=(
                Q(date_fin_suivi__isnull=True) & Q(date_debut_suivi__isnull=True)
            )),
            nbre_suivi=Count('id', filter=(
                Q(date_fin_suivi__isnull=True) & Q(date_debut_suivi__isnull=False)
            )),
        )

        context.update({
            'labels': [c[0] for c in cols],
            'col_keys': [c[1] for c in cols],
            'form': self.filter_form,
            'ma_charge': {
                'heures': ma_charge['charge'] + ma_charge['nbre_coord'],
                'nbre_eval': ma_charge['nbre_eval'],
                'nbre_suivi': ma_charge['nbre_suivi'],
                'charge_diff': current_user.charge_max - (ma_charge['charge'] + ma_charge['nbre_coord']),
            } if ma_charge['charge'] is not None else '',
        })
        if self.filter_form.is_bound and self.filter_form.cleaned_data.get('duos'):
            selected_duo = self.filter_form.cleaned_data.get('duos')
            duo_rdvs = DuoSPERdv.objects.filter(duo_abr=selected_duo).order_by('-rendez_vous')
            context.update({
                'duo_rdv_passes': [rdv for rdv in duo_rdvs if rdv.rendez_vous < date.today()],
                'prochain_duo_rdv': next((rdv for rdv in duo_rdvs if rdv.rendez_vous >= date.today()), None),
                'can_view_rdvs': (
                    current_user.is_responsable or current_user.sigle in selected_duo.split('/')
                ),
                'can_edit_rdvs': current_user.is_responsable,
            })
        return context


class SPESuiviView(EquipeRequiredMixin, JournalAccesMixin, BaseSuiviView):
    template_name = 'spe/suivi_edit.html'
    model = SuiviSPE
    famille_model = FamilleSPE
    form_class = SuiviSPEForm

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            "today": date.today(),
            "intervenants": self.object.famille.interventions_actives(aussi_futures=True).order_by('role'),
            "niveaux": self.object.famille.niveaux_spe.all().annotate(
                date_fin_calc=Coalesce('date_fin', self.object.date_fin_suivi)
            ).order_by('pk'),
        })
        return context


class SuiviIntervenantCreate(EquipeRequiredMixin, CreateView):
    model = IntervenantSPE
    form_class = IntervenantSPEForm
    template_name = 'croixrouge/form_in_popup.html'
    titre_page = "Ajout d’un intervenant"
    titre_formulaire = "Intervenant"

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'suivi': get_object_or_404(SuiviSPE, famille__pk=self.kwargs['pk']),
        }

    def get_success_url(self):
        return reverse('spe-famille-suivi', args=[self.kwargs['pk']])


class SuiviIntervenantUpdateView(EquipeRequiredMixin, UpdateView):
    model = IntervenantSPE
    form_class = IntervenantSPEEditForm
    template_name = 'croixrouge/form_in_popup.html'
    titre_page = "Modification d’une intervention"
    titre_formulaire = "Intervenant"

    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('spe-famille-suivi', args=[self.kwargs['pk']])


class SuivisTerminesListView(FamilleListView):
    template_name = 'spe/suivis_termines_list.html'

    def get_queryset(self):
        suivi_name = self.model.suivi_name
        familles = self.model.objects.filter(**{
            f'{suivi_name}__date_fin_suivi__isnull': False,
            'archived_at__isnull': True
        }).select_related(suivi_name)

        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles


class AgendaSuiviView(EquipeRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'spe/agenda_suivi.html'
    model = SuiviSPE
    famille_model = FamilleSPE
    form_class = AgendaForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, famille__pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(),
                'readonly': self.readonly,
                'destination': self.get_object().famille.destination,
                'request': self.request}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dfin = self.object.date_fin_suivi
        ddebut = self.object.date_debut_suivi
        context.update({
            'bilans_et_rapports': sorted(
                list(self.famille.bilans_spe.all()) + list(self.famille.rapports_spe.all()),
                key=attrgetter('date'),
            ),
            'mode': 'reactivation' if dfin else ('evaluation' if ddebut is None else 'suivi'),
            'interv_temporaires': self.object.intervenantspe_set.filter(date_fin__isnull=False),
            'niveaux': self.object.famille.niveaux_spe.all().annotate(
                date_fin_calc=Coalesce('date_fin', self.object.date_fin_suivi)
            ).order_by('pk'),
        })
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        if (
            'date_debut_suivi' in form.changed_data and form.cleaned_data['date_debut_suivi'] and
            form.cleaned_data['date_debut_suivi'] < date.today()
        ):
            # Contrôle attribution des prestations (accompagnement) depuis début suivi.
            self.famille.prestations.filter(
                date_prestation__gte=form.cleaned_data['date_debut_suivi'], lib_prestation__code='spe01'
            ).update(lib_prestation=LibellePrestation.objects.get(code='spe02'))
        return response

    def get_success_url(self):
        if self.object.date_fin_suivi:
            return reverse('spe-famille-list')
        return reverse('spe-famille-agenda', args=[self.object.famille.pk])


class DemandeView(EquipeRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'croixrouge/demande_edit.html'
    model = SuiviSPE
    famille_model = FamilleSPE
    form_class = DemandeSPEForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}

    def get_object(self, queryset=None):
        suivi = self.famille.suivi
        items = [
            "Selon le professionnel",
            "Selon les parents",
            "Selon l'enfant",
        ]
        if suivi.difficultes == '':
            suivi.difficultes = ''.join(
                ['<p><strong>{}</strong></p><p></p>'.format(item) for item in items if suivi.difficultes == '']
            )
        if suivi.aides == '':
            suivi.aides = ''.join(
                ['<strong>{}</strong></p><p></p>'.format(item) for item in items if suivi.aides == '']
            )
        if suivi.disponibilites == '':
            suivi.disponibilites = ''.join(
                ['<strong>{}</strong></p><p></p>'.format(item) for item in ['Parents', 'Enfants']
                 if suivi.disponibilites == '']
            )
        return suivi

    def get_success_url(self):
        return reverse('spe-demande', args=[self.object.famille.pk])


class EvaluationPDFView(BasePDFView):
    obj_class = FamilleSPE
    pdf_class = EvaluationPdf


class CoordonneesPDFView(BasePDFView):
    obj_class = FamilleSPE
    pdf_class = CoordonneesFamillePdf


class JournalPDFView(BasePDFView):
    obj_class = FamilleSPE
    pdf_class = JournalPdf


class PrestationMenu(ListView):
    template_name = 'spe/prestation_menu.html'
    model = PrestationSPE
    paginate_by = 15
    context_object_name = 'familles'

    def get_queryset(self):
        user = self.request.user
        typ = FamilleSPE.default_typ
        rdv_manques = dict(
            FamilleSPE.actives().annotate(
                rdv_manques=ArrayAgg(
                    'prestations_spe__date_prestation',
                    filter=Q(prestations_spe__manque=True),
                    ordering='prestations_spe__date_prestation',
                    default=Value([])
                )
            ).values_list('pk', 'rdv_manques')
        )
        familles = FamilleSPE.actives(
        ).annotate(
            user_prest=Sum(f'prestations_{typ}__duree',
                           filter=Q(**{f'prestations_{typ}__intervenants': user}))
        ).annotate(
            spe1=Sum(f'prestations_{typ}__duree',
                     filter=(Q(**{f'prestations_{typ}__lib_prestation__code': 'spe01'}))
                     ) or None
        ).annotate(
            spe2=Sum(f'prestations_{typ}__duree',
                     filter=(Q(**{f'prestations_{typ}__lib_prestation__code': 'spe02'}))
                     ) or None
        )
        for famille in familles:
            famille.rdv_manques = [format_d_m_Y(rdv) for rdv in rdv_manques[famille.pk]]
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'annee': date.today().year
        })
        return context


class PrestationBaseView:
    model = PrestationSPE
    famille_model = FamilleSPE
    form_class = PrestationSPEForm
    template_name = 'spe/prestation_edit.html'

    def dispatch(self, *args, **kwargs):
        self.famille = get_object_or_404(self.famille_model, pk=self.kwargs['pk']) if self.kwargs.get('pk') else None
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille}

    def get_success_url(self):
        if self.famille:
            return reverse('spe-journal-list', args=[self.famille.pk])
        return reverse('spe-prestation-gen-list')


class PrestationListView(PrestationBaseView, ListView):
    template_name = 'spe/prestation_list.html'
    model = PrestationSPE
    paginate_by = 15
    context_object_name = 'prestations'

    def get(self, request, **kwargs):
        self.filter_form = JournalFilterForm(famille=self.famille, data=request.GET or None)
        return super().get(request, **kwargs)

    def get_queryset(self):
        if self.famille:
            prestations = self.famille.prestations.all().order_by('-date_prestation')
            if self.filter_form.is_bound and self.filter_form.is_valid():
                prestations = self.filter_form.filter(prestations)
            return prestations
        return self.request.user.prestations_spe.filter(famille=None)

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs, filter_form=self.filter_form)


class PrestationCreateView(PrestationBaseView, CreateView):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('spe.add_prestationspe'):
            raise PermissionDenied("Vous n'avez pas les droits pour ajouter une prestation.")
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial_text = (
            "<p><b>Observations:\n"
            "(relations et interactions dans la famille / disponibilité des parents / "
            "réponses données aux besoins des enfants / …)</b></p><p></p>"
            "<p><b>Discussion (thème-s abordé-s) / activités:</b></p><p></p>"
            "<p><b>Particularités en termes de ressources et/ou de limites:</b></p><p></p>"
            "<p><b>Ressentis de l’intervenant-e:</b></p><p></p>"
            "<p><b>Objectif-s traité-s:</b></p><p></p>"
            "<p><b>Objectif-s à suivre:</b></p><p></p>"
        )
        initial = {
            **super().get_initial(),
            'intervenants': [self.request.user.pk],
            'texte': initial_text if self.famille else '',
        }
        return initial

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def get_lib_prestation(self, date_prest):
        """
        Renvoie la prestation en fonction de la famille et du rôle de l'utilisateur connecté.
        """
        if self.famille is None:
            code = 'spe03'  # Prestation générale
        elif self.request.user.has_role(['ASE']):
            code = 'spe04'
        elif self.request.user.has_role(['IPE']):
            code = 'spe05'
        elif self.request.user.has_role(['Assistant-e social-e']):
            code = 'spe06'
        elif self.request.user.has_role(['Coach APA']):
            code = 'spe07'
        elif self.famille.suivi.date_debut_suivi and date_prest >= self.famille.suivi.date_debut_suivi:
            code = 'spe02'  # Accompagnement
        else:
            code = 'spe01'  # Évaluation
        return LibellePrestation.objects.get(code=code)

    def form_valid(self, form):
        if self.famille:
            form.instance.famille = self.famille
        form.instance.auteur = self.request.user
        form.instance.lib_prestation = self.get_lib_prestation(form.cleaned_data['date_prestation'])
        if 'duree' not in form.cleaned_data:
            form.instance.duree = timedelta()
        return super().form_valid(form)


class PrestationUpdateView(CheckCanEditMixin, PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def delete_url(self):
        fam_id = self.famille.pk if self.famille else 0
        return reverse('spe-prestation-delete', args=[fam_id, self.object.pk])


class PrestationDeleteView(PrestationBaseView, DeleteView):
    template_name = 'croixrouge/object_confirm_delete.html'
    pk_url_kwarg = 'obj_pk'
    form_class = DeleteView.form_class


class BilanPDFView(BasePDFView):
    obj_class = Bilan
    pdf_class = BilanPdf


class UtilisateurChargeDossierView(PermissionRequiredMixin, TemplateView):
    template_name = 'spe/charge_utilisateurs.html'
    permission_required = 'croixrouge.change_utilisateur'

    def get_queryset(self):
        self.filter_form = EquipeFilterForm(data=self.request.GET or None)

        qs_utilisateurs = Utilisateur.objects.filter(
            roles__nom__in=['Educ', 'Psy'], date_desactivation__isnull=True
        ).exclude(
            roles__nom__in=['Responsable/coordinateur']
        ).order_by('nom', 'prenom').distinct()
        if self.filter_form.is_bound and self.filter_form.is_valid():
            qs_utilisateurs = qs_utilisateurs.filter(equipe=self.filter_form.cleaned_data['equipe'])

        # cf requete similaire dans FamilleListView
        intervs = IntervenantSPE.objects.filter(
            Q(suivi__date_fin_suivi__isnull=True) & (
                Q(date_fin__isnull=True) | Q(date_fin__gt=date.today())
            )
        ).values('intervenant').annotate(
            familles=ArrayAgg(
                "suivi__famille_id",
                distinct=True
            ),
            nbre_coord=Count(
                'id', filter=Q(suivi__heure_coord=True),
                distinct=True
            ),
            nbre_eval=Count('id', filter=(
                Q(suivi__date_debut_suivi__isnull=True)
            ), distinct=True),
            nbre_suivi=Count('id', filter=(
                Q(suivi__date_debut_suivi__isnull=False)
            ), distinct=True),
        ).order_by('intervenant__id').values(
            'intervenant__id', 'familles', 'nbre_coord', 'nbre_eval', 'nbre_suivi'
        )
        intervs_dict = {line['intervenant__id']: line for line in intervs}

        familles = dict(FamilleSPE.objects.filter(
            suivispe__date_fin_suivi__isnull=True
        ).with_niveau_interv().values_list('pk', 'niveau_interv'))

        utilisateurs = []
        charge_map = {None: 0, 0: 0, 1: 1, 2: 2, 3: 5}
        for util in qs_utilisateurs:
            for key in 'familles', 'nbre_coord', 'nbre_eval', 'nbre_suivi':
                setattr(util, key, intervs_dict.get(util.pk, {}).get(key, [] if key == 'familles' else 0))
            util.charge = sum([charge_map[familles[fam_pk]] for fam_pk in util.familles])
            if util.taux_activite == 0 and util.charge is None:
                continue
            util.heures = util.charge + util.nbre_coord if util.charge is not None else 0
            util.charge_diff = util.charge_max - util.heures
            utilisateurs.append(util)
        return utilisateurs

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'utilisateurs': self.get_queryset(),
            'filter_form': self.filter_form,
        }


class DuoSPERdvEditView(UpdateView):
    template_name = 'spe/date_entretien_duo_change.html'
    model = DuoSPERdv
    form_class = DuoDateEditForm

    @property
    def duo_abr(self):
        return '/'.join(self.kwargs['sigles'])

    def get_object(self):
        try:
            return DuoSPERdv.objects.filter(duo_abr=self.kwargs['sigles']).latest('rendez_vous')
        except DuoSPERdv.DoesNotExist:
            return None

    def form_valid(self, form):
        form.instance.duo_abr = self.kwargs['sigles']
        form.save()
        return HttpResponseRedirect(reverse('spe-famille-list'))


class FamilleArchivableListe(View):
    """Return all family ids which are archivable by the current user."""

    def get(self, request, *args, **kwargs):
        data = [
            famille.pk for famille in FamilleSPE.objects.filter(
                archived_at__isnull=True, suivispe__date_fin_suivi__isnull=False
            )
            if famille.can_be_archived(request.user)
        ]
        return JsonResponse(data, safe=False)


class NiveauSPECreateUpdateView(CreateUpdateView):
    model = NiveauSPE
    form_class = NiveauSPEForm
    template_name = 'croixrouge/form_in_popup.html'

    def dispatch(self, request, *args, **kwargs):
        self.is_create = 'pk' not in kwargs
        self.famille = get_object_or_404(FamilleSPE, pk=kwargs['obj_pk'])
        self.titre_page = f"Famille: {self.famille.nom}"
        self.titre_formulaire = "Nouveau niveau d'intervention" if self.is_create \
            else "Modification de l'enregistrement"
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        return kwargs

    def form_valid(self, form):
        if self.is_create:
            der_niv = self.famille.niveaux_spe.last() if self.famille.niveaux_spe.exists() else None
            if der_niv:
                der_niv.date_fin = form.cleaned_data['date_debut'] - timedelta(days=1)
                der_niv.save()

            form.instance.famille = self.famille
            form.instance.date_fin = None
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('spe-famille-suivi', args=[self.famille.pk])

    def delete_url(self):
        return reverse('spe-niveau-delete', args=[self.famille.pk, self.object.pk])


class NiveauSPEDeleteView(DeleteView):
    template_name = 'croixrouge/object_confirm_delete.html'
    form_class = DeleteView.form_class
    model = NiveauSPE

    def get_success_url(self):
        return reverse('spe-famille-suivi', args=[self.kwargs['obj_pk']])


class ExportPrestationView(PermissionRequiredMixin, FormView):
    permission_required = 'croixrouge.export_stats'
    template_name = "spe/statistiques/export.html"
    form_class = MonthSelectionForm
    filename = 'croixrouge_reporting_{}'.format(date.strftime(date.today(), '%Y-%m-%d'))

    def _prepare_query(self, query, prest_gen):
        """Return a list of families, sorted by prest and name."""
        return query.annotate(
            prest_gen=Value(prest_gen, output_field=DurationField())
        ).order_by('nom')

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'mois': date.today().month - 1 if date.today().month > 1 else 12,
            'annee': date.today().year if date.today().month > 1 else date.today().year - 1,
        })
        return initial

    def suivis_en_cours(self, debut, fin):
        base = FamilleSPE.objects.annotate(
            date_deb=Coalesce(
                'suivispe__date_demande',
                'suivispe__date_debut_evaluation',
                'suivispe__date_debut_suivi'
            ),
            num_enf_suivis=Count("membres", filter=Q(membres__role__nom="Enfant suivi")),
        ).prefetch_related('membres', 'niveaux_spe')
        return base.filter(
            date_deb__lte=fin
        ).filter(
            Q(suivispe__date_fin_suivi__isnull=True) |
            Q(suivispe__date_fin_suivi__gte=debut)
        ).exclude(suivispe__motif_fin_suivi='erreur')

    def suivis_nouveaux(self, annee, mois):
        base = FamilleSPE.objects.annotate(
            date_dem=F('suivispe__date_demande')
        ).prefetch_related('membres', 'niveaux_spe')
        return base.filter(**{
            'date_dem__year': annee, 'date_dem__month': mois,
        }).exclude(suivispe__motif_fin_suivi='erreur')

    def suivis_termines(self, annee, mois):
        base = FamilleSPE.objects.all().prefetch_related('membres', 'niveaux_spe')
        return base.filter(
            suivispe__date_fin_suivi__year=annee,
            suivispe__date_fin_suivi__month=mois,
        ).exclude(suivispe__motif_fin_suivi='erreur')

    def form_valid(self, form):
        mois = int(form.cleaned_data['mois'])
        annee = int(form.cleaned_data['annee'])
        debut_mois = date(annee, mois, 1)
        fin_mois = date(annee, mois, calendar.monthrange(annee, mois)[1])

        export = ExportReporting()
        familles_en_cours = self.suivis_en_cours(debut_mois, fin_mois)
        # Considérer au min. 1 enfant par famille, car les familles sans enfant
        # suivi occupent aussi une ligne de la stat des familles en cours.
        num_enfants = (
            familles_en_cours.aggregate(
                num_enf=Sum("num_enf_suivis")
            )["num_enf"] or 0
        ) + familles_en_cours.filter(num_enf_suivis=0).count()
        # Calcul des prestations générales réparties sur chaque enfant suivi
        # Renvoie une durée (timedelta()) par enfant suivi
        prest_gen = PrestationSPE.objects.filter(
            date_prestation__month=mois,
            date_prestation__year=annee,
            famille=None
        ).annotate(
            num_util=Count('intervenants')
        ).aggregate(
            total=Sum(F('duree') * F('num_util'), output_field=DurationField())
        )['total'] or timedelta()
        logger.info("Total heures prest. gén. du mois: %s", prest_gen.total_seconds() / 3600)
        prest_gen_par_enfant = prest_gen / (num_enfants or 1)

        # Demandes en cours
        export.produce_suivis(
            'En_cours_{}'.format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(familles_en_cours, prest_gen_par_enfant),
            debut_mois
        )

        #  Nouvelles demandes
        familles_nouvelles = self.suivis_nouveaux(annee, mois)
        export.produce_nouveaux(
            "Nouveaux_{}".format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(familles_nouvelles, prest_gen_par_enfant),
        )

        #  Fins de suivis
        familles_terminees = self.suivis_termines(annee, mois)
        export.produce_termines(
            "Terminés_{}".format(debut_mois.strftime("%m.%Y")),
            self._prepare_query(familles_terminees, prest_gen_par_enfant),
        )
        export.produce_totaux("Totaux heures du mois")

        return export.get_http_response(
            'croixrouge_reporting_{}.xlsx'.format(date.strftime(debut_mois, '%m_%Y'))
        )
