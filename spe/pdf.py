from functools import partial
from io import BytesIO

from django.utils.text import slugify
from pypdf import PdfWriter
from reportlab.lib.units import cm
from reportlab.platypus import PageBreak, Paragraph, Spacer

from croixrouge.pdf import BaseCroixrougePDF, DemandeAccompagnement
from croixrouge.utils import format_d_m_Y, format_Ymd

"""
Organisation des documents PDF pour SPE:

1. EvaluationPdf (CoordonneesPagePdf + DemandeAccompagnementPagePdf)
2. CoordonneesPdf
3. BilanPdf
"""


class EvaluationPdf:
    def __init__(self, tampon, famille):
        self.tampon = tampon
        self.famille = famille
        self.merger = PdfWriter()

    def append_pdf(self, PDFClass):
        tampon = BytesIO()
        pdf = PDFClass(tampon, self.famille)
        pdf.produce()
        self.merger.append(tampon)

    def produce(self):

        self.append_pdf(CoordonneesPagePdf)
        self.append_pdf(DemandeAccompagnementPagePdf)
        self.merger.write(self.tampon)

    def get_filename(self):
        return '{}_spe_evaluation.pdf'.format(slugify(self.famille.nom))


class CoordonneesFamillePdf(BaseCroixrougePDF):
    title = "Informations"

    def get_filename(self):
        return '{}_coordonnees.pdf'.format(slugify(self.instance.nom))

    def produce(self):
        famille = self.instance
        suivi = famille.suivi
        self.set_title('Famille {} - {}'.format(famille.nom, self.title))

        # Parents
        self.story.append(Paragraph('Parents', self.style_sub_title))
        data = self.formate_persons(famille.parents())
        if data:
            self.story.append(self.get_table(data, columns=[2, 7, 2, 7], before=0, after=0))
        else:
            self.story.append(Paragraph('-', self.style_normal))

        # Situation matrimoniale
        data = [
            ['Situation matrimoniale: {}'.format(famille.get_statut_marital_display()),
             'Autorité parentale: {}'.format(famille.get_autorite_parentale_display())],
        ]
        self.story.append(self.get_table(data, columns=[9, 9], before=0, after=0))

        # Personnes significatives
        autres_parents = list(famille.autres_parents())
        if autres_parents:
            self.story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            self.story.append(self.get_table(data, columns=[2, 7, 3, 6], before=0, after=0))
            if len(autres_parents) > 2:
                self.story.append(PageBreak())

        # Enfants suivis
        self.write_paragraph(
            "Enfant(s)",
            '<br/>'.join(self.enfant_data(enfant) for enfant in famille.membres_suivis())
        )
        # Réseau
        self.story.append(Paragraph("Réseau", self.style_sub_title))
        data = [
            ['AS OPE', '{} - (Mandat: {})'.format(
                ', '.join(ope.nom_prenom for ope in suivi.ope_referents),
                suivi.get_mandat_ope_display()
            )],
            ['Interv. CRNE', '{}'.format(
                ', '.join('{}'.format(i.nom_prenom) for i in suivi.intervenants.all().distinct())
            )]
        ]
        for enfant in famille.membres_suivis():
            for contact in enfant.reseaux.all():
                data.append([
                    enfant.prenom,
                    '{} ({})'.format(contact, contact.contact)
                ])
        self.story.append(self.get_table(data, columns=[2, 16], before=0, after=0))

        self.write_paragraph("Motif de la demande", famille.suivi.motif_detail)
        self.write_paragraph("Collaborations", famille.suivi.collaboration)

        # Historique
        self.story.append(Paragraph('Historique', self.style_sub_title))
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)
        fields = ['date_demande', 'date_debut_evaluation', 'date_fin_evaluation',
                  'date_debut_suivi', 'date_fin_suivi']
        data = []
        for field_name in fields:
            field = famille.suivi._meta.get_field(field_name)
            if getattr(famille.suivi, field_name):
                data.append(
                    [Pbold(f"{field.verbose_name} :"), P(format_d_m_Y(getattr(famille.suivi, field_name)))]
                )
        if famille.suivi.motif_fin_suivi:
            data.append([Pbold("Motif de fin de suivi :"), famille.suivi.get_motif_fin_suivi_display()])
        if famille.destination:
            data.append([Pbold("Destination :"), famille.get_destination_display()])
        if famille.archived_at:
            data.append([Pbold("Date d'archivage :"), format_d_m_Y(famille.archived_at)])

        self.story.append(self.get_table(data, [4, 5]))
        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class DemandeAccompagnementPagePdf(DemandeAccompagnement):
    title = "Évaluation SPE"


class BilanPdf(BaseCroixrougePDF):
    title = "Bilan SPE"

    def get_filename(self):
        return "{}_bilan_{}.pdf".format(
            slugify(self.instance.famille.nom),
            format_Ymd(self.instance.date)
        )

    def produce(self):
        bilan = self.instance
        self.style_normal.fontSize += 2
        self.style_normal.leading += 2
        self.style_justifie.fontSize += 2
        self.style_justifie.leading += 2
        self.set_title('Famille {} - {}'.format(bilan.famille.nom, self.title))
        self._add_subtitle([f"Date: {format_d_m_Y(bilan.date)}"])

        for title, text, html in (
            ("Enfant(s)", '<br/>'.join(
                [f"{enfant.nom_prenom} (*{format_d_m_Y(enfant.date_naissance)})"
                 for enfant in bilan.famille.membres_suivis()]), False),
            ("Intervenant-e-s", ', '.join(
                [i.intervenant.nom_prenom for i in bilan.famille.interventions_actives(bilan.date)]
            ), False),
            ("Début du suivi", format_d_m_Y(bilan.famille.suivi.date_debut_suivi), False),
            ("Besoins et objectifs", bilan.objectifs, True),
            ("Rythme et fréquence", bilan.rythme, True),
        ):
            self.write_paragraph(title, text, html=html, justifie=True)

        self.story.append(Spacer(0, 0.5 * cm))
        self.sig_interv_bloc(bilan.sig_interv.all())

        if bilan.sig_famille:
            self.write_paragraph("Signature de la famille :", '')

        self.doc.build(self.story, onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer)


class CoordonneesPagePdf(CoordonneesFamillePdf):
    title = "Informations"

    def get_filename(self, famille):
        return '{}_spe_evaluation.pdf'.format(slugify(famille.nom))


def str_or_empty(value):
    return '' if not value else str(value)
