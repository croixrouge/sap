from django.db import models


class Archive(models.Model):
    # Model prévu pour archives SPE
    nom = models.CharField(max_length=40)
    unite = models.CharField(max_length=10)
    intervenant = models.CharField(max_length=120, blank=True)
    ope = models.CharField(max_length=50, blank=True)
    motif_fin = models.CharField(max_length=30, blank=True)
    date_debut = models.DateField(null=True)
    date_fin = models.DateField()
    key = models.TextField()
    pdf = models.FileField(upload_to='archives/', null=True)

    class Meta:
        ordering = ('nom',)

    def __str__(self):
        return f"{self.unite}: {self.nom}"


class ArchiveCIPE(models.Model):
    nom_famille = models.CharField(max_length=40)  # nom dans modèle "Famille"
    nom = models.CharField(max_length=40)  # Nom dans modèle "Personne"
    prenom = models.CharField(max_length=30)
    npa = models.CharField(max_length=5, blank=True)
    localite = models.CharField(max_length=30, blank=True)
    region = models.CharField(max_length=30, blank=True)
    key = models.TextField()
    pdf = models.FileField(upload_to='archives/cipe/%Y/', null=True)

    class Meta:
        ordering = ['nom', 'prenom']

    def __str__(self):
        return f"{self.nom} - {self.prenom}"
