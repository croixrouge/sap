from django.urls import path

from archive import views

urlpatterns = [
    path('<str:unite>/famille/<int:pk>/add/', views.ArchiveCreateView.as_view(), name='archive-add'),
    path('<str:unite>/list/', views.ArchiveListView.as_view(), name='archive-list'),
    path('<int:pk>/decrypt/', views.ArchiveDecryptView.as_view(), name='archive-decrypt'),
]
