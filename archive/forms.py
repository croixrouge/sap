from django import forms

from croixrouge.forms import BootstrapMixin


class ArchiveKeyUploadForm(BootstrapMixin, forms.Form):
    file = forms.FileField(label='Clé de déchiffrement des archives', required=True)


class ArchiveFilterForm(BootstrapMixin, forms.Form):

    search_famille = forms.CharField(
        label='Recherche par nom de famille',
        max_length=30,
        required=False
    )

    search_intervenant = forms.CharField(
        label='Recherche par interv. CRNE',
        max_length=30,
        required=False
    )

    def filter(self, archives):
        if not self.cleaned_data['search_famille'] and not self.cleaned_data['search_intervenant']:
            return archives.none()
        if self.cleaned_data['search_famille']:
            archives = archives.filter(nom__icontains=self.cleaned_data['search_famille'])
        if self.cleaned_data['search_intervenant']:
            archives = archives.filter(intervenant__icontains=self.cleaned_data['search_intervenant'])
        return archives
