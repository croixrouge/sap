from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0002_longer_intervenant'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArchiveCIPE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom_famille', models.CharField(max_length=40)),
                ('nom', models.CharField(max_length=40)),
                ('prenom', models.CharField(max_length=30)),
                ('npa', models.CharField(blank=True, max_length=5)),
                ('localite', models.CharField(blank=True, max_length=30)),
                ('region', models.CharField(blank=True, max_length=30)),
                ('key', models.TextField()),
                ('pdf', models.FileField(null=True, upload_to='archives/cipe/%Y/')),
            ],
            options={
                'ordering': ['nom', 'prenom'],
            },
        ),
    ]
