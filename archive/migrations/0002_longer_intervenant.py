from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0001_Add_archive_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archive',
            name='intervenant',
            field=models.CharField(blank=True, max_length=120),
        ),
    ]
