import base64
from io import BytesIO

from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.files.base import ContentFile
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.text import slugify
from django.views.generic import FormView, TemplateView, View

from cipe.models import FamilleCIPE
from croixrouge.models import Personne, Role
from croixrouge.utils import is_ajax
from spe.models import FamilleSPE

from .forms import ArchiveFilterForm, ArchiveKeyUploadForm
from .models import Archive, ArchiveCIPE
from .pdf import ArchivePdfCIPE, ArchivePdfSPE


def encrypt_pdf(content):
    # Create a symmetric Fernet key to encrypt the PDF, and encrypt that key with asymmetric RSA key.
    fkey = Fernet.generate_key()
    fernet = Fernet(fkey)
    pdf_crypted = fernet.encrypt(content)
    padd = padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)
    with open(settings.CRNE_RSA_PUBLIC_KEY, "rb") as key_file:
        public_key = serialization.load_ssh_public_key(key_file.read())
    fernet_crypted = public_key.encrypt(fkey, padding=padd)
    return pdf_crypted, base64.b64encode(fernet_crypted).decode()


class ArchiveCreateView(View):
    default_unite = None

    @staticmethod
    @transaction.atomic
    def archiver_cipe(enfant):
        temp = BytesIO()
        pdf = ArchivePdfCIPE(temp, enfant)
        pdf.produce()
        temp.seek(0)
        pdf_crypted, key = encrypt_pdf(temp.read())

        arch = ArchiveCIPE.objects.create(
            nom_famille=enfant.famille.nom,
            nom=enfant.nom,
            prenom=enfant.prenom,
            npa = enfant.npa,
            localite = enfant.localite,
            region = enfant.famille.region.nom if enfant.famille.region else '',
            key = key,
            pdf = None,
        )
        arch.pdf.save(pdf.get_filename(), ContentFile(pdf_crypted))

        enfant.suivienfant.anonymiser()
        enfant.anonymiser_prestations_cipe()
        enfant.role = Role.objects.get_enfant_non_suivi()
        enfant.archived_at = timezone.now()
        enfant.save()

        if not enfant.famille.membres_suivis():
            enfant.famille.anonymiser()

    @staticmethod
    @transaction.atomic
    def archiver_spe(unite, famille):
        famille.archived_at = timezone.now()
        famille.save()

        temp = BytesIO()
        pdf = ArchivePdfSPE(temp, famille)
        pdf.produce()
        temp.seek(0)
        pdf_crypted, key = encrypt_pdf(temp.read())

        intervenants_list = '/'.join([f"{interv.nom} {interv.prenom[0].upper()}."
                                        for interv in famille.suivi.intervenants.all()])
        ope_list = famille.suivi.ope_referents
        motif_fin = famille.suivi.get_motif_fin_suivi_display()

        nom_famille = famille.nom
        arch = Archive.objects.create(
            nom=nom_famille,
            unite=unite,
            intervenant=intervenants_list,
            ope=", ".join([ope.nom_prenom for ope in ope_list]),
            motif_fin=motif_fin,
            date_debut=famille.suivi.date_debut_suivi,
            date_fin=famille.suivi.date_fin_suivi,
            key=key,
            pdf=None
        )
        arch.pdf.save(f"{unite}/{pdf.get_filename()}", ContentFile(pdf_crypted))
        famille.anonymiser()

    def post(self, request, *args, **kwargs):
        unite = kwargs.get('unite', self.default_unite)

        if unite == 'spe':
            famille = get_object_or_404(FamilleSPE, pk=kwargs['pk'], archived_at__isnull=True)
            if not famille.can_be_archived(self.request.user):
                raise PermissionDenied("Vous n'avez pas les droits nécessaires pour accéder à cette page.")
            nom_famille = famille.nom
            self.archiver_spe(unite, famille)

            if is_ajax(self.request):
                return JsonResponse({'id': famille.pk}, safe=True)

            messages.success(self.request, f"La famille «{nom_famille}» a bien été archivée.")
            return HttpResponseRedirect(reverse(f"{unite}-suivis-termines"))
        elif unite == 'cipe':
            # On archive un enfant suivi (modèle Personne) et non une famille
            enfant = get_object_or_404(
                Personne.objects.select_related("famille"), pk=kwargs['pk'],
                role__nom="Enfant suivi", famille__in=FamilleCIPE.objects.all(),
                archived_at__isnull=True,
            )
            enfant.famille.__class__ = FamilleCIPE
            if not self.request.user.has_perm('cipe.can_archive'):
                raise PermissionDenied("Vous n'avez pas les droits nécessaires pour accéder à cette page.")

            nom_enfant = f"{enfant.nom} {enfant.prenom}"
            self.archiver_cipe(enfant)

            messages.success(self.request, f"«{nom_enfant}» a bien été archivée.")
            return HttpResponseRedirect(reverse("cipe:suivis-archives"))
        else:
            raise NotImplementedError


class ArchiveListView(TemplateView):
    template_name = 'archive/list.html'
    model = Archive

    def dispatch(self, request, *args, **kwargs):
        self.unite = self.kwargs['unite']
        self.filter_form = ArchiveFilterForm(data=request.GET or None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if is_ajax(request) and self.unite == 'spe':
            if self.filter_form.is_bound and self.filter_form.is_valid():
                archives = self.filter_form.filter(Archive.objects.filter(unite=self.unite))
            else:
                archives = Archive.objects.none()
            response = render_to_string(
                template_name='archive/list_partial.html',
                context={
                    'archives': archives,
                    'can_download': request.user.has_perm('spe.can_archive'),
                }
            )
            return JsonResponse(response, safe=False)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(*args, **kwargs),
            'unite': self.unite,
            'form': self.filter_form,
            'archives': Archive.objects.filter(unite=self.unite),
            'can_download': self.request.user.has_perm('spe.can_archive'),
        }


class ArchiveDecryptView(PermissionRequiredMixin, FormView):
    form_class = ArchiveKeyUploadForm
    template_name = 'archive/key_upload.html'
    permission_required = 'spe.can_archive'
    archive_class = Archive
    redirect_url = reverse_lazy('archive-list', args=['spe'])

    def form_valid(self, form):
        arch = get_object_or_404(self.archive_class, pk=self.kwargs['pk'])
        try:
            with open(arch.pdf.path, "rb") as fh:
                pdf_crypted = fh.read()
        except OSError as err:
            messages.error(self.request, f"Erreur lors de la lecture du document ({str(err)})")
            return HttpResponseRedirect(self.redirect_url)

        try:
            private_key_file = self.request.FILES['file'].read()
            private_key = serialization.load_pem_private_key(private_key_file, password=None)
            padd = padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)
            sim_key = private_key.decrypt(base64.b64decode(arch.key), padding=padd)

            fernet = Fernet(sim_key)
            pdf_content = fernet.decrypt(pdf_crypted)
        except ValueError as err:
            messages.error(self.request, f"Erreur lors de la lecture de la clé ({str(err)})")
            return HttpResponseRedirect(self.redirect_url)
        except InvalidToken:
            messages.error(self.request, "Erreur lors du déchiffrement")
            return HttpResponseRedirect(self.redirect_url)

        filename = f"{slugify(arch.nom)}.pdf"
        response = HttpResponse(pdf_content, content_type='application/pdf')
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response


class ArchiveCIPEDecryptView(ArchiveDecryptView):
    permission_required = 'cipe.can_archive'
    archive_class = ArchiveCIPE
    redirect_url = reverse_lazy('cipe-suivis-archives')
