from django.contrib import admin

from .models import Archive, ArchiveCIPE


@admin.register(Archive)
class ArchiveAdmin(admin.ModelAdmin):
    list_display = ['nom', 'unite', 'date_debut', 'date_fin']
    search_fields = ['nom']


@admin.register(ArchiveCIPE)
class ArchiveCIPEAdmin(admin.ModelAdmin):
    list_display = ['nom_famille', 'nom', 'prenom', 'npa', 'localite', 'region',]
    search_fields = ['nom_famille', 'nom', 'prenom']
