from io import BytesIO

from django.utils.text import slugify
from pypdf import PdfWriter

from cipe.percentile import GraphEmpty
from croixrouge.pdf import BaseCroixrougePDF


class PDFMergerBase(BaseCroixrougePDF):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.merger = PdfWriter()

    def get_filename(self):
        return f"{slugify(self.instance.nom)}-{self.instance.pk}.pdf"

    def append_pdf(self, PDFClass, obj, **kwargs):
        temp = BytesIO()
        pdf = PDFClass(temp, obj, **kwargs)
        try:
            pdf.produce()
        except GraphEmpty:
            pass
        else:
            self.merger.append(temp)
