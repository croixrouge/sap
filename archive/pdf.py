import subprocess
import tempfile
from pathlib import Path

from django.utils.text import slugify
from pypdf import PdfReader

from cipe import pdf as cipe_pdf
from cipe.models import FamilleCIPE
from croixrouge.pdf import JournalPdf, MessagePdf, RapportPdf
from spe import pdf as spe_pdf

from .pdf_utils import PDFMergerBase


class ArchiveBase(PDFMergerBase):
    def append_other_docs(self, documents):
        msg = []
        for doc in documents:
            doc_path = Path(doc.fichier.path)
            if not doc_path.exists():
                msg.append(f"Le fichier «{doc.titre}» n'existe pas!")
                continue
            if doc_path.suffix.lower() == '.pdf':
                self.merger.append(PdfReader(str(doc_path)))
            elif doc_path.suffix.lower() in ['.doc', '.docx']:
                with tempfile.TemporaryDirectory() as tmpdir:
                    cmd = ['libreoffice', '--headless', '--convert-to', 'pdf', '--outdir', tmpdir, doc_path]
                    subprocess.run(cmd, capture_output=True)
                    converted_path = Path(tmpdir) / f'{doc_path.stem}.pdf'
                    if converted_path.exists():
                        self.merger.append(PdfReader(str(converted_path)))
                    else:
                        msg.append(f"La conversion du fichier «{doc.titre}» a échoué")
            elif doc_path.suffix.lower() == '.msg':
                self.append_pdf(MessagePdf, doc)
            else:
                msg.append(f"Le format du fichier «{doc.titre}» ne peut pas être intégré.")
        return msg


class ArchivePdfCIPE(ArchiveBase):
    title = "Archive CIPE"

    def __init__(self, tampon, enfant, *args, **kwargs):
        enfant.famille.__class__ = FamilleCIPE
        self.enfant = enfant
        super().__init__(tampon, enfant, *args, **kwargs)

    def get_filename(self):
        return f"{slugify(self.enfant.nom)}-{slugify(self.enfant.prenom)}-{self.enfant.pk}.pdf"

    def produce(self):
        self.append_pdf(cipe_pdf.CoordonneesFamillePdf, self.enfant.famille)
        self.append_pdf(cipe_pdf.DossierEnfantPdf, self.enfant)
        self.append_pdf(cipe_pdf.PercentilePdf, self.enfant, typ="taille")
        self.append_pdf(cipe_pdf.PercentilePdf, self.enfant, typ="poids")
        self.append_pdf(cipe_pdf.JournalPdf, self.enfant)
        self.merger.write(self.doc.filename)


class ArchivePdfSPE(ArchiveBase):
    title = "Archive SPE"

    def produce(self):
        famille = self.instance
        self.append_pdf(spe_pdf.EvaluationPdf, famille)
        self.append_pdf(JournalPdf, famille)
        for bilan in famille.bilans_spe.all():
            self.append_pdf(spe_pdf.BilanPdf, bilan)
        for rapport in famille.rapports_spe.all():
            self.append_pdf(RapportPdf, rapport)
        msg = self.append_other_docs(famille.documents.all())
        self.merger.write(self.doc.filename)
        return msg
