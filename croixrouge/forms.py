from datetime import date, datetime, time, timedelta

import nh3
from city_ch_autocomplete.forms import CityChField, CityChMixin
from dal import autocomplete
from dal.widgets import WidgetMixin
from django import forms
from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils.dates import MONTHS
from django.utils.html import escape
from django.utils.safestring import mark_safe
from tinymce.widgets import TinyMCE

from common.choices import PROVENANCE_DESTINATION_CHOICES

from .models import (
    Contact,
    Document,
    Famille,
    Formation,
    LibellePrestation,
    Personne,
    PrestationBase,
    Rapport,
    Region,
    Role,
    Service,
    Utilisateur,
)
from .utils import ANTICIPATION_POUR_DEBUT_SUIVI, CSVFile, XLSXFile, format_nom_prenom, unaccent


class BootstrapMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for ffield in self.fields.values():
            if (isinstance(ffield.widget, (PickSplitDateTimeWidget, PickDateWidget, WidgetMixin)) or
                    getattr(ffield.widget, '_bs_enabled', False)):
                continue
            elif isinstance(ffield.widget, (forms.Select, forms.NullBooleanSelect)):
                self.add_attr(ffield.widget, 'form-select')
            elif isinstance(ffield.widget, (forms.CheckboxInput, forms.RadioSelect)):
                self.add_attr(ffield.widget, 'form-check-input')
            else:
                self.add_attr(ffield.widget, 'form-control')

    @staticmethod
    def add_attr(widget, class_name):
        if 'class' in widget.attrs:
            widget.attrs['class'] += f' {class_name}'
        else:
            widget.attrs.update({'class': class_name})


class BootstrapChoiceMixin:
    """
    Mixin to customize choice widgets to set 'form-check' on container and
    'form-check-input' on sub-options.
    """
    _bs_enabled = True

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        if 'class' in context['widget']['attrs']:
            context['widget']['attrs']['class'] += ' form-check'
        else:
            context['widget']['attrs']['class'] = 'form-check'
        return context

    def create_option(self, *args, attrs=None, **kwargs):
        attrs = attrs or {}
        if 'class' in attrs:
            attrs['class'] += ' form-check-input'
        else:
            attrs.update({'class': 'form-check-input'})
        return super().create_option(*args, attrs=attrs, **kwargs)


class BSRadioSelect(BootstrapChoiceMixin, forms.RadioSelect):
    pass


class ReadOnlyableMixin:
    def __init__(self, *args, readonly=False, **kwargs):
        self.readonly = readonly
        super().__init__(*args, **kwargs)
        if self.readonly:
            for field in self.fields.values():
                field.disabled = True


class BSCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    """
    Custom widget to set 'form-check' on container and 'form-check-input' on sub-options.
    """
    _bs_enabled = True

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['widget']['attrs']['class'] = 'form-check'
        return context

    def create_option(self, *args, attrs=None, **kwargs):
        attrs = attrs.copy() if attrs else {}
        if 'class' in attrs:
            attrs['class'] += ' form-check-input'
        else:
            attrs.update({'class': 'form-check-input'})
        return super().create_option(*args, attrs=attrs, **kwargs)


class RichTextField(forms.CharField):
    widget = TinyMCE

    def __init__(self, *args, **kwargs):
        kwargs['widget'] = self.widget
        super().__init__(*args, **kwargs)

    def clean(self, value):
        value = super().clean(value)
        return nh3.clean(
            value, tags={'p', 'br', 'b', 'strong', 'u', 'i', 'em', 'ul', 'li'}
        )


class HMDurationField(forms.DurationField):
    """A duration field taking HH:MM as input."""
    widget = forms.TextInput(attrs={'placeholder': 'hh:mm'})

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        value += ':00'  # Simulate seconds
        return super().to_python(value)

    def prepare_value(self, value):
        if isinstance(value, timedelta):
            seconds = value.days * 24 * 3600 + value.seconds
            hours = seconds // 3600
            minutes = seconds % 3600 // 60
            value = '{:02d}:{:02d}'.format(hours, minutes)
        return value


class ImmediateSelect(forms.Select):
    def __init__(self, attrs=None, **kwargs):
        attrs = {"class": "form-select d-inline w-auto immediate-submit", **(attrs or {})}
        super().__init__(attrs=attrs, **kwargs)


class DateInput(forms.DateInput):
    input_type = "date"

    def format_value(self, value):
        return str(value) if value is not None else None


class PickDateWidget(forms.DateInput):
    class Media:
        js = [
            'admin/js/core.js',
            'admin/js/calendar.js',
            # Include the Django 3.2 version without today link.
            'js/DateTimeShortcuts.js',
        ]

    def __init__(self, attrs=None, **kwargs):
        attrs = {'class': 'vDateField vDateField-rounded', 'size': '10', **(attrs or {})}
        super().__init__(attrs=attrs, **kwargs)


class PickSplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, attrs=None):
        widgets = [PickDateWidget, forms.TimeInput(attrs={'class': 'TimeField'}, format='%H:%M')]
        forms.MultiWidget.__init__(self, widgets, attrs)


class ContactForm(CityChMixin, BootstrapMixin, forms.ModelForm):
    service = forms.ModelChoiceField(queryset=Service.objects.exclude(sigle='CRNE'), required=False)
    roles = forms.ModelMultipleChoiceField(
        label='Rôles',
        queryset=Role.objects.exclude(famille=True).order_by('nom'),
        widget=BSCheckboxSelectMultiple,
        required=False
    )
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'

    class Meta:
        model = Contact
        fields = [
            'nom', 'prenom', 'profession', 'service', 'roles', 'rue', 'city_auto', 'npa', 'localite',
            'tel_prof', 'tel_prive', 'email', 'remarque'
        ]


class RoleForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Role
        fields = '__all__'


class ServiceForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Service
        fields = ['sigle', 'nom_complet']

    def clean_sigle(self):
        return self.cleaned_data['sigle'].upper()


class FormationForm(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):
    class Meta:
        model = Formation
        exclude = ('personne',)


class GroupSelectMultiple(BSCheckboxSelectMultiple):
    option_template_name = "widgets/group_checkbox_option.html"

    def create_option(self, name, value, *args, **kwargs):
        try:
            help_ = value.instance.groupinfo.description
        except ObjectDoesNotExist:
            help_= ''
        return {
            **super().create_option(name, value, *args, **kwargs),
            'help': help_,
        }


class UtilisateurForm(BootstrapMixin, forms.ModelForm):
    roles = forms.ModelMultipleChoiceField(
        label="Rôles",
        queryset=Role.objects.exclude(
            Q(famille=True) | Q(nom__in=['Personne significative', 'Référent'])
        ).order_by('nom'),
        widget=BSCheckboxSelectMultiple,
        required=False
    )

    class Meta:
        model = Utilisateur
        fields = [
            'nom', 'prenom', 'sigle', 'profession', 'tel_prof', 'tel_prive', 'username',
            'taux_activite', 'decharge', 'email', 'equipe', 'roles', 'groups'
        ]
        widgets = {'groups': GroupSelectMultiple}
        labels = {'profession': 'Titre'}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['groups'].queryset = self.fields['groups'].queryset.order_by('name')


class UtilisateurFilterForm(forms.Form):
    groupe = forms.ModelChoiceField(
        label="Groupe", queryset=Group.objects.all(), widget=ImmediateSelect, required=False
    )

    def filter(self, users):
        if self.cleaned_data["groupe"]:
            users = users.filter(groups=self.cleaned_data["groupe"])
        return users


class JournalAccesFilterForm(forms.Form):
    date_acces = forms.DateField(
        label="Date accès", widget=DateInput(attrs={"class": "immediate-submit"}), required=False
    )
    ordinaire = forms.NullBooleanField(
        label="Accès ordinaire", widget=ImmediateSelect(choices=forms.NullBooleanSelect().choices),
        required=False
    )

    def filter(self, journaux):
        if self.cleaned_data["date_acces"]:
            journaux = journaux.filter(quand__date=self.cleaned_data["date_acces"])
        if self.cleaned_data["ordinaire"] is not None:
            journaux = journaux.filter(ordinaire=self.cleaned_data["ordinaire"])
        return journaux


class SigFileInput(forms.ClearableFileInput):
    template_name = 'widgets/signature_fileinput.html'


class SignatureForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Utilisateur
        fields = ["signature"]
        widgets = {"signature": SigFileInput}


class PersonneForm(CityChMixin, BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):
    role = forms.ModelChoiceField(label="Rôle", queryset=Role.objects.filter(famille=True), required=True)

    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'

    class Meta:
        model = Personne
        fields = (
            'role', 'nom', 'prenom', 'date_naissance', 'genre', 'filiation',
            'rue', 'npa', 'localite', 'telephone',
            'email', 'profession', 'pays_origine', 'niveau_fr', 'decedee',
            'allergies', 'remarque', 'remarque_privee',
        )
        widgets = {
            'date_naissance': PickDateWidget,
        }
        labels = {
            'remarque_privee': 'Remarque privée (pas imprimée)',
        }

    def __init__(self, famille=None, **kwargs):
        role_id = kwargs.pop('role', None)

        super().__init__(**kwargs)
        self.famille = famille or self.instance.famille
        if self.famille and not self.instance.pk:
            self.initial['nom'] = self.famille.nom
            self.initial['rue'] = self.famille.rue
            self.initial['npa'] = self.famille.npa
            self.initial['localite'] = self.famille.localite
            self.initial['telephone'] = self.famille.telephone

        if role_id:
            if role_id == 'ps':  # personne significative
                excl = Role.ROLES_PARENTS + ['Enfant suivi', 'Enfant non-suivi']
                self.fields['role'].queryset = Role.objects.filter(
                    famille=True
                ).exclude(nom__in=excl)
            elif role_id == 'parent':
                self.fields['role'].queryset = Role.objects.filter(nom__in=Role.ROLES_PARENTS)
            else:
                self.role = Role.objects.get(pk=role_id)

                if self.role.nom in Role.ROLES_PARENTS:
                    self.fields['role'].queryset = Role.objects.filter(nom=self.role.nom)
                    self.initial['genre'] = 'M' if self.role.nom == 'Père' else 'F'
                elif self.role.nom in ['Enfant suivi', 'Enfant non-suivi']:
                    self.fields['role'].queryset = Role.objects.filter(nom__in=['Enfant suivi', 'Enfant non-suivi'])
                    self.fields['profession'].label = 'Profession/École'
                self.initial['role'] = self.role.pk
        else:
            if self.instance.pk:
                # Cloisonnement des choix pour les rôles en fonction du rôle existant
                if self.instance.role.nom in Role.ROLES_PARENTS:
                    self.fields['role'].queryset = Role.objects.filter(nom__in=Role.ROLES_PARENTS)
                elif self.instance.role.nom in ['Enfant suivi', 'Enfant non-suivi']:
                    self.fields['role'].queryset = Role.objects.filter(nom__in=['Enfant suivi', 'Enfant non-suivi'])
                else:
                    excl = ['Enfant suivi', 'Enfant non-suivi']
                    if len(self.famille.parents()) == 2:
                        excl.extend(Role.ROLES_PARENTS)
                    self.fields['role'].queryset = Role.objects.filter(
                        famille=True
                    ).exclude(nom__in=excl)

    def clean_nom(self):
        return format_nom_prenom(self.cleaned_data['nom'])

    def clean_prenom(self):
        return format_nom_prenom(self.cleaned_data['prenom'])

    def clean_localite(self):
        localite = self.cleaned_data.get('localite')
        if localite and localite[0].islower():
            localite = localite[0].upper() + localite[1:]
        return localite

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['decedee'] and cleaned_data['role'] == 'Enfant suivi':
            raise forms.ValidationError('Un enfant décédé ne peut pas être «Enfant suivi»')
        return cleaned_data

    def save(self, **kwargs):
        if self.instance.pk is None:
            self.instance.famille = self.famille
            pers = Personne.objects.create_personne(**self.instance.__dict__)
        else:
            pers = super().save(**kwargs)
            pers = Personne.objects.add_formation(pers)
        return pers


class AgendaFormBase(BootstrapMixin, forms.ModelForm):
    destination = forms.ChoiceField(choices=PROVENANCE_DESTINATION_CHOICES, required=False)
    change_famille_perm = None

    class Meta:
        fields = (
            'date_demande', 'date_debut_evaluation', 'date_fin_evaluation',
            'date_debut_suivi', 'date_fin_suivi', 'motif_fin_suivi', 'destination'
        )
        widgets = {field: PickDateWidget for field in fields[:-2]}

    def __init__(self, *args, **kwargs):
        destination = kwargs.pop('destination', '')
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)
        if self.instance.date_debut_suivi is not None or self.instance.motif_fin_suivi:
            for fname in ('date_demande', 'date_debut_evaluation', 'date_fin_evaluation'):
                self.fields[fname].disabled = True
        if self.instance.motif_fin_suivi:
            self.fields['date_debut_suivi'].disabled = True
        else:
            # Choix 'Autres' obsolète (#435), pourrait être supprimé quand plus référencé
            self.fields['motif_fin_suivi'].choices = [
                ch for ch in self.fields['motif_fin_suivi'].choices if ch[0] != 'autres'
            ]

        if "destination" in self.fields:
            self.fields['destination'].choices = [('', '------')] + [
                ch for ch in PROVENANCE_DESTINATION_CHOICES if (ch[0] != 'autre' or destination == 'autre')
            ]
            self.initial['destination'] = destination

    def clean(self):
        cleaned_data = super().clean()

        # Check date chronology
        date_preced = None
        for field_name in self._meta.fields:
            if not field_name.startswith("date_"):
                continue
            dt = cleaned_data.get(field_name)
            if not dt:
                continue
            if date_preced and dt < date_preced:
                raise forms.ValidationError(
                    "La date «{}» ne respecte pas l’ordre chronologique!".format(self.fields[field_name].label)
                )
            date_preced = dt

        # Check mandatory dates
        workflow = self._meta.model.WORKFLOW
        for field, etape in reversed((workflow.items())):
            if field == 'archivage':
                continue
            date_etape = cleaned_data.get(etape.date_nom())
            etape_courante = etape
            etape_preced_oblig = workflow[etape.preced_oblig]
            date_preced_oblig = cleaned_data.get(etape_preced_oblig.date_nom())
            while True:
                etape_preced = workflow[etape_courante.precedente]
                date_preced = cleaned_data.get(etape_preced.date_nom())
                if date_preced is None and etape_courante.num > 1:
                    etape_courante = etape_preced
                else:
                    break
            if date_etape and date_preced_oblig is None:
                raise forms.ValidationError("La date «{}» est obligatoire".format(etape_preced_oblig.nom))

        # Check dates out of range
        for field_name in self.changed_data:
            value = self.cleaned_data.get(field_name)
            if isinstance(value, date):
                if value > date.today():
                    if field_name in ['date_debut_suivi', 'date_debut_evaluation', 'date_fin_evaluation']:
                        if value > date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI):
                            self.add_error(
                                field_name,
                                forms.ValidationError(
                                    "La saisie de cette date ne peut être "
                                    f"anticipée de plus de {ANTICIPATION_POUR_DEBUT_SUIVI} jours !")
                            )
                    else:
                        self.add_error(field_name, forms.ValidationError("La saisie anticipée est impossible !"))
                elif not PrestationBase.check_date_allowed(self.request.user, value, app=self.instance.unite):
                    if self.request and self.request.user.has_perm(self.change_famille_perm):
                        messages.warning(
                            self.request,
                            'Les dates saisies peuvent affecter les statistiques déjà communiquées !'
                        )
                    else:
                        self.add_error(
                            field_name,
                            forms.ValidationError("La saisie de dates pour le mois précédent n’est pas permise !")
                        )

        ddebut = cleaned_data.get('date_debut_suivi')
        dfin = cleaned_data.get('date_fin_suivi')
        motif = cleaned_data.get('motif_fin_suivi')
        has_dest = "destination" in self.fields
        dest = cleaned_data.get('destination')

        if ddebut and dfin and motif and (not has_dest or dest):  # dossier terminé
            return cleaned_data
        elif ddebut and (dfin is None or motif == '' or (has_dest and dest == '')):  # suivi en cours
            if any([dfin, motif, dest]):
                raise forms.ValidationError(
                    "Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                    "sont obligatoires pour fermer le dossier."
                )
        elif ddebut is None and dfin is None:  # evaluation
            if motif != '':  # abandon
                cleaned_data['date_fin_suivi'] = date.today()
        return cleaned_data

    def save(self):
        instance = super().save()
        if instance.date_fin_suivi and "destination" in self.fields:
            instance.famille.destination = self.cleaned_data['destination']
            instance.famille.save()
        return instance


class PrestationRadioSelect(BSRadioSelect):
    option_template_name = 'widgets/prestation_radio.html'


class PrestationFormBase(BootstrapMixin, forms.ModelForm):
    class Meta:
        exclude = ['auteur', 'famille', 'familles_actives', 'medlink_date']
        widgets = {
            'date_prestation': PickDateWidget,
            'intervenants': BSCheckboxSelectMultiple,
            'lib_prestation': PrestationRadioSelect,
        }
        labels = {
            'lib_prestation': 'Prestation',
        }
        field_classes = {
            'duree': HMDurationField,
            'texte': RichTextField,
        }

    def __init__(self, famille=None, user=None, **kwargs):
        self.user = user
        self.famille = famille
        super().__init__(**kwargs)
        if 'intervenants' in self.fields:
            self.fields['intervenants'].queryset = self.defaults_intervenants()
            if famille:
                intervenants = list(famille.suivi.intervenants.all())
                if len(intervenants):
                    if self.user not in intervenants:
                        intervenants.insert(0, self.user)
                    self.fields['intervenants'].choices = [(i.pk, i.nom_prenom) for i in intervenants]
        if self.user.has_role(['ASE']):
            del self.fields['duree']

    def defaults_intervenants(self):
        return Utilisateur.objects.all()

    def clean_date_prestation(self):
        date_prestation = self.cleaned_data['date_prestation']
        today = date.today()
        if date_prestation > today:
            raise forms.ValidationError("La saisie anticipée est impossible !")

        if not self.instance.check_date_allowed(self.user, date_prestation, app=self.instance.CURRENT_APP):
            raise forms.ValidationError(
                "La saisie des prestations des mois précédents est close !"
            )
        return date_prestation

    def clean_texte(self):
        texte = self.cleaned_data['texte']
        for snip in ('<p></p>', '<p><br></p>', '<p> </p>'):
            while texte.startswith(snip):
                texte = texte[len(snip):].strip()
        for snip in ('<p></p>', '<p><br></p>', '<p> </p>'):
            while texte.endswith(snip):
                texte = texte[:-len(snip)].strip()
        return texte


class JournalFilterForm(forms.Form):
    recherche = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'search-form-fields form-control d-inline-block',
            'placeholder': 'recherche',
        }),
        required=False,
    )
    auteur = forms.ModelChoiceField(
        queryset=Utilisateur.objects.all(),
        widget=forms.Select(attrs={'class': 'search-form-fields form-select d-inline-block immediate-submit'}),
        required=False,
    )

    def __init__(self, famille=None, **kwargs):
        super().__init__(**kwargs)
        self.fields['auteur'].queryset = Utilisateur.objects.filter(
            pk__in=famille.prestations.values('auteur').distinct()
        ) if famille else Utilisateur.objects.all()

    def filter(self, prestations):
        if self.cleaned_data['auteur']:
            prestations = prestations.filter(auteur=self.cleaned_data['auteur'])
        if self.cleaned_data['recherche']:
            prestations = prestations.annotate(
                search=SearchVector("texte", config="french_unaccent")
            ).filter(
                search=SearchQuery(self.cleaned_data['recherche'], config="french_unaccent")
            )
        return prestations


class DocumentUploadForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Document
        fields = '__all__'
        widgets = {'famille': forms.HiddenInput}
        labels = {'fichier': ''}


class RapportEditForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Rapport
        exclude = ['famille', 'auteur']
        widgets = {
            'famille': forms.HiddenInput,
            'date': PickDateWidget,
            'pres_interv': BSCheckboxSelectMultiple,  # SPE
            'sig_interv': BSCheckboxSelectMultiple,  # SPE
        }
        field_classes = {
            'situation': RichTextField,
            'evolutions': RichTextField,
            'evaluation': RichTextField,
            'projet': RichTextField,
            'observations': RichTextField,
            'observ_ipe': RichTextField,
            'observ_sof': RichTextField,
            'observ_apa': RichTextField,
        }
    field_order = [
        'date', 'pres_interv', 'situation', 'observations', 'evolutions', 'evaluation',
        'observ_ipe', 'observ_sof', 'observ_apa', 'projet', 'sig_interv',
    ]

    def __init__(self, is_obs_enable=True, user=None, **kwargs):
        super().__init__(**kwargs)
        if 'sig_interv' in self.fields:
            suivi = kwargs['initial']['famille'].suivi
            interv_qs = Utilisateur.objects.filter(
                pk__in=getattr(suivi, f'intervenant{suivi.unite}_set').actifs(
                    self.instance.date or date.today()
                ).values_list('intervenant', flat=True)
            )
            self.fields['pres_interv'].queryset = interv_qs
            self.fields['sig_interv'].queryset = interv_qs
        if is_obs_enable:
            self.fields.pop('evolutions')
            self.fields.pop('evaluation')
        else:
            self.fields.pop('observations')
        read_only_fields = []
        if user and user.has_role(['IPE']):
            # Limit edition to observ_ipe field
            for field_name in list(self.fields.keys()):
                if field_name != 'observ_ipe':
                    read_only_fields.append(field_name)
        elif user and user.has_role(['Assistant-e social-e']):
            # Limit edition to observ_sof field
            for field_name in list(self.fields.keys()):
                if field_name != 'observ_sof':
                    read_only_fields.append(field_name)
        elif user and user.has_role(['Coach APA']):
            # Limit edition to observ_apa field
            for field_name in list(self.fields.keys()):
                if field_name != 'observ_apa':
                    read_only_fields.append(field_name)
        else:
            read_only_fields.extend(['observ_ipe', 'observ_sof', 'observ_apa'])
        for fname in read_only_fields:
            if getattr(self.instance, fname):
                self.fields[fname].disabled = True
            else:
                del self.fields[fname]


class MonthSelectionForm(BootstrapMixin, forms.Form):
    mois = forms.ChoiceField(choices=((mois_idx, MONTHS[mois_idx]) for mois_idx in range(1, 13)))
    annee = forms.ChoiceField(
        label='Année',
        choices=((year, year) for year in range(date.today().year - 3, date.today().year + 1)),
    )


class DateYearForm(forms.Form):
    year = forms.ChoiceField(choices=[(str(y), str(y)) for y in range(2020, date.today().year + 1)])

    def __init__(self, data=None, **kwargs):
        if not data:
            data = {'year': date.today().year}
        super().__init__(data, **kwargs)


class ContactExterneAutocompleteForm(forms.ModelForm):

    class Meta:
        model = Personne
        fields = ('reseaux',)
        widgets = {'reseaux': autocomplete.ModelSelect2Multiple(url='contact-externe-autocomplete')}
        labels = {'reseaux': 'Contacts'}


class ContactFilterForm(forms.Form):
    service = forms.ModelChoiceField(
        label="Service", queryset=Service.objects.all(), widget=ImmediateSelect, required=False
    )
    role = forms.ModelChoiceField(
        label="Rôle", queryset=Role.objects.exclude(famille=True), widget=ImmediateSelect, required=False
    )
    texte = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Recherche…', 'autocomplete': 'off'}),
        required=False
    )
    sort_by = forms.CharField(widget=forms.HiddenInput(), required=False)

    sort_by_mapping = {
        'nom': ['nom', 'prenom'],
        'service': ['service'],
        'role': ['roles__nom'],
        'activite': ['profession'],
    }

    def filter(self, contacts):
        if self.cleaned_data['service']:
            contacts = contacts.filter(service=self.cleaned_data['service'])
        if self.cleaned_data['role']:
            contacts = contacts.filter(roles=self.cleaned_data['role'])
        if self.cleaned_data['texte']:
            contacts = contacts.filter(nom__icontains=self.cleaned_data['texte'])
        if self.cleaned_data['sort_by']:
            order_desc = self.cleaned_data['sort_by'].startswith('-')
            contacts = contacts.order_by(*([
                ('-' if order_desc else '') + key
                for key in self.sort_by_mapping.get(self.cleaned_data['sort_by'].strip('-'), [])
            ]))
        return contacts


class FamilleAdresseForm(CityChMixin, BootstrapMixin, forms.ModelForm):
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'

    class Meta:
        model = Famille
        fields = ('rue', 'npa', 'localite')

    def __init__(self, **kwargs):
        self.famille = kwargs.pop('famille', None)
        super().__init__(**kwargs)

        membres = [
            (m.pk, f"{m.nom_prenom} ({m.role}), {m.adresse}")
            for m in self.famille.membres.all()
        ]
        self.fields['membres'] = forms.MultipleChoiceField(
            widget=BSCheckboxSelectMultiple,
            choices=membres,
            required=False
        )


class FamilleFormBase(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance and self.instance.pk:
            self.fields['rue'].widget.attrs['readonly'] = True
            self.fields['npa'].widget.attrs['readonly'] = True
            self.fields['localite'].widget.attrs['readonly'] = True
        typ = self.instance.__class__.default_typ
        if 'region' in self.fields:
            self.fields['region'] = forms.ModelChoiceField(
                label='Centre' if typ == 'cipe' else 'Région',
                queryset=Region.objects.par_secteur(typ).order_by('nom'),
                required=False,
                disabled=self.fields['region'].disabled,
                widget=forms.Select(attrs={'class': 'form-select'})
            )

    def clean_nom(self):
        return format_nom_prenom(self.cleaned_data['nom'])

    def clean_localite(self):
        localite = self.cleaned_data.get('localite')
        if localite and localite[0].islower():
            localite = localite[0].upper() + localite[1:]
        return localite


class MedlinkImportForm(forms.Form):
    fichier = forms.FileField(
        label='Fichier',
        widget=forms.FileInput(attrs={'class': 'form-control'})
    )

    def _search_user(self, user_str):
        if user_str.startswith(('ASE-', 'GED-')):
            user_str = user_str[4:]
        user_name = user_str.replace('-ASE ', ' ').replace('-GED ', ' ')
        user_name = ' '.join([part for part in user_name.split(' ') if part.isupper()])
        user_name = unaccent(user_name)
        if user_name in self.users_db:
            return self.users_db[user_name]
        else:
            self.users_non_trouves.add(user_str)
            return None

    def _search_client(self, prest, client_str):
        if client_str in (None, ''):
            return None
        nom = unaccent(client_str)
        if nom in self.clients_db[prest]:
            return self.clients_db[prest][nom].famille
        else:
            self.clients_non_trouves[prest].add(client_str)
            return None

    def clean(self):
        cleaned_data = super().clean()
        filename = cleaned_data['fichier']
        if filename.name[-5:].lower() == '.xlsx':
            imp_file = XLSXFile(filename)
        elif filename.name[-4:].lower() == '.csv':
            imp_file = CSVFile(filename)
        else:
            raise forms.ValidationError("L’extension du fichier est incorrecte.")

        self.users_non_trouves = set()
        self.clients_non_trouves = {"spe": set(), "sof": set()}
        self.ignore_fields = []
        user_qs = Utilisateur.objects.filter(date_desactivation__isnull=True, roles__nom='ASE')
        self.users_db = {unaccent(u.nom): u for u in user_qs}
        assert len(user_qs) == len(self.users_db)  # Pas de doublons qui s'écrasent
        mois_passe = (date.today() - timedelta(days=60)).replace(day=1)
        self.clients_db = {
            "spe": {
                unaccent([p.nom, p.prenom]): p
                for p in Personne.objects.filter(famille__suivispe__isnull=False).filter(
                    Q(famille__suivispe__date_fin_suivi__isnull=True) |
                    Q(famille__suivispe__date_fin_suivi__gte=mois_passe)
                )
            },
            "sof": {
                unaccent([p.nom, p.prenom]): p
                for p in Personne.objects.filter(famille__suivisof__isnull=False).filter(
                    Q(famille__suivisof__date_fin_suivi__isnull=True) |
                    Q(famille__suivisof__date_fin_suivi__gte=mois_passe)
                )
            },
        }
        self.prestations = {"spe": [], "sof": []}
        prest_gen_spe = LibellePrestation.objects.get(code='spe03')
        prest_fam_spe = LibellePrestation.objects.get(code='spe04')  # Activités ASE
        prest_gen_sof = LibellePrestation.objects.get(code='sof03')
        prest_fam_sof = LibellePrestation.objects.get(code='sof04')  # Activités ASE

        # expected_headers = [
        #    'Object Id', 'Equipe', 'Collaborateur', 'Id externe', 'Date Releve',
        #    'Début', 'Fin', 'Client', 'Prestation', 'Durée'
        #]
        USER = 2
        DATE = 4
        CLIENT = 7
        PREST = 8
        DUREE = 9
        for idx, row in enumerate(imp_file.lines):
            if idx == 0 and row[0] == 'Mois':
                continue
            if idx < 2:
                try:
                    # if the A1 cell is a number, we infer that no headers are present.
                    int(row[0])
                except (TypeError, ValueError):
                    has_headers = True
                else:
                    has_headers = False
                if has_headers:
                    try:
                        USER = row.index('Collaborateur')
                        DATE = row.index('Date Releve')
                        CLIENT = row.index('Client')
                        PREST = row.index('Prestation')
                        DUREE = row.index('Durée')
                    except ValueError:
                        raise forms.ValidationError(
                            "Les en-têtes du fichier ne correspondent pas à ce qui est attendu. "
                            "Êtes-vous certain-e qu’il s'agit d’un fichier Medlink ?"
                        )
                    continue

            if not any(row):
                # Skip completely empty rows
                continue
            if "SPE" in row[PREST]:
                prest = "spe"
            elif "SOF" in row[PREST]:
                prest = "sof"
            else:
                # Skip non-SPE lines
                continue
            famille = self._search_client(prest, row[CLIENT])
            t = row[DUREE]
            if isinstance(t, str):
                t = time(*[int(p) for p in t.split(':')])
            duree = timedelta(hours=t.hour, minutes=t.minute)
            if not famille and not duree:
                # Par ex déplacements km
                continue

            if isinstance(row[DATE], str):
                match row[DATE].count(':'):
                    case 2:
                        pattern = '%d.%m.%Y %H:%M:%S'
                    case 1:
                        pattern = '%d.%m.%Y %H:%M'
                    case _:
                        pattern = '%d.%m.%Y'
                dt = datetime.strptime(row[DATE], pattern).date()
            else:
                dt = row[DATE]
            if prest == "spe":
                prestation = prest_fam_spe if famille else prest_gen_spe
            elif prest == "sof":
                prestation = prest_fam_sof if famille else prest_gen_sof
            self.prestations[prest].append(
                dict(
                    date_prestation=dt,
                    duree=duree,
                    famille=famille,
                    lib_prestation=prestation,
                    texte=row[PREST],
                    intervenant=self._search_user(row[USER])
                )
            )

        imp_file.close()
        error = ''
        if len(self.users_non_trouves) > 0:
            error += "Impossible de trouver les intervenant-e-s suivant-e-s dans la base: <br>"
            error += '<br>'.join([escape(f' - {u}') for u in self.users_non_trouves])
            error += '<br>'
        clients_ignores = set([val for key, val in self.data.items() if key.startswith('non_trouve_')])
        non_trouves = self.clients_non_trouves["spe"] | self.clients_non_trouves["sof"]
        if len(non_trouves - clients_ignores) > 0:
            error += "Impossible de trouver les personnes suivantes dans la base:"
            idx = 0
            for cl in self.clients_non_trouves["spe"]:
                self.fields[f'non_trouve_{idx}'] = forms.BooleanField(
                    label=f"[SPE] {cl}", required=False, widget=forms.CheckboxInput(attrs={'value': cl})
                )
                self.ignore_fields.append(f'non_trouve_{idx}')
                idx += 1
            for cl in self.clients_non_trouves["sof"]:
                self.fields[f'non_trouve_{idx}'] = forms.BooleanField(
                    label=f"[SOF] {cl}", required=False, widget=forms.CheckboxInput(attrs={'value': cl})
                )
                self.ignore_fields.append(f'non_trouve_{idx}')
                idx += 1
        if error:
            raise forms.ValidationError(mark_safe(error))
