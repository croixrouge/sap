from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0053_alter_libelleprestation_unite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='personne',
            name='animaux',
        ),
        migrations.RemoveField(
            model_name='personne',
            name='employeur',
        ),
        migrations.RemoveField(
            model_name='personne',
            name='validite',
        ),
    ]
