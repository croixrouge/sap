from django.db import migrations, models

import common.fields


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0045_personne_npa_5_chars'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='secteurs',
            field=common.fields.ChoiceArrayField(
                base_field=models.CharField(choices=[('amf', 'AMF'), ('cipe', 'IPE'), ('sof', 'SOF'), ('spe', 'SPE')], max_length=10),
                blank=True, null=True, size=None, verbose_name='Secteurs'
            ),
        ),
    ]
