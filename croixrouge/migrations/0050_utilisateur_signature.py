from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0049_personne_archived_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='utilisateur',
            name='signature',
            field=models.ImageField(blank=True, null=True, upload_to='signatures/'),
        ),
    ]
