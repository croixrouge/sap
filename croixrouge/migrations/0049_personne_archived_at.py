from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0048_remove_region_cipe_spe_sifp'),
    ]

    operations = [
        migrations.AddField(
            model_name='personne',
            name='archived_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Archivée le'),
        ),
    ]
