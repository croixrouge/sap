from django.db import migrations


def migrate_secteurs(apps, schema_editor):
    """ Migre le champ Role vers une relation M2M  """

    Region = apps.get_model('croixrouge', 'Region')

    for region in Region.objects.all():
        region.secteurs = [val for val in [
            sect if getattr(region, sect) else None for sect in ['cipe', 'sifp', 'spe']
        ] if val]
        region.save()


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0046_region_secteurs'),
    ]

    operations = [
        migrations.RunPython(migrate_secteurs)
    ]
