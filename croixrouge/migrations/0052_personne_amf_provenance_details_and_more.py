from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0051_personne_amf_debut_de_placement_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='personne',
            name='amf_provenance_details',
            field=models.CharField(blank=True, max_length=50, verbose_name='Provenance (détails)'),
        ),
        migrations.AlterField(
            model_name='personne',
            name='amf_provenance',
            field=models.CharField(blank=True, choices=[('Famille biologique', 'Famille biologique'), ('IES', 'Institution d’Education Spécialisée'), ('Autre FA', 'Autre famille d’accueil'), ('Centre Couvet', 'Centre Couvet'), ('Autre', 'Autre')], max_length=20, verbose_name='Provenance'),
        ),
        migrations.AlterField(
            model_name='personne',
            name='amf_type_accueil',
            field=models.CharField(blank=True, choices=[('FAO', 'Famille d’accueil par opportunité'), ('Relais FA', 'relais d’une famille d’accueil'), ('Intra', 'Intrafamilial'), ('Intra relais', 'Relais intrafamilial'), ('Relais IES', 'Relais d’une institution d’éducation spécialisée'), ('FA', 'Famille d’accueil'), ('SMI', 'Dossier transmis par le Service des Migrations'), ('Relais biologique', 'relais d’une famille biologique')], max_length=20, verbose_name="Type d'accueil"),
        ),
    ]
