import django.contrib.auth.models
import django.contrib.auth.validators
import django.contrib.postgres.fields
import django.core.validators
import django.db.models.deletion
import django.utils.timezone
import django_countries.fields
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='CercleScolaire',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, unique=True)),
                ('telephone', models.CharField(blank=True, max_length=35, verbose_name='tél.')),
            ],
            options={
                'verbose_name': 'Cercle scolaire',
                'verbose_name_plural': 'Cercles scolaires',
                'ordering': ('nom',),
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=30, unique=True)),
                ('rue', models.CharField(blank=True, max_length=100, verbose_name='Rue')),
                ('cipe', models.BooleanField(default=False)),
                ('sifp', models.BooleanField(default=False)),
                ('spe', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, unique=True, verbose_name='Nom')),
                ('famille', models.BooleanField(default=False, verbose_name='Famille')),
                ('intervenant', models.BooleanField(default=False, verbose_name='Intervenant')),
                ('editeur', models.BooleanField(default=False, help_text='Un rôle éditeur donne le droit de modification des dossiers familles si la personne est intervenante.', verbose_name='Éditeur')),
            ],
            options={
                'ordering': ('nom',),
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sigle', models.CharField(max_length=20, unique=True, verbose_name='Sigle')),
                ('nom_complet', models.CharField(blank=True, max_length=80, verbose_name='Nom complet')),
            ],
            options={
                'ordering': ('sigle',),
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prenom', models.CharField(max_length=30, verbose_name='Prénom')),
                ('nom', models.CharField(max_length=30, verbose_name='Nom')),
                ('rue', models.CharField(blank=True, max_length=30, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=4, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('tel_prive', models.CharField(blank=True, max_length=30, verbose_name='Tél. privé')),
                ('tel_prof', models.CharField(blank=True, max_length=30, verbose_name='Tél. prof.')),
                ('email', models.EmailField(blank=True, max_length=100, verbose_name='Courriel')),
                ('profession', models.CharField(blank=True, max_length=100, verbose_name='Activité/prof.')),
                ('remarque', models.TextField(blank=True, verbose_name='Remarque')),
                ('est_actif', models.BooleanField(default=True, verbose_name='actif')),
                ('roles', models.ManyToManyField(blank=True, related_name='contacts', to='croixrouge.role', verbose_name='Rôles')),
                ('service', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='croixrouge.service')),
            ],
            options={
                'verbose_name': 'Contact',
                'ordering': ('nom', 'prenom'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='contact',
            unique_together={('nom', 'prenom', 'service')},
        ),
        migrations.CreateModel(
            name='Famille',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', django.contrib.postgres.fields.ArrayField(
                    base_field=models.CharField(
                        choices=[('amf', 'AMF'), ('cipe', 'IPE'), ('spe', 'SPE'), ('sifp', 'SIFP')],
                        max_length=5), size=None
                    )
                ),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('archived_at', models.DateTimeField(blank=True, null=True, verbose_name='Archivée le')),
                ('nom', models.CharField(max_length=40, verbose_name='Nom de famille')),
                ('rue', models.CharField(blank=True, max_length=60, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=4, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('region', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='croixrouge.region')),
                ('telephone', models.CharField(blank=True, max_length=60, verbose_name='Tél.')),
                ('autorite_parentale', models.CharField(blank=True, choices=[('conjointe', 'Conjointe'), ('pere', 'Père'), ('mere', 'Mère'), ('tutelle', 'Tutelle')], max_length=20, verbose_name='Autorité parentale')),
                ('monoparentale', models.BooleanField(blank=True, default=None, null=True, verbose_name='Famille monoparent.')),
                ('statut_marital', models.CharField(blank=True, choices=[('celibat', 'Célibataire'), ('mariage', 'Marié'), ('pacs', 'PACS'), ('concubin', 'Concubin'), ('veuf', 'Veuf'), ('separe', 'Séparé'), ('divorce', 'Divorcé')], max_length=20, verbose_name='Statut marital')),
                ('connue', models.BooleanField(default=False, verbose_name='famille déjà suivie')),
                ('accueil', models.BooleanField(default=False, verbose_name="famille d'accueil")),
                ('garde', models.CharField(blank=True, choices=[('partage', 'garde partagée'), ('droit', 'droit de garde'), ('visite', 'droit de visite')], max_length=20, verbose_name='Type de garde')),
                ('destination', models.CharField(blank=True, choices=[('famille', 'Famille'), ('ies-ne', 'IES-NE'), ('ies-hc', 'IES-HC'), ('aemo', 'SAEMO'), ('fah', "Famille d'accueil"), ('refug', 'Centre d’accueil réfugiés'), ('hopital', 'Hôpital'), ('autre', 'Autre')], max_length=30, verbose_name='Destination')),
                ('provenance', models.CharField(blank=True, choices=[('famille', 'Famille'), ('ies-ne', 'IES-NE'), ('ies-hc', 'IES-HC'), ('aemo', 'SAEMO'), ('fah', "Famille d'accueil"), ('refug', 'Centre d’accueil réfugiés'), ('hopital', 'Hôpital'), ('autre', 'Autre')], max_length=30, verbose_name='Provenance')),
                ('statut_financier', models.CharField(blank=True, choices=[('ai', 'AI PC'), ('gsr', 'GSR'), ('osas', 'OSAS'), ('revenu', 'Revenu'), ('chomage', 'Chômage')], max_length=30, verbose_name='Statut financier')),
                ('sap', models.BooleanField(default=False, verbose_name='famille s@p')),
                ('besoins_part', models.BooleanField(default=False, verbose_name='famille à besoins particuliers')),
                ('remarques', models.TextField(blank=True)),
            ],
            options={
                'ordering': ('nom', 'npa'),
                'permissions': (('export_stats', 'Exporter les statistiques'),),
            },
        ),
        migrations.CreateModel(
            name='LibellePrestation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=6, unique=True, verbose_name='Code')),
                ('nom', models.CharField(max_length=30, verbose_name='Nom')),
                ('unite', models.CharField(choices=[('spe', 'SPE'), ('sifp', 'SIFP')], max_length=10, verbose_name='Unité')),
                ('actes', models.TextField(blank=True, verbose_name='Actes à prester')),
            ],
            options={
                'ordering': ('code',),
            },
        ),
        migrations.CreateModel(
            name='Personne',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('prenom', models.CharField(blank=True, max_length=30, verbose_name='Prénom')),
                ('nom', models.CharField(max_length=30, verbose_name='Nom')),
                ('date_naissance', models.DateField(blank=True, null=True, verbose_name='Date de naissance')),
                ('genre', models.CharField(choices=[('M', 'M'), ('F', 'F')], default='M', max_length=1, verbose_name='Genre')),
                ('rue', models.CharField(blank=True, max_length=60, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=4, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('pays_origine', django_countries.fields.CountryField(blank=True, max_length=2, verbose_name='Nationalité')),
                ('telephone', models.CharField(blank=True, max_length=60, verbose_name='Tél.')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Courriel')),
                ('remarque', models.TextField(blank=True)),
                ('remarque_privee', models.TextField(blank=True, verbose_name='Remarque privée')),
                ('profession', models.CharField(blank=True, max_length=50, verbose_name='Profession')),
                ('filiation', models.CharField(blank=True, max_length=80, verbose_name='Filiation')),
                ('employeur', models.CharField(blank=True, max_length=50, verbose_name='Adresse empl.')),
                ('permis', models.CharField(blank=True, max_length=30, verbose_name='Permis/séjour')),
                ('validite', models.DateField(blank=True, null=True, verbose_name='Date validité')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='membres', to='croixrouge.famille')),
                ('reseaux', models.ManyToManyField(blank=True, to='croixrouge.contact')),
                ('role', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='croixrouge.role')),
                ('animaux', models.BooleanField(default=None, null=True, verbose_name='Animaux')),
                ('allergies', models.TextField(blank=True, verbose_name='Allergies')),
                ('decedee', models.BooleanField(default=False, verbose_name='Cette personne est décédée')),
            ],
            options={
                'verbose_name': 'Personne',
                'ordering': ('nom', 'prenom'),
            },
        ),
        migrations.CreateModel(
            name='Formation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('statut', models.CharField(blank=True, choices=[('pre_scol', 'Pré-scolaire'), ('cycle1', 'Cycle 1'), ('cycle2', 'Cycle 2'), ('cycle3', 'Cycle 3'), ('apprenti', 'Apprentissage'), ('etudiant', 'Etudiant'), ('en_emploi', 'En emploi'), ('sans_emploi', 'Sans emploi'), ('sans_occupation', 'Sans occupation')], max_length=20, verbose_name='Scolarité')),
                ('college', models.CharField(blank=True, max_length=50, verbose_name='Collège')),
                ('classe', models.CharField(blank=True, max_length=50, verbose_name='Classe')),
                ('enseignant', models.CharField(blank=True, max_length=50, verbose_name='Enseignant')),
                ('creche', models.CharField(blank=True, max_length=50, verbose_name='Crèche')),
                ('creche_resp', models.CharField(blank=True, max_length=50, verbose_name='Resp.crèche')),
                ('entreprise', models.CharField(blank=True, max_length=50, verbose_name='Entreprise')),
                ('maitre_apprentissage', models.CharField(blank=True, max_length=50, verbose_name="Maître d'appr.")),
                ('remarque', models.TextField(blank=True)),
                ('cercle_scolaire', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='croixrouge.cerclescolaire', verbose_name='Cercle scolaire')),
                ('personne', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='croixrouge.personne')),
            ],
            options={
                'verbose_name': 'Scolarité',
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fichier', models.FileField(upload_to='doc', verbose_name='Nouveau fichier')),
                ('titre', models.CharField(max_length=100)),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='croixrouge.famille')),
            ],
            options={
                'unique_together': {('famille', 'titre')},
            },
        ),
        migrations.CreateModel(
            name='Utilisateur',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='croixrouge.contact')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('sigle', models.CharField(blank=True, max_length=5)),
                ('date_desactivation', models.DateField(blank=True, null=True, verbose_name='Date désactivation')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.permission', verbose_name='user permissions')),
                ('taux_activite', models.PositiveSmallIntegerField(blank=True, default=0, validators=[django.core.validators.MaxValueValidator(100)], verbose_name='Taux d’activité (en %)')),
                ('equipe', models.CharField(blank=True, choices=[('montagnes', 'Montagnes et V-d-T'), ('littoral', 'Littoral et V-d-R')], max_length=10, verbose_name='Équipe')),
                ('decharge', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Heures de décharge')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            bases=('croixrouge.contact', models.Model),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='JournalAcces',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ordinaire', models.BooleanField(default=True)),
                ('quand', models.DateTimeField(auto_now_add=True)),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='croixrouge.famille')),
                ('utilisateur', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='GroupInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True)),
                ('group', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='auth.group')),
            ],
        ),
    ]
