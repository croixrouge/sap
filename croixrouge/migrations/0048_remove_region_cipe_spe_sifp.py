from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0047_migrate_region_secteurs'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='region',
            name='cipe',
        ),
        migrations.RemoveField(
            model_name='region',
            name='sifp',
        ),
        migrations.RemoveField(
            model_name='region',
            name='spe',
        ),
    ]
