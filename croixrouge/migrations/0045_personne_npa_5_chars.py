from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0001_squashed_0044_famille_besoins_part'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personne',
            name='npa',
            field=models.CharField(blank=True, max_length=5, verbose_name='NPA'),
        ),
    ]
