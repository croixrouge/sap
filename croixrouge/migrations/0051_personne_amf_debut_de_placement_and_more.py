from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0050_utilisateur_signature'),
    ]

    operations = [
        migrations.AddField(
            model_name='personne',
            name='amf_debut_de_placement',
            field=models.DateField(blank=True, null=True, verbose_name='Début de placement'),
        ),
        migrations.AddField(
            model_name='personne',
            name='amf_fin_de_placement',
            field=models.DateField(blank=True, null=True, verbose_name='Fin de placement'),
        ),
        migrations.AddField(
            model_name='personne',
            name='amf_provenance',
            field=models.CharField(blank=True, choices=[('famille_biolo', 'Famille biologique'), ('institution', 'Institution'), ('famille_accueil', "Famille d'accueil"), ('ctre_specialise', 'Centre spécialisé')], max_length=15, verbose_name='Provenance'),
        ),
        migrations.AddField(
            model_name='personne',
            name='amf_type_accueil',
            field=models.CharField(blank=True, choices=[('famille_origine', "Famille d'origine"), ('relais_famille', 'Relais famille'), ('implant', 'Implant'), ('intrafamilial', 'Intrafamilial'), ('relais_intrafam', 'Relais intrafamilial'), ('relais_institut', 'Relais institution'), ('famille_accueil', "Famille d'accueil")], max_length=15, verbose_name="Type d'accueil"),
        ),
    ]
