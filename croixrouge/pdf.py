from functools import partial

import extract_msg
import nh3
from bs4 import BeautifulSoup
from django.contrib.staticfiles.finders import find
from django.utils.text import slugify
from PIL import Image
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas
from reportlab.platypus import Image as rlImage
from reportlab.platypus import Paragraph, Preformatted, SimpleDocTemplate, Spacer, Table, TableStyle

from .utils import format_d_m_Y, format_duree, format_Ymd


def format_booleen(val):
    return '?' if val is None else ('oui' if val else 'non')


class PageNumCanvas(canvas.Canvas):
    """A special canvas to be able to draw the total page number in the footer."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = []

    def showPage(self):
        self._pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self._pages)
        for page in self._pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)
        super().save()

    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 9)
        self.drawRightString(self._pagesize[0] - 1.6*cm, 2.3*cm, "p. %s/%s" % (self._pageNumber, page_count))


class CleanParagraph(Paragraph):
    """If the (HTML) text cannot be parsed, try to clean it."""
    def __init__(self, text, *args, **kwargs):
        if text:
            text = text.replace('</p>', '</p><br/>')
            if '<ul>' in text:
                text = text.replace('<li>', '&nbsp;&nbsp;&bull; ').replace('</ul>','<br>').replace('</li>', '<br>')
        try:
            super().__init__(text, *args, **kwargs)
        except ValueError:
            text = nh3.clean(
                text, tags={'p', 'br', 'b', 'strong', 'u', 'i', 'em', 'ul', 'li'}
            ).replace('<br>', '<br/>')
        super().__init__(text, *args, **kwargs)


class RawParagraph(Paragraph):
    """Raw text, replace new lines by <br/>."""
    def __init__(self, text='', *args, **kwargs):
        if text:
            text = text.replace('\r\n', '\n').replace('\n', '\n<br/>')
        super().__init__(text, *args, **kwargs)


class StyleMixin:
    FONTSIZE = 9
    MAXLINELENGTH = 120

    def __init__(self, base_font='Helvetica', **kwargs):
        self.styles = getSampleStyleSheet()
        self.style_title = ParagraphStyle(
            name='title', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 4,
            leading=self.FONTSIZE + 5, alignment=TA_CENTER
        )
        self.style_normal = ParagraphStyle(
            name='normal', fontName='Helvetica', fontSize=self.FONTSIZE, alignment=TA_LEFT,
            leading=self.FONTSIZE + 1, spaceAfter=0
        )
        self.style_justifie = ParagraphStyle(
            name='justifie', parent=self.style_normal, alignment=TA_JUSTIFY, spaceAfter=0.2 * cm
        )
        self.style_sub_title = ParagraphStyle(
            name='sous_titre', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 2,
            alignment=TA_LEFT, spaceBefore=0.5 * cm, spaceAfter=0.1 * cm
        )
        self.style_inter_title = ParagraphStyle(
            name='inter_titre', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 1,
            alignment=TA_LEFT, spaceBefore=0.3 * cm, spaceAfter=0
        )
        self.style_bold = ParagraphStyle(
            name='bold', fontName='Helvetica-Bold', fontSize=self.FONTSIZE, leading=self.FONTSIZE + 1
        )
        self.style_italic = ParagraphStyle(
            name='italic', fontName='Helvetica-Oblique', fontSize=self.FONTSIZE - 1, leading=self.FONTSIZE
        )
        self.style_indent = ParagraphStyle(
            name='indent', fontName='Helvetica', fontSize=self.FONTSIZE, alignment=TA_LEFT,
            leftIndent=1 * cm
        )
        super().__init__(**kwargs)


class HeaderFooterMixin:
    LOGO = find('img/logo-cr.png')
    EDUQUA = find('img/eduqua.png')
    DON = find('img/logo-zewo.png')

    def draw_header(self, canvas, doc):
        canvas.saveState()
        canvas.drawImage(
            self.LOGO, doc.leftMargin + 316, doc.height+60, 7 * cm, 1.6 * cm, preserveAspectRatio=True, mask='auto'
        )
        canvas.restoreState()

    def draw_footer(self, canvas, doc):
        canvas.saveState()
        canvas.drawImage(
            self.EDUQUA, doc.leftMargin, doc.height - 670, 1.8 * cm, 0.8 * cm, preserveAspectRatio=True
        )
        canvas.drawImage(
            self.DON, doc.leftMargin + 60, doc.height - 670, 2.5 * cm, 0.8 * cm, preserveAspectRatio=True, mask='auto'
        )
        tab = [220, 365]
        line = [658, 667, 676, 685]

        canvas.setFont("Helvetica", 8)
        canvas.drawRightString(doc.leftMargin + tab[0], doc.height - line[2], "CCP 20-1504-8")
        canvas.drawRightString(doc.leftMargin + tab[0], doc.height - line[3], "IBAN CH90 0900 0000 2000 1504 8")
        canvas.setLineWidth(0.5)
        canvas.line(doc.leftMargin + 230, 2.2 * cm, doc.leftMargin + 230, 1.0 * cm)

        canvas.drawRightString(doc.leftMargin + tab[1], doc.height - line[0], "Rue de la Paix 71")
        canvas.setFont("Helvetica-Bold", 8)
        canvas.drawRightString(doc.leftMargin + tab[1], doc.height - line[1], "2300 La Chaux-de-Fonds")
        canvas.setFont("Helvetica", 8)
        canvas.drawRightString(doc.leftMargin + tab[1], doc.height - line[2], "Avenue du Premier-Mars 2a")
        canvas.setFont("Helvetica-Bold", 8)
        canvas.drawRightString(doc.leftMargin + tab[1], doc.height - line[3], "2000 Neuchâtel")
        canvas.setFont("Helvetica", 8)
        canvas.line(doc.leftMargin + 375, 2.2 * cm, doc.leftMargin + 375, 1.0 * cm)
        canvas.drawRightString(doc.leftMargin + doc.width, doc.height - line[0], "+41 32 886 88 60")
        canvas.drawRightString(doc.leftMargin + doc.width, doc.height - line[1], "contact@croix-rouge-ne.ch")
        canvas.drawRightString(doc.leftMargin + doc.width, doc.height - line[2], "www.croix-rouge-ne.ch")
        canvas.restoreState()


class BaseCroixrougePDF(HeaderFooterMixin, StyleMixin):

    def __init__(self, tampon, instance, **kwargs):
        self.instance = instance
        self.kwargs = kwargs
        self.doc = SimpleDocTemplate(
            tampon, title=self.title, pagesize=A4,
            leftMargin=1.5 * cm, rightMargin=1.5 * cm, topMargin=2 * cm, bottomMargin=2.5 * cm
        )
        self.story = []
        super().__init__(**kwargs)

    def draw_header_footer(self, canvas, doc):
        self.draw_header(canvas, doc)
        self.draw_footer(canvas, doc)

    def produce(self):
        # subclass should call self.doc.build(self.story, onFirstPage=self.draw_header_footer)
        raise NotImplementedError

    def get_filename(self):
        raise NotImplementedError

    @staticmethod
    def format_note(note, length=StyleMixin.MAXLINELENGTH):
        return Preformatted(note.replace('\r\n', '\n'), maxLineLength=length)

    def set_title(self, title=''):
        self.story.append(Spacer(0, 1 * cm))
        self.story.append(Paragraph(title, self.style_title))
        self.story.append(Spacer(0, 1.2 * cm))

    def parent_data(self, person):
        """Return parent data ready to be used in a 2-column table."""
        parents = person.parents()
        par1 = parents[0] if len(parents) > 0 else None
        par2 = parents[1] if len(parents) > 1 else None
        data = [
            [('Parent 1 (%s)' % par1.role.nom) if par1 else '-',
                ('Parent 2 (%s)' % par2.role.nom) if par2 else '-'],
            [par1.contact.nom_prenom if par1 else '', par2.contact.nom_prenom if par2 else ''],
            [par1.contact.adresse if par1 else '', par2.contact.adresse if par2 else ''],
            [par1.contact.contact if par1 else '', par2.contact.contact if par2 else ''],

            ['Autorité parentale: {}'.format(format_booleen(par1.contact.autorite_parentale)) if par1 else '',
                'Autorité parentale: {}'.format(format_booleen(par2.contact.autorite_parentale)) if par2 else ''],
        ]
        return data

    def formate_persons(self, parents_list):
        labels = (
            ("Nom", "nom"), ("Prénom", "prenom"), ("Adresse", "rue"),
            ("Localité", 'localite_display'), ("Profession", 'profession'), ("Tél.", 'telephone'),
            ("Courriel", 'email'), ('Remarque', 'remarque'),
        )
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)
        data = []
        parents = [parent for parent in parents_list if parent is not None]
        if len(parents) == 0:
            pass
        elif len(parents) == 1:
            data.append([Pbold('Rôle:'), Pbold(parents[0].role.nom), '', ''])
            for label in labels:
                data.append([
                    label[0], P(getattr(parents[0], label[1])), '', ''
                ])
        elif len(parents) == 2:
            data.append([
                Pbold('Rôle:'), Pbold(parents[0].role.nom),
                Pbold('Rôle:'), Pbold(parents[1].role.nom)
            ])
            for label in labels:
                data.append([
                    label[0], P(getattr(parents[0], label[1]) if parents[0] else ''),
                    label[0], P(getattr(parents[1], label[1]) if parents[1] else '')
                ])
        return data

    def get_table(self, data, columns, before=0.0, after=0.0, inter=None):
        """Prepare a Table instance with data and columns, with a common style."""
        if inter:
            inter = inter * cm
        cols = [c * cm for c in columns]

        t = Table(
            data=data, colWidths=cols, hAlign=TA_LEFT,
            spaceBefore=before * cm, spaceAfter=after * cm
        )
        t.hAlign = 0
        t.setStyle(tblstyle=TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('FONTSIZE', (0, 0), (-1, -1), self.FONTSIZE),
            ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ('LEFTPADDING', (0, 0), (0, -1), 1),
            ('LEADING', (0, 0), (-1, -1), 7),
        ]))
        return t

    def _add_subtitle(self, data):
        t = Table(
            data=[data], colWidths=[18 * cm / len(data)] * len(data),
            hAlign=TA_LEFT,
            spaceBefore=0.2 * cm, spaceAfter=0.5 * cm
        )
        t.hAlign = 0
        t.setStyle(tblstyle=TableStyle([
            ('FONT', (0, 0), (-1, -1), "Helvetica-Bold"),
            ('FONTSIZE', (0, 0), (-1, -1), self.FONTSIZE + 2),
            ('LINEBELOW', (0, -1), (-1, -1), 0.25, colors.black),
            ('ALIGN', (-1, -1), (-1, -1), 'RIGHT'),
        ]))
        self.story.append(t)

    def write_paragraph(self, title, text, html=False, justifie=False):
        if title:
            self.story.append(Paragraph(title, self.style_sub_title))
        style = self.style_justifie if justifie else self.style_normal
        if html:
            if text.startswith('<p>') and text.endswith('</p>'):
                soup = BeautifulSoup(text, features="html5lib")
                for tag in soup.find_all(['p', 'ul']):
                    self.story.append(CleanParagraph(str(tag), style))
            else:
                self.story.append(CleanParagraph(text, style))
        else:
            self.story.append(RawParagraph(text, style))

    def enfant_data(self, enfant):
        labels = [('Tél.', enfant.telephone)]
        if hasattr(enfant, 'formation'):
            labels.extend([
                ('Statut scol', enfant.formation.get_statut_display()),
                ('Centre', enfant.formation.cercle_scolaire),
                ('Collège', enfant.formation.college),
                ('Classe', enfant.formation.classe),
                ('Struct. extra-fam.', enfant.formation.creche),
                ('Ens.', enfant.formation.enseignant),
            ])
        labels.extend([
            ('Permis de séjour', enfant.permis),
        ])
        row = [f"{enfant.nom_prenom} (*{format_d_m_Y(enfant.date_naissance)})"]
        for label in labels:
            if label[1]:
                row.append(f"{label[0]}: {label[1]}")
        return '; '.join(row)

    def insert_image(self, path, max_w=None, max_h=None, hAlign='LEFT'):
        def _max_image_dim(current_w, current_h, max_w, max_h):
            shrink_w = shrink_h = 1
            if max_w is not None and current_w > max_w:
                shrink_w = max_w / float(current_w)
            if max_h is not None and current_h > max_h:
                shrink_h = max_h / float(current_h)
            shrink = min([shrink_h, shrink_w])
            return shrink * current_w, shrink * current_h

        with Image.open(path) as im:
            width, height = im.size
        if max_w or max_h:
            width, height = _max_image_dim(width, height, max_w=max_w, max_h=max_h)
        self.story.append(Spacer(0, 0.15 * cm))
        self.story.append(rlImage(path, height=height, width=width, hAlign=hAlign))

    def sig_interv_bloc(self, intervs):
        for idx, interv in enumerate(intervs):
            if idx == 0:
                self.write_paragraph("Signature des intervenant-e-s SPE :", '')
                self.story.append(Spacer(0, 0.2 * cm))
            self.write_paragraph('', interv.nom_prenom + (f', {interv.profession}' if interv.profession else ''))
            if interv.signature:
                self.insert_image(interv.signature.path, max_h=1.2 * cm)
            else:
                self.story.append(Spacer(0, 1 * cm))


class DemandeAccompagnement(BaseCroixrougePDF):
    title = None

    def produce(self):
        famille = self.instance
        suivi = famille.suivi
        self.set_title("Famille {} - {}".format(famille.nom, self.title))

        self.story.append(Paragraph("<strong>Motif(s) de la demande:</strong> {}".format(
            suivi.get_motif_demande_display()), self.style_normal
        ))
        self.story.append(Paragraph('_' * 90, self.style_normal))

        self.write_paragraph("Dates", suivi.dates_demande)
        self.write_paragraph(
            "Difficultés",
            "{}<br/>{}<br/><br/>{}".format(
                "<em>Quelles sont les difficultés éducatives que vous rencontrez et depuis combien de "
                "temps ?",
                "Fonctionnement familial: règles, coucher, lever, repas, jeux, relations "
                "parent-enfants, rapport au sein de la fratrie, … (exemple)</em>",
                suivi.difficultes
            ),
            html=True
        )
        self.write_paragraph(
            "Autres services",
            "<em>{}</em><br/>{}".format(
                "Avez-vous fait appel à d'autres services ? Si oui, avec quels vécus ?",
                suivi.autres_contacts
            ),
            html=True
        )
        self.write_paragraph("Aides souhaitées", suivi.aides, html=True)
        self.write_paragraph("Ressources/Compétences", suivi.competences, html=True)
        self.write_paragraph("Disponibilités", suivi.disponibilites, html=True)
        self.write_paragraph("Remarques", suivi.remarque)

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class JournalPdf(BaseCroixrougePDF):
    title = "Journal de bord"

    def get_filename(self):
        return '{}_journal.pdf'.format(slugify(self.instance.nom))

    def get_title(self):
        return 'Famille {} - {}'.format(self.instance.nom, self.title)

    def produce(self):
        famille = self.instance
        self.set_title(self.get_title())

        self.style_bold.spaceAfter = 0.2*cm
        self.style_italic.spaceBefore = 0.2 * cm
        self.style_italic.spaceAfter = 0.7 * cm
        for prest in famille.prestations.all().prefetch_related('intervenants'):
            self.story.append(CleanParagraph(prest.texte, self.style_normal))
            self.story.append(
                Paragraph('{} - {} ({})'.format(
                    '/'.join(interv.sigle for interv in prest.intervenants.all()),
                    format_d_m_Y(prest.date_prestation),
                    format_duree(prest.duree)
                ), self.style_italic)
            )

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
            canvasmaker=PageNumCanvas
        )


class RapportPdf(BaseCroixrougePDF):
    title = None

    def get_filename(self):
        return "{}_resume_{}.pdf".format(
            slugify(self.instance.famille.nom),
            format_Ymd(self.instance.date)
        )

    def produce(self):
        rapport = self.instance
        self.style_normal.fontSize += 2
        self.style_normal.leading += 2
        self.style_justifie.fontSize += 2
        self.style_justifie.leading += 2
        self.doc.title = "Résumé SPE"
        self.set_title('Famille {} - {}'.format(rapport.famille.nom, self.doc.title))
        self._add_subtitle([f"Date: {format_d_m_Y(rapport.date)}"])

        data = [
            ("Enfant(s)", '<br/>'.join(
                [f"{enfant.nom_prenom} (*{format_d_m_Y(enfant.date_naissance)})"
                 for enfant in rapport.famille.membres_suivis()]), False),
            ("Intervenant-e-s", ', '.join([i.nom_prenom for i in rapport.intervenants()]), False),
            ("Début du suivi", format_d_m_Y(rapport.famille.suivi.date_debut_suivi), False),
            ("Situation / contexte familial", rapport.situation, True),
        ]
        if rapport.is_observations_enable():
            data.append(("Observations", rapport.observations, True))
        else:
            data.append(("Évolutions et observations", rapport.evolutions, True))
            data.append(("Évaluation / Hypothèses", rapport.evaluation, True))
        if rapport.observ_ipe:
            data.append(("Observations IPE", rapport.observ_ipe, True))
        if rapport.observ_sof:
            data.append(("Observations SOF", rapport.observ_sof, True))
        if rapport.observ_apa:
            data.append(("Observations APA", rapport.observ_apa, True))
        data.append(("Perspectives d'avenir", rapport.projet, True))

        for title, text, html in data:
            self.write_paragraph(title, text, html=html, justifie=True)

        if hasattr(rapport, 'sig_interv'):
            self.story.append(Spacer(0, 0.5 * cm))
            self.sig_interv_bloc(rapport.sig_interv.all())

        secret_style = ParagraphStyle(
            name='italic', fontName='Helvetica-Oblique', fontSize=self.FONTSIZE - 1, leading=self.FONTSIZE + 1,
            backColor=colors.Color(0.96, 0.96, 0.96, 1), borderRadius=12,
        )
        self.story.append(Paragraph(
            "Le présent résumé comporte des éléments <b>couverts par le secret professionnel au sens "
            "de la LPSy et du Code pénal</b>. Seuls les propriétaires des données, à savoir les membres "
            "de la famille faisant l’objet du résumé, peuvent <b>ensemble</b> lever ce secret ou "
            "accepter la divulgation des données. Si cette autorisation n’est pas donnée, l’autorité "
            "compétente en matière de levée du secret professionnel doit impérativement être saisie.",
            secret_style
        ))

        self.doc.build(self.story, onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer)


class MessagePdf(BaseCroixrougePDF):
    title = 'Message'

    def get_filename(self):
        return '{}_message.pdf'.format(slugify(self.instance.subject))

    def produce(self):
        doc = self.instance
        self.set_title('{} - Famille {}'.format(self.title, doc.famille.nom))
        with extract_msg.Message(doc.fichier.path) as msg:
            P = partial(Paragraph, style=self.style_normal)
            Pbold = partial(Paragraph, style=self.style_bold)

            msg_headers = [
                [Pbold('De:'), P(msg.sender)],
                [Pbold('À:'), P(msg.to)],
            ]
            if msg.cc:
                msg_headers.append([Pbold('CC:'), P(msg.cc)])
            if msg.date:
                msg_headers.append([Pbold('Date:'), P(msg.date)])
            msg_headers.append([Pbold('Sujet:'), P(msg.subject)])
            self.story.append(self.get_table(msg_headers, [3, 15]))
            self.story.append(Pbold('Message:'))
            self.story.append(RawParagraph(msg.body, style=self.style_normal))
        return self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
            canvasmaker=PageNumCanvas
        )
