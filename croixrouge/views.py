import calendar
import io
import logging
from collections import OrderedDict, defaultdict
from datetime import date, datetime, timedelta

from dal import autocomplete
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.views import PasswordChangeView as AuthPasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Prefetch, Sum
from django.db.models.deletion import ProtectedError
from django.db.models.functions import TruncMonth
from django.forms import HiddenInput, modelform_factory
from django.http import FileResponse, Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.defaults import page_not_found as django_page_not_found
from django.views.generic import CreateView, DetailView, FormView, ListView, TemplateView, UpdateView, View
from django.views.generic import DeleteView as DjangoDeleteView
from django.views.static import serve
from two_factor.views import SetupView as TwoFactSetupView

from amf.forms import EnfantAmfForm
from sof.models import PrestationSOF
from spe.models import PrestationSPE

from . import forms
from .export import OpenXMLExport
from .models import (
    CercleScolaire,
    Contact,
    Document,
    Famille,
    Formation,
    JournalAcces,
    LibellePrestation,
    Personne,
    Rapport,
    Role,
    Service,
    Utilisateur,
)
from .pdf import RapportPdf
from .pdf_fact_interne import FacturationPDF
from .utils import format_d_m_Y, format_duree, is_ajax

logger = logging.getLogger('django')


class CreateUpdateView(UpdateView):
    """Mix generic Create and Update views."""
    is_create = False

    def get_object(self):
        return None if self.is_create else super().get_object()


class DeleteView(DjangoDeleteView):
    """
    Nous ne suivons pas la méthode Django d'afficher une page de confirmation
    avant de supprimer un objet, mais nous avertissons avec un message JS avant
    de POSTer directement la suppression. Pour cela, nous autorisons uniquement
    la méthode POST.
    """
    http_method_names = ['post']


class JournalAccesMixin:
    """
    Classe Mixin pour journaliser les accès aux familles.
    """
    def get(self, *args, **kwargs):
        acces_ordinaire = self.famille.access_ok(self.request.user)
        if not acces_ordinaire and not self.request.GET.get('confirm') == '1':
            return render(
                self.request, 'croixrouge/acces_famille.html',
                {'url': self.request.path, 'famille': self.famille}
            )
        JournalAcces.objects.create(
            famille=self.famille,
            utilisateur=self.request.user,
            ordinaire=acces_ordinaire
        )
        return super().get(*args, **kwargs)


class FilterFormMixin:
    filter_form_class = None

    def get(self, request, *args, **kwargs):
        self.filter_form = self.filter_form_class(data=request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self, base_qs):
        if self.filter_form.is_bound and self.filter_form.is_valid():
            base_qs = self.filter_form.filter(base_qs)
        return base_qs

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs, filter_form=self.filter_form)


class BasePDFView(View):
    obj_class = None
    pdf_class = None
    produce_kwargs = {}

    def get_object(self):
        return get_object_or_404(self.obj_class, pk=self.kwargs[getattr(self, 'pk_url_kwarg', 'pk')])

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        temp = io.BytesIO()
        pdf = self.pdf_class(temp, instance, **self.produce_kwargs)
        pdf.produce()
        filename = pdf.get_filename()
        temp.seek(0)
        return FileResponse(temp, as_attachment=True, filename=filename)


class ServeMedia(View):
    def get(self, request, *args, **kwargs):
        if kwargs["path"].startswith("signatures"):
            raise Http404()
        return serve(request, kwargs["path"], document_root=settings.MEDIA_ROOT, show_indexes=False)


class HomeView(TemplateView):
    template_name = 'index.html'


class SetupView(TwoFactSetupView):
    def get(self, request, *args, **kwargs):
        # The original view is short-circuiting to complete is a device exists.
        # We want to allow adding a second device if needed.
        return super(TwoFactSetupView, self).get(request, *args, **kwargs)


class PasswordChangeView(AuthPasswordChangeView):
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Votre mot de passe a bien été modifié.")
        return response


class ContactCreateView(CreateView):
    template_name = 'croixrouge/contact_edit.html'
    model = Contact
    form_class = forms.ContactForm
    success_url = reverse_lazy('contact-list')
    action = 'Création'

    def form_valid(self, form):
        contact = form.save()
        for_pers = self.request.GET.get('forpers')
        if for_pers:
            pers = get_object_or_404(Personne, pk=for_pers)
            pers.reseaux.add(contact)
            return HttpResponseRedirect(reverse('personne-reseau-list', args=[pers.pk]))
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            "role_form": forms.RoleForm(),
            "service_form": forms.ServiceForm(),
        })
        return context


class ContactListView(FilterFormMixin, ListView):
    template_name = 'croixrouge/contact_list.html'
    model = Contact
    paginate_by = 20
    filter_form_class = forms.ContactFilterForm

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Contact.objects.filter(est_actif=True).exclude(
                utilisateur__isnull=False
            ).prefetch_related("roles")
        )


class ContactUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = 'croixrouge.change_contact'
    template_name = 'croixrouge/contact_edit.html'
    model = Contact
    form_class = forms.ContactForm
    success_url = reverse_lazy('contact-list')
    success_message = 'Le contact %(nom)s %(prenom)s a bien été modifié'
    action = 'Modification'

    def delete_url(self):
        return reverse('contact-delete', args=[self.object.pk])


class ContactDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'croixrouge.delete_contact'
    model = Contact
    success_url = reverse_lazy('contact-list')

    def form_valid(self, form):
        self.object.est_actif = False
        self.object.save()
        messages.success(self.request, "Le contact %s a bien été archivé." % self.object)
        return HttpResponseRedirect(self.success_url)


class ContactAutocompleteView(autocomplete.Select2QuerySetView):
    ope = False

    def get_queryset(self):
        qs = Contact.membres_ope() if self.ope else Contact.objects.filter(est_actif=True)
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class ContactExterneAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Contact.objects.\
            filter(est_actif=True).\
            exclude(service__sigle__startswith='OPE').\
            exclude(service__sigle='CRNE')
        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class ContactTestDoublon(View):
    def post(self, request, *args, **kwargs):
        nom = request.POST.get('nom')
        prenom = request.POST.get('prenom')
        contacts = Contact.objects.filter(nom=nom, prenom=prenom)
        data = ''
        if contacts.exists():
            data = [{'nom': c.nom, 'prenom': c.prenom} for c in contacts]
        return JsonResponse(data, safe=False)


class ServiceCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'croixrouge.add_service'
    template_name = 'croixrouge/service_edit.html'
    model = Service
    form_class = forms.ServiceForm
    success_url = reverse_lazy('service-list')
    action = 'Création'

    def form_valid(self, form):
        if is_ajax(self.request):
            service = form.save()
            return JsonResponse({'pk': service.pk, 'sigle': service.sigle})
        return super().form_valid(form)

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': form.errors.as_text()})
        return super().form_invalid(form)


class ServiceListView(ListView):
    template_name = 'croixrouge/service_list.html'
    model = Service

    def get_queryset(self):
        return Service.objects.exclude(sigle='FAMILLE')


class ServiceUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'croixrouge.change_service'
    template_name = 'croixrouge/service_edit.html'
    model = Service
    form_class = forms.ServiceForm
    success_url = reverse_lazy('service-list')
    action = 'Modification'

    def delete_url(self):
        return reverse('service-delete', args=[self.object.pk])


class ServiceDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'croixrouge.delete_service'
    model = Service
    success_url = reverse_lazy('service-list')


class FormationView(SuccessMessageMixin, UpdateView):
    template_name = 'croixrouge/formation_edit.html'
    model = Formation
    form_class = forms.FormationForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        personne = get_object_or_404(Personne, pk=self.kwargs['pk'])
        self.famille = Famille.get_from_request(self.request, pk=personne.famille_id)
        return personne.formation

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'readonly': not self.famille.can_edit(self.request.user)
        }

    def get_success_url(self):
        return self.famille.edit_url

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille}


class PersonneBaseMixin:
    template_name = 'croixrouge/personne_edit.html'
    model = Personne
    require_edit = False

    @property
    def form_class(self):
        return EnfantAmfForm if self.object and self.object.est_enfant_suivi_amf else forms.PersonneForm

    def dispatch(self, request, *args, **kwargs):
        self.famille = Famille.get_from_request(request, pk=kwargs['pk'])
        if not self.famille.can_view(request.user):
            raise PermissionDenied("Vous n’avez pas la permission d’accéder à cette page.")
        elif self.require_edit and not self.famille.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas la permission d’accéder à cette page.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = {**super().get_context_data(**kwargs), 'famille': self.famille}
        context["est_enfant_suivi_amf"] = self.object.est_enfant_suivi_amf if self.object else False
        if self.object and self.object.role.nom == 'Enfant suivi':
            context['enfant'] = self.object
        return context

    def get_success_url(self):
        return self.famille.edit_url


class PersonneCreateView(PersonneBaseMixin, CreateView):
    action = 'Membre de la famille '
    require_edit = True

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.famille
        kwargs['role'] = self.request.GET.get('role', None)
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.object.famille.archived_at:
            self.object.famille.archived_at = None
            self.object.famille.save()
            messages.info(self.request, "La famille a été sortie des archives")
        return response

    def get_success_url(self):
        return self.famille.redirect_after_personne_creation(self.object)


class PersonneUpdateView(SuccessMessageMixin, PersonneBaseMixin, UpdateView):
    pk_url_kwarg = 'obj_pk'
    action = 'Modification'
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': not self.famille.can_edit(self.request.user)}

    def delete_url(self):
        return reverse('personne-delete', args=[self.famille.pk, self.object.pk])


class PersonneDeleteView(PersonneBaseMixin, DeleteView):
    require_edit = True
    form_class = DeleteView.form_class

    def get_object(self, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=self.kwargs['obj_pk'])
        if pers.role.nom == "Enfant suivi":
            raise PermissionDenied(
                "Un enfant suivi ne peut pas être directement supprimé. Si c’est "
                "vraiment ce que vous voulez, mettez-le d’abord comme Enfant non suivi."
            )
        return pers


class PersonneReseauView(DetailView):
    template_name = 'croixrouge/personne_reseau_list.html'
    model = Personne

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        famille = Famille.get_from_request(self.request, pk=self.object.famille_id)
        context.update({
            'famille': famille,
            'form': forms.ContactExterneAutocompleteForm(),
            'reseau': list(self.object.reseaux.all().order_by('nom')),
        })
        try:
            pediatre = self.object.suivienfant.pediatre
        except AttributeError:
            pass
        else:
            if pediatre:
                context['reseau'].append(pediatre)
        return context


class PersonneReseauAdd(View):
    def post(self, request, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=kwargs['pk'])
        obj_pk = request.POST.getlist('contacts[]')
        for obj in obj_pk:
            contact = get_object_or_404(Contact, pk=obj)
            pers.reseaux.add(contact)
        return JsonResponse({'is_valid': True})


class PersonneReseauRemove(View):
    def post(self, request, *args, **kwargs):
        pers = get_object_or_404(Personne, pk=kwargs['pk'])
        contact = get_object_or_404(Contact, pk=kwargs['obj_pk'])
        pers.reseaux.remove(contact)
        return HttpResponseRedirect(reverse('personne-reseau-list', args=[pers.pk]))


class UtilisateurListView(FilterFormMixin, ListView):
    template_name = 'croixrouge/utilisateur_list.html'
    model = Utilisateur
    is_active = True
    paginate_by = 50
    filter_form_class = forms.UtilisateurFilterForm

    def dispatch(self, request, *args, **kwargs):
        request.session['current_app'] = 'admin'
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Utilisateur.objects.filter(date_desactivation__isnull=self.is_active)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'active_users': self.is_active,
        })
        return context


class UtilisateurOwnView(UpdateView):
    """Affichage des propres données de l’utilisateur connecté."""
    template_name = "croixrouge/utilisateur.html"
    form_class = forms.SignatureForm
    user_fields = [
        'prenom', 'nom', 'username', 'sigle', 'signature', 'tel_prive', 'tel_prof',
        'email', 'equipe', 'roles', 'taux_activite', 'groups',
    ]

    def get_object(self):
        if self.request.user.username != self.kwargs['username']:
            raise PermissionDenied("Vous ne pouvez consulter que votre propre profil.")
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        user_data = [
            (f.verbose_name, value) for f in Utilisateur._meta.get_fields()
            if (
                f.name in self.user_fields and f.name not in ['roles', 'groups', 'signature'] and
                (value := getattr(user, f.name)) not in ("", None)
            )
        ]
        if val := user.roles_str():
            user_data.append(("Rôles", val))
        if val := user.groupes:
            user_data.append(("Groupes", ", ".join(val)))
        context.update({
            'user_data': user_data,
        })
        return context

    def get_success_url(self):
        return self.request.path


class UtilisateurReactivateView(PermissionRequiredMixin, View):
    permission_required = 'croixrouge.delete_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        utilisateur.is_active = True
        utilisateur.est_actif = True
        utilisateur.date_desactivation = None
        utilisateur.save()
        return HttpResponseRedirect(reverse('utilisateur-list'))


class UtilisateurCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'croixrouge.add_utilisateur'

    template_name = 'croixrouge/utilisateur_edit.html'
    model = Utilisateur
    form_class = forms.UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')
    success_message = "L’utilisateur «%(username)s» a été créé avec le mot de passe «%(password)s»."

    def form_valid(self, form):
        form.instance.first_name = form.cleaned_data['prenom']
        form.instance.last_name = form.cleaned_data['nom']
        form.instance.service, _ = Service.objects.get_or_create(sigle='CRNE', defaults={'sigle': 'CRNE'})
        pwd = get_random_string(length=10)
        form.instance.set_password(pwd)
        form.cleaned_data['password'] = pwd  # for success message
        return super().form_valid(form)


class UtilisateurUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'croixrouge.change_utilisateur'

    template_name = 'croixrouge/utilisateur_edit.html'
    model = Utilisateur
    form_class = forms.UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')

    def delete_url(self):
        return reverse('utilisateur-delete', args=[self.object.pk])


class UtilisateurPasswordReinitView(PermissionRequiredMixin, View):
    permission_required = 'croixrouge.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        pwd = get_random_string(length=10)
        utilisateur.set_password(pwd)
        utilisateur.save()
        messages.success(request, 'Le nouveau mot de passe de «%s» est «%s».' % (utilisateur, pwd))
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))


class UtilisateurOtpDeviceReinitView(PermissionRequiredMixin, View):
    permission_required = 'croixrouge.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        if utilisateur.totpdevice_set.exists():
            utilisateur.totpdevice_set.all().delete()
            messages.success(request, 'Le mobile de «%s» a été réinitialisé.' % utilisateur)
        else:
            messages.error(request, 'Aucune configuration mobile trouvée pour «%s»' % utilisateur)
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))


class UtilisateurJournalAccesView(PermissionRequiredMixin, FilterFormMixin, ListView):
    permission_required = 'croixrouge.change_utilisateur'
    template_name = 'croixrouge/utilisateur_journal.html'
    paginate_by = 50
    model = JournalAcces
    filter_form_class = forms.JournalAccesFilterForm

    def get_queryset(self):
        return super().get_queryset(
            base_qs=self.model.objects.filter(utilisateur_id=self.kwargs["pk"]).order_by("-quand")
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'utilisateur': get_object_or_404(Utilisateur, pk=self.kwargs['pk']),
        }


class UtilisateurDeleteView(PermissionRequiredMixin, DeleteView):
    """Archive, ne supprime pas réellement."""
    permission_required = 'croixrouge.change_utilisateur'

    model = Utilisateur
    success_url = reverse_lazy('utilisateur-list')

    def form_valid(self, form):
        self.object.is_active = False  # C'est ce flag qui empêche la connexion au système
        self.object.est_actif = False
        self.object.date_desactivation = date.today()
        self.object.save()
        return HttpResponseRedirect(self.success_url)


class UtilisateurAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Utilisateur.objects.filter(service__sigle='CRNE', date_desactivation__isnull=True)

        if self.q:
            qs = qs.filter(nom__istartswith=self.q)
        return qs


class CercleScolaireListView(ListView):
    template_name = 'croixrouge/cercle_scolaire_list.html'
    model = CercleScolaire


class CercleScolaireUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'croixrouge.change_cerclescolaire'
    template_name = 'croixrouge/cercle_scolaire_edit.html'
    model = CercleScolaire
    fields = '__all__'
    success_url = reverse_lazy('cercle-list')

    def delete_url(self):
        return reverse('cercle-delete', args=[self.object.pk])


class CercleScolaireCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'croixrouge.add_cerclescolaire'
    template_name = 'croixrouge/cercle_scolaire_edit.html'
    model = CercleScolaire
    fields = '__all__'
    success_url = reverse_lazy('cercle-list')


class CercleScolaireDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'croixrouge.delete_cerclescolaire'
    model = CercleScolaire
    success_url = reverse_lazy('cercle-list')


class RoleListView(ListView):
    template_name = 'croixrouge/role_list.html'
    model = Role

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'editeur_help': self.model._meta.get_field('editeur').help_text,
        }


class RoleUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'croixrouge.change_role'
    template_name = 'croixrouge/role_edit.html'
    model = Role
    fields = '__all__'
    success_url = reverse_lazy('role-list')

    def delete_url(self):
        if self.object.personne_set.count() == 0:
            return reverse('role-delete', args=[self.object.pk])


class RoleCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'croixrouge.add_role'
    template_name = 'croixrouge/role_edit.html'
    model = Role
    fields = '__all__'
    success_url = reverse_lazy('role-list')

    def form_valid(self, form):
        if is_ajax(self.request):
            role = form.save()
            return JsonResponse({'pk': role.pk, 'nom': role.nom})
        return super().form_valid(form)

    def form_invalid(self, form):
        if is_ajax(self.request):
            return JsonResponse({'error': form.errors.as_text()})
        return super().form_invalid(form)


class RoleDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'croixrouge.delete_role'
    model = Role
    success_url = reverse_lazy('role-list')

    def form_valid(self, form):
        try:
            return super().form_valid(form)
        except ProtectedError as e:
            # TODO: Il y a certainement mieux...
            messages.add_message(self.request, messages.ERROR, str(e), "Suppression impossible")
            return HttpResponseRedirect(reverse('role-list'))


class DocumentBaseView:
    template_name = 'croixrouge/document_upload.html'
    titre_page = ''
    titre_formulaire = ''

    def dispatch(self, request, *args, **kwargs):
        self.famille = Famille.get_from_request(self.request, pk=kwargs['pk'])
        self.titre_page = self.titre_page.format(famille=self.famille.nom)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.famille.suivi_url


class DocumentUploadView(DocumentBaseView, CreateView):
    form_class = forms.DocumentUploadForm
    titre_page = 'Documents externes de la famille {famille}'
    titre_formulaire = 'Nouveau document'

    def get_initial(self):
        initial = super().get_initial()
        initial['famille'] = self.famille
        return initial

    def form_valid(self, form):
        form.instance.famille = self.famille
        form.instance.auteur = self.request.user
        return super().form_valid(form)


class DocumentDeleteView(DeleteView):
    model = Document
    pk_url_kwarg = 'doc_pk'

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Le document a été supprimé avec succès")
        return response

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER') or self.object.famille.suivi_url



class BilanEditView(DocumentUploadView, CreateUpdateView):
    pk_url_kwarg = 'obj_pk'
    titre_page = 'Bilan pour la famille {famille}'
    titre_formulaire = 'Nouveau bilan'
    form_class = None

    def get_form_class(self):
        if self.form_class:
            return self.form_class
        return modelform_factory(
            model=self.model,
            exclude=['famille', 'auteur'],
            field_classes={
                'objectifs': forms.RichTextField,
                'rythme': forms.RichTextField,  # SPE
            },
            widgets={
                'famille': HiddenInput,
                'date': forms.PickDateWidget,
            },
            labels={'fichier': ''},
        )

    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            initial['objectifs'] = (
                "<p><b>Besoin de l’enfant</b></p><p></p>\n"
                "<p><b>Objectifs de la famille</b></p><p></p>\n"
                "<p><b>Moyens</b></p><p></p>\n"
                "<p><b>Critères</b></p><p></p>"
            )
        return initial

    def get_success_url(self):
        return reverse(f'{self.famille.default_typ}-famille-agenda', args=[self.famille.pk])


class BilanDetailView(DetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'croixrouge/bilan.html'

    def get_context_data(self, **kwargs):
        meta = self.model._meta
        data_fields = [
            f.name for f in meta.get_fields()
            if f.name not in ['id', 'date', 'famille', 'auteur', 'fichier', 'phase', 'sig_famille', 'sig_interv']
        ]
        return {
            **super().get_context_data(**kwargs),
            'famille': self.object.famille,
            'data': [(meta.get_field(f).verbose_name, getattr(self.object, f)) for f in data_fields]
        }


class BilanDeleteView(DeleteView):
    pk_url_kwarg = 'obj_pk'

    def dispatch(self, request, *args, **kwargs):
        self.famille = Famille.get_from_request(request, pk=kwargs['pk'])
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied("Vous n'avez pas les droits de supprimer ce bilan.")
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse(f'{self.famille.default_typ}-famille-agenda', args=[self.famille.pk])


class BaseRapportView:
    def dispatch(self, request, *args, **kwargs):
        self.famille = Famille.get_from_request(request, pk=kwargs['pk'])
        self.model = getattr(self.famille, f'rapports_{self.famille.default_typ}').model
        return super().dispatch(request, *args, **kwargs)


class RapportCreateView(DocumentUploadView, BaseRapportView, CreateView):
    form_class = forms.RapportEditForm
    template_name = 'croixrouge/rapport_edit.html'

    def get_form_class(self):
        return modelform_factory(self.model, form=self.form_class)

    def get_success_url(self):
        return reverse(f'{self.famille.default_typ}-famille-agenda', args=[self.famille.pk])


class RapportDisplayView(BaseRapportView, DetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'croixrouge/rapport.html'

    def get_context_data(self, **kwargs):
        meta = self.model._meta
        removed_fields = ['evolutions', 'evaluation'] if self.object.is_observations_enable() else ['observations']
        fields = [
            f for f in [
                'situation', 'evolutions', 'evaluation', 'observations', 'observ_ipe',
                'observ_sof', 'observ_apa', 'projet'
            ] if f not in removed_fields
        ]
        return {
            **super().get_context_data(**kwargs),
            'famille': self.object.famille,
            'source': self.object.famille.default_typ,
            'rapport': self.object,
            'intervenants': ', '.join([i.nom_prenom for i in self.object.intervenants()]),
            'enfants': '\n'.join(
                [f"{enfant.nom_prenom} (*{format_d_m_Y(enfant.date_naissance)})"
                 for enfant in self.object.famille.membres_suivis()]
            ),
            'data': [
                (meta.get_field(f).verbose_name, getattr(self.object, f)) for f in fields
                if getattr(self.object, f) or f in ['situation', 'projet']
            ]
        }


class RapportUpdateView(DocumentUploadView, BaseRapportView, CreateUpdateView):
    pk_url_kwarg = 'obj_pk'
    form_class = forms.RapportEditForm
    template_name = 'croixrouge/rapport_edit.html'

    def get_form_class(self):
        return modelform_factory(self.model, form=self.form_class)

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'is_obs_enable': self.object.is_observations_enable(),
            'user': self.request.user,
        }

    def get_success_url(self):
        return reverse(f'{self.famille.default_typ}-famille-agenda', args=[self.famille.pk])


class RapportDeleteView(BaseRapportView, DeleteView):
    model = Rapport
    pk_url_kwarg = 'obj_pk'

    def form_valid(self, form):
        if not self.object.can_edit(self.request.user):
            raise PermissionDenied("Vous n'avez pas le droit de supprimer ce résumé.")
        return super().form_valid(form)

    def get_success_url(self):
        return reverse(f'{self.famille.default_typ}-famille-agenda', args=[self.famille.pk])


class RapportPDFView(BaseRapportView, BasePDFView):
    pdf_class = RapportPdf
    pk_url_kwarg = 'obj_pk'

    @property
    def obj_class(self):
        return self.model


class BaseSuiviView(SuccessMessageMixin, UpdateView):
    model = None
    famille_model = None
    form_class = None
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, famille__pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'famille': self.object.famille,
        }

    def get_success_url(self):
        return self.object.famille.suivi_url


class FamilleAccessRequiredMixin:
    famille_class = None
    MSG_READ_ONLY = "Vous n'avez pas les droits nécessaires pour modifier cette page"
    MSG_ACCESS_DENIED = "Vous n’avez pas la permission d’accéder à cette page."

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(self.famille_class, pk=kwargs['pk'])
        self.check_access(request)
        return super().dispatch(request, *args, **kwargs)

    def check_access(self, request):
        if not self.famille.can_view(request.user):
            raise PermissionDenied(self.MSG_ACCESS_DENIED)
        self.readonly = not self.famille.can_edit(request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.readonly:
            messages.info(self.request, self.MSG_READ_ONLY)
        context['can_edit'] = not self.readonly
        context['famille'] = self.famille
        return context


class FamilleUpdateViewBase(UpdateView):
    context_object_name = 'famille'
    title = 'Modification'
    archive_url = None

    def get_success_url(self):
        return self.success_url or reverse(f'{self.object.default_typ}-famille-edit', args=[self.object.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'enfant_suivi': Role.objects.get(nom='Enfant suivi'),
            'enfant_non_suivi': Role.objects.get(nom='Enfant non-suivi'),
        })
        return context


class FamilleAdresseChangeView(UpdateView):
    template_name = 'croixrouge/famille_adresse.html'
    form_class = forms.FamilleAdresseForm
    context_object_name = 'famille'

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas la permission d’accéder à cette page.")
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        return Famille.get_from_request(self.request, pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['famille'] = self.get_object()
        return kwargs

    def form_valid(self, form):
        famille = form.save()
        for membre_id in form.cleaned_data['membres']:
            membre = get_object_or_404(Personne, pk=membre_id)
            membre.rue = famille.rue
            membre.npa = famille.npa
            membre.localite = famille.localite
            membre.save()
        return HttpResponseRedirect(famille.edit_url)


class FamilleAutoCompleteView(autocomplete.Select2QuerySetView):
    typ = None

    def get_queryset(self):
        qs = Famille.objects.filter(archived_at__isnull=True)
        if self.typ is not None:
            qs = qs.filter(typ__contains=[self.typ])
        if self.q:
            qs = qs.filter(nom__icontains=self.q)
        return qs.prefetch_related('membres')

    def get_result_label(self, result):
        enfants = ", ".join(enf.prenom for enf in result.membres_suivis())
        label = f'{result.nom}, {result.npa} {result.localite}'
        if enfants:
            label += f" ({enfants})"
        return label


class PermissionOverview(TemplateView):
    template_name = 'croixrouge/permissions.html'
    perm_map = {'add': 'création', 'change': 'modification', 'delete': 'suppression', 'view': 'affichage'}

    def get_context_data(self, **kwargs):
        from spe.models import SuiviSPE

        def verbose(perm):
            return self.perm_map.get(perm.codename.split('_')[0], perm.codename)

        context = super().get_context_data(**kwargs)
        groups = Group.objects.all().prefetch_related('permissions')
        grp_perms = {gr.name: [perm.codename for perm in gr.permissions.all()] for gr in groups}
        objects = [CercleScolaire, Contact, Role, Service, Utilisateur]
        all_perms = Permission.objects.filter(
            content_type__app_label__in=['croixrouge', 'spe'],
            content_type__model__in=[o.__name__.lower() for o in objects]
        )

        def perms_for_model(model):
            return [
                (perm.codename, verbose(perm)) for perm in all_perms
                if perm.content_type.model == model.__name__.lower()
            ]

        perm_groups = OrderedDict()
        # {'Contact': [('view_contact', 'affichage), ('change_contact', 'modification'), ...]}
        for obj in objects:
            perm_groups[obj._meta.verbose_name] = perms_for_model(obj)
        perm_groups['SPE'] = list(SuiviSPE._meta.permissions)
        perm_groups['SPE'].extend([('delete_famillespe', 'Supprimer une famille')])
        context.update({
            'groups': groups,
            'grp_perms': grp_perms,
            'perms_by_categ': perm_groups,
        })
        return context


class MedlinkImportView(PermissionRequiredMixin, FormView):
    permission_required = 'croixrouge.export_stats'
    template_name = 'spe/medlink_upload.html'
    form_class = forms.MedlinkImportForm

    def form_valid(self, form):
        maintenant = timezone.now()
        for model, prest_list in [
            (PrestationSPE, form.prestations["spe"]),
            (PrestationSOF, form.prestations["sof"]),
        ]:
            for prestation in prest_list:
                p = model.objects.create(
                    auteur=self.request.user,
                    date_prestation=prestation['date_prestation'],
                    duree=prestation['duree'],
                    famille=prestation['famille'],
                    texte=prestation['texte'],
                    lib_prestation=prestation['lib_prestation'],
                    medlink_date=maintenant,
                )
                p.intervenants.add(prestation['intervenant'])
        messages.success(self.request, "L’importation s'est déroulée avec succès.")
        return HttpResponseRedirect(reverse('medlink-historique'))


class MedlinkHistoriqueView(ListView):
    template_name = 'spe/medlink_history.html'
    model = PrestationSPE

    def get_queryset(self):
        return self.model.objects.filter(
            medlink_date__date__gt=date.today() - timedelta(days=730)
        ).values('medlink_date').order_by('-medlink_date').distinct()


class MedlinkDetailView(TemplateView):
    template_name = 'spe/medlink_detail.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            timestamp = int(request.GET.get('ts', None))
        except TypeError:
            raise Http404
        self.dt = datetime.fromtimestamp(timestamp)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self, model):
        return model.objects.filter(
            medlink_date__date=self.dt.date(),
            medlink_date__hour=self.dt.hour,
            medlink_date__minute=self.dt.minute
        ).order_by('date_prestation').prefetch_related('intervenants')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            "date": self.dt,
            "spe": {
                "object_list": self.get_queryset(model=PrestationSPE),
            },
            "sof": {
                "object_list": self.get_queryset(model=PrestationSOF),
            },
        })
        context["spe"]["total"] = context["spe"]["object_list"].aggregate(Sum('duree'))['duree__sum']
        context["sof"]["total"] = context["sof"]["object_list"].aggregate(Sum('duree'))['duree__sum']
        return context


class FacturationInterneView(PermissionRequiredMixin, TemplateView):
    permission_required = 'croixrouge.export_stats'
    template_name = 'croixrouge/facturation.html'

    def dispatch(self, request, *args, **kwargs):
        last_month = date.today().replace(day=1) - timedelta(days=2)
        default_data = {'mois': str(last_month.month), 'annee': str(last_month.year)}
        self.form = forms.MonthSelectionForm(data=request.GET or default_data)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        return context


class FacturationBase:
    typ_role_map = {
        "ipe-spe": {"nom": "IPE", "pour": "SPE", "role": "IPE", "tarif": 150},
        "sof-spe": {"nom": "SOF", "pour": "SPE", "role": "Assistant-e social-e", "tarif": 150},
        "apa-spe": {"nom": "APA", "pour": "SPE", "role": "Coach APA", "tarif": 80},
        "ase-spe": {"nom": "ASE", "pour": "SPE", "role": "ASE", "prestations": ["spe03", "spe04"], "tarif": 80},
        "ase-sof": {"nom": "ASE", "pour": "SOF", "role": "ASE", "prestations": ["sof03", "sof04"], "tarif": 80},
    }

    def dispatch(self, *args, **kwargs):
        self.mois = date(self.kwargs['annee'], self.kwargs['mois'], 1)
        if self.kwargs['typ'] not in self.typ_role_map:
            raise Http404("Type inconnu")
        self.typ = self.typ_role_map[self.kwargs['typ']]
        return super().dispatch(*args, **kwargs)

    def prestations(self):
        filters = {'mois': self.mois, 'intervenants__roles__nom': self.typ['role'], 'duree__gt': timedelta(0)}
        if self.typ.get('prestations'):
            filters['lib_prestation__code__in'] = self.typ['prestations']
        totals = defaultdict(timedelta)
        PrestationModel = {"SPE": PrestationSPE, "SOF": PrestationSOF}.get(self.typ["pour"])
        prestations = PrestationModel.objects.annotate(
            mois=TruncMonth('date_prestation')
        ).filter(
            **filters
        ).order_by('date_prestation', 'famille__nom').distinct().prefetch_related(
            Prefetch('intervenants', queryset=Utilisateur.objects.prefetch_related('roles'))
        )
        lines = []
        for prest in prestations:
            for interv in prest.intervenants.all():
                if not interv.has_role([self.typ['role']]):
                    continue
                lines.append((interv, prest))
                totals[interv] += prest.duree
                totals['total'] += prest.duree
        lines.sort(key=lambda line: line[0].nom_prenom)
        return lines, totals


class FacturationPDFView(PermissionRequiredMixin, FacturationBase, BasePDFView):
    permission_required = 'croixrouge.export_stats'
    pdf_class = FacturationPDF

    def get_object(self):
        pass

    @property
    def produce_kwargs(self):
        return {
            'typ': self.typ,
            'mois': self.mois,
            'prestations': self.prestations(),
        }


class FacturationDetailsView(PermissionRequiredMixin, FacturationBase, TemplateView):
    permission_required = 'croixrouge.export_stats'
    template_name = 'croixrouge/facturation_details.html'

    def get_queryset(self):
        return self.prestations()

    def get_context_data(self, **kwargs):
        lines, totals = self.prestations()
        prev_interv = None
        table_lines = []
        for interv, prest in lines:
            if prev_interv != interv and prev_interv is not None:
                table_lines.append(['', '', 'Sous-total', format_duree(totals[prev_interv])])
                table_lines.append(['', '', '', ''])
            table_lines.append([
                str(interv) if prev_interv != interv else '',
                prest.date_prestation.strftime('%d.%m.%Y'),
                prest.famille.nom if prest.famille else 'Prestation générale',
                format_duree(prest.duree)
            ])
            prev_interv = interv
        table_lines.append(['', '', 'Sous-total', format_duree(totals[prev_interv])])

        return {
            **super().get_context_data(**kwargs),
            'service': self.typ['nom'],
            'periode': self.mois,
            'lines': table_lines,
        }

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('export') == '1':
            context = self.get_context_data()
            export = OpenXMLExport(sheet_title=f'Facturation interne {context["service"]} - détails')
            export.write_line(["Période:", self.mois.strftime('%m.%Y')])
            export.write_line([""])
            for line in context['lines']:
                export.write_line(line, col_widths=[20, 13, 24, 8])
            fname = f"facturation_interne_{self.typ['nom'].lower()}_{self.mois.year}{self.mois.month:02}.xlsx"
            return export.get_http_response(fname)
        return super().render_to_response(context, **response_kwargs)


class BasePrestationGeneraleEtPersonnelle(ListView):

    def dispatch(self, request, *args, **kwargs):
        str_curdate = request.GET.get('date', None)
        if str_curdate:
            cur_year = int(str_curdate[-4:])
            cur_month = int(str_curdate[:2])
        else:
            today = date.today()
            cur_year = today.year
            cur_month = today.month
        self.dfrom = date(cur_year, cur_month, 1)
        self.dto = self.dfrom + timedelta(days=-1 + calendar.monthrange(self.dfrom.year, self.dfrom.month)[1])
        self.unite = self.kwargs['unite']
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prev_month = self.dfrom - timedelta(days=2)
        next_month = self.dfrom + timedelta(days=33)

        context.update({
            'current_date': self.dfrom,
            'prev_month': prev_month if prev_month.year > 2019 else None,
            'next_month': next_month if next_month.month <= date.today().month else None,
        })
        return context


class PrestationPersonnelleListView(BasePrestationGeneraleEtPersonnelle):
    template_name = 'croixrouge/prestation_personnelle.html'

    def get_queryset(self):
        return getattr(self.request.user, f'prestations_{self.unite}').filter(
            date_prestation__gte=self.dfrom, date_prestation__lte=self.dto
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'source': self.unite,
            'totaux': [(pp.code, self.get_queryset().filter(
                    lib_prestation__code=f"{pp.code}"
                ).aggregate(
                    tot=Sum('duree')
                )['tot']) for pp in LibellePrestation.objects.filter(unite=self.unite)],
            'total_final': self.get_queryset().aggregate(tot=Sum('duree'))['tot'] if self.unite else 0,
        })
        return context


class PrestationGeneraleListView(BasePrestationGeneraleEtPersonnelle):
    template_name = 'croixrouge/prestation_generale.html'

    def get_queryset(self):
        self.model = getattr(self.request.user, f'prestations_{self.unite}').model
        return super().get_queryset().filter(
            famille=None,
            date_prestation__gte=self.dfrom,
            date_prestation__lte=self.dto
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'source': self.unite,
            'prestations': self.get_queryset(),
        })
        return context


class FamilleReactivationView(View):

    def post(self, request, *args, **kwargs):
        famille = Famille.get_from_request(request, pk=self.kwargs['pk'])
        if famille.can_be_reactivated(request.user):
            famille.suivi.date_fin_suivi = None
            famille.suivi.motif_fin_suivi = ""
            famille.suivi.save()
            famille.destination = ''
            famille.save()
        return HttpResponseRedirect(reverse(f'{famille.default_typ}-famille-list'))


class FamilleJournalAccesView(PermissionRequiredMixin, FilterFormMixin, ListView):
    permission_required = 'croixrouge.change_utilisateur'
    template_name = 'croixrouge/famille_journalacces.html'
    paginate_by = 50
    model = JournalAcces
    filter_form_class = forms.JournalAccesFilterForm

    def get_queryset(self):
        return super().get_queryset(
            base_qs=self.model.objects.filter(famille_id=self.kwargs["pk"]).order_by("-quand")
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'famille': get_object_or_404(Famille, pk=self.kwargs['pk']),
        }


def page_not_found(request, *args, **kwargs):
    if not request.user.is_authenticated:
        kwargs['template_name'] = '404-public.html'
    return django_page_not_found(request, *args, **kwargs)
