import csv
import random
import string
import unicodedata
from io import StringIO

import requests
from django.conf import settings
from django.core.mail import EmailMessage, mail_admins
from django.utils.dateformat import format as django_format
from django.utils.html import Urlizer
from openpyxl import load_workbook

ANTICIPATION_POUR_DEBUT_SUIVI = 90


def format_d_m_Y(date):
    return django_format(date, 'd.m.Y') if date else ''


def format_d_m_Y_HM(date):
    return django_format(date, 'l d.m.Y - G:i') if date else ''


def format_duree(duree, centiemes=False):
    if duree is None:
        return '00:00'
    elif isinstance(duree, str):
        return duree
    secondes = duree.total_seconds()
    heures = secondes // 3600
    if centiemes:
        # Arrondi, cf. #455
        minutes = round((secondes % 3600) / 36)
        return '{:02}.{:02}'.format(int(heures), minutes)
    else:
        minutes = (secondes % 3600) // 60
        return '{:02}:{:02}'.format(int(heures), int(minutes))


def format_Ymd(date):
    return django_format(date, 'Ymd') if date else ''


def format_adresse(rue, npa, localite):
    """ Formate the complete adress """

    if rue and npa and localite:
        return f"{rue}, {npa} {localite}"
    if npa and localite:
        return f"{npa} {localite}"
    return f"{localite}"


def format_contact(telephone, email):
    """ Formate the contact data (phone and email) """
    return '{} {} {}'.format(telephone, '-' if telephone != '' and email != '' else '', email)


def unaccent(text):
    if isinstance(text, list):
        text = ' '.join(text)
    text = text.replace('-', ' ')
    text = text.lower()
    t = unicodedata.normalize('NFD', text).encode('ascii', 'ignore')
    return t.decode('utf-8')


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') in ['XMLHttpRequest', 'Fetch']


def format_nom_prenom(nom):
    if nom is None or nom == '':
        return ''
    if nom.startswith('de ') and len(nom) > 3:
        return f"de {nom[3].upper()}{nom[4:]}"
    return f"{nom[0].upper()}{nom[1:]}"


def is_valid_for_sms(phone):
    return phone and phone[:2] == '07'


class SMSUrlizer(Urlizer):
    """
    Replace links with short links: "<-short->" -> take 19 chars
    See https://doc.smsup.ch/en/api/sms/send/short-url
    """

    url_template = '<-short->'

    def __init__(self):
        self.links = []

    def trim_url(self, url, **kwargs):
        self.links.append(url)
        return super().trim_url(url, **kwargs)


def send_sms(text, to, err):
    # Try to remain below 160 chars
    # https://pypi.org/project/smsutil/ if we want to check against GSM-7
    url = settings.SMSUP_SEND_URL
    urlizer = SMSUrlizer()
    if 'https://' in text:
        text = urlizer(text)

    headers = {
        "Authorization": f"Bearer {settings.SMSUP_API_TOKEN}",
        "Accept": "application/json",
    }
    if urlizer.links:
        # Presence of links requires usage of the POST API.
        params = {
            'sms': {
                'message': {
                    'text': text,
                    'pushtype': 'alert',
                    'sender': 'CroixRouge',  # Max 11 chars
                    'links': urlizer.links,
                },
                'recipients': {
                    'gsm': [{'gsmsmsid': '', 'value': to.replace(' ', '')}],
                },
            },
        }
        response = requests.post(url, json=params, headers=headers)
    else:
        params = {
            'text': text,
            'to': to.replace(' ', ''),
            'sender': 'CroixRouge',  # Max 11 chars
        }
        response = requests.get(url, params=params, headers=headers)

    result = response.json()
    if not result or result.get('message') != 'OK':
        mail_admins(
            "[Croix-Rouge] Erreur SMS",
            f"{err}\n\nParams: {params}\n\n{result}",
        )
        return False
    return True


def send_email(subject, message, to):
    email = EmailMessage(
        subject,
        message,
        to=[to],
        reply_to=['contact@croix-rouge-ne.ch'],
    )
    try:
        email.send()
        return True
    except OSError:
        return False


CONTINENTS = {
    'NA': [
        'AI', 'AG', 'AW', 'BS', 'BB', 'BQ', 'BZ', 'BM', 'CA', 'KY', 'CR', 'CU',
        'CW', 'DM', 'DO', 'SV', 'GL', 'GD', 'GP', 'GT', 'HT', 'HN', 'JM', 'MQ',
        'MX', 'PM', 'MS', 'CW', 'KN', 'NI', 'PA', 'PR', 'KN', 'LC', 'PM',
        'SX', 'TT', 'TC', 'VI', 'US', 'VC', 'VG',
    ],
    'SA': [
        'AR', 'BO', 'BR', 'CL', 'CO', 'EC', 'FK', 'GF', 'GY', 'PY', 'PE', 'SR',
        'UY', 'VE',
    ],
    'EU': [
        'AL', 'AD', 'AT', 'BY', 'BE', 'BA', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE',
        'FO', 'FI', 'FR', 'DE', 'GI', 'GR', 'HU', 'IS', 'IE', 'IT', 'LV', 'LI',
        'LT', 'LU', 'MK', 'MT', 'MD', 'MC', 'NL', 'NO', 'PL', 'PT', 'RO', 'RU',
        'SM', 'RS', 'SK', 'SI', 'ES', 'SE', 'CH', 'UA', 'GB', 'VA', 'RS', 'IM',
        'RS', 'ME',
    ],
    'AF': [
        'AO', 'DZ', 'BJ', 'BW', 'BF', 'BI', 'CD', 'CI', 'CM', 'CV', 'CF', 'KM',
        'CG', 'DJ', 'EG', 'GQ', 'ER', 'ET', 'GA', 'GH', 'GM', 'GW', 'GN', 'KE',
        'LS', 'LR', 'LY', 'MG', 'MW', 'ML', 'MR', 'MU', 'YT', 'MA', 'MZ', 'NA',
        'NE', 'NG', 'ST', 'RE', 'RW', 'SH', 'ST', 'SN', 'SC', 'SL', 'SO', 'SH',
        'SD', 'SZ', 'TD', 'TG', 'TN', 'TZ', 'UG', 'ZM', 'TZ', 'ZW', 'SS', 'ZA',
    ],
    'AS': [
        'AF', 'AM', 'AZ', 'BH', 'BD', 'BT', 'BN', 'KH', 'CN', 'CX', 'CC', 'IO',
        'GE', 'HK', 'IN', 'ID', 'IR', 'IQ', 'IL', 'JP', 'JO', 'KZ', 'KP', 'KR',
        'KW', 'KG', 'LA', 'LB', 'MO', 'MY', 'MV', 'MN', 'MM', 'NP', 'OM', 'PK',
        'PH', 'QA', 'SA', 'SG', 'LK', 'SY', 'TW', 'TJ', 'TH', 'TR', 'TM', 'AE',
        'UZ', 'VN', 'YE', 'PS',
    ],
    'OC': [
        'AS', 'AU', 'NZ', 'CK', 'FJ', 'PF', 'GU', 'KI', 'MP', 'MH', 'FM', 'UM',
        'NR', 'NC', 'NZ', 'NU', 'NF', 'PW', 'PG', 'MP', 'SB', 'TK', 'TO', 'TV',
        'VU', 'UM', 'WF', 'WS', 'TL',
    ],
    'AN': ['AQ'],
}


def continent_from_country_code(code):
    for cont_code, country_codes in CONTINENTS.items():
        if code in country_codes:
            return cont_code


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class XLSXFile:
    def __init__(self, filename):
        self.wb = load_workbook(filename=filename)
        self.ws = self.wb.active

    @property
    def lines(self):
        for row in self.ws:
            yield [cell.value for cell in row]

    def close(self):
        self.wb.close()


class CSVFile:
    def __init__(self, filename):
        content = StringIO(filename.read().decode('latin-1'))
        self.csv = csv.reader(content, delimiter=';')

    @property
    def lines(self):
        for row in self.csv:
            yield row

    def close(self):
        pass  # self.fh.close()
