from django.apps import apps
from django.urls import include, path

from croixrouge import views

urlpatterns = [
    path('contact/add/', views.ContactCreateView.as_view(), name='contact-add'),
    path('contact/list/', views.ContactListView.as_view(), name='contact-list'),
    path('contact/<int:pk>/edit/', views.ContactUpdateView.as_view(), name='contact-edit'),
    path('contact/<int:pk>/delete/', views.ContactDeleteView.as_view(), name='contact-delete'),
    path('contact/dal/', views.ContactAutocompleteView.as_view(), name='contact-autocomplete'),
    path('contact/dal-ope/', views.ContactAutocompleteView.as_view(ope=True),
         name='contact-ope-autocomplete'),
    path('contact/dal-externe/', views.ContactExterneAutocompleteView.as_view(),
         name='contact-externe-autocomplete'),
    path('contact/test/doublon/', views.ContactTestDoublon.as_view(), name='contact-doublon'),

    path('service/add/', views.ServiceCreateView.as_view(), name='service-add'),
    path('service/list/', views.ServiceListView.as_view(), name='service-list'),
    path('service/<int:pk>/edit/', views.ServiceUpdateView.as_view(), name='service-edit'),
    path('service/<int:pk>/delete/', views.ServiceDeleteView.as_view(), name='service-delete'),

    path('famille/<slug:typ>/dal/', views.FamilleAutoCompleteView.as_view(), name="famille-autocomplete"),

    # Personne
    path('famille/<int:pk>/personne/add/', views.PersonneCreateView.as_view(),
         name="personne-add"),
    path('famille/<int:pk>/personne/<int:obj_pk>/edit/',
         views.PersonneUpdateView.as_view(), name="personne-edit"),
    path('famille/<int:pk>/personne/<int:obj_pk>/delete/',
         views.PersonneDeleteView.as_view(), name='personne-delete'),

    path('personne/<int:pk>/formation/', views.FormationView.as_view(), name='formation'),
    path('personne/<int:pk>/contacts/', views.PersonneReseauView.as_view(),
         name='personne-reseau-list'),
    path('personne/<int:pk>/contact/add/', views.PersonneReseauAdd.as_view(),
         name='personne-reseau-add'),
    path('personne/<int:pk>/contact/<int:obj_pk>/remove/', views.PersonneReseauRemove.as_view(),
         name='personne-reseau-remove'),

    path('famille/<int:pk>/upload/', views.DocumentUploadView.as_view(),
         name='famille-doc-upload'),
    path('famille/<int:pk>/doc/<int:doc_pk>/delete/', views.DocumentDeleteView.as_view(),
         name='famille-doc-delete'),
    path('famille/<int:pk>/reactivation/', views.FamilleReactivationView.as_view(),
         name='famille-reactivation'),
    path('famille/<int:pk>/journalacces/', views.FamilleJournalAccesView.as_view(),
         name='famille-journalacces'),

    # Rapport
    path('famille/<int:pk>/rapport/add/', views.RapportCreateView.as_view(),
         name='famille-rapport-add'),
    path('famille/<int:pk>/rapport/<int:obj_pk>/', views.RapportDisplayView.as_view(),
         name='famille-rapport-view'),
    path('famille/<int:pk>/rapport/<int:obj_pk>/edit/', views.RapportUpdateView.as_view(),
         name='famille-rapport-edit'),
    path('famille/<int:pk>/rapport/<int:obj_pk>/delete/', views.RapportDeleteView.as_view(),
         name='famille-rapport-delete'),
    path('famille/<int:pk>/rapport/<int:obj_pk>/print/', views.RapportPDFView.as_view(),
         name='famille-rapport-print'),
    path('famille/<int:pk>/adresse/change/', views.FamilleAdresseChangeView.as_view(),
         name='famille-adresse-change'),

    path('utilisateur/', views.UtilisateurListView.as_view(),
         name='utilisateur-list'),
    path('utilisateur/add/', views.UtilisateurCreateView.as_view(),
         name='utilisateur-add'),
    path('utilisateur/<int:pk>/edit/', views.UtilisateurUpdateView.as_view(),
         name='utilisateur-edit'),
    path('utilisateur/<int:pk>/delete/', views.UtilisateurDeleteView.as_view(),
         name='utilisateur-delete'),
    path('utilisateur/<int:pk>/password_reinit/', views.UtilisateurPasswordReinitView.as_view(),
         name='utilisateur-password-reinit'),
    path('utilisateur/<int:pk>/otp_device/reinit/', views.UtilisateurOtpDeviceReinitView.as_view(),
         name='utilisateur-otp-device-reinit'),
    path('utilisateur/<int:pk>/journalacces/', views.UtilisateurJournalAccesView.as_view(),
         name='utilisateur-journalacces'),
    path('utilisateur/dal/', views.UtilisateurAutocompleteView.as_view(),
         name='utilisateur-autocomplete'),
    path('utilisateur/desactive/list/', views.UtilisateurListView.as_view(is_active=False),
         name='utilisateur-desactive-list'),
    path('utilisateur/<int:pk>/reactiver/', views.UtilisateurReactivateView.as_view(),
         name='utilisateur-reactiver'),
    path('utilisateur/<username>/', views.UtilisateurOwnView.as_view(),
         name='utilisateur-connecte'),

    path('<unite>/utilisateur/prestation/', views.PrestationPersonnelleListView.as_view(),
         name='prestation-personnelle'),
    path('<unite>/prestation/generale/', views.PrestationGeneraleListView.as_view(),
         name='prestation-generale'),

    path('cerclescolaire/', views.CercleScolaireListView.as_view(),
         name='cercle-list'),
    path('cerclescolaire/<int:pk>/edit/', views.CercleScolaireUpdateView.as_view(),
         name='cercle-edit'),
    path('cerclescolaire/add/', views.CercleScolaireCreateView.as_view(),
         name='cercle-add'),
    path('cerclescolaire/<int:pk>/delete/', views.CercleScolaireDeleteView.as_view(),
         name='cercle-delete'),

    path('role/', views.RoleListView.as_view(), name='role-list'),
    path('role/<int:pk>/edit/', views.RoleUpdateView.as_view(), name='role-edit'),
    path('role/add/', views.RoleCreateView.as_view(), name='role-add'),
    path('role/<int:pk>/delete/', views.RoleDeleteView.as_view(), name='role-delete'),

    path('permissions/', views.PermissionOverview.as_view(), name='permissions'),

    path('medlink/import/', views.MedlinkImportView.as_view(), name='medlink-import'),
    path('medlink/historique/', views.MedlinkHistoriqueView.as_view(), name='medlink-historique'),
    path('medlink/detail/', views.MedlinkDetailView.as_view(), name='medlink-detail'),

    path('facturation/', views.FacturationInterneView.as_view(), name='facturation-interne'),
    path('facturation/<str:typ>/<int:mois>/<int:annee>/', views.FacturationPDFView.as_view(),
        name='facturation-pdf'),
    path('facturation/<str:typ>/<int:mois>/<int:annee>/details/', views.FacturationDetailsView.as_view(),
        name='facturation-details'),
]

if apps.is_installed('debug_toolbar'):
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
