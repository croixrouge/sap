from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import Group, Permission

from .models import (
    CercleScolaire,
    Contact,
    Document,
    Famille,
    Formation,
    GroupInfo,
    LibellePrestation,
    Personne,
    Region,
    Role,
    Service,
    Utilisateur,
)


class TypePrestationFilter(admin.SimpleListFilter):
    title = 'Prest. famil./générales'
    parameter_name = 'prest'
    default_value = None

    def lookups(self, request, model_admin):
        return (
            ('fam', 'Familiales'),
            ('gen', 'Générales'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'fam':
            return queryset.filter(famille__isnull=False).order_by('famille__nom')
        elif value == 'gen':
            return queryset.filter(famille__isnull=True).order_by('-date_prestation')
        return queryset


class DocumentInline(admin.TabularInline):
    model = Document
    extra = 1


class PersonneInLine(admin.StackedInline):
    model = Personne
    exclude = ('reseaux', 'remarque')
    extra = 1


@admin.register(Personne)
class PersonneAdmin(admin.ModelAdmin):
    list_display = ('nom_prenom', 'adresse')
    search_fields = ('nom', 'prenom')


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('nom', 'prenom', 'service', 'roles_display', 'est_actif')
    list_filter = ('service', 'est_actif')
    search_fields = ('nom', 'prenom', 'service__sigle')

    def roles_display(self, contact):
        return contact.roles_str(sep=' / ')
    roles_display.short_description = 'Rôles'


@admin.register(Famille)
class FamilleAdmin(admin.ModelAdmin):
    inlines = [PersonneInLine, DocumentInline]
    search_fields = ('nom', 'localite')


@admin.register(Formation)
class FormationAdmin(admin.ModelAdmin):
    list_display = ('personne', 'statut')
    search_fields = ('personne__nom',)
    ordering = ('personne__nom',)


class UtilisateurChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Utilisateur


@admin.register(Utilisateur)
class UtilisateurAdmin(UserAdmin):
    form = UtilisateurChangeForm
    list_display = [
        'nom', 'prenom', 'sigle', 'tel_prof', 'tel_prive', 'email',
        'taux_activite', 'is_active', 'last_login'
    ]
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': (
            'sigle', 'prenom', 'nom', 'rue', 'npa', 'localite',
            'tel_prof', 'tel_prive', 'service', 'equipe', 'taux_activite',
            'decharge', 'signature', 'date_desactivation',
        )}),
    )


admin.site.unregister(Group)


class GroupInfoInline(admin.StackedInline):
    model = GroupInfo


@admin.register(Group)
class GroupAdmin(GroupAdmin):
    inlines = [GroupInfoInline]


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ['nom', 'rue', 'secteurs']


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['nom', 'famille']


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ['sigle', 'nom_complet']


@admin.register(LibellePrestation)
class LibellePrestationAdmin(admin.ModelAdmin):
    list_display = ('code', 'nom', 'unite')


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    search_fields = ['name', 'codename']


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('famille', 'titre')
    search_fields = ('famille__nom',)
    ordering = ('famille__nom',)


class RegionFilter(admin.SimpleListFilter):
    title = 'Région'
    parameter_name = 'region'

    def lookups(self, request, model_admin):
        typ = model_admin.model._meta.app_label
        return [(reg.pk, reg.nom) for reg in Region.objects.par_secteur(typ)]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(region_id=self.value())
        return queryset


class FamilleAdminBase(admin.ModelAdmin):
    list_display = ('nom', 'npa', 'localite', 'get_region')
    list_filter = (RegionFilter,)
    search_fields = ['nom']
    ordering = ('nom',)

    def get_region(self, obj):
        return obj.region.nom if obj.region else None

    get_region.short_description = 'Région'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'region':
            app_label = self.model._meta.app_label
            kwargs['queryset'] = Region.objects.par_secteur(app_label)
            kwargs['label'] = 'Centre' if app_label == 'cipe' else 'Région'
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(CercleScolaire)
