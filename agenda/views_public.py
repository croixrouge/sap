import itertools
from collections import defaultdict
from datetime import date, datetime, timedelta, timezone

import phonenumbers
from dateutil import parser
from django import forms
from django.contrib import messages
from django.core import signing
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.db import transaction
from django.db.models import Value
from django.db.models.functions import Replace
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.dateformat import format as django_format
from django.utils.timezone import localtime, now
from django.views.generic import TemplateView
from formtools.wizard.views import SessionWizardView

from croixrouge.models import Personne, Region
from croixrouge.utils import is_valid_for_sms

from .models import AnyRegion, Availability, Event, EventCategory
from .utils import lock_table, send_confirmation_email, send_confirmation_sms

MIN_APPOINTMENT_DELAY = timedelta(hours=2)
APPOINTMENT_TYPES = {
    'centre': {'nom': "Dans un centre", 'duration': 60},
    'tel': {'nom': "Par téléphone", 'duration': 30},
}


def phone_validator(value):
    try:
        num = phonenumbers.parse(value, "CH")
    except phonenumbers.NumberParseException:
        raise ValidationError("Le format du n° de téléphone ne semble pas correct")
    if not phonenumbers.is_possible_number(num):
        raise ValidationError("Saisissez un n° de téléphone suisse à 10 chiffres")


class AvailabilityTaken(Exception):
    pass


class TypeForm(forms.Form):
    type_app = forms.ChoiceField(
        choices=((code, tp['nom']) for code, tp in APPOINTMENT_TYPES.items()),
        widget=forms.RadioSelect(attrs={'class': 'immediate-submit'}),
        required=True
    )


class LocationChoiceField(forms.ModelChoiceField):
    def to_python(self, value):
        if value == 'any':
            return AnyRegion
        return super().to_python(value)


class LocationForm(forms.Form):
    location = LocationChoiceField(
        queryset=Region.objects.par_secteur("cipe").exclude(rue="").order_by("nom"),
        widget=forms.RadioSelect(attrs={'class': 'immediate-submit'}),
        required=False
    )


class DayForm(forms.Form):
    date_avail = forms.ChoiceField(
        choices=(),
        widget=forms.RadioSelect(attrs={'class': 'immediate-submit'}),
        required=False  # to enable back step
    )

    def __init__(self, *args, location=None, days=(), **kwargs):
        self.location = location
        super().__init__(*args, **kwargs)
        if days:
            self.fields['date_avail'].choices = (
                (
                    f"{day}|{centre.pk if centre else 'none'}",
                    django_format(day, 'l d.m.Y') +
                    (f" à {centre}" if ((location is None or location is AnyRegion) and centre) else '')
                )
                for day, centre in days
            )
        else:
            del self.fields['date_avail']

    def prompt_string(self):
        if self.location is AnyRegion:
            return "Choisissez une date :"
        elif self.location is None:
            return "Choisissez une date pour une consultation par téléphone :"
        else:
            return f"Choisissez une date à «{self.location}» :"

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('date_avail'):
            raise forms.ValidationError("Il est obligatoire de choisir un jour")


def choice_to_avail(value):
    start, end, dispo_pk = value.split('$')
    return Availability(parser.parse(start), parser.parse(end), Event.objects.get(pk=dispo_pk))


class AppointmentForm(forms.Form):
    avail = forms.TypedChoiceField(
        choices=(),
        widget=forms.RadioSelect(attrs={'class': 'immediate-submit'}),
        coerce=choice_to_avail,
        required=False  # to enable back step
    )

    def __init__(self, *args, location=None, avails=(), **kwargs):
        self.location = avails[0].dispo.centre if location is AnyRegion else location
        super().__init__(*args, **kwargs)
        self.fields['avail'].choices = (
            ('$'.join([str(av.start), str(av.end), str(av.dispo.pk)]), '%s - %s' % (
                django_format(localtime(av.start), 'l d.m.Y H:i'),
                django_format(localtime(av.end), 'H:i')
            ))
            for av in avails
        )

    def clean(self):
        data = super().clean()
        if not data.get('avail'):
            raise forms.ValidationError("Vous devez choisir un rendez-vous")
        return data


class DateWidget(forms.DateInput):
    input_type = 'date'


class ContactForm(forms.Form):
    template_name = "agenda/forms/contact_template.html"

    nom = forms.CharField(label="Nom de l’enfant", max_length=100, required=True)
    prenom = forms.CharField(label="Prénom de l’enfant", max_length=100, required=True)
    date_naissance = forms.DateField(
        label="Date de naissance de l’enfant", widget=DateWidget, required=True
    )
    telephone = forms.CharField(label="Téléphone", max_length=15, required=False, validators=[phone_validator])
    email = forms.EmailField(label="Courriel", max_length=100, required=False)
    raison = forms.CharField(
        label="Raison de votre demande (optionnel)", max_length=250, required=False, widget=forms.Textarea
    )

    def clean_telephone(self):
        value = self.cleaned_data.get('telephone')
        if value:
            # Standardize and re-add the initial 0
            return '0' + str(phonenumbers.parse(value, "CH").national_number)
        return value

    def clean(self):
        data = super().clean()
        if not data.get('email') and not data.get('telephone'):
            raise forms.ValidationError("Vous devez saisir une adresse de courriel ou un n° de téléphone")
        return data

    def __init__(self, *args, location=None, avail=None, **kwargs):
        self.location = location
        self.avail = avail
        super().__init__(*args, **kwargs)


class PreviousDataMissing(RuntimeError):
    pass


class AppointmentWizard(SessionWizardView):
    form_list = [
        ('type', TypeForm),
        ('location', LocationForm),
        ('day', DayForm),
        ('appointment', AppointmentForm),
        ('contact', ContactForm),
    ]
    back_labels = {
        'type': "Retour au choix du type de consultation",
        'location': "Essayer un autre lieu",
        'day': "Essayer un autre jour",
        'appointment': "Changer d’heure",
    }

    def get(self, request, *args, **kwargs):
        """
        Unlike the default method, we don't blindly reset data here (an
        accidental refresh shouldn't clear all data). We try to detect the current
        step based on stored data.
        """
        # detect the current step.
        current_step = self.steps.first
        for step in self.form_list:
            current_step = step
            data = self.get_cleaned_data_for_step(step)
            if data is None:
                break
        self.storage.current_step = current_step
        return self.render(self.get_form())

    def post(self, *args, **kwargs):
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
        if wizard_goto_step == 'day-tel':
            # Clear storage and skip to tel date choice.
            self.storage.reset()
            self.storage.set_step_data('type', {'type-type_app': ['tel']})
            self.storage.set_step_data('location', {})
            self.storage.current_step = self.steps.all[2]
            return self.render_goto_step('day')
        return super().post(*args, **kwargs)

    def process_step(self, form):
        # if choice is 'tel', ensure location data is unset
        if form.cleaned_data.get('type_app') == 'tel':
            self.storage.set_step_data('location', {})
        return super().process_step(form)

    def get_template_names(self):
        TEMPLATES = {
            'type': 'agenda/agenda-public-type.html',
            'location': 'agenda/agenda-public-location.html',
            'day': 'agenda/agenda-public-dates.html',
            'appointment': 'agenda/agenda-public-appointments.html',
            'contact': 'agenda/agenda-public-contact.html',
        }
        return [TEMPLATES[self.steps.current]]

    def get_duration(self):
        typ = self.get_cleaned_data_for_step('type')['type_app']
        return APPOINTMENT_TYPES[typ]['duration']

    def get_form(self, step=None, data=None, files=None):
        try:
            return super().get_form(step=step, data=data, files=files)
        except PreviousDataMissing:
            # Return to first step
            return super().get_form(step='type', data=data, files=files)

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step=step)
        if step not in ('type', 'location'):
            location_step_data = self.get_cleaned_data_for_step('location')
            if not location_step_data or 'location' not in location_step_data:
                # Something went wrong as previous data is missing
                raise PreviousDataMissing("No location in step data")
            region = location_step_data['location']
            kwargs['location'] = region
        if step == 'day':
            kwargs['days'] = available_days_for_region(region, self.get_duration())
            if self.get_cleaned_data_for_step('type')['type_app'] == 'tel':
                # merge identical days and remove centre hint.
                dates = sorted(list(set([tpl[0] for tpl in kwargs['days']])))
                kwargs['days'] = [(dt, None) for dt in dates]
        elif step == 'appointment':
            day_data = self.get_cleaned_data_for_step('day')
            if not day_data:
                raise PreviousDataMissing("No day in step data")
            jour, centre_pk = day_data['date_avail'].split('|')
            jour = datetime.strptime(jour, "%Y-%m-%d").date()
            if (region is None or region is AnyRegion) and centre_pk != 'none':
                region = Region.objects.get(pk=centre_pk)
                kwargs['location'] = region
            # get occurrences for region/day
            kwargs['avails'] = available_hours_for_region_day(region, jour, self.get_duration())
        elif step == 'contact':
            app_data = self.get_cleaned_data_for_step('appointment')
            if not app_data:
                raise PreviousDataMissing("No appointment data in contact data")
            avail = app_data.get('avail')
            kwargs['avail'] = avail
        return kwargs

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        if step == 'type':
            # skip 'location' step if type is 'tel'
            typ = self.get_cleaned_data_for_step('type')['type_app']
            if typ == 'tel':
                return super().get_next_step('location')
        return super().get_next_step(step)

    def get_prev_step(self, step=None):
        if step is None:
            step = self.steps.current
        if step == 'day':
            # skip 'location' step if type is 'tel'
            typ = self.get_cleaned_data_for_step('type')['type_app']
            if typ == 'tel':
                return super().get_prev_step('location')
        return super().get_prev_step(step)

    def get_step_back_label(self):
        prev = self.get_prev_step()
        return self.back_labels[prev]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # The 2 next lines can be dropped with django-formtools 2.4
        if hasattr(self, 'msg'):
            context['message'] = self.msg
        if self.steps.current != 'type':
            context['typ'] = self.get_cleaned_data_for_step('type')['type_app']
        if self.steps.current == 'contact':
            if context['typ'] != 'tel':
                context['centre'] = context['form'].avail.dispo.centre
        return context

    def render(self, form=None, **kwargs):
        if form is not None and form.is_bound and not form.is_valid():
            mail_admins("[Croix-Rouge] Erreur formulaire public", f"POST: {self.request.POST}\nErrors: {form.errors}")
        return super().render(form=form, **kwargs)

    def render_goto_step(self, goto_step, **kwargs):
        if self.steps.all.index(goto_step) > self.steps.all.index(self.steps.current):
            # Prevent going forward (browser next button)
            return super().render_goto_step(self.steps.current, **kwargs)
        # Clean previous step data when returning to it
        self.storage.set_step_data(goto_step, None)
        return super().render_goto_step(goto_step, **kwargs)

    def render_done(self, form, **kwargs):
        try:
            return super().render_done(form, **kwargs)
        except AvailabilityTaken:
            msg = (
                "Nous sommes vraiment désolé, mais le rendez-vous a été réservé "
                "pendant que vous remplissiez le formulaire. Veuillez s'il vous plaît en choisir un autre."
            )
            self.msg = msg  # Hack, can be dropped with django-formtools 2.4
            return self.render_goto_step('appointment', message=msg)

    def done(self, form_list, form_dict, **kwargs):
        avail = form_dict['appointment'].cleaned_data['avail']
        contact_data = form_dict['contact'].cleaned_data
        category = EventCategory.objects.get(
            name='rendez-vous tel' if form_dict['type'].cleaned_data['type_app'] == 'tel' else 'rendez-vous')
        famille = None
        # Try to link with an existing family, if possible
        if contact_data.get('nom') and contact_data.get('telephone'):
            personnes = Personne.objects.annotate(
                tel_raw=Replace('telephone', Value(' '), Value(''))
            ).filter(
                nom__iexact=contact_data['nom'],
                tel_raw__iexact=contact_data['telephone'].replace(' ', ''),
            )
            familles = {pers.famille for pers in personnes if 'cipe' in pers.famille.typ}
            if len(familles) == 1:
                famille = list(familles)[0]
        # Check race condition overlapping (an event may have been saved in-between)
        with lock_table(Event, 'EXCLUSIVE'):
            avails = available_hours_for_region_day(avail.dispo.centre, avail.start.date(), self.get_duration())
            if avail not in avails:
                raise AvailabilityTaken()
            event = Event.objects.create(
                last_author=None,
                centre=avail.dispo.centre,
                person=avail.dispo.person,
                famille=famille,
                start=avail.start,
                end=avail.end,
                description=contact_data.get('raison', ''),
                category=category,
                online_form_data=contact_data,
            )
        if is_valid_for_sms(contact_data['telephone']):
            transaction.on_commit(lambda: send_confirmation_sms(event, contact_data['telephone']))
        elif contact_data['email']:
            transaction.on_commit(lambda: send_confirmation_email(event))
        return JsonResponse({'result': 'OK', 'redirect_to': reverse('agenda-public-termine', args=[event.pk])})


class AppointmentDoneView(TemplateView):
    template_name = 'agenda/agenda-public-termine.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'event': get_object_or_404(Event, pk=self.kwargs['pk']),
        }


class AppointmentCancelView(TemplateView):
    template_name = 'agenda/agenda-public-annuler.html'

    def get_object(self):
        try:
            pk = signing.loads(self.kwargs['signature'])
        except signing.BadSignature:
            raise Http404
        return get_object_or_404(Event, pk=pk)

    def get_context_data(self, **kwargs):
        event = self.get_object()
        by_phone = event.category.name == 'rendez-vous tel'
        domicile = event.category.name == 'rendez-vous dom'
        tel = 'téléphonique ' if by_phone else ''
        start = django_format(localtime(event.start), 'l j F à H:i')
        centre = f' à «{event.centre.nom}»' if (event.centre and not by_phone and not domicile) else ''
        return {
            **super().get_context_data(**kwargs),
            'event': event,
            'cancel_msg': f'Voulez-vous réellement annuler le rendez-vous {tel}du {start}{centre} ?',
        }

    def post(self, request, *args, **kwargs):
        event = self.get_object()
        event_start = django_format(localtime(event.start), 'j F à H:i')
        if event.famille:
            # Also add an entry in family journal
            event.famille.prestationcipe_set.model.objects.create(
                auteur=None, date=date.today(), type_consult='hors_stats', duree=timedelta(0),
                famille=event.famille,
                faits_courants=f"Annulation du rendez-vous du {event_start} (par la personne elle-même)"
            )
        event.delete()
        messages.success(request, f"Le rendez-vous du {event_start} a bien été annulé.")
        return HttpResponseRedirect(reverse('agenda-public'))


def min_delay_for_region(region):
    # Example: hardcoded value (5 days) for Fleurier.
    # return timedelta(days=5) if (region and region.nom == "Fleurier") else MIN_APPOINTMENT_DELAY
    return MIN_APPOINTMENT_DELAY


def available_days_for_region(region, duration):
    """
    Return all possible days ((date, location) tuples) with at least one availability for the
    Region `region` during the next 31 days.
    If `region` is AnyRegion, exclude dispos with null region.
    If `region` is None, return any Event.
    """
    dispo_categ = EventCategory.objects.get(name='dispo')
    start = now()
    end = now() + timedelta(days=31)
    events = Event.objects.get_for_location(region, start, end)
    # If asking for any region, exclude dispos with region=None (tel dispos)
    dispos = [
        ev for ev in events
        if (
            ev.category == dispo_categ and
            (region is not AnyRegion or ev.centre is not None) and
            # For tel dispos, ensure dispo allows tel meetings
            ((region is None and ev.dispo_tel) or region is not None) and
            (ev.end - timedelta(minutes=60) > now() + min_delay_for_region(ev.centre))
        )
    ]
    rzvs_by_date = defaultdict(list)
    for ev in events:
        if ev.category != dispo_categ:
            rzvs_by_date[ev.start.date()].append(ev)
    days_dispo = set()
    dispo_minimal_end = start + timedelta(minutes=duration)
    for dispo in dispos:
        avails = dispo.get_availabilities(rzvs_by_date.get(dispo.start.date(), []), min_duration=duration)
        if any(avail.end >= dispo_minimal_end for avail in avails):
            days_dispo.add((dispo.start.date(), dispo.centre))
    return sorted(days_dispo, key=lambda tpl: (tpl[0], tpl[1] or Region()))


def available_hours_for_region_day(region, day, duration):
    dispo_categ = EventCategory.objects.get(name='dispo')
    if day == date.today():
        start = now() + min_delay_for_region(region)
    else:
        start = datetime.combine(day, datetime.min.time(), tzinfo=timezone.utc)
    end = datetime.combine(day, datetime.max.time())
    events = Event.objects.get_for_location(region, start, end)
    dispos = [
        ev for ev in events
        if (
            ev.category == dispo_categ and
            (region is not AnyRegion or ev.centre is not None) and
            # For tel dispos, ensure dispo allows tel meetings
            ((region is None and ev.dispo_tel) or region is not None)
        )
    ]
    rzvs = [ev for ev in events if ev.category != dispo_categ]
    availabilities = []
    for dispo in dispos:
        availabilities.extend(dispo.get_availabilities(rzvs, min_duration=duration))
    # Split availabilities by duration
    availabilities = list(itertools.chain(
        *(avail.split_by(duration) for avail in availabilities)
    ))
    if region is None:
        # Remove duplicate avails for same hour
        availabilities = dict((av.start, av) for av in availabilities).values()
    return [av for av in availabilities if av.start >= start]
