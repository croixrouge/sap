import math
from datetime import date, datetime, time, timedelta

from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import DeleteView, TemplateView, View
from workalendar.europe import Neuchatel

from cipe.models import PrestationCIPE
from croixrouge.models import Utilisateur
from croixrouge.utils import format_d_m_Y
from croixrouge.views import CreateUpdateView

from .forms import AgendaEditForm, AgendaFilterForm, SendMessageForm, make_datetime
from .models import Event, EventCategory, Occurrence, Region
from .utils import send_confirmation_sms

HOLIDAYS_TRANS = {
    'New year': 'Nouvel an',
    "Berchtold's Day": 'Deux de l’an',
    'Republic Day': 'Instauration de la République',
    'Good Friday': 'Vendredi saint',
    'Easter Monday': 'Lundi de Pâques',
    'Labour Day': 'Fête du Travail',
    'Ascension Thursday': 'Ascension',
    'Whit Monday': 'Lundi de Pentecôte',
    'National Holiday': 'Fête nationale',
    'Federal Thanksgiving Monday': 'Jeûne Fédéral',
    'Christmas Day': 'Noël',
    'Boxing Day': 'Lendemain de Noël',
}


class CRHolidays(Neuchatel):
    extra_days = {
        2022: [
            (date(2022, 2, 28), "Congé supplémentaire"),
            (date(2022, 12, 27), "Congé supplémentaire"),
            (date(2022, 12, 28), "Congé supplémentaire"),
        ],
    }

    def has_berchtolds_day(self, year):
        return True

    def get_variable_days(self, year):
        days = super().get_variable_days(year)
        # 26.12 is conditional in official calendar, add it inconditionally.
        if date(year, 12, 26) not in [day for day, _ in days]:
            days.append((date(year, 12, 26), self.boxing_day_label))
        days.append((date(year, 12, 31), 'St-Sylvestre'))
        # Now check if the number of those days on working days.
        num_real = self.num_holidays_on_working_days(days)
        # If < 12: add more
        if num_real < 12:
            days.append((self.get_ascension_thursday(year) + timedelta(days=1), "Vendredi de l’Ascension"))
            num_real += 1
        for extra in self.extra_days.get(year, []):
            days.append(extra)
        return days

    def num_holidays_on_working_days(self, holidays):
        return len([day for day, label in holidays if day.weekday() not in self.get_weekend_days()])


def check_overlap(self, event_list):
    """
    For each event in event_list, check if there are overlaps and attach new
    `overlap` ('odd'/'even') attributes to events.
    """
    categs_to_skip = ['dispo']
    overlap = 'odd'
    for ev in event_list:
        if ev.category_id and ev.category.name in categs_to_skip:
            continue
        for other in event_list:
            if other.category_id and other.category.name in categs_to_skip:
                continue
            if ev != other and ev.overlaps(other):
                ev.overlap = overlap
                overlap = 'even' if overlap == 'odd' else 'odd'
                break
    return 1


def day_overlap(self, event_list):
    """
    Attach a column attribute to each event in event_list, so as no events overlap
    (except rendez-vous overlapping matching dispo).
    """
    dispo_events = [ev for ev in event_list if ev.category.name == 'dispo']
    # Set column for each dispo event
    cols = [[]]

    def find_column_for_event(ev):
        for idx, evts in enumerate(cols):
            if any(ev.overlaps(other) for other in evts):
                continue
            else:
                # Add event to this col
                cols[idx].append(ev)
                ev.column = idx
                break
        else:
            # Add event to a new col
            cols.append([ev])
            ev.column = len(cols) - 1

    for ev in dispo_events:
        find_column_for_event(ev)

    # Set column for each rzv event related to its dispo
    def get_dispo_for_ev(ev):
        for dispo in dispo_events:
            if (ev.person_id == dispo.person_id and
                    (dispo.end >= ev.start >= dispo.start or dispo.start <= ev.end <= dispo.end)):
                return dispo

    for ev in event_list:
        if ev.category.name == 'dispo':
            continue
        match = get_dispo_for_ev(ev)
        if match:
            ev.column = match.column
        else:
            find_column_for_event(ev)

    for ev in event_list:
        ev.leftoffset = ev.column * (100.0 / len(cols))
        if ev.leftoffset != 0:
            ev.leftoffset = f'{ev.leftoffset}%'
    return len(cols)


class WeekEventsMixin:
    START_HOUR = 8
    END_HOUR = 19
    NUM_DAYS = 5
    overlap_func = check_overlap

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_target_week(self):
        if self.kwargs.get('week'):
            year, week_num = int(self.kwargs['year']), int(self.kwargs['week'])
        else:
            year, week_num, _ = date.today().isocalendar()
        week_start = timezone.make_aware(
            datetime.strptime(f'{year}-{week_num}-1', "%G-%V-%w")
        )
        return year, week_num, week_start

    def events_by_day(self, events, week_start, num_days=NUM_DAYS):
        """Return a dict {day: {'time(8, 30)': [events], ...}."""
        monday = week_start.date() if isinstance(week_start, datetime) else week_start
        week_days = [
            monday + timedelta(days=i)
            for i in range(num_days)
        ]
        years = list({week_days[0].year, week_days[-1].year})
        holidays = dict([item for sublist in [CRHolidays().holidays(year) for year in years] for item in sublist])
        week_events = {
            dt: {'events': [], 'cols': 0, 'holiday': HOLIDAYS_TRANS.get(holidays.get(dt))} for dt in week_days
        }
        # Start by putting each event in its week day
        for event in events:
            try:
                week_events[timezone.localtime(event.start).date()]['events'].append(event)
            except KeyError:
                pass
        # Checking events overlap
        for day in week_events:
            num_cols = self.overlap_func(week_events[day]['events'])
            week_events[day]['cols'] = num_cols
            week_events[day]['col_width'] = round(100.0 / num_cols, 2)
        # Putting events as a dict with the hour quarter as key
        for day in week_events:
            events = week_events[day]['events']
            week_events[day]['events'] = {}
            for event in events:
                hour = round_quarter(timezone.localtime(event.start))
                week_events[day]['events'].setdefault(hour, []).append(event)
        return week_events

    def get_day_divs(self, start_hour, end_hour):
        return [time(h, m) for h in range(start_hour, end_hour + 1) for m in range(0, 60, 15)]

    def get_events(self, person, week_start):
        return Event.objects.get_for_person(person, week_start, week_start + timedelta(days=7))

    def prev_next_url(self, person, year, week):
        raise NotImplementedError()

    def agenda_context(self, entity):
        year, week_num, week_start = self.get_target_week()

        events = self.get_events(entity, week_start)
        week_events = self.events_by_day(events, week_start, num_days=self.NUM_DAYS)

        # Prev/next week links
        prev_iso = (week_start - timedelta(days=7)).isocalendar()
        prev_events = self.prev_next_url(entity, prev_iso.year, prev_iso.week)

        next_iso = (week_start + timedelta(days=7)).isocalendar()
        next_events = self.prev_next_url(entity, next_iso.year, next_iso.week)

        on_own_agenda = isinstance(entity, Utilisateur) and self.request.user == entity
        context = {
            'person': entity,
            'own_agenda': on_own_agenda,
            'classes': 'week-grid own-agenda' if on_own_agenda else 'week-grid',
            'has_holidays': any(item['holiday'] for item in week_events.values()),
            'title': 'Mon agenda' if on_own_agenda else f'Agenda de «{entity}»',
            'week_events': week_events,
            'prev_events': prev_events,
            'next_events': next_events,
        }
        if isinstance(entity, Utilisateur):
            context.update({
                'add_rzv_url': reverse('person-agenda-add', args=[entity.username, 'rzv']),
                'print_url': '',  # reverse('person-agenda-print', args=[entity.pk, year, week_num]),
            })
            if on_own_agenda or self.request.user.has_perm('agenda.manage_dispos'):
                context['add_dispo_url'] = reverse('person-agenda-add', args=[entity.username, 'dispo'])
        if isinstance(entity, Region):
            context['add_rzv_url'] = reverse('centre-agenda-add', args=[entity.pk, 'rzv'])

        return context


class PersonAgendaView(WeekEventsMixin, TemplateView):
    template_name = 'agenda/person-agenda.html'
    mode = 'week'

    def prev_next_url(self, person, year, week):
        return reverse('person-agenda', args=[person.username, year, week])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        person = get_object_or_404(Utilisateur, username=self.kwargs['username'])

        on_own_agenda = self.request.user == person
        context.update({
            'start_hour': self.START_HOUR,
            'end_hour': self.END_HOUR,
            'day_divs': self.get_day_divs(self.START_HOUR, self.END_HOUR),
            'form_media': AgendaEditForm().media,  # Needed as form is loaded inside modal.
            'agendas': [self.agenda_context(person)],
            'filter_form': AgendaFilterForm() if not on_own_agenda else '',
            'show_infs': False,
        })
        return context


class PersonAgendaEditView(CreateUpdateView):
    model = Event
    form_class = AgendaEditForm
    template_name = 'agenda/person-agenda-add.html'

    @property
    def is_create(self):
        return self.pk_url_kwarg not in self.kwargs

    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            if self.request.GET.get('day'):
                initial['date'] = date.fromisoformat(self.request.GET.get('day'))
            if self.request.GET.get('hour'):
                initial['heure_de'] = time.fromisoformat(self.request.GET.get('hour'))
            if self.request.GET.get('centre'):
                initial['centre'] = self.request.GET.get('centre')
        else:
            initial['date'] = timezone.localtime(self.object.start).date()
            initial['heure_de'] = timezone.localtime(self.object.start).time()
            initial['heure_a'] = timezone.localtime(self.object.end).time()
            if self.object.rule:
                if self.object.rule.name == 'Chaque jour, du lundi au vendredi':
                    initial['repetition'] = 'DAILY-open'
                else:
                    initial['repetition'] = self.object.rule.frequency
                if self.object.end_recurring_period:
                    initial['repetition_fin'] = self.object.end_recurring_period.date()
        return initial

    def get_form_kwargs(self):
        typ_map = {'dispo': 'dispo', 'rzv': 'rendez-vous'}
        return {
            **super().get_form_kwargs(),
            'category': get_object_or_404(
                EventCategory, name=typ_map.get(self.kwargs['typ'])
            ) if 'typ' in self.kwargs else None,
            'person': get_object_or_404(Utilisateur, username=self.kwargs['username']),
            # Only owner or admins can enter events outside dispos
            'inside_dispo': not (
                self.kwargs['username'] == self.request.user.username or
                self.request.user.is_admin_ipe
            ),
        }

    def get_context_data(self, **kwargs):
        context = {**super().get_context_data(**kwargs), 'can_delete': True}
        if 'occ' in self.request.GET:
            occ_date = datetime.fromisoformat(self.request.GET['occ'])
            context['occ_date'] = occ_date
            context['occ_obj'] = self.object.exceptions.filter(orig_start=occ_date).first()
            if self.object.start.date() < date.today():
                # Don't authorize deletion of a dispo that already started.
                context['can_delete'] = False
        context['categ_color_map'] = {
            cat.pk: cat.default_color for cat in EventCategory.objects.all()
        }
        return context

    def form_valid(self, form):
        if form.instance.rule and self.request.POST.get("modify-occ-btn") == "on":
            # Supprimer cette occurrence et créer une dispo unique avec les infos du formulaire
            occ_date = datetime.fromisoformat(self.request.POST["occ_date"])
            with transaction.atomic():
                Occurrence.objects.create(
                    event=form.instance, orig_start=occ_date, cancelled=True,
                )
                start = make_datetime(occ_date, form.cleaned_data["heure_de"])
                end = make_datetime(occ_date, form.cleaned_data["heure_a"])
                self.object = Event.objects.create(
                    last_author=self.request.user, start=start, end=end,
                    centre=form.cleaned_data["centre"],
                    description=form.cleaned_data["description"],
                    dispo_tel=form.cleaned_data.get("dispo_tel", False),
                    category=form.instance.category,
                    person=form.instance.person,
                )
            response = HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.last_author = self.request.user
            response = super().form_valid(form)
            if form.cleaned_data.get('sms_confirm') and self.object.famille:
                tel_num = self.object.famille.get_mobile_num()
                if tel_num:
                    transaction.on_commit(
                        lambda: send_confirmation_sms(self.object, self.object.famille.get_mobile_num())
                    )
        return response

    def get_success_url(self):
        year, week, _ = self.object.start.isocalendar()
        return reverse('person-agenda', args=[self.kwargs['username'], year, week])


class PersonAgendaDeleteView(DeleteView):
    model = Event
    template_name = 'agenda/event-delete.html'

    def form_valid(self, form):
        event = self.object
        event_start = django_format(timezone.localtime(event.start), 'j F à H:i')
        if not event.can_edit(self.request.user):
            raise PermissionDenied()
        famille = event.famille
        if famille:
            form = SendMessageForm(famille=event.famille, data=self.request.POST)
            if not form.is_valid():
                return self.render_to_response(self.get_context_data(message_form=form))
            transaction.on_commit(lambda: form.send("Annulation rendez-vous Croix-Rouge", event_start))
        resp = super().form_valid(form)
        if famille:
            # Also add an entry in family journal
            PrestationCIPE.objects.create(
                auteur=self.request.user, date=date.today(), type_consult='hors_stats', duree=timedelta(0),
                famille=famille, faits_courants=f"Annulation du rendez-vous du {event_start}"
            )
        return resp

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = django_format(timezone.localtime(self.object.start), 'j F à H:i')
        context['start'] = start
        if self.object.famille and 'message_form' not in context:
            context.update({
                'message_form': SendMessageForm(
                    famille=self.object.famille,
                    initial={'message': (
                        f'Désolé, nous devons annuler votre rendez-vous Croix-Rouge du {start}. '
                        'Prenez-en un nouveau ici: https://sap.croix-rouge-ne.ch/agenda/rendez-vous/'
                    )},
                ),
            })
        return context

    def get_success_url(self):
        year, week, _ = self.object.start.isocalendar()
        return reverse('person-agenda', args=[self.kwargs['username'], year, week])


class PersonAgendaToggleOccurrence(View):
    def post(self, request, *args, **kwargs):
        event = get_object_or_404(Event, pk=self.kwargs['pk'])
        if not event.can_edit(request.user):
            raise PermissionDenied()
        occ_date = datetime.fromisoformat(request.POST['occ_date'])
        with transaction.atomic():
            try:
                occ = event.exceptions.select_for_update().get(orig_start=occ_date)
                occ.delete()
            except Occurrence.DoesNotExist:
                Occurrence.objects.create(
                    event=event,
                    orig_start=occ_date,
                    cancelled=True,
                )
        year, week, _ = occ_date.isocalendar()
        return HttpResponseRedirect(reverse('person-agenda', args=[self.kwargs['username'], year, week]))


class AgendaChoiceView(TemplateView):
    template_name = 'agenda/agenda-week.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'filter_form': AgendaFilterForm(),
            'agendas': [],
        })
        return context


class IPEAgendasView(WeekEventsMixin, TemplateView):
    template_name = 'agenda/agenda-week.html'

    def prev_next_url(self, person, year, week):
        return reverse('agendas-ipe', args=[year, week]) + '#' + person.username

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        persons = Utilisateur._intervenants(groupes=['ipe'])
        context.update({
            'start_hour': self.START_HOUR,
            'end_hour': self.END_HOUR,
            'day_divs': self.get_day_divs(self.START_HOUR, self.END_HOUR),
            'form_media': AgendaEditForm().media,  # Needed as form is loaded inside modal.
            'filter_form': AgendaFilterForm(),
            'agendas': [self.agenda_context(person) for person in persons],
            'show_infs': False,
        })
        return context


class CentreAgendaView(WeekEventsMixin, TemplateView):
    template_name = 'agenda/agenda-week.html'

    def get_events(self, centre, week_start):
        return Event.objects.get_for_location(centre, week_start, week_start + timedelta(days=7))

    def prev_next_url(self, centre, year, week):
        return reverse('agenda-centre', args=[centre.nom, year, week])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        centre = get_object_or_404(Region, nom=self.kwargs['centre'])
        context.update({
            'start_hour': self.START_HOUR,
            'end_hour': self.END_HOUR,
            'day_divs': self.get_day_divs(self.START_HOUR, self.END_HOUR),
            'form_media': AgendaEditForm().media,  # Needed as form is loaded inside modal.
            'filter_form': AgendaFilterForm(),
            'agendas': [self.agenda_context(centre)],
            'show_infs': True,
        })
        return context


class CentreAgendaEditView(PersonAgendaEditView):
    is_create = True

    def get_form_kwargs(self):
        return {
            **super(PersonAgendaEditView, self).get_form_kwargs(),
            'category': get_object_or_404(EventCategory, name='rendez-vous'),
            'person': None,
            'centre': get_object_or_404(Region, pk=self.kwargs['centre_pk']),
            'inside_dispo': True,
        }

    def get_success_url(self):
        year, week, _ = self.object.start.isocalendar()
        return reverse('agenda-centre', args=[self.object.centre.nom, year, week])


class DayAgendaView(WeekEventsMixin, TemplateView):
    template_name = 'agenda/agenda-week.html'
    overlap_func = day_overlap

    def get(self, request, *args, **kwargs):
        try:
            self.day = date(kwargs['year'], kwargs['month'], kwargs['day'])
        except ValueError:
            raise Http404("Date non valable")
        return super().get(request, *args, **kwargs)

    def get_events(self, day):
        return Event.objects.get_for_location(
            None,
            datetime.combine(day, datetime.min.time()),
            datetime.combine(day, datetime.max.time())
        )

    def agenda_context(self, day):
        events = self.get_events(day)
        day_events = self.events_by_day(events, day, num_days=1)
        next_day = day + timedelta(days=1)
        next_events = reverse('agenda-day', args=[next_day.year, next_day.month, next_day.day])
        prev_day = day - timedelta(days=1)
        prev_events = reverse('agenda-day', args=[prev_day.year, prev_day.month, prev_day.day])

        context = {
            # 'person': entity,
            'title': f'Agenda du «{format_d_m_Y(day)}»',
            'classes': 'day-grid',
            'week_events': day_events,
            'prev_events': prev_events,
            'next_events': next_events,
        }
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'start_hour': self.START_HOUR,
            'end_hour': self.END_HOUR,
            'day_divs': self.get_day_divs(self.START_HOUR, self.END_HOUR),
            'form_media': AgendaEditForm().media,  # Needed as form is loaded inside modal.
            'filter_form': AgendaFilterForm(),
            'agendas': [self.agenda_context(self.day)],
            'show_infs': True,
        })
        return context


def round_quarter(time_):
    return time(time_.hour, math.floor(time_.minute / 15) * 15)
