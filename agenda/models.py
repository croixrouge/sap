from dataclasses import dataclass
from datetime import datetime, timedelta, timezone

from dateutil import rrule
from django.core import signing
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.utils.dateformat import format as dateformat
from django.utils.timezone import localtime

from cipe.models import FamilleCIPE
from croixrouge.models import Region, Utilisateur

# Thanks https://github.com/llazzaro/django-scheduler/ for inspiration
# TODO: implement dateutil rruleset for exceptions

RRULE_FREQ_MAP = {
    "YEARLY": rrule.YEARLY,
    "MONTHLY": rrule.MONTHLY,
    "WEEKLY": rrule.WEEKLY,
    "DAILY": rrule.DAILY,
    "HOURLY": rrule.HOURLY,
    "MINUTELY": rrule.MINUTELY,
    "SECONDLY": rrule.SECONDLY,
}
RRULE_WDAY_MAP = {'MO': 0, 'TU': 1, 'WE': 2, 'TH': 3, 'FR': 4, 'SA': 5, 'SU': 6}


class Rule(models.Model):
    # https://tools.ietf.org/html/rfc5545
    # See also https://dateutil.readthedocs.io/en/stable/rrule.html
    FREQ_CHOICES = (
        ("YEARLY", "Chaque année"),
        ("MONTHLY", "Chaque mois"),
        ("WEEKLY", "Chaque semaine"),
        ("DAILY", "Chaque jour"),
        ("HOURLY", "Chaque heure"),
        ("MINUTELY", "Chaque minute"),
        ("SECONDLY", "Chaque seconde"),
    )
    name = models.CharField("Nom", max_length=40)
    description = models.TextField("Description")
    frequency = models.CharField("Fréquence", choices=FREQ_CHOICES, max_length=10)
    params = models.TextField("Params", blank=True)

    class Meta:
        verbose_name = "Règle"
        verbose_name_plural = "Règles"

    def __str__(self):
        return "Règle %s - params %s" % (self.name, self.params)


class AnyRegion:
    """Class to represent a choice for any region."""
    nom = ''

    def __bool__(self):
        return False


class EventManager(models.Manager):
    def get_for_person_back(self, pers, start, limit, categories=None):
        events = []
        days_back = 7
        while len(events) < limit:
            # provide one week of events
            start_less_one_week = start - timedelta(days=days_back)
            source = self.get_for_person(pers, start_less_one_week, start, categories=categories)
            if not source:
                days_back = days_back * 10
                if days_back > 100:
                    break
            source.extend(events)
            events = source
            start = start_less_one_week
        if len(events) > limit:
            events = events[-limit:]
        return events

    def get_for_persons(self, persons, start, end, categories=None, limit=None):
        events_qs = Event.objects.filter(person__in=persons).select_related('category', 'centre')
        return self._filter_events(events_qs, start, end, categories=categories, limit=limit)

    def get_for_person(self, pers, start, end, categories=None, limit=None, exclude=()):
        events_qs = pers.events.select_related('category', 'centre')
        if exclude:
            events_qs = events_qs.exclude(pk__in=[ev.pk for ev in exclude])
        return self._filter_events(events_qs, start, end, categories=categories, limit=limit)

    def get_for_location(self, location, start, end, categories=None, limit=None):
        if location is None or location is AnyRegion:
            events_qs = Event.objects.all().select_related('category', 'centre')
            return self._filter_events(events_qs, start, end, categories=categories, limit=limit)

        events_qs = Event.objects.filter(centre=location).select_related('category', 'centre')
        events = self._filter_events(events_qs, start, end, categories=categories, limit=limit)
        # Also include rzv dom/tél if related to a location dispo (same person and time interval).
        dispo_evs = [ev for ev in events if ev.category_id and ev.category.name == 'dispo']
        for ev in dispo_evs:
            dom_tel_evs = Event.objects.filter(
                person=ev.person, start__lt=ev.end, end__gt=ev.start,
                category__name__startswith='rendez-vous',
            )
            if dom_tel_evs:
                events.extend(dom_tel_evs)
        return sorted(events)

    def _filter_events(self, events_qs, start, end, categories=None, limit=None):
        """
        `limit`: maximum number of events to return.
        """
        start = start.astimezone(timezone.utc)
        end = end.astimezone(timezone.utc)
        unique_events = events_qs.filter(
            rule__isnull=True, end__gte=start, start__lt=end
        ).select_related('famille')
        if categories is not None:
            unique_events = unique_events.filter(category__in=categories)

        recur_events = events_qs.filter(
            rule__isnull=False
        ).filter(
            models.Q(end_recurring_period__gte=start) | models.Q(end_recurring_period__isnull=True)
        ).select_related('rule', 'famille').prefetch_related('exceptions')
        if categories is not None:
            recur_events = recur_events.filter(category__in=categories)

        if limit:
            events = []
            # Create a list of generators, then pick the most recent Event at each
            # iteration. Remove the generator when it is exhausted.
            queues = ([iter(unique_events)] if len(unique_events) else []) + [
                ev.get_occurrences(start, end=None) for ev in recur_events
            ]
            nexts = [next(q) for q in queues]
            while nexts:
                next_idx = nexts.index(min(nexts))
                events.append(nexts[next_idx])
                try:
                    nexts[next_idx] = next(queues[next_idx])
                except StopIteration:
                    del nexts[next_idx]
                    del queues[next_idx]
                if len(events) >= limit:
                    break
        else:
            events = list(unique_events)
            for ev in recur_events:
                events.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(events)

    def get_by_category(self, categ, start, end):
        start = start.astimezone(timezone.utc)
        end = end.astimezone(timezone.utc)
        unique_events = self.get_queryset().filter(
            rule__isnull=True, end__gte=start, start__lt=end, category=categ
        ).select_related('famille')
        recur_events = self.get_queryset().filter(
            rule__isnull=False, category=categ
        ).filter(
            models.Q(end_recurring_period__gte=start) | models.Q(end_recurring_period__isnull=True)
        ).select_related('rule', 'famille').prefetch_related('exceptions')
        events = list(unique_events)
        for ev in recur_events:
            events.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(events)


class EventCategory(models.Model):
    name = models.CharField('Nom', max_length=50)
    default_color = models.CharField("Couleur par défaut", max_length=7, blank=True)

    class Meta:
        ordering = ['name']
        verbose_name = "Catégorie d’évènement"
        verbose_name_plural = "Catégories d’évènement"
        permissions = [
            ("manage_dispos", "Autorisé à gérer les événements de type disponibilité"),
        ]

    def __str__(self):
        return self.name

    @property
    def is_rendezvous(self):
        return 'rendez-vous' in self.name


class Event(models.Model):
    last_author = models.ForeignKey(Utilisateur, on_delete=models.CASCADE, related_name='+', blank=True, null=True)
    last_modif = models.DateTimeField(auto_now=True)
    start = models.DateTimeField("Début", db_index=True)
    end = models.DateTimeField("Fin", db_index=True, null=True, blank=True)
    description = models.TextField("Description", blank=True)
    centre = models.ForeignKey(
        Region, on_delete=models.PROTECT, verbose_name="Centre", null=True, blank=True,
        limit_choices_to={'secteurs__contains': ['cipe']}
    )
    dispo_tel = models.BooleanField("Dispo pour consultation tél. (IPE)", default=True)
    category = models.ForeignKey(
        EventCategory, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="Catégorie"
    )
    color = models.CharField("Couleur", max_length=7, blank=True)
    person = models.ForeignKey(Utilisateur, on_delete=models.CASCADE, related_name='events')
    famille = models.ForeignKey(FamilleCIPE, blank=True, null=True, on_delete=models.CASCADE)
    # Content from the public reservation form (personal data).
    online_form_data = models.JSONField(encoder=DjangoJSONEncoder, null=True, blank=True)
    rule = models.ForeignKey(
        Rule, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    end_recurring_period = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    objects = EventManager()
    cancelled = False

    class Meta:
        verbose_name = "Évènement"
        verbose_name_plural = "Évènements"

    def __str__(self):
        return "%s: %s (%s)" % (
            self.title, dateformat(self.start, 'd.m.Y'), self.hours,
        )

    def __lt__(self, other):
        # Order by start and category
        if self.start == other.start and self.category_id and other.category_id:
            return self.category.name < other.category.name
        return self.start < other.start

    @property
    def title(self):
        return self.description.split('\n')[0]

    @property
    def cancel_hash(self):
        return signing.dumps(self.pk)

    def overlaps(self, other):
        return self.start < other.end and self.end > other.start

    def can_edit(self, user):
        if self.category and self.category.name == 'dispo':
            return user == self.person or user.has_perm('agenda.manage_dispos')
        return (
            user == self.person or user == self.last_author or
            user.has_perm('agenda.change_event') or user.has_perm('agenda.manage_dispos')
        )

    @property
    def date_hours(self):
        return f"{dateformat(localtime(self.start), 'd.m.Y')}, {self.hours}"

    @property
    def hours(self):
        return f"{dateformat(localtime(self.start), 'H:i')} - {dateformat(localtime(self.end), 'H:i')}"

    @property
    def bg_color(self):
        return self.color if self.color else (
            self.category.default_color if self.category and self.category.default_color else 'white'
        )

    @property
    def text_color(self):
        return 'white' if color_is_dark(self.bg_color) else 'black'

    def spans(self):
        """Return number of 15min slices it occupies."""
        return round((self.end - self.start).seconds / 60 / 15, 1)

    def get_availabilities(self, events, min_duration=60):
        """Return a list of Availabilities of `duration` inside self, considering existing events."""
        if self.cancelled:
            return []
        avails = [Availability(self.start, self.end, self)]

        def split_by_event(avails, ev):
            new_avails = []
            for avail in avails:
                if ev.end < avail.start or ev.start > avail.end:
                    # No intersection between avail and ev, keep avail
                    new_avails.append(avail)
                elif ev.start <= avail.start and ev.end < avail.end:
                    # the event strip some time at the start of the avail
                    new_avails.append(Availability(ev.end, avail.end, self))
                elif ev.start > avail.start and ev.end < avail.end:
                    # the event split the avail in two
                    new_avails.append(Availability(avail.start, ev.start, self))
                    new_avails.append(Availability(ev.end, avail.end, self))
                elif ev.end >= avail.end:
                    # the event strip some time at the end of the avail
                    new_avails.append(Availability(avail.start, ev.start, self))
            return new_avails

        for ev in events:
            if ev.person != self.person:
                continue
            avails = split_by_event(avails, ev)
        return [av for av in avails if (av.duration.seconds / 60) >= min_duration]

    def get_rrule(self):
        if self.rule is None:
            return
        frequency = RRULE_FREQ_MAP[self.rule.frequency]

        def convert_param(key, value):
            if key == 'byweekday':
                return key, [RRULE_WDAY_MAP[v] for v in value.split(',')]
            elif key == 'interval':
                return key, int(value)
            else:
                return key, value

        if self.rule.params:
            params = {key: val for key, val in [
                convert_param(*(param.split(':'))) for param in self.rule.params.split(';')
            ]}
        else:
            params = {}
        return rrule.rrule(frequency, dtstart=self.start, until=self.end_recurring_period, **params)

    def get_occurrences(self, start, end=None):
        """Return list of date occurrences of this event between start and end."""
        start_rule = self.get_rrule()
        duration = self.end - self.start
        exceptions = {occ.orig_start: occ for occ in self.exceptions.all()} if self.pk else {}
        initial_offset = localtime(self.start).utcoffset()
        # Adjust start/end to consider timezone change from initial event timezone
        start = fix_occurrence_date(start, initial_offset, reverse=True)
        if end:
            fix_occurrence_date(end, initial_offset, reverse=True)
        if self.start.time() < start.time() < self.end.time():
            # Adjust start time to include events overlapping start.
            start = datetime.combine(start, self.start.time(), tzinfo=start.tzinfo)
        # FIXME: limit results if end is None and event has no end.
        for occ_start in start_rule.xafter(start, inc=True):
            occ_start = fix_occurrence_date(occ_start, initial_offset)
            if end and occ_start >= end:
                break
            yield MemOccurrence(
                id=self.id, start=occ_start, end=occ_start + duration,
                description=self.description, category=self.category, centre=self.centre,
                dispo_tel=self.dispo_tel, color=self.color, person=self.person,
                famille=self.famille, exc=exceptions,
            )


class MemOccurrence(Event):
    class Meta:
        proxy = True

    is_recurring = True

    def __init__(self, *args, exc={}, **kwargs):
        super().__init__(*args, **kwargs)
        if self.start in exc and exc[self.start].cancelled:
            self.cancelled = True


class Occurrence(models.Model):
    """Recurring event exceptions."""
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='exceptions')
    orig_start = models.DateTimeField("Début d’origine")
    cancelled = models.BooleanField(default=False)

    def __str__(self):
        return "Exception pour l’événement «%s»" % self.event


@dataclass
class Availability:
    start: datetime
    end: datetime
    dispo: Event

    @property
    def duration(self) -> timedelta:
        return self.end - self.start

    def split_by(self, minutes) -> list:
        duration_in_min = self.duration.seconds / 60
        avails = []
        start = self.start
        while duration_in_min >= minutes:
            avails.append(Availability(start, start + timedelta(seconds=minutes * 60), self.dispo))
            start = avails[-1].end
            duration_in_min -= minutes
        return avails


def fix_occurrence_date(dt, initial_offset, reverse=False):
    if reverse:
        tz_diff = initial_offset - localtime(dt).utcoffset()
    else:
        tz_diff = localtime(dt).utcoffset() - initial_offset
    if tz_diff:
        return dt - tz_diff
    return dt


def color_is_dark(c):
    if c == 'black':
        return True
    elif c == 'white':
        return False
    # Assume a rgb '#aabbcc' syntax
    rgb = int(c.strip('#'), 16)
    red = rgb >> 16 & 0xff
    green = rgb >> 8 & 0xff
    blue = rgb >> 0 & 0xff
    luma = 0.2126 * red + 0.7152 * green + 0.0722 * blue  # per ITU-R BT.709
    return luma < 120
