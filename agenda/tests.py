from datetime import date, datetime, time, timedelta, timezone
from unittest import mock

from django.conf import settings
from django.core import mail
from django.template import Context, Template
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.dateformat import format as django_format
from django.utils.timezone import localtime, make_aware, now
from freezegun import freeze_time

from cipe.tests import InitialDataMixin
from croixrouge.models import Region, Role, Utilisateur
from croixrouge.utils import send_sms

from .forms import AgendaEditForm, ipes, make_datetime
from .models import AnyRegion, Availability, Event, EventCategory, Occurrence, Rule, fix_occurrence_date
from .views import WeekEventsMixin
from .views_public import (
    MIN_APPOINTMENT_DELAY,
    ContactForm,
    available_days_for_region,
    available_hours_for_region_day,
    send_confirmation_email,
    send_confirmation_sms,
)


class AgendaTests(InitialDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create(username='jill', prenom='Jill', nom='Martin')
        cls.neuch = Region.objects.par_secteur('cipe').get(nom='Neuchâtel')

    def test_hour_widgets(self):
        """Accept hours without minutes and dot (.) separator"""
        form = AgendaEditForm(person=self.user, data={
            'date': '12.3.2020',
            'heure_de': '14',
            'heure_a': '14.20',
            'description': 'Rendez-vous important',
        })
        self.assertTrue(form.is_valid(), msg=form.errors)
        self.assertEqual(form.cleaned_data['heure_de'], time(14, 0))
        self.assertEqual(form.cleaned_data['heure_a'], time(14, 20))

    def test_partial_repetition(self):
        form = AgendaEditForm(person=self.user, category=EventCategory.objects.get(name='dispo'), data={
            'date': '12.3.2020',
            'heure_de': '14:00',
            'heure_a': '14:20',
            'description': 'Rendez-vous important',
            'repetition': '',
            'repetition_fin': '12.4.2020',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ['Vous ne pouvez pas indiquer une fin de répétition sans valeur de répétition']
        )

    def test_create_event(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '12.3.2020',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'description': 'Rendez-vous important',
        })
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.user.username, 2020, 11]), fetch_redirect_response=False
        )
        event = self.user.events.first()
        self.assertEqual(event.person, self.user)
        self.assertEqual(event.last_author, self.user)
        self.assertIsNotNone(event.last_modif)
        self.assertEqual(event.start, make_aware(datetime(2020, 3, 12, 13, 15)))
        self.assertEqual(event.end, make_aware(datetime(2020, 3, 12, 16, 0)))
        events = Event.objects.get_for_person(
            self.user, make_aware(datetime(2020, 3, 1, 0, 0)), make_aware(datetime(2020, 3, 30, 23, 59))
        )
        self.assertEqual(events, [event])
        events = Event.objects.get_for_person(
            self.user, make_aware(datetime(2020, 3, 1, 0, 0)), make_aware(datetime(2020, 3, 30, 23, 59)),
            limit=4
        )
        self.assertEqual(events, [event])

    def _test_create_event_with_sms_confirmation(self, centre, message, mock_send):
        famille, _ = self._add_famille_avec_enfant()
        self.client.force_login(self.user)
        post_data = {
            'date': '12.3.2020',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'description': 'Rendez-vous important',
            'famille': famille.pk,
            'centre': centre,
            'sms_confirm': True,
        }
        # Fixed time so cancel_link remains stable
        with mock.patch('django.core.signing.time.time', mock.MagicMock(return_value=1650889585)):
            with self.captureOnCommitCallbacks(execute=True):
                response = self.client.post(
                    reverse('person-agenda-add', args=[self.user.username, 'rzv']), data=post_data,
                )
            self.assertRedirects(
                response, reverse('person-agenda', args=[self.user.username, 2020, 11]), fetch_redirect_response=False
            )
            new_event = self.user.events.first()
            cancel_link = reverse('agenda-public-annuler', args=[new_event.cancel_hash])
        mock_send.assert_called_with(
            message.format(cancel_link=cancel_link),
            '078 444 44 44',
            f'Une erreur est survenue lors de l’envoi du SMS pour le rendez-vous ID {new_event.pk}.'
        )

    @mock.patch('agenda.utils.send_sms')
    def test_create_centre_event_with_sms_confirmation(self, mock_send):
        msg = (
            "Rendez-vous Croix-Rouge confirmé: le jeudi 12 mars à 13:15 à Neuchâtel, "
            "av. du Premier-Mars 2a. "
            "Pour annuler: https://sap.croix-rouge-ne.ch{cancel_link}"
        )
        self._test_create_event_with_sms_confirmation(str(self.neuch.pk), msg, mock_send)

    @mock.patch('agenda.utils.send_sms')
    def test_create_tel_event_with_sms_confirmation(self, mock_send):
        msg = (
            'Rendez-vous téléphonique Croix-Rouge confirmé: le jeudi 12 mars à 13:15. '
            'Pour annuler: https://sap.croix-rouge-ne.ch{cancel_link}'
        )
        self._test_create_event_with_sms_confirmation('tel', msg, mock_send)

    @mock.patch('agenda.utils.send_sms')
    def test_create_dom_event_with_sms_confirmation(self, mock_send):
        msg = (
            'Rendez-vous Croix-Rouge confirmé: le jeudi 12 mars à 13:15 à votre domicile. '
            'Pour annuler: https://sap.croix-rouge-ne.ch{cancel_link}'
        )
        self._test_create_event_with_sms_confirmation('dom', msg, mock_send)

    def test_create_event_for_centre(self):
        user_cipe2 = Utilisateur.objects.create_user(
            'user_cipe2', 'user_cipe2@example.org', nom='Ipe2',
        )
        user_cipe2.groups.add(self.cipe_group)
        user_cipe2.roles.add(Role.objects.get(nom='IPE'))
        category = EventCategory.objects.get(name='dispo')
        next_year = date.today().year + 1
        # Start in UTC+2 timezone
        start_time = make_aware(datetime(next_year, 5, 29, 13, 0))
        end_time = make_aware(datetime(next_year, 5, 29, 17, 45))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=user_cipe2, centre=self.neuch,
            start=start_time, end=end_time, description='Recur', category=category, rule=weekly_rule
        )
        self.client.force_login(self.user)
        self.assertIn(self.user_cipe, ipes())
        # Now we are in UTC+1 timezone
        response = self.client.post(reverse('centre-agenda-add', args=[self.neuch.pk, 'rzv']), data={
            'date': f'27.11.{next_year}',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'person': str(user_cipe2.pk),
            'description': 'Rendez-vous important',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        week_num = date(next_year, 11, 27).isocalendar().week
        self.assertRedirects(
            response,
            reverse('agenda-centre', args=[self.neuch.nom, next_year, week_num]),
            fetch_redirect_response=False
        )
        response = self.client.post(reverse('centre-agenda-add', args=[self.neuch.pk, 'rzv']), data={
            'date': f'27.11.{next_year}',
            'heure_de': '17:00',
            'heure_a': '17:45',
            'person': str(user_cipe2.pk),
            'description': 'Rendez-vous en bordure de dispo',
        })
        self.assertEqual(response.status_code, 302)

    def test_create_event_phone_domicile(self):
        next_year = date.today().year + 1
        category = EventCategory.objects.get(name='dispo')
        start_time = make_aware(datetime(next_year, 5, 29, 7, 30))
        end_time = make_aware(datetime(next_year, 5, 29, 11, 45))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Recur',
            centre=self.neuch, category=category, rule=weekly_rule
        )
        self.client.force_login(self.user)
        self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': f'29.5.{next_year}',
            'heure_de': '8:00',
            'heure_a': '9:00',
            'centre': 'dom',
            'description': 'À domicile',
        })
        event_dom = self.user.events.get(description='À domicile')
        self.assertEqual(event_dom.category.name, 'rendez-vous dom')
        response = self.client.get(reverse('person-agenda-edit', args=[self.user.username, event_dom.pk]))
        self.assertContains(response, '<option value="dom" selected>À domicile</option>', html=True)

        form = AgendaEditForm(person=self.user, inside_dispo=True, data={
            'date': f'29.5.{next_year}',
            'heure_de': '11:00',
            'heure_a': '11:30',
            'centre': 'tel',
            'description': 'Par telephone',
        })
        self.assertTrue(form.is_valid(), msg=form.errors)
        event_tel = form.save()
        self.assertEqual(event_tel.category.name, 'rendez-vous tel')
        # Overlap check still works
        form = AgendaEditForm(
            person=self.user, inside_dispo=True, category=EventCategory.objects.get(name='rendez-vous'),
            data={
                'date': f'29.5.{next_year}',
                'heure_de': '8:30',
                'heure_a': '9:30',
                'description': 'Rendez-vous important',
            }
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'][0],
            'Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire'
        )
        # get_for_location retrieves also dom/tel events if related to location dispo
        events = Event.objects.get_for_location(
            self.neuch, datetime(next_year, 5, 29, 0, 0), datetime(next_year, 5, 29, 23, 59)
        )
        self.assertIn(event_dom, events)
        self.assertIn(event_tel, events)

    def test_event_form_clean(self):
        form = AgendaEditForm(
            person=self.user, category=EventCategory.objects.get(name='rendez-vous'),
            data={
                'date': "09.'9.2021",
                'heure_de': '11:30',
                'heure_a': '12:00',
                'centre': str(self.neuch.pk),
                'famille': '',
                'description': '',
            }
        )
        # Should not crash
        self.assertFalse(form.is_valid())

        famille, _ = self._add_famille_avec_enfant()
        form = AgendaEditForm(
            person=self.user, category=EventCategory.objects.get(name='rendez-vous'),
            inside_dispo=False,
            data={
                'date': "09.09.2021",
                'heure_de': '11:30',
                'heure_a': '12:00',
                'centre': '',  # will raise a form error
                'famille': famille.pk,
                'description': '',
            }
        )
        # Should not crash
        self.assertFalse(form.is_valid())

        Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        form = AgendaEditForm(
            person=self.user, category=EventCategory.objects.get(name='dispo'),
            data={
                'date': "09.09.2021",
                'heure_de': '11:30',
                'heure_a': '12:00',
                'centre': str(self.neuch.pk),
                'famille': '',
                'description': '',
                'repetition': 'WEEKLY',
            }
        )
        # Should not crash
        self.assertFalse(form.is_valid())

    def test_event_hour_consistency(self):
        categ = EventCategory.objects.get(name='rendez-vous')
        form_data = {
            'date': "9.9.2021",
            'heure_de': '11:30',
            'heure_a': '11:00',
            'centre': str(self.neuch.pk),
            'famille': '',
            'description': 'blah',
        }
        form = AgendaEditForm(person=self.user, category=categ, inside_dispo=False, data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'][0], "L’heure de fin doit être plus grande que l’heure de début")

        form_data['heure_a'] = '11:30'
        form = AgendaEditForm(person=self.user, category=categ, inside_dispo=False, data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'][0], "L’heure de fin doit être plus grande que l’heure de début")

    def test_event_occurrences(self):
        start_time = make_aware(datetime(2021, 5, 29, 9, 30))
        end_time = make_aware(datetime(2021, 5, 29, 10, 45))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event = Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Recur', rule=weekly_rule
        )
        self.assertEqual(
            [ev.start.date() for ev in event.get_occurrences(
                make_aware(datetime(2021, 5, 28, 8, 0)),
                end=make_aware(datetime(2021, 6, 15, 8, 0)))
             ],
            [date(2021, 5, 29), date(2021, 6, 5), date(2021, 6, 12)]
        )

    def test_no_overlap(self):
        """Event for same person and category cannot overlap."""
        category = EventCategory.objects.get(name='rendez-vous')
        start_time = make_aware(datetime(2021, 5, 29, 9, 30))
        end_time = make_aware(datetime(2021, 5, 29, 10, 45))
        Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Single', category=category
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '29.5.2021',
            'heure_de': '10:30',
            'heure_a': '11:00',
            'description': 'Pas permis',
        })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire']
        )

    def test_no_overlap_recurring(self):
        """Event for same person and category cannot overlap."""
        category = EventCategory.objects.get(name='dispo')
        old_date = date.today() - timedelta(days=7 * 52)
        start_time = make_aware(datetime(old_date.year, old_date.month, old_date.day, 9, 30))
        end_time = make_aware(datetime(old_date.year, old_date.month, old_date.day, 10, 45))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Recur', category=category, rule=weekly_rule
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data={
            'date': date.today().strftime('%d.%m.%Y'),
            'centre': self.neuch.pk,
            'heure_de': '10:30',
            'heure_a': '11:00',
            'description': 'Pas permis',
            'repetition': 'WEEKLY',
        })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire']
        )
        # Same test for an event starting *before* the existing one
        older_date = old_date - timedelta(days=7)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data={
            'date': older_date.strftime('%d.%m.%Y'),
            'centre': self.neuch.pk,
            'heure_de': '10:30',
            'heure_a': '11:00',
            'description': 'Pas permis',
            'repetition': 'WEEKLY',
        })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire']
        )

    def test_add_dispo_tel(self):
        EventCategory.objects.get(name='dispo')
        other_user = Utilisateur.objects.create(username='joe', first_name='Joe', last_name='Doe')
        post_data = {
            'date': '5.6.2022',
            'heure_de': '10:30',
            'heure_a': '11:00',
            'centre': '',
            'description': 'Dispo unique',
            'repetition': '',
            'repetition_fin': '',
        }
        form = AgendaEditForm(
            instance=None, initial={}, person=self.user, inside_dispo=False,
            category=EventCategory.objects.get(name='dispo'),
            data=post_data
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'centre': ['Une disponibilité doit soit être pour un centre, soit par téléphone.']}
        )

        self.client.force_login(other_user)
        post_data['dispo_tel'] = 'on'
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data=post_data)
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, 2022, 22]))
        dispo = Event.objects.get(description='Dispo unique')
        self.assertIsNone(dispo.centre)

    def test_add_rzv_outside_dispo(self):
        dispo_categ = EventCategory.objects.get(name='dispo')
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        other_user = Utilisateur.objects.create(username='joe', first_name='Joe', last_name='Doe')
        start_time = make_aware(datetime(2021, 5, 29, 9, 30))
        end_time = make_aware(datetime(2021, 5, 29, 10, 45))
        Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Recur', category=dispo_categ, rule=weekly_rule,
            centre=self.neuch
        )
        self.client.force_login(other_user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '5.6.2021',
            'heure_de': '10:30',
            'heure_a': '11:00',
            'description': 'Pas permis hors dispo',
        })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Vous ne pouvez pas ajouter de rendez-vous en dehors des plages de disponibilité.']
        )
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '5.6.2021',
            'heure_de': '10:00',
            'heure_a': '10:30',
            'description': 'Lieu différent',
        })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Le lieu du rendez-vous ne correspond pas au lieu de la disponibilité.']
        )
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '5.6.2021',
            'heure_de': '10:00',
            'heure_a': '10:30',
            'description': 'OK',
            'centre': self.neuch.pk,
        })
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, 2021, 22]))
        # User can enter outside dispo for its own agenda
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '5.6.2021',
            'heure_de': '15:00',
            'heure_a': '15:30',
            'description': 'OK',
        }, follow=True)
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, 2021, 22]))
        self.assertNotContains(response, "en dehors des plages de disponibilité")
        # Or member of the admin group
        admin_user = Utilisateur.objects.create_user(
            'admin_user', 'admin_user@example.org',
            nom='Ipe', prenom='Admin',
        )
        admin_user.groups.add(self.cipe_admin_group)
        self.client.force_login(admin_user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': '5.6.2021',
            'heure_de': '16:00',
            'heure_a': '16:30',
            'description': 'OK',
        }, follow=True)
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, 2021, 22]))
        self.assertNotContains(response, "en dehors des plages de disponibilité")

    def test_edit_event(self):
        demain = datetime.today() + timedelta(days=1)
        start_time = make_aware(datetime(demain.year, demain.month, demain.day, 9, 30))
        end_time = make_aware(datetime(demain.year, demain.month, demain.day, 10, 45))
        event = Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Single', rule=None
        )
        self.client.force_login(self.user)
        edit_url = reverse('person-agenda-edit', args=[self.user.username, event.pk])
        response = self.client.get(edit_url)
        self.assertContains(
            response,
            '<input type="text" name="heure_de" value="09:30" class="TimeField" placeholder="00:00" '
            'required id="id_heure_de">',
            html=True
        )
        self.assertContains(
            response,
            '<input type="text" name="heure_a" value="10:45" class="TimeField" placeholder="00:00" '
            'required id="id_heure_a">',
            html=True
        )
        # Test POSTing
        response = self.client.post(edit_url, data={
            'date': str(demain.date()),
            'heure_de': '9:30',
            'heure_a': '10:45',
            'description': 'Titre modifié',
        })
        iso = demain.isocalendar()
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, iso.year, iso.week]))

    def test_edit_recur_occurrence(self):
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event = Event.objects.create(
            person=self.user,
            start=datetime(2020, 2, 1, 15, 0, tzinfo=timezone.utc), description='Recur over DST', rule=weekly_rule
        )
        with freeze_time("2020-02-26"):
            self.client.force_login(self.user)
            response = self.client.get(
                reverse('person-agenda-edit', args=[self.user.username, event.pk]),
            )
            self.assertContains(
                response,
                '<input type="text" name="heure_de" value="16:00" class="TimeField" '
                'placeholder="00:00" required id="id_heure_de">',
                html=True
            )
        with freeze_time("2020-06-11"):
            self.client.force_login(self.user)
            response = self.client.get(
                reverse('person-agenda-edit', args=[self.user.username, event.pk]),
            )
            self.assertContains(
                response,
                '<input type="text" name="heure_de" value="16:00" class="TimeField" '
                'placeholder="00:00" required id="id_heure_de">',
                html=True
            )

    def test_create_recurring_event(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data={
            'date': '12.3.2020',
            'centre': '',
            'dispo_tel': 'on',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'description': 'Rendez-vous important',
            'repetition': 'WEEKLY',
            'repetition_fin': '8.10.2020',
        })
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.user.username, 2020, 11]), fetch_redirect_response=False
        )
        event = self.user.events.first()
        self.assertEqual(event.person, self.user)
        self.assertEqual(event.rule.frequency, 'WEEKLY')
        self.assertEqual(event.end_recurring_period, make_aware(datetime(2020, 10, 8, 23, 59)))
        # Time stored in UTC
        self.assertEqual(event.start.tzname(), 'UTC')
        self.assertEqual(event.start.hour, 12)
        occs = list(event.get_occurrences(start=event.start, end=event.start + timedelta(days=30)))
        # UTC hours adapt to consider the DST correction.
        self.assertEqual([occ.start.hour for occ in occs], [12, 12, 12, 11, 11])

    def test_person_events_with_recurrence(self):
        single = Event.objects.create(
            person=self.user,
            start=now() + timedelta(days=1), end=now() + timedelta(days=1, hours=2),
            description='Single', rule=None
        )
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=self.user,
            start=now() - timedelta(days=30), end=now() - timedelta(days=29, hours=23),
            description='Recurring', rule=weekly_rule
        )
        Event.objects.create(
            person=self.user,
            start=now() - timedelta(days=30), end=now() - timedelta(days=29, hours=23),
            description='Recurred in the past', rule=weekly_rule,
            end_recurring_period=now() - timedelta(days=10)
        )
        # Get events for one week
        events = Event.objects.get_for_person(
            self.user, now() - timedelta(days=3), now() + timedelta(days=4)
        )
        self.assertEqual(len(events), 2)
        self.assertEqual(events[1], single)
        self.assertEqual(events[0].description, 'Recurring')

        # Get events with a limit (used by lists)
        events = Event.objects.get_for_person(
            self.user, now(), now() + timedelta(days=1000), limit=10
        )
        self.assertEqual(len(events), 10)

    @freeze_time("2021-09-09")  # doesn't work on week-end without freeze
    def test_cancel_event_occurrence(self):
        daily_rule = Rule.objects.create(name='Chaque jour', frequency='DAILY')
        today = date.today()
        maint = datetime(today.year, today.month, today.day, 13, 15, tzinfo=timezone.utc)
        ev = Event.objects.create(
            person=self.user,
            start=maint - timedelta(days=30), end=maint - timedelta(days=29, hours=23),
            description='Recurring', rule=daily_rule
        )

        self.client.force_login(self.user)
        response = self.client.post(
            reverse('person-agenda-toggleocc', args=[self.user.username, ev.pk]),
            data={'occ_date': fix_occurrence_date(maint, localtime(ev.start).utcoffset()).isoformat()},
        )
        iso = today.isocalendar()
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.user.username, iso.year, iso.week])
        )
        self.assertEqual(ev.exceptions.count(), 1)
        response = self.client.get(
            reverse('person-agenda', args=[self.user.username]),
        )
        self.assertContains(response, 'class="event-content cancelled "', count=1)
        # Restore it
        response = self.client.post(
            reverse('person-agenda-toggleocc', args=[self.user.username, ev.pk]),
            data={'occ_date': fix_occurrence_date(maint, localtime(ev.start).utcoffset()).isoformat()},
        )
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.user.username, iso.year, iso.week]),
        )
        self.assertEqual(ev.exceptions.count(), 0)

    def test_modify_event_occurrence(self):
        """
        Modification d'une occurrence seule: suppression de l'occurrence répétitive
        et création d'une dispo unique.
        """
        today = date.today()
        maint = datetime(today.year, today.month, today.day, 13, 15, tzinfo=timezone.utc)
        daily_rule = Rule.objects.create(name="Chaque jour", frequency="DAILY")
        ev = Event.objects.create(
            person=self.user, category=EventCategory.objects.get(name="dispo"),
            start=maint - timedelta(days=5), end=maint - timedelta(days=5) + timedelta(hours=2),
            description="Recurring", rule=daily_rule
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("person-agenda-edit", args=[self.user.username, ev.pk]),
            data={
                "date": ev.start.date().strftime("%d.%m.%Y"),
                "occ_date": fix_occurrence_date(maint, localtime(ev.start).utcoffset()).isoformat(),
                "heure_de": "15:30",
                "heure_a": "17:30",
                "dispo_tel": "on",
                "description": "Special occurrence",
                "modify-occ-btn": "on",
            },
        )
        iso = today.isocalendar()
        self.assertRedirects(
            response, reverse("person-agenda", args=[self.user.username, iso.year, iso.week]),
        )
        events = Event.objects.get_for_person(
            self.user, maint, maint + timedelta(hours=3)
        )
        self.assertEqual(len(events), 2)
        self.assertEqual(events[0].description, "Recurring")
        self.assertTrue(events[0].cancelled)
        self.assertIsNone(events[1].rule)
        self.assertEqual(events[1].description, "Special occurrence")

    def test_cancel_event(self):
        demain = now() + timedelta(days=1)
        event = Event.objects.create(
            person=self.user,
            start=demain, end=demain + timedelta(hours=1),
            description='Single',
        )
        self.client.force_login(self.user)
        delete_url = reverse('person-agenda-delete', args=[self.user.username, event.pk])
        response = self.client.get(delete_url)
        self.assertContains(response, 'Souhaitez-vous réellement supprimer le rendez-vous')
        response = self.client.post(delete_url, {})
        redir_to = reverse('person-agenda', args=[self.user.username, demain.year, demain.isocalendar()[1]])
        self.assertRedirects(response, redir_to)
        self.assertFalse(Event.objects.filter(pk=event.pk).exists())

    def test_cancel_famille_event(self):
        demain = (now() + timedelta(days=1)).replace(hour=14, minute=30)
        utc_hour = 14 + localtime(demain).utcoffset().seconds // 3600
        famille, _ = self._add_famille_avec_enfant()
        event = Event.objects.create(
            person=self.user,
            start=demain, end=demain + timedelta(hours=1),
            famille=famille,
            description='Single',
        )
        self.client.force_login(self.user)
        delete_url = reverse('person-agenda-delete', args=[self.user.username, event.pk])
        response = self.client.get(delete_url)
        self.assertContains(response, 'par SMS au numéro 078 444 44 44')

        # Delete without message
        response = self.client.post(delete_url, {'msg_method': '', 'message': ''})
        year, week, _ = event.start.isocalendar()
        self.assertRedirects(response, reverse('person-agenda', args=[self.user.username, year, week]))

        # Delete with SMS message
        message = "Désolé, nous devons annuler ce rendez-vous. Voir https://example.org/some/url/"
        event.save()  # Resurrect event
        with mock.patch('agenda.forms.send_sms') as mock_send:
            with self.captureOnCommitCallbacks(execute=True):  # SMS sent in on_commit callback
                response = self.client.post(delete_url, {'msg_method': 'sms', 'message': message})
        redir_to = reverse('person-agenda', args=[self.user.username, demain.year, demain.isocalendar()[1]])
        self.assertRedirects(response, redir_to)
        self.assertFalse(Event.objects.filter(pk=event.pk).exists())
        mock_send.assert_called_with(
            'Désolé, nous devons annuler ce rendez-vous. Voir https://example.org/some/url/',
            '078 444 44 44',
            "Erreur d’envoi du message «Annulation rendez-vous Croix-Rouge» "
            f'(rendez-vous du {django_format(event.start, "j F")} à {utc_hour}:30)',
        )

        # Once again with email message
        event.save()  # Resurrect event
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(delete_url, {'msg_method': 'email', 'message': message})
        self.assertRedirects(response, redir_to)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].body, message)
        self.assertEqual(mail.outbox[0].recipients(), ['greta@example.org'])

    def test_adjacent_events(self):
        demain = datetime.today() + timedelta(days=1)
        start_time = make_aware(datetime(demain.year, demain.month, demain.day, 9, 30))
        end_time = make_aware(datetime(demain.year, demain.month, demain.day, 10, 45))
        category = EventCategory.objects.get(name='rendez-vous')
        Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Single', category=category
        )
        self.client.force_login(self.user)
        redir_to = reverse('person-agenda', args=[self.user.username, demain.year, demain.isocalendar()[1]])
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': str(demain.date()),
            'heure_de': '10:45',
            'heure_a': '11:15',
            'description': 'Rendez-vous adjacent après',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, redir_to, fetch_redirect_response=False)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'rzv']), data={
            'date': str(demain.date()),
            'heure_de': '8:45',
            'heure_a': '9:30',
            'description': 'Rendez-vous adjacent avant',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, redir_to, fetch_redirect_response=False)
        self.assertEqual(self.user.events.filter(category=category).count(), 3)

    def test_adjacent_dispos(self):
        last_week = date.today() - timedelta(days=7)
        start_time = make_aware(datetime(last_week.year, last_week.month, last_week.day, 9, 30))
        end_time = make_aware(datetime(last_week.year, last_week.month, last_week.day, 10, 45))
        category = EventCategory.objects.get(name='dispo')
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=self.user, centre=self.neuch,
            start=start_time, end=end_time, rule=weekly_rule,
            description='Recurring dispo', category=category,
        )
        self.client.force_login(self.user)
        redir_to = reverse(
            'person-agenda', args=[self.user.username, date.today().year, date.today().isocalendar()[1]]
        )
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data={
            'date': str(date.today()),
            'centre': self.neuch.pk,
            'heure_de': '10:45',
            'heure_a': '11:15',
            'description': 'Rendez-vous adjacent après occurence',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, redir_to, fetch_redirect_response=False)
        response = self.client.post(reverse('person-agenda-add', args=[self.user.username, 'dispo']), data={
            'date': str(date.today()),
            'centre': self.neuch.pk,
            'heure_de': '8:45',
            'heure_a': '9:30',
            'description': 'Rendez-vous adjacent avant occurrence',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, redir_to, fetch_redirect_response=False)
        self.assertEqual(self.user.events.filter(category=category).count(), 3)

    def test_edit_dispo(self):
        last_week = date.today() - timedelta(days=7)
        start_time = make_aware(datetime(last_week.year, last_week.month, last_week.day, 9, 30))
        end_time = make_aware(datetime(last_week.year, last_week.month, last_week.day, 10, 45))
        category = EventCategory.objects.get(name='dispo')
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event = Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, rule=weekly_rule,
            description='Recurring dispo', category=category,
        )

        # Modifiying the end of the recurring period.
        form = AgendaEditForm(
            instance=event, initial={}, person=self.user, inside_dispo=False, category=category,
            data={
                'person': self.user.pk, 'description': 'Recurring dispo', 'category': category,
                'dispo_tel': 'on', 'rule': weekly_rule, 'date': last_week,
                'heure_de': start_time.strftime('%H:%M'), 'heure_a': end_time.strftime('%H:%M'),
                'end_recurring_period': (date.today() + timedelta(days=7)).strftime('%Y-%m-%d'),
            }
        )
        if not form.is_valid():
            self.fail(form.errors)

    def test_event_details_filter(self):
        start_time = localtime(now() + timedelta(days=1)).replace(hour=14, minute=30)
        event = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=start_time, end=start_time + timedelta(hours=1),
            description='Urgence',
            category=EventCategory.objects.get(name='rendez-vous'),
            online_form_data={
                'nom': "Smith", 'prenom': "Jill", 'date_naissance': "2023-02-10",
                'email': 'test@example.org', 'raison': 'Urgence',
            },
        )

        template = Template('{% load agenda %}{{ event|event_details }}')
        output = template.render(Context({'event': event}))
        self.assertEqual(
            output,
            '<div>Urgence</div><div>[Web] Enfant: Smith Jill (2023-02-10)</div>'
            '<div>Courriel: test@example.org</div>'
            '<div>Raison: Urgence</div>'
        )
        # With famille set
        famille, _ = self._add_famille_avec_enfant()
        event.famille = famille
        event.save()
        output = template.render(Context({'event': event}))
        self.assertEqual(
            output,
            '<br><i>Fam: </i><a href="%s">Martinet - 2000 Neuchâtel</a><br>'
            'Urgence' % famille.suivi_url
        )

    def test_get_target_week(self):
        klass = WeekEventsMixin()
        klass.kwargs = {}
        with freeze_time("2020-01-03"):
            week_data = klass.get_target_week()
            self.assertEqual(week_data[:2], (2020, 1))
            self.assertEqual(week_data[2].date(), date(2019, 12, 30))
        with freeze_time("2021-01-11"):
            week_data = klass.get_target_week()
            self.assertEqual(week_data[:2], (2021, 2))
            self.assertEqual(week_data[2].date(), date(2021, 1, 11))
        klass.kwargs = {'year': '2020', 'week': '3'}
        week_data = klass.get_target_week()
        self.assertEqual(week_data[:2], (2020, 3))

    def test_dispo_availabilities(self):
        other_user = Utilisateur.objects.create(username='sam', last_name='Example')
        category = EventCategory.objects.get(name='dispo')
        start_time = make_aware(datetime(2021, 5, 29, 8, 0))
        end_time = make_aware(datetime(2021, 5, 29, 12, 0))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event = Event.objects.create(
            person=self.user,
            start=start_time, end=end_time, description='Recur', category=category, rule=weekly_rule
        )
        # No events, return the avail untouched
        self.assertEqual(event.get_availabilities([]), [Availability(start_time, end_time, event)])

        ev_8_0__8_15 = Event(start=start_time, end=start_time + timedelta(minutes=15), person=self.user)
        # "parasite" event (other user)
        ev_8_15__9_00other = Event(
            start=start_time + timedelta(minutes=15),
            end=start_time + timedelta(minutes=60), person=other_user
        )
        ev_10_0__10_30 = Event(
            start=start_time + timedelta(minutes=120),
            end=start_time + timedelta(minutes=150), person=self.user
        )
        ev_10_30__11_00 = Event(
            start=start_time + timedelta(minutes=150),
            end=start_time + timedelta(minutes=180), person=self.user
        )
        self.assertEqual(
            [(avail.start, avail.end) for avail in event.get_availabilities([
                ev_8_0__8_15, ev_8_15__9_00other, ev_10_0__10_30, ev_10_30__11_00
            ])],
            [
                (start_time + timedelta(minutes=15), start_time + timedelta(minutes=120)),  # 8:15-10:00
                (start_time + timedelta(minutes=180), end_time),  # 11:00-12:00
            ]
        )
        # Cancelled occurrence gives no availabilities
        Occurrence.objects.create(
            event=event, orig_start=start_time + timedelta(days=7), cancelled=True,
        )
        dispo_occ = next(event.get_occurrences(start_time + timedelta(days=7), start_time + timedelta(days=8)))
        self.assertEqual(dispo_occ.get_availabilities([]), [])

    def test_sms_link_replacement(self):
        with mock.patch('requests.post', side_effect=mocked_requests) as mocked:
            send_sms("Ce message contient un lien https://example.org/blah. Parfait!", '0777777777', None)
        self.assertEqual(
            mocked.call_args.kwargs['json']['sms']['message']['text'],
            "Ce message contient un lien <-short->. Parfait!"
        )
        self.assertEqual(
            mocked.call_args.kwargs['json']['sms']['message']['links'],
            ["https://example.org/blah"],
        )

    def test_holidays(self):
        from agenda.views import CRHolidays
        holi = CRHolidays()
        self.assertEqual(holi.num_holidays_on_working_days(holi.holidays(2022)), 12)
        self.assertEqual(holi.num_holidays_on_working_days(holi.holidays(2023)), 12)
        # In fact 13, as 24.12 and 31.12 are half holidays.
        self.assertEqual(holi.num_holidays_on_working_days(holi.holidays(2024)), 14)
        # In fact 12, as 24.12 and 31.12 are half holidays.
        self.assertEqual(holi.num_holidays_on_working_days(holi.holidays(2025)), 13)

    '''
    def test_week_print(self):
        self.client.force_login(self.user)
        url_kwargs = {'pk': self.user.username, 'year': 2020, 'week': 5}
        request = RequestFactory().get(reverse('person-agenda-print', kwargs=url_kwargs))
        view = PersonAgendaHebdoPrintView()

        # No events
        response = self.client.get(reverse('person-agenda-print', kwargs=url_kwargs))
        self.assertEqual(response.status_code, 200)

        daily_rule = Rule.objects.create(name='Chaque jour', frequency='DAILY')
        Event.objects.create(
            person=self.user,
            start=make_aware(datetime(2020, 1, 1, 13, 15)),
            end=make_aware(datetime(2020, 1, 1, 14, 15)),
            description='Recurring', rule=daily_rule
        )
        Event.objects.create(
            person=self.user,
            start=make_aware(datetime(2020, 1, 30, 8, 0)),
            end=make_aware(datetime(2020, 1, 30, 10, 0)),
            description='Single',
        )
        view.setup(request, **url_kwargs)
        ctxt = view.pdf_context()
        self.assertEqual(len(ctxt['events'][date(2020, 1, 30)]), 2)
    '''


def mocked_requests(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data
    if args[0] == settings.SMSUP_SEND_URL:
        return MockResponse({
            "status": 1,
            "message": "OK",
            "cost": 1,
            "credits": 642,
            "total": 1,
            "sent": 1,
            "blacklisted": 0,
            "duplicated": 0,
            "invalid": 0,
            "npai": 0,
        }, 200)
    else:
        return MockResponse(None, 200)


class AgendaPublicTestsBase(InitialDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create(username='jill', prenom='Jill', nom='Martin')
        cls.neuch = Region.objects.par_secteur('cipe').get(nom='Neuchâtel')
        cls.neuch.rue = "Av. du Premier-Mars 2A"
        cls.neuch.save()

    def setUp(self):
        super().setUp()
        self.dispo_cat = EventCategory.objects.get(name='dispo')

    def _create_weekly_dispo(self, start_day, **event_kwargs):  # centre=not_set):
        start_time = make_datetime(start_day, time(8, 30))
        end_time = make_datetime(start_day, time(11, 45))
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event_params = dict(
            person=self.user,
            centre=self.neuch,  # if centre is not_set else centre,
            start=start_time, end=end_time,
            description='Recur',
            category=self.dispo_cat,
            rule=weekly_rule
        )
        event_params.update(event_kwargs)
        dispo = Event.objects.create(**event_params)
        dispo.refresh_from_db()  # Times will be normalized to UTC
        return dispo


class AgendaPublicTests(AgendaPublicTestsBase):
    def test_region_availabilities(self):
        days = available_days_for_region(self.neuch, 60)
        # No dispo, no avail
        self.assertEqual(days, [])

        demain = date.today() + timedelta(days=1)
        self._create_weekly_dispo(demain - timedelta(days=49))
        self._create_weekly_dispo(demain - timedelta(days=49), centre=None)
        self._create_weekly_dispo(demain - timedelta(days=48), dispo_tel=False)

        days = available_days_for_region(self.neuch, 60)
        # One day each week for the next 31 days
        self.assertEqual(len(days), 5 * 2)
        days_no_centre = available_days_for_region(None, 60)  # tel dispos
        self.assertEqual(len(days_no_centre), 10)
        days_any_centre = available_days_for_region(AnyRegion, 60)
        self.assertEqual(len(days_any_centre), 5 * 2)

    def test_region_availabilities_cancelled_dispos(self):
        """Take exceptions into account (next week dispo is cancelled)."""
        demain = date.today() + timedelta(days=1)
        dispo = self._create_weekly_dispo(demain - timedelta(days=49))
        Occurrence.objects.create(
            event=dispo, orig_start=make_datetime(demain + timedelta(days=7), time(8, 30)),
            cancelled=True,
        )
        days = available_days_for_region(self.neuch, 60)
        self.assertEqual(len(days), 4)

    def test_region_availabilities_fleurier(self):
        """
        For Fleurier: dispos are at least 5 days away from now, to be able to book the room (#312).
        """
        fleurier = Region.objects.get(nom='Fleurier')
        dans4jours = date.today() + timedelta(days=4)
        with mock.patch('agenda.views_public.min_delay_for_region', return_value=timedelta(days=5)):
            self._create_weekly_dispo(dans4jours, centre=fleurier)
            days = available_days_for_region(fleurier, 60)
        self.assertGreater((days[0][0] - date.today()).days, 5)

    def test_region_availabilities_full_days(self):
        """Full days with events are ignored."""
        demain = date.today() + timedelta(days=1)
        self._create_weekly_dispo(demain - timedelta(days=49))
        # Fill the day in two weeks with events
        rzv_cat = EventCategory.objects.get(name='rendez-vous')
        home_cat = EventCategory.objects.get(name='rendez-vous dom')
        start_rzv = make_datetime(demain + timedelta(days=14), time(9, 0))
        # Should work even if centre is not matching dispo centre.
        Event.objects.create(
            person=self.user, centre=None,
            start=start_rzv, end=start_rzv + timedelta(hours=1),
            description='Rendez-vous 9h-10h', category=rzv_cat
        )
        Event.objects.create(
            person=self.user, centre=None,
            start=start_rzv + timedelta(hours=1), end=start_rzv + timedelta(hours=2),
            description='Rendez-vous 10h-11h', category=home_cat
        )
        days = available_days_for_region(self.neuch, 60)
        self.assertEqual(len(days), 4)

    def test_region_availabilities_limit(self):
        """Only consider availabilities from now() + MIN_APPOINTMENT_DELAY."""
        dispo = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=now() + timedelta(minutes=5),
            end=now() + MIN_APPOINTMENT_DELAY + timedelta(minutes=5),
            description='One-time dispo',
            category=self.dispo_cat,
        )
        days = available_days_for_region(self.neuch, 60)
        self.assertEqual(len(days), 0)
        avails = available_hours_for_region_day(self.neuch, date.today(), 60)
        self.assertEqual(len(avails), 0)

        dispo.end += timedelta(hours=1)
        dispo.save()
        days = available_days_for_region(self.neuch, 60)
        self.assertEqual(len(days), 1)
        avails = available_hours_for_region_day(self.neuch, date.today(), 60)
        self.assertEqual(len(avails), 1)

    def test_hours_availabilities_no_region(self):
        """
        When querying day with Region=None (e.g. for rendez-vous tel), each time
        only appears once, even if there are two dispos for the same period.
        """
        demain = date.today() + timedelta(days=1)
        dispo = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=make_datetime(demain, time(8, 30)),
            end=make_datetime(demain, time(10, 30)),
            description='One-time dispo',
            category=self.dispo_cat,
        )
        apresdemain = date.today() + timedelta(days=2)
        # Dispo no tel, should not interfere with this test.
        Event.objects.create(
            person=self.user, centre=self.neuch, dispo_tel=False,
            start=make_datetime(apresdemain, time(8, 30)),
            end=make_datetime(apresdemain, time(10, 30)),
            description='One-time dispo',
            category=self.dispo_cat,
        )
        # Duplicate for another user/centre
        dispo.pk = None
        dispo.person = Utilisateur.objects.create(username='hans', last_name='Martin')
        dispo.centre = Region.objects.par_secteur('cipe').exclude(nom='Neuchâtel').first()
        dispo.save()
        avails = available_hours_for_region_day(None, demain, 60)
        self.assertEqual(len(avails), 2)

    def test_contact_form_validation(self):
        form_data = {
            'nom': 'Abc', 'prenom': 'def', 'date_naissance': '12.2.0000', 'telephone': '9676767',
            'email': 'blah', 'raison': '',
        }
        form = ContactForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['telephone'][0], 'Saisissez un n° de téléphone suisse à 10 chiffres')
        self.assertEqual(form.errors['email'][0], 'Saisissez une adresse de courriel valide.')
        self.assertEqual(form.errors['date_naissance'][0], 'Saisissez une date valide.')
        form_data['date_naissance'] = '12.2.2020'
        form_data['telephone'] = '077 666 23 14'
        form_data['email'] = 'test@example.org'
        form = ContactForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_contact_tel_validation(self):
        form_data = {
            'nom': 'Abc', 'prenom': 'Def', 'date_naissance': '12.2.2020', 'email': '', 'raison': '',
        }
        valid_tels = [
            '077 777 77 77',
            '0777777777',
            '0041777777777',
            '+41777777777',
        ]
        for tel in valid_tels:
            with self.subTest(tel=tel):
                form = ContactForm(data={**form_data, 'telephone': tel})
                self.assertTrue(form.is_valid())
                self.assertEqual(form.cleaned_data['telephone'], '0777777777')

        form = ContactForm(data={**form_data, 'telephone': "077'77'77'77"})
        self.assertFalse(form.is_valid())

    def test_post_day_without_date(self):
        self._create_weekly_dispo(date.today() - timedelta(days=48))
        self.client.post(
            reverse('agenda-public'),
            data={'appointment_wizard-current_step': 'location', 'location-location': str(self.neuch.pk)},
            follow=True
        )
        self.client.post(reverse('agenda-public'), data={'appointment_wizard-current_step': 'day'}, follow=True)

    def test_confirmation_sms(self):
        start_time = localtime(now() + timedelta(days=1)).replace(hour=14, minute=30)
        event = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=start_time, end=start_time + timedelta(hours=1),
            description='From the Web',
            category=EventCategory.objects.get(name='rendez-vous'),
            online_form_data={'telephone': '077 777 77 77'},
        )
        event.refresh_from_db()
        event_time = django_format(start_time, 'l j F')
        with mock.patch('requests.post', side_effect=mocked_requests) as mocked:
            send_confirmation_sms(event, '077 777 77 77')
        self.maxDiff = None
        sms_params = mocked.call_args.kwargs['json']
        self.assertEqual(
            sms_params['sms']['message'],
            {
                'text': f"Rendez-vous Croix-Rouge confirmé: le {event_time} à "
                        f"14:30 à Neuchâtel, Av. du Premier-Mars 2A. Pour annuler: <-short->",
                'pushtype': 'alert',
                'sender': 'CroixRouge',
                'links': [
                    f'https://sap.croix-rouge-ne.ch/agenda/rendez-vous/annuler/{event.cancel_hash}/'
                ],
            }
        )
        self.assertEqual(
            sms_params['sms']['recipients']['gsm'],
            [{'gsmsmsid': '', 'value': '0777777777'}]
        )
        # Tel:
        event.category = EventCategory.objects.get(name='rendez-vous tel')
        event.save()
        with mock.patch('requests.post', side_effect=mocked_requests) as mocked:
            send_confirmation_sms(event, '077 777 77 77')
        self.assertEqual(
            mocked.call_args.kwargs['json']['sms']['message']['text'],
            f"Rendez-vous téléphonique Croix-Rouge confirmé: le {event_time} à 14:30. Pour annuler: <-short->"
        )

    def test_confirmation_email(self):
        start_time = localtime(now() + timedelta(days=1)).replace(hour=14, minute=30)
        event = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=start_time, end=start_time + timedelta(hours=1),
            description='From the Web',
            category=EventCategory.objects.get(name='rendez-vous'),
            online_form_data={'email': 'test@example.org'},
        )
        event.refresh_from_db()
        send_confirmation_email(event)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            "Nous avons le plaisir de vous confirmer votre rendez-vous pour une "
            "consultation petite enfance Croix-Rouge :\n"
            f"- le {django_format(event.start, 'l j F')} à 14:30 à Neuchâtel, "
            "Av. du Premier-Mars 2A.\n"
            f"Lien d’annulation : https://sap.croix-rouge-ne.ch/agenda/rendez-vous/annuler/{event.cancel_hash}/\n\n"
            "À bientôt!"
        )
        mail.outbox = []
        # Tel:
        event.category = EventCategory.objects.get(name='rendez-vous tel')
        event.save()
        send_confirmation_email(event)
        self.assertIn("consultation téléphonique petite enfance", mail.outbox[0].body)

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.smtp.EmailBackend')
    def test_confirmation_email_failure(self):
        start_time = localtime(now() + timedelta(days=1)).replace(hour=14, minute=30)
        event = Event.objects.create(
            person=self.user, centre=self.neuch,
            start=start_time, end=start_time + timedelta(hours=1),
            description='From the Web',
            category=EventCategory.objects.get(name='rendez-vous'),
            online_form_data={'email': 'test@example.org'},
        )
        event.refresh_from_db()
        with mock.patch('socket.create_connection', side_effect=OSError):
            success = send_confirmation_email(event)
        self.assertIs(success, False)

    def test_cancel_event(self):
        start_time = localtime(now() + timedelta(days=1)).replace(hour=14, minute=30)
        famille, _ = self._add_famille_avec_enfant()
        event = Event.objects.create(
            person=self.user, centre=self.neuch, famille=famille,
            start=start_time, end=start_time + timedelta(hours=1),
            description='From the Web',
            category=EventCategory.objects.get(name='rendez-vous'),
            online_form_data={'email': 'test@example.org'},
        )

        cancel_url = reverse('agenda-public-annuler', args=['arbitrary_chain'])
        response = self.client.get(cancel_url)
        self.assertEqual(response.status_code, 404)

        cancel_url = reverse('agenda-public-annuler', args=[event.cancel_hash])
        response = self.client.get(cancel_url)
        self.assertContains(
            response,
            f"Voulez-vous réellement annuler le rendez-vous du {django_format(event.start, 'l j F')} "
            f"à 14:30 à «Neuchâtel» ?"
        )
        event.category = EventCategory.objects.get(name='rendez-vous tel')
        event.save()
        response = self.client.get(cancel_url)
        self.assertEqual(
            response.context['cancel_msg'],
            f"Voulez-vous réellement annuler le rendez-vous téléphonique du {django_format(event.start, 'l j F')} "
            f"à 14:30 ?"
        )
        event.category = EventCategory.objects.get(name='rendez-vous dom')
        event.save()
        response = self.client.get(cancel_url)
        self.assertEqual(
            response.context['cancel_msg'],
            f"Voulez-vous réellement annuler le rendez-vous du {django_format(event.start, 'l j F')} à 14:30 ?"
        )
        # Do the cancellation
        response = self.client.post(cancel_url, data={})
        self.assertRedirects(response, reverse('agenda-public'))
        self.assertFalse(Event.objects.filter(pk=event.pk).exists())
        self.assertEqual(
            famille.prestations.first().faits_courants,
            f"Annulation du rendez-vous du {django_format(start_time, 'j F')} à 14:30 (par la personne elle-même)"
        )


class AgendaPublicProcessusTests(AgendaPublicTestsBase):
    """Test the whole reservation process with posting to the wizard."""
    def setUp(self):
        super().setUp()
        self.dispo = self._create_weekly_dispo(date.today() - timedelta(days=48))

    def _test_wizard_inscription(self, wizard_data, assert_func):
        demain = now() + timedelta(days=1)

        # UTC hour of third availability (10:30-11:30)
        tz_diff = (localtime(self.dispo.start).utcoffset().seconds - localtime(demain).utcoffset().seconds) // 3600
        utc_hour = '%02d' % (self.dispo.start.hour + 2 + tz_diff)
        utc_hour_plus1 = '%02d' % (self.dispo.start.hour + 3 + tz_diff)

        wizard_form_data = {
            'type': {
                'appointment_wizard-current_step': 'type',
                'type-type_app': 'centre',
            },
            'location': {
                'appointment_wizard-current_step': 'location',
                'location-location': str(self.neuch.pk),
            },
            'day': {
                'appointment_wizard-current_step': 'day',
                'day-date_avail': f'{demain.date()}|{self.neuch.pk}',
            },
            'appointment': {
                'appointment_wizard-current_step': 'appointment',
                # Choice value is UTC
                'appointment-avail': f"{str(demain.date())} {utc_hour}:30:00+00:00${str(demain.date())} "
                                     f"{utc_hour_plus1}:30:00+00:00${self.dispo.pk}",
            },
            'contact': {
                'appointment_wizard-current_step': 'contact',
                'contact-nom': 'Simon',
                'contact-prenom': 'Julia',
                'contact-date_naissance': '02.12.2020',
                'contact-telephone': '078 747 86 34',
                'contact-email': 'julia@example.org',
                'contact-raison': '',
            },
        }
        if '__update__' in wizard_data:
            wizard_data.pop('__update__')
            wizard_form_data.update(wizard_data)
        elif wizard_data:
            wizard_form_data = wizard_data

        response = self.client.get(reverse('agenda-public'))
        self.assertContains(
            response,
            '<input type="hidden" name="appointment_wizard-current_step" value="type" '
            'id="id_appointment_wizard-current_step">',
            html=True
        )
        new_event = None
        for step, data_step in wizard_form_data.items():
            if data_step is None:
                continue
            if step == 'contact':
                # Try to perturbate the process (e.g. the page is open in a separate tab, it should NOT reset data)
                self.client.get(reverse('agenda-public'))

            with self.captureOnCommitCallbacks(execute=True):  # SMS sent in on_commit callback
                response = self.client.post(reverse('agenda-public'), data=data_step, follow=True)
            if (
                response.context and 'form' in response.context and response.context['form'].is_bound and
                not response.context['form'].is_valid()
            ):
                self.fail(response.context['form'].errors)

            if step == 'contact':
                new_event = Event.objects.get(online_form_data__isnull=False)
            elif assert_func:
                assert_func(step, response)
        return new_event

    @mock.patch('agenda.views_public.send_confirmation_sms')
    def test_processus_inscription(self, mock_send):
        def assert_after_step(step, response):
            demain = date.today() + timedelta(days=1)
            demain_str = django_format(demain, "l d.m.Y")
            if step == 'location':
                self.assertContains(response, "Choisissez une date à «Neuchâtel»")
                self.assertContains(response, f'{demain_str}</label>')
            elif step == 'day':
                self.assertContains(response, 'Choisissez un rendez-vous possible à «Neuchâtel» :')
                self.assertContains(response, f'{demain_str} 08:30 - 09:30')
                self.assertContains(response, f'{demain_str} 09:30 - 10:30')
                self.assertContains(response, f'{demain_str} 10:30 - 11:30')
            elif step == 'contact':
                self.assertEqual(
                    response.json(),
                    {'result': 'OK', 'redirect_to': f'/agenda/rendez-vous/{new_event.pk}/termine/'}
                )
                response = self.client.get(response.json()['redirect_to'])
                self.assertContains(
                    response,
                    f"Votre inscription pour le <strong>{django_format(demain, 'l j F')} "
                    "à 10:30</strong> a bien été prise en compte."
                )
            else:
                self.assertEqual(response.status_code, 200)

        new_event = self._test_wizard_inscription({}, assert_after_step)
        self.assertEqual(new_event.centre, self.neuch)
        self.assertEqual(new_event.person, self.user)
        self.assertEqual(new_event.category.name, 'rendez-vous')
        # SMS confirmation method called
        mock_send.assert_called_with(new_event, '0787478634')

    @mock.patch('agenda.views_public.send_confirmation_sms')
    def test_processus_inscription_lieu_egal(self, mock_send):
        demain = date.today() + timedelta(days=1)
        self._create_weekly_dispo(demain + timedelta(days=1), centre=None)
        dispo_other = self._create_weekly_dispo(
            demain, centre=Region.objects.par_secteur('cipe').get(nom='Couvet')
        )

        def assert_after_step(step, response):
            if step == 'location':
                demain_str = django_format(demain, "l d.m.Y")
                apres_demain_str = django_format(demain + timedelta(days=1), "l d.m.Y")
                self.assertContains(response, 'Choisissez une date :')
                self.assertContains(response, f'{demain_str} à Neuchâtel')
                # Tel dispos (with no centre) should not appear.
                self.assertNotContains(response, apres_demain_str)
            elif step == 'day':
                demain_str = django_format(demain, "l d.m.Y")
                self.assertContains(response, 'Choisissez un rendez-vous possible à «Neuchâtel» :')
                start_utc = 8 - (localtime(dispo_other.start).utcoffset().seconds // 3600)
                expected = [
                    (f'{demain} {start_utc:02}:30:00+00:00${demain} {start_utc + 1:02}:30:00+00:00${self.dispo.pk}',
                     f'{demain_str} 08:30 - 09:30'
                     ),
                    (f'{demain} {start_utc + 1:02}:30:00+00:00${demain} {start_utc + 2:02}:30:00+00:00$'
                     f'{self.dispo.pk}', f'{demain_str} 09:30 - 10:30'
                     ),
                    (f'{demain} {start_utc + 2:02}:30:00+00:00${demain} {start_utc + 3:02}:30:00+00:00$'
                     f'{self.dispo.pk}', f'{demain_str} 10:30 - 11:30'
                     ),
                ]
                self.assertEqual(response.context['form'].fields['avail'].choices, expected)

        new_event = self._test_wizard_inscription(
            {
                '__update__': True,
                'location': {
                    'appointment_wizard-current_step': 'location',
                    'location-location': 'any',
                },
            },
            assert_after_step
        )
        self.assertEqual(new_event.centre, self.neuch)
        # SMS confirmation method called
        mock_send.assert_called_with(new_event, '0787478634')

    @mock.patch('agenda.views_public.send_confirmation_sms')
    def test_processus_inscription_tel(self, mock_send):
        demain = localtime(now() + timedelta(days=1))
        utc_hour = '08' if demain.utcoffset().seconds == 7200 else '09'
        utc_hour_plus1 = '09' if demain.utcoffset().seconds == 7200 else '10'

        def assert_after_step(step, response):
            if step == 'type':
                self.assertContains(response, "Retour au choix du type de consultation")
                self.assertContains(response, "Choisissez une date pour une consultation par téléphone")
            elif step == 'day':
                self.assertContains(response, "Choisissez un rendez-vous possible pour une consultation par téléphone")
            elif step == 'appointment':
                self.assertContains(
                    response,
                    f"Rendez-vous le {django_format(demain, 'l j F')} à 10:30 pour une consultation par téléphone :"
                )

        wizard_form_data = {
            'type': {
                'appointment_wizard-current_step': 'type',
                'type-type_app': 'tel',
            },
            'day': {
                'appointment_wizard-current_step': 'day',
                'day-date_avail': f'{demain.date()}|none',
            },
            'appointment': {
                'appointment_wizard-current_step': 'appointment',
                # Choice value is UTC
                'appointment-avail': f"{str(demain.date())} {utc_hour}:30:00+00:00${str(demain.date())} "
                                     f"{utc_hour_plus1}:00:00+00:00${self.dispo.pk}",
            },
            'contact': {
                'appointment_wizard-current_step': 'contact',
                'contact-nom': 'Simon',
                'contact-prenom': 'Julia',
                'contact-date_naissance': '02.02.2020',
                'contact-telephone': '078 747 86 34',
                'contact-email': 'julia@example.org',
                'contact-raison': '',
            },
        }
        new_event = self._test_wizard_inscription(wizard_form_data, assert_after_step)
        self.assertEqual(new_event.centre, self.neuch)
        self.assertEqual(new_event.category.name, 'rendez-vous tel'),
        # SMS confirmation method called
        mock_send.assert_called_with(new_event, '0787478634')

    @mock.patch('agenda.views_public.send_confirmation_sms')
    def test_processus_inscription_lien_famille(self, mock_send):
        famille, enfant = self._add_famille_avec_enfant()
        mere = famille.parents()[0]
        enfant.telephone = mere.telephone
        enfant.save()
        form_data = {
            '__update__': True,
            'contact': {
                'appointment_wizard-current_step': 'contact',
                'contact-nom': mere.nom,
                'contact-prenom': mere.prenom,
                'contact-date_naissance': '02.02.2020',
                'contact-telephone': mere.telephone.replace(' ', ''),
                'contact-email': '',
                'contact-raison': '',
            },
        }
        new_event = self._test_wizard_inscription(form_data, None)
        self.assertEqual(new_event.famille, famille)

    def test_no_avail_button_to_tel(self):
        """When no avails for a location, a button allows to skip to tel choices."""
        other_loc = Region.objects.par_secteur('cipe').exclude(nom='Neuchâtel').first()
        wizard_form_data = {
            'type': {
                'appointment_wizard-current_step': 'type',
                'type-type_app': 'centre',
            },
            'location': {
                'appointment_wizard-current_step': 'location',
                'location-location': str(other_loc.pk),
            },
            'day': {
                'appointment_wizard-current_step': 'day',
                'wizard_goto_step': 'day-tel',
            },
        }

        def assert_after_step(step, response):
            if step == 'location':
                self.assertContains(response, "il n’y a plus de rendez-vous disponible")
                self.assertContains(response, "Réserver une consultation téléphonique")
            elif step == 'day':
                # We get the choices from self.neuch dispo
                self.assertEqual(len(response.context['form']['date_avail'].field.choices), 5)
                self.assertEqual(response.context['wizard']['steps']._wizard.storage.get_step_data('location'), {})

        self._test_wizard_inscription(wizard_form_data, assert_after_step)

    def test_post_day_to_appointment_forward(self):
        """Triggered by browser forward navigation."""
        wizard_form_data = {
            '__update__': True,
            'day': {
                'appointment_wizard-current_step': 'day',
                'wizard_goto_step': 'appointment',
            },
            'appointment': None,
            'contact': None,
        }
        # Test no crash
        self._test_wizard_inscription(wizard_form_data, None)

    def test_post_appointment_no_data(self):
        wizard_form_data = {
            '__update__': True,
            'appointment': {
                'appointment_wizard-current_step': 'appointment',
            },
            'contact': None,
        }
        with self.assertRaises(AssertionError):
            self._test_wizard_inscription(wizard_form_data, None)
