"use strict";

function submitForm(form, stepBack='') {
    const action = form.getAttribute('action') || '.';

    var formData = new FormData(form);
    if (stepBack) formData.append('wizard_goto_step', stepBack);
    fetch(action, {
        method: 'post',
        headers: {'X-Requested-With': 'Fetch'},
        body: formData
    }).then(response => {
        if (response.headers.get('Content-Type') == 'application/json') {
            return response.json();
        } else if (response.status == 500) {
            const msg = "Désolé, une erreur s’est produite sur le serveur. Veuillez réessayer dans quelques heures.";
            const p = document.createElement('p');
            p.appendChild(document.createTextNode(msg));
            form.replaceWith(p);
            return '';
        } else {
            return response.text();
        }
    }).then(output => {
        if (!output) return;
        // Final step returns a JSON response with a redirect_to URL.
        if (output.result && output.result == 'OK' && output.redirect_to) {
            window.location.href = output.redirect_to;
            return;
        }
        const parser = new DOMParser();
        const doc = parser.parseFromString(output, "text/html");
        const newForm = doc.querySelector('form');
        if (newForm) {
            form.replaceWith(newForm);
            attachFormHandlers();
            if (document.querySelector('.errorlist')) {
                const parent = document.querySelector('.errorlist').closest('.form-input');
                parent ? parent.scrollIntoView() : document.querySelector('.errorlist').scrollIntoView();
            }
            if (!stepBack) {
                const curStep = document.querySelector('#id_appointment_wizard-current_step').value;
                console.log('pushState curStep: ' + curStep);
                history.pushState({step: curStep}, "");
            }
        };
    }).catch(error => {
        alert("Désolé, une erreur s’est produite.");
        throw new Error(error);
    });
}

function immediateSubmit(ev) {
    ev.preventDefault();
    const input = ev.target.querySelector('input');
    if (input) input.checked = true;
    submitForm(document.querySelector('#reservation'));
}

function attachFormHandlers() {
    const form = document.querySelector('#reservation');
    if (form) {
        // Submit reservation forms through AJAX only.
        form.addEventListener('submit', ev => {
            ev.preventDefault();
            submitForm(form);
        });
    }

    document.querySelectorAll('.immediate-submit').forEach(immediate => {
        immediate.addEventListener('click', immediateSubmit);
        // With screen readers, users don't click but change the radio value
        immediate.addEventListener('change', immediateSubmit);
    });
    document.querySelectorAll('.wizard_goto_step').forEach(btn => {
        btn.addEventListener('click', (ev) => {
            ev.preventDefault();
            const toStep = ev.currentTarget.value;
            submitForm(form, toStep);
        });
    });
}

document.addEventListener("DOMContentLoaded", function() {
    attachFormHandlers();
    window.addEventListener('popstate', (event) => {
        console.log("pop state: " + JSON.stringify(event.state));
        const form = document.querySelector('#reservation');
        if (event.state === null) {
            submitForm(form, 'location');
        } else {
            submitForm(form, event.state.step);
        }
    });
});
