function newEventEditor(ev) {
    let url = document.querySelector('#add-rzv-btn').href;
    let curCell = null;
    let centre = null;
    if (ev.currentTarget.classList.contains('dispo')) {
        ev.stopPropagation();  // don't bubble to underlying event cell
        let elements = document.elementsFromPoint(ev.clientX, ev.clientY).filter(elem => elem.classList.contains('event-cell'));
        curCell = elements[0];
        centre = ev.currentTarget.dataset.location;
    } else {
        if (ev.currentTarget.classList.contains('event-cell')) {
            curCell = ev.currentTarget;
        }
    }
    if (curCell) {
        url += `?day=${curCell.dataset.day}&hour=${curCell.dataset.hour}`;
        if (centre) {
            url += `&centre=${centre}`;
        }
        openFormInModal(url);
    }
}

function attachFamilySelectedEvent(popup) {
    /*
     * When a family is chosen, ask server if family can receive SMS and
     * if yes, show the Send SMS checkbox.
     */
    const familleSelect = popup.querySelector('#id_famille');
    const confLine = popup.querySelector("#sms_confirm_container");
    if (familleSelect && confLine) {
        $(familleSelect).on("select2:select", async (ev) => {
            if (ev.target.value) {
                const url = confLine.dataset.smscheckurl;
                const resp = await fetch(url + `?famille_pk=${ev.target.value}`);
                const result = await resp.json();
                result.has_sms ? confLine.removeAttribute('hidden') : confLine.setAttribute('hidden', true);
            } else confLine.setAttribute('hidden', true);
        });
    }
}

function updateTextNumChars(elem) {
    let t = elem.value.trim();
    const countSpan = document.querySelector('#char_count');
    const max = parseInt(countSpan.dataset.max);

    let re = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    t = t.replace(re, '<-short->');  // short link utility thanks to SMS provider

    countSpan.textContent = t.length;
    if (t.length > max) {
        countSpan.closest('div').classList.add('overflow');
    } else {
        countSpan.closest('div').classList.remove('overflow');
    }
}

document.addEventListener("DOMContentLoaded", () => {
    /*
     * Handle "Centre" and "Infirmiere" agenda selectors on top of Agendas page.
     */
    document.querySelectorAll('#agenda-filter-form select').forEach(select => {
        select.addEventListener('change', ev => {
            location.href = ev.target.value;
            //ev.target.closest('form').submit();
        });
    });
    /*
     * Handle "Journée du" agenda date picker on top of Agendas page.
     */
    const dayPicker = document.querySelector('#agenda-filter-form .vDateField');
    if (dayPicker) {
        dayPicker.addEventListener('change', ev => {
            const dateSplitted = ev.target.value.split('.');
            location.href = ev.target.dataset.url.replace('/0/', '/' + dateSplitted[2] + '/'
                ).replace('/0/', '/' + dateSplitted[1] + '/'
                ).replace('/0/', '/' + dateSplitted[0] + '/');
        });
    }

    const grid = document.querySelector('.agenda-grid');
    if (grid && grid.classList.contains('own-agenda')) {
        document.querySelectorAll('.event-cell').forEach(cell => {
            cell.addEventListener('dblclick', newEventEditor);
        });
    }
    document.querySelectorAll('.event-content.dispo').forEach(dispo => {
        dispo.addEventListener('dblclick', newEventEditor);
    });
    document.querySelector('#popup0').addEventListener('change', ev => {
        // On event delete confirm, toggle the send message form part
        if (ev.target.id == "send_message_cb") {
            ev.target.closest('div').nextElementSibling.querySelectorAll('input, textarea').forEach(el => {
                el.disabled = !ev.target.checked;
            });
            if (ev.target.checked) {
                ev.target.closest('form').querySelector('input[name=msg_method]').checked = true;
                updateTextNumChars(document.querySelector('textarea[name=message]'));
                // Show/hide the char counter
                const counter = document.querySelector('.char_counter');
                if (document.querySelector('input[name="msg_method"]:checked').value == 'sms') counter.classList.remove('hidden');
                else counter.classList.add('hidden');
            }
        }
        if (ev.target.name == 'msg_method') {
            // Show/hide the char counter
            const counter = document.querySelector('.char_counter');
            if (document.querySelector('input[name="msg_method"]:checked').value == 'sms') counter.classList.remove('hidden');
            else counter.classList.add('hidden');
        }
    });
    document.querySelector('#popup0').addEventListener('keyup', ev => {
        if (ev.target.name == "message") {
            updateTextNumChars(ev.target);
        }
    });
    document.querySelector('#popup0').addEventListener('modalLoaded', ev => {
        attachFamilySelectedEvent(ev.target);
    });
});
