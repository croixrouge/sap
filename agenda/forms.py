from datetime import date, datetime, timedelta

from dal import autocomplete
from django import forms
from django.db.models import Q
from django.forms.widgets import Input
from django.urls import reverse
from django.utils.timezone import make_aware, now

from croixrouge.forms import PickDateWidget
from croixrouge.models import Utilisateur
from croixrouge.utils import send_email, send_sms

from .models import Event, EventCategory, Region, Rule


class ColorInput(Input):
    input_type = 'color'


class PermissiveTimeField(forms.TimeField):
    input_formats = ['%H:%M', '%H', '%H.%M']


class EventCategoryForm(forms.ModelForm):
    class Meta:
        model = EventCategory
        fields = '__all__'
        widgets = {
            'default_color': ColorInput,
        }


class CustomRadioSelect(forms.RadioSelect):
    # input before label for styling
    option_template_name = 'widgets/input_option.html'


class SendMessageForm(forms.Form):
    # Only the widgets are initially disabled, not the fields
    msg_method = forms.ChoiceField(choices=(), widget=CustomRadioSelect(attrs={'disabled': True}), required=False)
    message = forms.CharField(widget=forms.Textarea(attrs={'disabled': True}), required=False)

    def __init__(self, *args, famille=None, **kwargs):
        self.mobile = famille.get_mobile_num()
        self.email = famille.get_email()
        super().__init__(*args, **kwargs)
        choices = []
        if self.mobile:
            choices.append(('sms', f"par SMS au numéro {self.mobile}"))
        if self.email:
            choices.append(('email', f"par courriel à l’adresse {self.email}"))
        self.fields['msg_method'].choices = choices

    def has_channel(self):
        return self.mobile or self.email

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('msg_method') and not cleaned_data.get('message', '').strip():
            self.add_error('message', "Vous devez saisir un message.")
        return cleaned_data

    def send(self, subject, event_start):
        if self.cleaned_data['msg_method'] == 'email':
            send_email(subject, self.cleaned_data['message'], self.email)
        elif self.cleaned_data['msg_method'] == 'sms':
            send_sms(
                self.cleaned_data['message'],
                self.mobile,
                f"Erreur d’envoi du message «{subject}» (rendez-vous du {event_start})",
            )


class AgendaEditForm(forms.ModelForm):
    """Formulaire pour édition évènement ou disponibilité."""
    REPET_CHOICES = (
        ('', 'Aucune'),
        ('WEEKLY', 'Chaque semaine'),
        ('BI-WEEKLY', 'Une semaine sur deux'),
        ('DAILY', 'Chaque jour'),
        ('DAILY-open', 'Chaque jour, du lundi au vendredi'),
    )

    date = forms.DateField(label="Date", widget=PickDateWidget)
    person = forms.ModelChoiceField(label="Infirmière", queryset=Utilisateur.objects.none(), required=True)
    centre = forms.ChoiceField(
        label="Centre",
        choices=(),  # Set in __init__
        required=False,  # Handled in clean
    )
    heure_de = PermissiveTimeField(
        label="De",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    heure_a = PermissiveTimeField(
        label="À",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=PickDateWidget, required=False
    )
    sms_confirm = forms.BooleanField(
        label="Envoyer un SMS de confirmation à la famille", initial=False, required=False
    )

    class Meta:
        model = Event
        fields = [
            'date', 'heure_de', 'heure_a', 'person', 'centre', 'dispo_tel', 'famille',
            'description', 'repetition', 'repetition_fin', 'sms_confirm',
        ]
        labels = {
            'dispo_tel': "Aussi dispo pour consultation tél.",
        }
        widgets = {
            'famille': autocomplete.ModelSelect2(
                url='cipe:famille-autocomplete',
                attrs={'data-minimum-input-length': 3, 'data-dropdown-parent': '#popup0'},
            ),
        }

    def __init__(self, *args, category=None, person=None, centre=None, inside_dispo=True, **kwargs):
        if person and not kwargs.get('instance') and category and category.is_rendezvous:
            # Set centre default if dispos for person are only for one centre
            dispo_locs = person.events.filter(
                category__name='dispo'
            ).filter(
                Q(end__isnull=True) | Q(end__lt=now())
            ).values_list('centre', flat=True)
            if len(dispo_locs) == 1:
                kwargs['initial']['centre'] = dispo_locs[0]
        if centre:
            kwargs['initial']['centre'] = centre.pk
        if kwargs.get('instance'):
            kwargs['initial']['centre'] = kwargs['instance'].centre_id or (
                'dom' if kwargs['instance'].category and kwargs['instance'].category.name == 'rendez-vous dom' else (
                    'tel' if kwargs['instance'].category and kwargs['instance'].category.name == 'rendez-vous tel'
                    else ''
                )
            )

        super().__init__(*args, **kwargs)

        self.person = person
        self.centre = centre
        self.inside_dispo = inside_dispo
        self.category = category or self.instance.category
        is_dispo = self.category and self.category.name == 'dispo'
        self.fields['centre'].choices = (
            ('', '----------'),
        ) + tuple((r.pk, r.nom) for r in Region.objects.par_secteur('cipe'))
        if not is_dispo:
            # Complete Centres by À domicile / Par téléphone choices (and handle value in clean)
            self.fields['centre'].choices += (
                (None, '──────────'),
                ('dom', 'À domicile'),
                ('tel', 'Par téléphone'),
            )
            del self.fields['dispo_tel']
            if not self.instance or not self.instance.rule:
                del self.fields['repetition']
                del self.fields['repetition_fin']
        if self.person:
            del self.fields['person']
        else:
            self.fields['person'].queryset = Utilisateur.objects.filter(pk__in=[p.pk for p in ipes()])
        if self.centre:
            self.fields['centre'].disabled = True

        elif is_dispo:
            del self.fields['famille']
            self.fields['date'].label = 'Date début'

    def clean_centre(self):
        centre = self.cleaned_data.get('centre')
        if centre.isdigit():
            return Region.objects.get(pk=centre)
        elif centre == '':
            return None
        return centre

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('repetition_fin') and not cleaned_data.get('repetition'):
            raise forms.ValidationError(
                "Vous ne pouvez pas indiquer une fin de répétition sans valeur de répétition"
            )
        if not cleaned_data.get('description') and not cleaned_data.get('famille'):
            self.add_error('description', "Veuillez saisir une description si aucune famille n’est sélectionnée")

        is_dispo = self.category and self.category.name == 'dispo'
        if is_dispo and not cleaned_data.get('centre') and not cleaned_data.get('dispo_tel'):
            self.add_error('centre', "Une disponibilité doit soit être pour un centre, soit par téléphone.")
        if cleaned_data.get('centre') in ['dom', 'tel']:
            if not is_dispo:
                self.instance.category = self.category = EventCategory.objects.get(
                    name='rendez-vous ' + cleaned_data['centre']
                )
            cleaned_data['centre'] = None
        elif not cleaned_data.get('centre') and cleaned_data.get('famille'):
            self.add_error('centre', "Le centre ne peut pas être vide pour un rendez-vous avec une famille")

        if 'date' not in cleaned_data or 'heure_de' not in cleaned_data or 'heure_a' not in cleaned_data:
            # Do not bother for further checks if basic date-related fields are not valid
            return cleaned_data

        # Do not accept overlapping event for same person and category
        start = make_datetime(cleaned_data['date'], cleaned_data['heure_de'])
        end = make_datetime(cleaned_data['date'], cleaned_data['heure_a'])
        if start >= end:
            self.add_error(None, "L’heure de fin doit être plus grande que l’heure de début")

        person = self.person or cleaned_data.get('person')
        if not person:
            # Further checks need a person defined.
            return cleaned_data
        if self.category and self.category.name.startswith('rendez-vous'):
            categs = EventCategory.objects.filter(name__startswith='rendez-vous')
        else:
            categs = [self.category] if self.category else None
        overlap = [ev for ev in Event.objects.get_for_person(
            person, start + timedelta(seconds=1), end, categories=categs,
            exclude=[self.instance] if self.instance.pk else [],
        ) if not ev.cancelled]
        if overlap:
            self.add_error(None, forms.ValidationError(
                "Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire"
            ))
        elif cleaned_data.get('repetition') and not self.errors:
            # Check occurrences overlap for the next six months (from today)
            temp_instance = self.save(commit=False)
            today = date.today()
            check_start = start.replace(year=today.year, month=today.month, day=today.day)
            in_six_months = date.today() + timedelta(days=180)
            check_end = end.replace(year=in_six_months.year, month=in_six_months.month, day=in_six_months.day)
            occs = temp_instance.get_occurrences(start=check_start, end=check_end)
            for occ in occs:
                overlap = Event.objects.get_for_person(
                    person, occ.start + timedelta(seconds=1), occ.end, categories=[self.category],
                    exclude=[self.instance] if self.instance.pk else [],
                )
                if overlap:
                    raise forms.ValidationError(
                        "Cet événement se chevauche avec un autre déjà existant, veuillez corriger l’horaire"
                    )

        if self.inside_dispo and self.category and self.category.is_rendezvous:
            # Check event is inside an existing dispo
            matching_dispos = Event.objects.get_for_person(
                person, start, end,
                categories=[EventCategory.objects.get(name='dispo')]
            )
            if not matching_dispos or matching_dispos[0].start > start or matching_dispos[0].end < end:
                raise forms.ValidationError(
                    "Vous ne pouvez pas ajouter de rendez-vous en dehors des plages de disponibilité."
                )
            if (not self.fields['centre'].disabled and
                    self.category.name == 'rendez-vous' and  # To exclude rendez-vous dom/tel
                    matching_dispos[0].centre != cleaned_data.get('centre')):
                raise forms.ValidationError(
                    "Le lieu du rendez-vous ne correspond pas au lieu de la disponibilité."
                )
        return cleaned_data

    def save(self, **kwargs):
        if not self.instance.category_id:
            self.instance.category = self.category
        if not self.instance.person_id:
            self.instance.person = self.person
        if not self.instance.centre_id and self.centre:
            self.instance.centre = self.centre
        if set(self.changed_data).intersection({'date', 'heure_de', 'heure_a'}):
            self.instance.start = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_de'])
            self.instance.end = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_a'])
            if self.instance.pk:
                for occ in self.instance.exceptions.all():
                    occ.orig_start = make_datetime(occ.orig_start, self.cleaned_data['heure_de'])
                    occ.save()

        if 'repetition' in self.changed_data:
            repet = self.cleaned_data.get('repetition')
            if repet == 'WEEKLY':
                self.instance.rule, _ = Rule.objects.get_or_create(name='Chaque semaine', frequency='WEEKLY')
            elif repet == 'BI-WEEKLY':
                self.instance.rule, _ = Rule.objects.get_or_create(
                    name='Une semaine sur deux', frequency='WEEKLY', params='interval:2'
                )
            elif repet == 'DAILY':
                self.instance.rule, _ = Rule.objects.get_or_create(name='Chaque jour', frequency='DAILY')
            elif repet == 'DAILY-open':
                self.instance.rule, _ = Rule.objects.get_or_create(
                    name='Chaque jour, du lundi au vendredi', frequency='DAILY',
                    params='byweekday:MO,TU,WE,TH,FR'
                )
        if 'repetition_fin' in self.changed_data:
            repet_fin = self.cleaned_data.get('repetition_fin')
            if repet_fin:
                self.instance.end_recurring_period = make_aware(
                    datetime(repet_fin.year, repet_fin.month, repet_fin.day, 23, 59)
                )
        return super().save(**kwargs)


class AgendaFilterForm(forms.Form):
    region = forms.ChoiceField(label="Centre", required=False)
    personne = forms.ChoiceField(label="Infirmière", required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['region'].choices = [('', '-------')] + [
            (reverse('agenda-centre', args=[region.nom]), region.nom)
            for region in Region.objects.par_secteur('cipe').exclude(rue="")
        ]
        self.fields['personne'].choices = [('', '-------')] + [
            (reverse('person-agenda', args=[pers.username]), pers.nom_prenom) for pers in ipes()
        ]


def ipes():
    return [user for user in Utilisateur._intervenants(groupes=['ipe']) if user.has_role(['IPE'])]


def make_datetime(dt, hour, timezone=None):
    return make_aware(datetime(dt.year, dt.month, dt.day, hour.hour, hour.minute), timezone=timezone)
