from django.db import migrations


def merge_titles(apps, schema_editor):
    Event = apps.get_model('agenda', 'Event')
    for ev in Event.objects.all():
        if ev.description:
            ev.description = f'{ev.title}\n{ev.description}'
        else:
            ev.description = ev.title
        ev.save()


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0004_manage_dispos_permission'),
    ]

    operations = [
        migrations.RunPython(merge_titles),
    ]
