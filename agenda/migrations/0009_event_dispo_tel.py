from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0008_event_online_form_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='dispo_tel',
            field=models.BooleanField(default=True, verbose_name='Dispo pour consultation tél. (IPE)'),
        ),
    ]
