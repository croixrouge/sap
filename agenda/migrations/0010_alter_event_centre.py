from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0009_event_dispo_tel'),
        ('croixrouge', '0048_remove_region_cipe_spe_sifp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='centre',
            field=models.ForeignKey(
                blank=True, limit_choices_to={'secteurs__contains': ['cipe']}, null=True,
                on_delete=models.deletion.PROTECT, to='croixrouge.region', verbose_name='Centre'
            ),
        ),
    ]
