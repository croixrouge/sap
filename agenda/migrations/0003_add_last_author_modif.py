from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agenda', '0002_add_categories'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='last_author',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='event',
            name='last_modif',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
