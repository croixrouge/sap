from django.db import migrations


def create_categories(apps, schema_editor):
    EventCategory = apps.get_model('agenda', 'EventCategory')
    EventCategory.objects.create(name='dispo', default_color='#ddd')
    EventCategory.objects.create(name='rendez-vous', default_color='#5ab0e2')


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_categories),
    ]
