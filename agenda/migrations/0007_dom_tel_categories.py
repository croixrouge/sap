from django.db import migrations


def create_categories(apps, schema_editor):
    EventCategory = apps.get_model('agenda', 'EventCategory')
    EventCategory.objects.create(name='rendez-vous tel', default_color='#adeded')
    EventCategory.objects.create(name='rendez-vous dom', default_color='#ca9cf1')


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0006_remove_event_title'),
    ]

    operations = [
        migrations.RunPython(create_categories),
    ]
