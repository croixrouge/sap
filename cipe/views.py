import io
from collections import defaultdict
from datetime import timedelta
from operator import attrgetter
from urllib.parse import urlparse

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.mail import EmailMessage
from django.db.models import F, Max, Prefetch, Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.urls import resolve, reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import (
    CreateView,
    DetailView,
    FormView,
    ListView,
    TemplateView,
    UpdateView,
    View,
)

from archive.models import ArchiveCIPE
from croixrouge.export import OpenXMLExport
from croixrouge.models import Personne
from croixrouge.views import (
    BasePDFView,
    BasePrestationGeneraleEtPersonnelle,
    CreateUpdateView,
    DeleteView,
    FamilleUpdateViewBase,
    JournalAccesMixin,
)
from croixrouge.views import PersonneCreateView as PersonneCreateViewBase

from . import forms
from .models import (
    FamilleCIPE,
    GeneralDoc,
    GeneralDocCateg,
    PrestationCIPE,
    PrestationGen,
    PsychoEval,
    PsychoItem,
    SuiviEnfant,
    ThemeDoc,
    ThemeSante,
)
from .pdf import CoordonneesFamillePdf, PercentilePdf
from .percentile import GraphEmpty, GraphPercentilePoids, GraphPercentileTaille

MSG_ACCESS_DENIED = "Vous n’avez pas la permission d’accéder à cette page."
MSG_READ_ONLY = "Vous n'avez pas la permission de modifier cette page"


class CIPERequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleCIPE, pk=kwargs['pk'])
        self.check_access(request)
        return super().dispatch(request, *args, **kwargs)

    def check_access(self, request):
        if not self.famille.can_view(request.user):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        self.readonly = not self.famille.can_edit(request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.readonly:
            messages.info(self.request, MSG_READ_ONLY)
        context['can_edit'] = not self.readonly
        context['famille'] = self.famille
        return context


class FamilleListView(PermissionRequiredMixin, ListView):
    template_name = 'cipe/famille_list.html'
    permission_required = 'cipe.view_famillecipe'
    model = FamilleCIPE
    base_queryset = FamilleCIPE.objects.actives().select_related('region')
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        request.session['current_app'] = 'cipe'
        self.filter_form = forms.FamilleFilterForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        familles = self.base_queryset
        if not self.request.user.has_role(['IPE']):
            # Les familles sap ne sont pas visibles pour les utilisateurs non IPE
            familles = familles.filter(sap=False)
        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles.prefetch_related(
            Prefetch(
                'membres',  to_attr='enfants',
                queryset=Personne.objects.filter(role__nom='Enfant suivi').avec_age().select_related(
                    'role', 'suivienfant'
                ).annotate(
                    derniere_prest=Max("prestationcipe__date", filter=Q(prestationcipe__manque=False)),
                ).order_by('-date_naissance')
            )
        )

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'form': self.filter_form,
            'title': "Familles CIPE",
            'archives': False,
        }

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('export') == '1':
            export = OpenXMLExport(sheet_title='Liste familles')
            export.write_line(
                [
                    'Nom', 'Rue', 'NPA Localite', 'Centre', 'Tél. famille',
                    'Parent 1', 'Tél./courriel parent 1', 'Parent 2','Tél./courriel parent 2',
                ],
                bold=True,
                col_widths=[20, 30, 25, 20, 18, 24, 24, 24, 24]
            )
            for famille in self.get_queryset():
                parents = famille.parents()[:2]
                parent1 = parents[0] if parents else None
                parent2 = parents[1] if len(parents) > 1 else None
                export.write_line([
                    famille.nom, famille.rue, " ".join([famille.npa, famille.localite]),
                    famille.region.nom if famille.region else '',
                    famille.telephone,
                    f'{parent1.nom_prenom}' if parent1 else '',
                    "/".join([val for val in [parent1.telephone, parent1.email] if val]) if parent1 else '',
                    f'{parent2.nom_prenom}' if parent2 else '',
                    "/".join([val for val in [parent2.telephone, parent2.email] if val]) if parent2 else '',
                ])
            return export.get_http_response('liste_familles_cipe.xlsx')
        return super().render_to_response(context, **response_kwargs)


class SuivisArchivesView(PermissionRequiredMixin, ListView):
    template_name = 'cipe/suivis_archives_list.html'
    permission_required = 'cipe.can_archive'
    model = ArchiveCIPE
    base_queryset = ArchiveCIPE.objects.all()
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        request.session['current_app'] = 'cipe'
        self.filter_form = forms.ArchiveCIPEFilterForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        archives = self.base_queryset
        if self.filter_form.is_bound and self.filter_form.is_valid():
            archives = self.filter_form.filter(archives)
        else:
            archives = archives.none()
        return archives

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'form': self.filter_form,
            'title': "Archives suivis CIPE",
        }


class FamilleCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'croixrouge/famille_edit.html'
    form_class = forms.FamilleCreateForm
    permission_required = 'cipe.add_famillecipe'
    action = 'Nouvelle famille'

    def get_success_url(self):
        return reverse('cipe:famille-edit', args=[self.object.pk])


class FamilleUpdateView(CIPERequiredMixin, JournalAccesMixin, FamilleUpdateViewBase):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleCIPE
    form_class = forms.FamilleForm
    success_url = reverse_lazy('cipe:famille-list')

    @property
    def delete_url(self):
        if self.object.can_be_deleted(self.request.user):
            return reverse('cipe:famille-delete', args=[self.object.pk])


class FamilleDeleteView(CIPERequiredMixin, DeleteView):
    model = FamilleCIPE
    success_url = reverse_lazy('cipe:famille-list')

    def form_valid(self, form):
        famille = self.object
        if not famille.can_be_deleted(self.request.user):
            raise PermissionDenied("Vous n’avez pas la permission de supprimer cette famille.")
        msg = f"La famille {famille} a bien été supprimée"
        for doc in famille.documents.all():
            doc.delete()
        resp = super().form_valid(form)
        messages.success(self.request, msg)
        return resp


class FamilleHasSMSView(View):
    """Simple API to know if family can be reached with SMS."""
    def get(self, request, *args, **kwargs):
        famille = get_object_or_404(FamilleCIPE, pk=request.GET.get('famille_pk', -1))
        return JsonResponse({'result': 'OK', 'has_sms': bool(famille.get_mobile_num())})


class PersonneCreateView(CIPERequiredMixin, PersonneCreateViewBase):
    form_class = forms.PersonneForm


class CoordonneesPDFView(BasePDFView):
    obj_class = FamilleCIPE
    pdf_class = CoordonneesFamillePdf


class EnfantPerinatalView(CIPERequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'cipe/enfant_perinatal.html'
    model = SuiviEnfant
    form_class = forms.PerinatalForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self):
        return self.model.objects.select_related('personne__famille').get(
            personne__famille__pk=self.kwargs['pk'], personne__pk=self.kwargs['pk_pers']
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'enfant': self.object.personne,
        }

    def get_success_url(self):
        return self.request.path


class LieuAccouchementAutocompleteView(View):
    def get(self, request, *args, **kwargs):
        values = []
        q = request.GET.get('q')
        if q:
            values = [
                {'label': res, 'value': res}
                for res in SuiviEnfant.objects.filter(
                    lieu_acc__icontains=q).values_list('lieu_acc', flat=True).distinct()
            ]
        return JsonResponse(values, safe=False)


class EnfantPsychoView(CIPERequiredMixin, TemplateView):  # FormView):
    template_name = 'cipe/psycho_dev.html'
    # form_class = PsychoForm

    def get_context_data(self, **kwargs):
        enfant = get_object_or_404(Personne, famille=self.kwargs['pk'], pk=self.kwargs['pk_pers'])
        existings = enfant.suivienfant.psychoeval_set.all().annotate(
            age_ref=F('item__age_ref')
        ).select_related('item').order_by('-item__age_ref', 'item__order')
        existing_acq_items = [ev.item.pk for ev in existings if ev.acquisition == 'acq']
        if enfant.date_naissance is not None:
            possible_items = PsychoItem.objects.filter(
                age_ref__lte=enfant.age_jours()
            ).exclude(
                pk__in=existing_acq_items
            ).order_by('-age_ref', 'order')
        else:
            possible_items = []
        return {
            **super().get_context_data(**kwargs),
            'enfant': enfant,
            # Mixed items sorted by age_ref
            'items': sorted(list(existings) + list(possible_items), key=attrgetter('age_ref'), reverse=True),
        }


class EnfantPsychoEditView(CIPERequiredMixin, FormView):
    template_name = 'cipe/partial_psycho_form.html'
    form_class = forms.PsychoForm

    def dispatch(self, *args, **kwargs):
        # Called with either pk/pkpers (new items) or pk_item (edit existing)
        if 'pk_item' not in self.kwargs:
            self.suivi = get_object_or_404(
                SuiviEnfant.objects.filter(personne__famille__pk=kwargs['pk']), personne_id=kwargs['pk_pers']
            )
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        if 'pk_item' in self.kwargs:
            kwargs.update({'instance': get_object_or_404(PsychoEval, pk=self.kwargs['pk_item'])})
        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        if 'pk_item' not in self.kwargs:
            initial['item'] = self.request.GET.get('itemid')
        return initial

    def form_valid(self, form):
        if not form.instance.suivi_id:
            form.instance.suivi = self.suivi
        form.instance.auteur = self.request.user
        self.object = form.save()
        return render(self.request, 'cipe/partial_psycho_line.html', {
            'famille': self.object.suivi.personne.famille,
            'item': self.object,
        })


class PrestationListView(UserPassesTestMixin, JournalAccesMixin, ListView):
    model = PrestationCIPE
    template_name = 'cipe/prestation_list.html'
    paginate_by = 15

    def test_func(self):
        self.famille = get_object_or_404(FamilleCIPE, pk=self.kwargs['pk'])
        self.enfant = None
        if 'pk_pers' in self.kwargs:
            self.enfant = get_object_or_404(Personne, pk=self.kwargs['pk_pers'])
        return self.famille.can_view(self.request.user)

    def get_queryset(self):
        self.marquants = self.request.GET.get('faits') == 'marq'
        exclude_params = {}
        if self.marquants:
            exclude_params = {'faits_marquants': ''}
        if 'pk_pers' in self.kwargs:
            query = self.enfant.prestationcipe_set.all().exclude(**exclude_params).union(
                self.famille.prestationcipe_set.exclude(**exclude_params).filter(type_consult='res_fam')
            ).order_by('-date')
        else:
            # Prestations pour toute la famille
            query = self.famille.prestationcipe_set.all().exclude(**exclude_params).order_by('-date')
        return query

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'famille': self.famille,
            'enfant': self.enfant,
            'suivi': self.enfant.suivienfant,
            'marquants': self.marquants,
            'rendez_vous': self.famille.event_set.filter(start__gt=timezone.now()).order_by('start'),
        }


class PrestationBaseView(PermissionRequiredMixin):
    model = PrestationCIPE
    form_class = forms.PrestationForm
    permission_required = 'cipe.add_prestationcipe'
    template_name = 'cipe/prestation_edit.html'

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille, 'enfant': self.enfant}

    def get_form_kwargs(self):
        themes_abordes = defaultdict(list)
        for prest in PrestationCIPE.objects.filter(
            enfant=self.enfant
        ).exclude(themes=None).order_by('date').prefetch_related('themes'):
            for theme in prest.themes.all():
                themes_abordes[theme.pk].append(prest.date)
        return {
            **super().get_form_kwargs(), 'user': self.request.user,
            'famille': self.famille, 'themes_abordes': themes_abordes,
        }

    def get_success_url(self):
        if self.enfant:
            return reverse('cipe:prestation-enfant-list', args=[self.famille.pk, self.enfant.pk])
        else:
            return self.request.POST.get('referer', reverse('cipe:famille-edit', args=[self.famille.pk]))


class PrestationCreateView(PrestationBaseView, CreateView):

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleCIPE, pk=self.kwargs['pk'])
        self.enfant = get_object_or_404(self.famille.membres, pk=self.kwargs['pk_pers'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.famille = self.famille
        if not form.cleaned_data.get('type_consult') == 'res_fam':
            form.instance.enfant = self.enfant
        form.instance.auteur = self.request.user
        return super().form_valid(form)


class PrestationUpdateView(PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleCIPE, pk=self.kwargs['pk'])
        prest = self.get_object()
        if not prest.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les droits pour modifier cette prestation.")
        self.enfant = prest.enfant
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        if self.request.method == 'GET':
            initial['referer'] = self.request.META.get('HTTP_REFERER')
        return initial

    def form_valid(self, form):
        if form.cleaned_data.get('type_consult') != 'res_fam' and form.instance.enfant is None:
            if self.enfant is None and self.request.POST.get('referer'):
                form.instance.enfant_id = resolve(urlparse(self.request.POST['referer']).path).kwargs.get('pk_pers')
                if not form.instance.enfant_id:
                    enfants = self.famille.membres_suivis()
                    if len(enfants) == 1:
                        form.instance.enfant = enfants[0]
            else:
                form.instance.enfant = self.enfant
        return super().form_valid(form)


class PrestationDeleteView(DeleteView):
    model = PrestationCIPE
    pk_url_kwarg = 'obj_pk'

    def form_valid(self, form):
        prest = self.object
        if not prest.can_delete(self.request.user):
            raise PermissionDenied("Vous n’avez pas les droits pour supprimer cette prestation.")
        if prest.enfant:
            self.success_url = reverse('cipe:prestation-enfant-list', args=[self.kwargs['pk'], prest.enfant_id])
        else:
            self.success_url = self.request.META.get(
                'HTTP_REFERER', reverse('cipe:famille-edit', args=[prest.famille.pk])
            )
        return super().form_valid(form)


class PrestationDeleteFile(View):
    def post(self, request, *args, **kwargs):
        prest = get_object_or_404(PrestationCIPE, pk=kwargs['obj_pk'])
        try:
            to_del = [fichier for fichier in prest.fichiers if fichier.name == request.POST.get('filename')][0]
        except IndexError:
            pass
        else:
            prest.fichiers = [fichier for fichier in prest.fichiers if fichier.name != request.POST.get('filename')]
            prest.save()
            to_del.delete()
        return JsonResponse({'result': 'OK'})


class PrestationGenEditView(PermissionRequiredMixin, CreateUpdateView):
    model = PrestationGen
    form_class = forms.PrestationGenForm
    permission_required = 'cipe.add_prestationgen'
    template_name = 'cipe/prestation_gen_edit.html'
    success_url = reverse_lazy('cipe:prestations-utilisateur')

    @property
    def delete_url(self):
        if self.object and self.object.can_delete(self.request.user):
            return reverse('cipe:prestation-gen-delete', args=[self.object.pk])

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def form_valid(self, form):
        if not form.instance.auteur_id:
            form.instance.auteur = self.request.user
        return super().form_valid(form)


class PrestationGenDeleteView(DeleteView):
    model = PrestationGen
    success_url = reverse_lazy('cipe:prestations-utilisateur')


class PrestationUtilisateurListView(PermissionRequiredMixin, BasePrestationGeneraleEtPersonnelle):
    """Liste des prestations par mois pour l'utilisateur connecté."""
    permission_required = 'cipe.view_prestationcipe'
    template_name = 'cipe/prestation_personnelle.html'

    def get_queryset(self):
        if (self.request.GET.get('prest_choice') == "gen" and
                self.request.user.has_perm('cipe.change_prestationgen')):
            self.show_part = True
            return PrestationGen.objects.filter(
                date__gte=self.dfrom, date__lte=self.dto
            ).order_by('date')
        else:
            self.show_part = False
            user = self.request.user
            return sorted(
                list(PrestationCIPE.objects.filter(
                    auteur=user, date__gte=self.dfrom, date__lte=self.dto
                )) +
                list(PrestationGen.objects.filter(
                    participants=user, date__gte=self.dfrom, date__lte=self.dto
                )),
                key=lambda p: p.date,
                reverse=True
            )

    def get_context_data(self, *args, **kwargs):
        total_gen = timedelta(seconds=sum(
            prest.duree.total_seconds() for prest in self.object_list if prest.typ == 'general' and prest.duree
        ))
        total_fam = timedelta(seconds=sum(
            prest.duree.total_seconds() for prest in self.object_list if prest.typ == 'famille' and prest.duree
        ))
        return {
            **super().get_context_data(**kwargs),
            'show_part': self.show_part,
            'total_gen': total_gen,
            'total_fam': total_fam,
        }


class PercentileView(DetailView):
    model = Personne
    template_name = 'cipe/percentile.html'

    def get(self, request, *args, **kwargs):
        self.enfant = self.get_object()
        if not self.enfant.date_naissance:
            messages.error(request, "Les percentiles ne peuvent pas être affichés sans date de naissance")
            return HttpResponseRedirect(self.enfant.edit_url)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        graph_class = {'poids': GraphPercentilePoids, 'taille': GraphPercentileTaille}.get(self.kwargs['typ'])
        try:
            graph = graph_class(enfant=self.enfant)
        except GraphEmpty:
            graph = None
        return {
            **super().get_context_data(**kwargs),
            'graph': graph,
            'enfant': self.enfant,
            'typ': self.kwargs['typ'],
        }


class PercentilePoidsPDFView(BasePDFView):
    obj_class = Personne
    pdf_class = PercentilePdf
    produce_kwargs = {'typ': 'poids'}


class PercentileTaillePDFView(BasePDFView):
    obj_class = Personne
    pdf_class = PercentilePdf
    produce_kwargs = {'typ': 'taille'}


class PercentileByEmailView(FormView):
    template_name = 'cipe/percentile-email.html'
    form_class = forms.SendByEmailForm

    def dispatch(self, *args, **kwargs):
        self.enfant = get_object_or_404(Personne, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'enfant': self.enfant}

    def get_percentile_content(self, typ):
        temp = io.BytesIO()
        pdf = PercentilePdf(temp, self.enfant, typ=typ)
        pdf.produce()
        temp.seek(0)
        return temp.getvalue()

    def form_valid(self, form):
        email_template_name = 'cipe/email/percentiles.txt'
        body = loader.render_to_string(email_template_name, {
            'prenom': self.enfant.prenom,
            'from': self.request.user.nom_prenom,
        }, using='django-text')
        recipients = form.recipients()
        email = EmailMessage(
            'Percentiles Croix-Rouge',
            body,
            to=recipients,
            reply_to=[self.request.user.email],
        )

        try:
            poids = self.get_percentile_content('poids')
        except GraphEmpty:
            pass
        else:
            email.attach('poids.pdf', poids, 'application/pdf')
        try:
            taille = self.get_percentile_content('taille')
        except GraphEmpty:
            pass
        else:
            email.attach('taille.pdf', taille, 'application/pdf')
        email.send()
        if len(recipients) > 1:
            msg = "Le message a bien été envoyé aux adresses: %s" % ', '.join(recipients)
        else:
            msg = f"Le message a bien été envoyé à {recipients[0]}"
        messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('cipe:prestation-enfant-list',
                                            args=[self.enfant.famille_id, self.enfant.pk]))

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'enfant': self.enfant,
        }


class ThemeSanteListView(PermissionRequiredMixin, ListView):
    permission_required = 'cipe.view_famillecipe'
    template_name = 'cipe/themesante_list.html'
    model = ThemeSante

    def get_queryset(self):
        return super().get_queryset().order_by('nom')


class ThemeSanteEditView(PermissionRequiredMixin, CreateUpdateView):
    template_name = 'cipe/themesante_edit.html'
    model = ThemeSante
    permission_required = 'cipe.view_famillecipe'
    fields = '__all__'
    success_url = reverse_lazy('cipe:themesante-list')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'read_only': not self.request.user.has_perm('cipe.add_themesante'),
            'document_list': self.object.documents.all().order_by('titre') if self.object else [],
        }


class ThemeDocListView(PermissionRequiredMixin, ListView):
    permission_required = 'cipe.view_famillecipe'
    template_name = 'cipe/themedoc_list.html'
    model = ThemeDoc
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.filter_form = forms.ThemeDocFilterForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        docs = super().get_queryset()
        if self.filter_form.is_bound and self.filter_form.is_valid():
            docs = self.filter_form.filter(docs)
        return docs.prefetch_related('themes').order_by('titre')

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'form': self.filter_form,
        }


class ThemeDocEditView(PermissionRequiredMixin, CreateUpdateView):
    template_name = 'cipe/themedoc_edit.html'
    model = ThemeDoc
    permission_required = 'cipe.add_themedoc'
    form_class = forms.ThemeDocForm
    success_url = reverse_lazy('cipe:themedoc-list')

    def delete_url(self):
        if self.request.user.has_perm('cipe.delete_themedoc'):
            return reverse('cipe:themedoc-delete', args=[self.object.pk])


class ThemeDocDeleteView(PermissionRequiredMixin, DeleteView):
    model = ThemeDoc
    permission_required = 'cipe.delete_themedoc'
    success_url = reverse_lazy('cipe:themedoc-list')

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, f"Le document {self.object.titre} a bien été supprimé.")
        return response


class GeneralDocListView(PermissionRequiredMixin, ListView):
    permission_required = 'cipe.view_generaldoc'
    template_name = 'cipe/generaldoc_list.html'
    model = GeneralDoc

    def get_queryset(self):
        return GeneralDoc.objects.all().select_related('categorie').order_by('categorie__nom', 'titre')


class GeneralDocEditView(PermissionRequiredMixin, CreateUpdateView):
    template_name = 'cipe/themedoc_edit.html'
    model = GeneralDoc
    permission_required = 'cipe.add_generaldoc'
    success_url = reverse_lazy('cipe:generaldoc-list')

    def get_form_class(self):
        return forms.GeneralDocFormAddCateg if self.request.user.has_perm('cipe.add_generaldoccateg') \
            else forms.GeneralDocForm

    def delete_url(self):
        if self.request.user.has_perm('cipe.delete_generaldoc'):
            return reverse('cipe:generaldoc-delete', args=[self.object.pk])


class GeneralDocDeleteView(PermissionRequiredMixin, DeleteView):
    model = GeneralDoc
    permission_required = 'cipe.delete_generaldoc'
    success_url = reverse_lazy('cipe:generaldoc-list')

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, f"Le document {self.object.titre} a bien été supprimé.")
        return response


class GeneralDocCategEditView(PermissionRequiredMixin, UpdateView):
    template_name = 'croixrouge/form_in_popup.html'
    model = GeneralDocCateg
    fields = ['nom']
    permission_required = 'cipe.change_generaldoccateg'
    success_url = reverse_lazy('cipe:generaldoc-list')
    titre_formulaire = "Édition de catégorie de documents"
