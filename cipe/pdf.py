import subprocess
import tempfile
from functools import partial
from pathlib import Path

import extract_msg
from django.utils.text import slugify
from pypdf import PdfReader
from reportlab.lib.units import cm
from reportlab.platypus import Image, PageBreak, Paragraph, Spacer

from archive.pdf_utils import PDFMergerBase
from croixrouge.pdf import BaseCroixrougePDF, CleanParagraph, PageNumCanvas, RawParagraph
from croixrouge.utils import format_d_m_Y, format_duree

from .models import FamilleCIPE
from .percentile import GraphPercentilePoids, GraphPercentileTaille


class CoordonneesFamillePdf(BaseCroixrougePDF):
    title = "Informations"

    def get_filename(self):
        return '{}_coordonnees.pdf'.format(slugify(self.instance.nom))

    def produce(self):
        famille = self.instance
        self.set_title('Famille {} - {}'.format(famille.nom, self.title))

        # Parents
        self.story.append(Paragraph('Parents', self.style_sub_title))
        data = self.formate_persons(famille.parents())
        if data:
            self.story.append(self.get_table(data, columns=[2, 7, 2, 7], before=0, after=0))
        else:
            self.story.append(Paragraph('-', self.style_normal))

        # Situation matrimoniale
        data = [
            ['Situation matrimoniale: {}'.format(famille.get_statut_marital_display()),
             'Autorité parentale: {}'.format(famille.get_autorite_parentale_display())],
        ]
        self.story.append(self.get_table(data, columns=[9, 9], before=0, after=0))

        # Personnes significatives
        autres_parents = list(famille.autres_parents())
        if autres_parents:
            self.story.append(Paragraph('Personne-s significative-s', self.style_sub_title))
            data = self.formate_persons(autres_parents)
            self.story.append(self.get_table(data, columns=[2, 7, 3, 6], before=0, after=0))
            if len(autres_parents) > 2:
                self.story.append(PageBreak())

        # Enfants suivis
        self.write_paragraph(
            "Enfant(s)",
            '<br/>'.join(self.enfant_data(enfant) for enfant in famille.membres_suivis())
        )
        # Réseau
        self.story.append(Paragraph("Réseau", self.style_sub_title))
        data = []
        for enfant in famille.membres_suivis():
            for contact in enfant.reseaux.all():
                data.append([
                    enfant.prenom,
                    f'{contact} ({contact.contact})' if contact.contact else str(contact)
                ])
            if enfant.suivienfant.pediatre:
                data.append([
                    enfant.prenom,
                    '{} ({})'.format(enfant.suivienfant.pediatre, enfant.suivienfant.pediatre.contact)
                ])
        if data:
            self.story.append(self.get_table(data, columns=[2, 16], before=0, after=0))

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class DossierEnfantPdf(BaseCroixrougePDF):
    title = "Dossier"
    excluded_fields = ['id', 'psychoeval', 'personne']

    def get_filename(self):
        return '{}_dossier.pdf'.format(slugify(self.instance.nom))

    def produce(self):
        suivi = self.instance.suivienfant
        P = partial(Paragraph, style=self.style_normal)
        Pbold = partial(Paragraph, style=self.style_bold)

        self.set_title(f'Dossier {self.instance.nom} {self.instance.prenom}')

        fields = [
            field for field in suivi._meta.get_fields()
            if field.name not in self.excluded_fields
        ]
        for field in fields:
            if hasattr(suivi, f'get_{field.name}_display'):
                field_value = getattr(suivi, f'get_{field.name}_display')()
            else:
                field_value = getattr(suivi, field.name)
            if field_value in [None, '']:
                continue
            self.story.append(Pbold(field.verbose_name))
            self.story.append(P(str(field_value)))
            self.story.append(Spacer(0, 0.5 * cm))
        return self.doc.build(
            self.story, onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
        )


class PercentilePdf(BaseCroixrougePDF):
    title = ''

    def __init__(self, *args, typ=None, **kwargs):
        self.typ = typ
        super().__init__(*args, **kwargs)

    def get_filename(self):
        return '{}_percentile.pdf'.format(slugify(self.instance.nom))

    def produce(self):
        graph_class = {'poids': GraphPercentilePoids, 'taille': GraphPercentileTaille}.get(self.typ)
        im = Image(graph_class(self.instance).get_buffer())
        self.story.append(im)
        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class MessagePrestationPdf(BaseCroixrougePDF):
    title = "Message"

    def __init__(self, tampon, obj, fichier, *args, **kwargs):
        self.fichier = fichier
        super().__init__(tampon, obj, *args, **kwargs)

    def get_filename(self):
        return 'message.pdf'

    def produce(self):
        self.set_title(self.title)
        with extract_msg.Message(self.fichier.path) as msg:
            P = partial(Paragraph, style=self.style_normal)
            Pbold = partial(Paragraph, style=self.style_bold)

            msg_headers = [
                [Pbold('De:'), P(msg.sender)],
                [Pbold('À:'), P(msg.to)],
            ]
            if msg.cc:
                msg_headers.append([Pbold('CC:'), P(msg.cc)])
            if msg.date:
                msg_headers.append([Pbold('Date:'), P(msg.date)])
            msg_headers.append([Pbold('Sujet:'), P(msg.subject)])
            self.story.append(self.get_table(msg_headers, [3, 15]))
            self.story.append(Pbold('Message:'))
            self.story.append(RawParagraph(msg.body, style=self.style_normal))
        return self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
            canvasmaker=PageNumCanvas
        )


class _JournalPdf(BaseCroixrougePDF):
    """Contient tout le journal de l'enfant sans les pièces jointes"""

    title = "Journal de bord"

    def __init__(self, tampon, enfant, *args, **kwargs):
        enfant.famille.__class__ = FamilleCIPE
        self.enfant = enfant
        super().__init__(tampon, enfant, *args, **kwargs)

    def get_filename(self):
        return '{}_{}_journal.pdf'.format(
            slugify(self.instance.nom),
            slugify(self.instance.prenom),
        )

    def get_title(self):
        return f'{self.instance.nom} {self.instance.prenom} - {self.title}'

    def produce(self):
        self.set_title(self.get_title())

        self.style_bold.spaceAfter = 0.2*cm
        self.style_italic.spaceBefore = 0.2 * cm
        self.style_italic.spaceAfter = 0.7 * cm

        for prest in self.enfant.prestationcipe_set.all().order_by("date"):
            self.story.append(
                Paragraph(
                    f"{prest.get_type_consult_display()} / {prest.get_mode_consult_display()}",
                    self.style_normal,
                )
            )
            self.story.append(CleanParagraph(prest.faits_courants, self.style_normal))
            self.story.append(CleanParagraph(prest.faits_marquants, self.style_bold))
            poids_taille = []
            if prest.poids is not None:
                poids_taille.append(f'Poids (kg) : {prest.poids:.2f}')
            if prest.taille is not None:
                poids_taille.append(f'Taille (cm) : {prest.taille:.2f}')
            if poids_taille:
                self.story.append(Paragraph(' - '.join(poids_taille), self.style_normal))

            if prest.a_domicile_prest:
                self.story.append(Paragraph(
                    f'Prestation à domicile ({prest.get_a_domicile_prest_display()})', self.style_normal
                ))
            if prest.sans_rendezvous:
                self.story.append(Paragraph('Sans rendez-vous', self.style_normal))
            if prest.manque:
                self.story.append(Paragraph('Rendez-vous manqué', self.style_normal))

            themes = ' / '.join([str(t) for t in prest.themes.all()])
            if themes:
                self.story.append(Paragraph(f'Thèmes abordés : {themes}', self.style_normal))

            num_pieces_jointes = len(prest.fichiers)
            if num_pieces_jointes:
                self.story.append(
                    Paragraph(
                        f'{num_pieces_jointes} pièces jointes'
                        if num_pieces_jointes > 1
                        else f'{num_pieces_jointes} pièce jointe',
                        self.style_normal,
                    )
                )

            self.story.append(
                Paragraph('{} - {} ({})'.format(
                    prest.auteur,
                    format_d_m_Y(prest.date),
                    format_duree(prest.duree)
                ), self.style_italic)
            )

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer, onLaterPages=self.draw_footer,
            canvasmaker=PageNumCanvas
        )


class JournalPdf(PDFMergerBase):
    """Ajoute les pièces jointes au journal de l'enfant"""

    title = "Journal de bord"

    def __init__(self, tampon, enfant, *args, **kwargs):
        enfant.famille.__class__ = FamilleCIPE
        self.enfant = enfant
        super().__init__(tampon, enfant, *args, **kwargs)

    def append_other_docs(self, fichiers):
        msg = []
        for fichier in fichiers:
            doc_path = Path(fichier.path)
            if not doc_path.exists():
                msg.append(f"Le fichier «{doc_path}» n'existe pas!")
                continue
            if doc_path.suffix.lower() == '.pdf':
                self.merger.append(PdfReader(str(doc_path)))
            elif doc_path.suffix.lower() in ['.doc', '.docx']:
                with tempfile.TemporaryDirectory() as tmpdir:
                    cmd = ['libreoffice', '--headless', '--convert-to', 'pdf', '--outdir', tmpdir, doc_path]
                    subprocess.run(cmd, capture_output=True)
                    converted_path = Path(tmpdir) / f'{doc_path.stem}.pdf'
                    if converted_path.exists():
                        self.merger.append(PdfReader(str(converted_path)))
                    else:
                        msg.append(f"La conversion du fichier «{doc_path}» a échoué")
            elif doc_path.suffix.lower() == '.msg':
                self.append_pdf(MessagePrestationPdf, obj=None, fichier=fichier)
            else:
                msg.append(f"Le format du fichier «{doc_path}» ne peut pas être intégré.")
        return msg

    def get_fichiers(self):
        fichiers = []
        for prest in self.enfant.prestationcipe_set.all().order_by("date"):
            fichiers += prest.fichiers
        return fichiers

    def get_filename(self):
        return '{}_{}_journal.pdf'.format(
            slugify(self.instance.nom),
            slugify(self.instance.prenom),
        )

    def get_title(self):
        return f'{self.instance.nom} {self.instance.prenom} - {self.title}'

    def produce(self):
        self.append_pdf(_JournalPdf, self.enfant)
        self.append_other_docs(self.get_fichiers())
        self.merger.write(self.doc.filename)
