from django.urls import path

from archive import views as archive_views
from croixrouge.views import FamilleAutoCompleteView

from . import views, views_stats

app_name = "cipe"
urlpatterns = [
    path('famille/cipe/dal/', FamilleAutoCompleteView.as_view(typ='cipe'), name="famille-autocomplete"),
    path('famille/cipe/can_sms/', views.FamilleHasSMSView.as_view(), name="famille-can-sms"),

    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='famille-list'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='famille-edit'),
    path('famille/<int:pk>/delete/', views.FamilleDeleteView.as_view(), name='famille-delete'),
    path('famille/<int:pk>/print-info/', views.CoordonneesPDFView.as_view(), name='print-coord-famille'),
    path('famille/suivis_archives/', views.SuivisArchivesView.as_view(), name='suivis-archives'),
    path('famille/<int:pk>/personne/add/', views.PersonneCreateView.as_view(), name="personne-add"),

    # Prestations
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(),
         name='prestation-famille-list'),
    path('famille/<int:pk>/enfant/<int:pk_pers>/prestation/list/', views.PrestationListView.as_view(),
         name='prestation-enfant-list'),
    path('famille/<int:pk>/enfant/<int:pk_pers>/prestation/add/', views.PrestationCreateView.as_view(),
         name='prestation-enfant-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='prestation-delete'),
    path('prestation/<int:obj_pk>/deletefile/', views.PrestationDeleteFile.as_view(),
         name='prestation-deletefile'),
    path('utilisateur/prestations/', views.PrestationUtilisateurListView.as_view(), {'unite': 'cipe'},
         name='prestations-utilisateur'),
    path('prestation_gen/add/', views.PrestationGenEditView.as_view(is_create=True),
         name='prestation-gen-add'),
    path('prestation_gen/<int:pk>/edit/', views.PrestationGenEditView.as_view(is_create=False),
         name='prestation-gen-edit'),
    path('prestation_gen/<int:pk>/delete/', views.PrestationGenDeleteView.as_view(),
         name='prestation-gen-delete'),

    path('famille/<int:pk>/enfant/<int:pk_pers>/perinatal/', views.EnfantPerinatalView.as_view(),
         name='enfant-perinatal'),
    path('lieuacc/auto/', views.LieuAccouchementAutocompleteView.as_view(), name='lieuacc-autocomplete'),
    path('famille/<int:pk>/enfant/<int:pk_pers>/psycho/', views.EnfantPsychoView.as_view(), name='enfant-psycho'),
    path('famille/<int:pk>/enfant/<int:pk_pers>/psychoedit/', views.EnfantPsychoEditView.as_view(),
         name='enfant-psychoedit'),
    path('famille/<int:pk>/psychoedit/<int:pk_item>/', views.EnfantPsychoEditView.as_view(),
         name='item-psychoedit'),

    # Percentile
    path('enfant/<int:pk>/percentile/poids/', views.PercentileView.as_view(), {'typ': 'poids'},
         name='enfant-percentile-poids'),
    path('enfant/<int:pk>/percentile/taille/', views.PercentileView.as_view(), {'typ': 'taille'},
         name='enfant-percentile-taille'),
    path('enfant/<int:pk>/print/percentile/poids/', views.PercentilePoidsPDFView.as_view(),
         name='print-percentile-poids'),
    path('enfant/<int:pk>/print/percentile/taille/', views.PercentileTaillePDFView.as_view(),
         name='print-percentile-taille'),
    path('enfant/<int:pk>/percentile/email/', views.PercentileByEmailView.as_view(),
         name='enfant-percentile-email'),
    path('enfant/<int:pk>/archiver/', archive_views.ArchiveCreateView.as_view(default_unite="cipe"),
         name='enfant-archiver'),
    path('archive/<int:pk>/desarchiver/', archive_views.ArchiveCIPEDecryptView.as_view(),
         name='enfant-desarchiver'),

    # Thèmes de santé
    path('themesante/list/', views.ThemeSanteListView.as_view(), name='themesante-list'),
    path('themesante/add/', views.ThemeSanteEditView.as_view(is_create=True), name='themesante-add'),
    path('themesante/<int:pk>/edit/', views.ThemeSanteEditView.as_view(), name='themesante-edit'),
    path('themedoc/list/', views.ThemeDocListView.as_view(), name='themedoc-list'),
    path('themedoc/add/', views.ThemeDocEditView.as_view(is_create=True), name='themedoc-add'),
    path('themedoc/<int:pk>/edit/', views.ThemeDocEditView.as_view(), name='themedoc-edit'),
    path('themedoc/<int:pk>/delete/', views.ThemeDocDeleteView.as_view(), name='themedoc-delete'),

    path('documents/', views.GeneralDocListView.as_view(), name='generaldoc-list'),
    path('documents/add/', views.GeneralDocEditView.as_view(is_create=True), name='generaldoc-add'),
    path('documents/<int:pk>/edit/', views.GeneralDocEditView.as_view(), name='generaldoc-edit'),
    path('documents/<int:pk>/delete/', views.GeneralDocDeleteView.as_view(), name='generaldoc-delete'),
    path('documents/categ/<int:pk>/edit/', views.GeneralDocCategEditView.as_view(), name='generaldoccateg-edit'),

    # Stats
    path('statistiques/familles/', views_stats.StatistiquesFamillesView.as_view(), name='stats-familles'),
    path('statistiques/interv/', views_stats.StatistiquesParIntervView.as_view(), name='stats-interv'),
    path('statistiques/localites/', views_stats.StatistiquesLocalitesView.as_view(), name='stats-localites'),
    path('statistiques/lieux/', views_stats.StatistiquesLieuxView.as_view(), name='stats-lieux'),
    path('statistiques/type/', views_stats.StatistiquesTypeView.as_view(), name='stats-type'),
    path('statistiques/nouvelles/', views_stats.StatistiquesNouvellesView.as_view(), name='stats-nouvelles'),
    path('statistiques/themes/', views_stats.StatistiquesThemesView.as_view(), name='stats-themes'),
    path('statistiques/nationalite/', views_stats.StatistiquesNationaliteView.as_view(),
         name='stats-nationalite'),
    path('statistiques/duree/', views_stats.StatistiquesDureeView.as_view(), name='stats-duree'),
    path('statistiques/gen/', views_stats.StatistiquesGenView.as_view(), name='stats-gen'),
    path('statistiques/export/', views_stats.StatistiquesExportView.as_view(), name='stats-export'),
    path('statistiques/export/santene/', views_stats.StatistiquesSanteNEView.as_view(),
         name='stats-export-santene'),
    path('statistiques/export/ascpe/', views_stats.StatistiquesASCPEView.as_view(), name='stats-export-ascpe'),
]
