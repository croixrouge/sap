/*
 * Dynamic psychomotor item form calling, then save/cancel handling.
 * In psycho_dev.html
 */
function evalLoadform(ev) {
    ev.preventDefault();
    const btn = ev.currentTarget;
    const parentTr = btn.closest('tr');
    const url = btn.href || btn.dataset.url;
    const editUrl = url + '?itemid=' + parentTr.dataset.itemid;
    fetch(editUrl).then(res => res.text()).then(html => {
        btn.setAttribute('hidden', true);
        parentTr.querySelector('.form-container').innerHTML = html;
        parentTr.classList.add('active');
        attachFormHandler(btn, parentTr);
    });
}

function attachFormHandler(btn, parentTr) {
    // cancel btn
    parentTr.querySelector('.cancel').addEventListener('click', ev => {
        ev.preventDefault();
        parentTr.classList.remove('active');
        btn.removeAttribute('hidden');
        parentTr.querySelector('.form-container').innerHTML = '';
    });
    // save btn
    parentTr.querySelector('form').addEventListener('submit', ev => {
        ev.preventDefault();
        parentTr.classList.remove('active');
        const formData = new FormData(ev.target);
        // POST with fetch
        fetch(ev.target.action, {
            method: 'POST',
            body: formData
        }).then(res => res.text()).then(html => {
            const formContainer = parentTr.querySelector('.form-container');
            if (html.includes('<form')) {
                // Redisplay form with errors
                formContainer.innerHTML = html;
                attachFormHandler(btn, parentTr);
            } else {
                parentTr.replaceWith(htmlToElem(html));
            }
        });
    });
}
/* Toggle prestation list display with all facts or only significant facts.
 * In prestation_list.html
 */
function togglePrestationFacts(ev) {
    const params = new URLSearchParams(window.location.search);
    const faits = params.get('faits');
    if (faits === null || faits == 'tous') params.set('faits', 'marq');
    else params.set('faits', 'tous');
    window.location = `${window.location.pathname}?${params.toString()}`;
}

function typeConsultChanged() {
    const typeSelect = document.querySelector('#id_type_consult');
    const lieuSelect = document.querySelector('#id_lieu');
    const prestDiv = document.querySelector('#adomicile_prestation');
    if (!typeSelect) return;
    /* "Prestation à domicile" should only appear if the Lieu choice is set to
     * "À domicile", in prestation_edit.html.
    */
    if (typeSelect.value == 'cons_dom' && prestDiv) {
        prestDiv.classList.remove('hidden');
    } else if (prestDiv) {
        prestDiv.classList.add('hidden');
        document.querySelector('#id_a_domicile_prest').value = '';
    }
    // Consult par tél/courriel/domicile, désactiver Lieu.
    if (['cons_dom', 'prest_fam', 'res_fam', 'hors_stats'].includes(typeSelect.value)) {
        lieuSelect.value = '';
        lieuSelect.setAttribute('disabled', true);
    } else {
        lieuSelect.removeAttribute('disabled')
    }
}
