import re
from datetime import date, timedelta

from city_ch_autocomplete.forms import CityChField, CityChMixin
from dal import autocomplete
from django import forms
from django.core.exceptions import ValidationError
from django.db.models import Max, OuterRef, Q, Subquery, Value
from django.db.models.functions import Replace
from django.forms.widgets import ChoiceWidget
from django.urls import reverse_lazy
from django.utils.choices import BaseChoiceIterator

from archive.models import ArchiveCIPE
from croixrouge.forms import (
    BootstrapMixin,
    BSCheckboxSelectMultiple,
    BSRadioSelect,
    FamilleFormBase,
    HMDurationField,
    PickDateWidget,
)
from croixrouge.forms import PersonneForm as PersonneFormBase
from croixrouge.models import Personne, Region, Role, Utilisateur

from .models import (
    FamilleCIPE,
    GeneralDoc,
    PrestationCIPE,
    PrestationGen,
    PsychoEval,
    SuiviEnfant,
    ThemeDoc,
    ThemeSante,
    contact_intervals,
)


class AgeGestWidget(forms.MultiWidget):
    template_name = 'widgets/age_gest.html'

    def __init__(self, attrs=None):
        widgets = [
            forms.NumberInput(attrs={'style': 'width:8ch;', **(attrs or {}), 'min': 15, 'max': 45}),
            forms.NumberInput(attrs={'style': 'width:8ch;', **(attrs or {}), 'min': 0, 'max': 6}),
        ]
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [int(value) // 7, int(value) % 7]
        return [None, None]

    def value_from_datadict(self, data, files, name):
        sems, days = super().value_from_datadict(data, files, name)
        if sems:
            return int(sems) * 7 + (int(days) if days else 0)
        return ''


class DatalistWidget(ChoiceWidget):
    template_name = "widgets/datalist.html"

    def format_value(self, value):
        return str(value)


class ModelChoiceFieldWithNew(forms.ModelChoiceField):
    widget = DatalistWidget

    def __init__(self, *args, **kwargs):
        kwargs['to_field_name'] = 'nom'
        super().__init__(*args, **kwargs)

    def prepare_value(self, value):
        if value is None:
            return ''
        if isinstance(value, int):
            return str(self.queryset.get(pk=value))
        else:
            return str(value)

    def to_python(self, value):
        if value in self.empty_values:
            return None
        try:
            key = self.to_field_name or 'pk'
            if isinstance(value, self.queryset.model):
                value = getattr(value, key)
            value = self.queryset.get(**{key: value})
        except self.queryset.model.DoesNotExist:
            value = self.queryset.model(**{key: value})
            value.full_clean()
            value.save()
        except (ValueError, TypeError):
            raise ValidationError(
                self.error_messages['invalid_choice'],
                code='invalid_choice',
                params={'value': value},
            )
        return value


class FamilleForm(FamilleFormBase):
    typ = 'cipe'

    class Meta:
        model = FamilleCIPE
        fields = [
            'nom', 'rue', 'npa', 'localite', 'telephone', 'region', 'autorite_parentale',
            'monoparentale', 'statut_marital', 'accueil', 'besoins_part', 'sap', 'garde',
            'statut_financier', 'remarques',
        ]

    def save(self, **kwargs):
        if self.instance.typ is None:
            self.instance.typ = ['cipe']
        return super().save(**kwargs)


class FamilleCreateForm(CityChMixin, FamilleForm):
    city_auto = CityChField(required=False)

    postal_code_model_field = 'npa'
    city_model_field = 'localite'
    field_order = ['nom', 'rue', 'city_auto']


class FamilleFilterForm(forms.Form):
    region = forms.ChoiceField(label="Centre", required=False)
    famille = forms.CharField(
        label='Famille',
        widget=forms.TextInput(attrs={'placeholder': 'Nom de famille…', 'autocomplete': 'off', 'autofocus': True}),
        required=False
    )
    adresse = forms.CharField(
        label="Adresse",
        widget=forms.TextInput(attrs={'placeholder': 'NPA ou localité…', 'autocomplete': 'off'}),
        required=False
    )
    tel = forms.CharField(
        label="N° tél.",
        widget=forms.TextInput(attrs={'placeholder': '078 111……', 'autocomplete': 'off'}),
        required=False
    )
    pediatre = forms.CharField(
        label="Pédiatre",
        widget=forms.TextInput(attrs={'placeholder': 'Pédiatre…', 'autocomplete': 'off'}),
        required=False
    )
    age_de = forms.IntegerField(required=False)
    age_a = forms.IntegerField(required=False)
    consult_de = forms.DateField(required=False, widget=PickDateWidget)
    consult_a = forms.DateField(required=False, widget=PickDateWidget)
    besoins_part = forms.BooleanField(label="Familles à besoins particuliers", required=False)
    a_contacter = forms.ChoiceField(choices=(
        ("", "-- Contact pour contrôle --"),
        ("contr9", "Contacter pour contrôle des 9 mois"),
        ("contr18", "Contacter pour contrôle des 18 mois"),
        ("contr36", "Contacter pour contrôle des 36 mois"),
    ), required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['region'].choices = [
            ('0', 'Tous')
        ] + [
            (region.pk, region.nom) for region in Region.objects.par_secteur('cipe')
        ] + [('', '-------'), ('none', 'Aucun')]

    def filter(self, familles):
        if self.cleaned_data['region'] and self.cleaned_data['region'] != '0':
            if self.cleaned_data['region'] == 'none':
                familles = familles.filter(region__isnull=True)
            else:
                familles = familles.filter(region=self.cleaned_data['region'])
        if self.cleaned_data['famille']:
            familles = familles.filter(
                Q(nom__icontains=self.cleaned_data['famille']) |
                Q(membres__nom__icontains=self.cleaned_data['famille'])
            ).distinct()
        if self.cleaned_data['adresse']:
            try:
                int(self.cleaned_data['adresse'])
            except ValueError:
                familles = familles.filter(localite__icontains=self.cleaned_data['adresse'])
            else:
                familles = familles.filter(npa=self.cleaned_data['adresse'])
        if self.cleaned_data['tel']:
            no_tel = self.cleaned_data['tel'].replace(' ', '')
            pers = Personne.objects.annotate(
                tel_no_space=Replace('telephone', Value(' '), Value(''))
            ).filter(tel_no_space__contains=no_tel)
            familles = familles.annotate(
                tel_no_space=Replace('telephone', Value(' '), Value(''))
            ).filter(
                Q(telephone__contains=no_tel) | Q(membres__in=pers)
            ).distinct()
        if self.cleaned_data['pediatre']:
            familles = familles.filter(
                membres__suivienfant__pediatre__nom__icontains=self.cleaned_data['pediatre']
            ).distinct()
        if self.cleaned_data['age_de'] or self.cleaned_data['age_a']:
            date_from = date_to = None
            if self.cleaned_data['age_de']:
                date_from = date.today() - timedelta(days=self.cleaned_data['age_de'] * (365 / 12))
            if self.cleaned_data['age_a']:
                date_to = date.today() - timedelta(days=self.cleaned_data['age_a'] * (365 / 12))
            qargs = {'membres__role__nom': 'Enfant suivi'}
            if date_from and date_to:
                qargs['membres__date_naissance__range'] = (date_to, date_from)
            elif date_from:
                qargs['membres__date_naissance__lte'] = date_from
            else:
                qargs['membres__date_naissance__gte'] = date_to
            familles = familles.filter(**qargs).distinct()
        if self.cleaned_data['consult_de'] or self.cleaned_data['consult_a']:
            dfrom, dto = self.cleaned_data['consult_de'], self.cleaned_data['consult_a']
            if not dfrom:
                dfrom = date(2000, 1, 1)
            if not dto:
                dto = date.today()
            familles = familles.annotate(
                last_consult=Max(
                    'prestationcipe__date',
                    filter=~Q(prestationcipe__type_consult='hors_stats') & Q(prestationcipe__manque=False)
                )
            ).filter(last_consult__range=(dfrom, dto)).order_by('nom')
        if self.cleaned_data['besoins_part']:
            familles = familles.filter(besoins_part=True)
        if self.cleaned_data["a_contacter"]:
            key = self.cleaned_data["a_contacter"].replace("contr", "")
            # +-1 car range() est inclusif
            date_from = date.today() - timedelta(days=contact_intervals[key][1] - 1)
            date_to = date.today() - timedelta(days=contact_intervals[key][0] + 1)
            familles = familles.annotate(
                enfant_a_contacter=Subquery(
                    Personne.objects.alias(
                        derniere_prest=Max("prestationcipe__date", filter=Q(prestationcipe__manque=False))
                    ).filter(
                        Q(famille=OuterRef("id")) &
                        Q(date_naissance__range=(date_from, date_to)) &
                        (Q(derniere_prest__isnull=True) | Q(derniere_prest__lt=date.today() - timedelta(days=60)))
                    ).values("pk")[:1]
                )
            ).filter(enfant_a_contacter__isnull=False)

        return familles


class ArchiveCIPEFilterForm(forms.Form):
    region = forms.ChoiceField(label="Centre", required=False)
    nom = forms.CharField(
        label='Nom/prénom',
        widget=forms.TextInput(attrs={'placeholder': "Nom/prénom", 'autocomplete': 'off', 'autofocus': True}),
        required=False
    )
    localite = forms.CharField(
        label="Localité",
        widget=forms.TextInput(attrs={'placeholder': 'NPA/localité', 'autocomplete': 'off'}),
        required=False
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['region'].choices = [
            ('', 'Tous')
        ] + [
            (region, region) for region in ArchiveCIPE.objects.values_list("region", flat=True).distinct()
        ]

    def filter(self, archives):
        if self.cleaned_data['region']:
            archives = archives.filter(region=self.cleaned_data['region'])
        if self.cleaned_data['nom']:
            archives = archives.filter(
                Q(nom_famille__icontains=self.cleaned_data['nom']) |
                Q(nom__icontains=self.cleaned_data['nom']) |
                Q(prenom__icontains=self.cleaned_data['nom'])
            )
        if self.cleaned_data['localite']:
            archives = archives.filter(
                Q(npa=self.cleaned_data['localite']) | Q(localite_icontains=self.cleaned_data['localite'])
            )
        return archives


class PersonneForm(PersonneFormBase):
    prescripteur = forms.ChoiceField(
        label="Prescripteur",
        choices=(('', '---------'),) + SuiviEnfant.PRESCRIPTEUR_CHOICES,
        required=False
    )

    def __init__(self, **kwargs):
        role_id = kwargs.get('role')
        super().__init__(**kwargs)
        role = Role.objects.get(pk=role_id) if (role_id and role_id != 'ps') else None
        if role and role.nom not in ['Enfant suivi', 'Enfant non-suivi']:
            del self.fields['prescripteur']
        elif self.instance.pk is None:
            self.fields['prescripteur'].choices = [ch for ch in self.fields['prescripteur'].choices if ch[0] != 'D']

    def clean(self):
        cleaned_data = super().clean()
        if (self.cleaned_data['role'] and self.cleaned_data['role'].nom == 'Enfant suivi' and not
                self.cleaned_data.get('prescripteur')):
            self.add_error('prescripteur', forms.ValidationError("Le champ prescripteur est obligatoire"))
        return cleaned_data

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        if instance.role.nom == 'Enfant suivi':
            instance.suivienfant.prescripteur = self.cleaned_data['prescripteur']
            instance.suivienfant.save()
        return instance


class PerinatalForm(BootstrapMixin, forms.ModelForm):
    allergies = forms.CharField(label="Allergies de l’enfant", widget=forms.Textarea, required=False)

    class Meta:
        model = SuiviEnfant
        fields = [
            'motif_demande', 'prescripteur', 'pediatre', 'lieu_acc', 'poids_naiss', 'taille',
            'poids_sortie', 'accouchement', 'particularites', 'gestite', 'parite',
            'age_gest', 'allaitement', 'lait_subst', 'traitement', 'allergies',
        ]
        widgets = {
            'motif_demande': BSCheckboxSelectMultiple,
            'lieu_acc': forms.TextInput(
                attrs={'data-url': reverse_lazy('cipe:lieuacc-autocomplete'), 'autocomplete': 'off'}
            ),
            'pediatre': autocomplete.ModelSelect2(url='contact-autocomplete'),
            'age_gest': AgeGestWidget(),
        }

    def __init__(self, *args, **kwargs):
        kwargs['initial']['allergies'] = kwargs['instance'].personne.allergies
        super().__init__(*args, **kwargs)

    def save(self, **kwargs):
        if 'allergies' in self.changed_data:
            self.instance.personne.allergies = self.cleaned_data['allergies']
            self.instance.personne.save()
        return super().save(**kwargs)


class PsychoForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = PsychoEval
        exclude = ['suivi', 'date', 'auteur']
        widgets = {'item': forms.HiddenInput, 'acquisition': BSRadioSelect}

    def save(self, **kwargs):
        if not self.instance.date:
            self.instance.date = date.today()
        return super().save(**kwargs)


class ThemeSelectMultiple(BSCheckboxSelectMultiple):
    option_template_name = 'widgets/input_option.html'

    def create_option(self, *args, **kwargs):
        option = super().create_option(*args, **kwargs)
        if option['value'].value in self._themes_abordes:
            option['label'] += ' (%s)' % ', '.join(
                dt.strftime("%d.%m.%Y") for dt in self._themes_abordes[option['value'].value]
            )
            option['attrs']['class'] += ' aborde'
        return option


class LieuChoiceIterator(BaseChoiceIterator):
    def __init__(self, queryset, limit_choices):
        self.queryset = queryset
        self.limit_choices = limit_choices

    def __iter__(self):
        for reg in self.queryset.filter(**self.limit_choices):
            yield (reg.pk, str(reg))
        for tpl in [('', '--------'), ('tel', "Par téléphone"), ('email', "Par courriel")]:
            yield tpl


class LieuField(forms.TypedChoiceField):

    def __init__(self, **kwargs):
        self.limit_choices_to = kwargs.pop('limit_choices_to', {})
        self.queryset = kwargs.pop('queryset', {})
        for key in ('to_field_name', 'blank'):
            kwargs.pop(key, None)
        super().__init__(choices=LieuChoiceIterator(self.queryset, self.limit_choices_to), **kwargs)
        self.empty_value = None

    def _coerce(self, value):
        if value == self.empty_value or value in self.empty_values:
            return self.empty_value
        if value.isdigit():
            return Region.objects.par_secteur('cipe').get(pk=value)
        return value


class PrestationForm(BootstrapMixin, forms.ModelForm):
    type_consult = forms.ChoiceField(
        label="Type",
        choices=PrestationCIPE.TYPE_CONSULT_CHOICES,
        required=True,
        help_text="Réseau familial: la prestation est visible dans les journaux de tous les enfants de la famille"
    )
    # Useful when redirecting after editing 'family' prestations.
    referer = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = PrestationCIPE
        fields = [
            'date', 'type_consult', 'mode_consult', 'lieu', 'a_domicile_prest', 'duree',
            'poids', 'taille', 'faits_courants', 'faits_marquants', 'themes',
            'sans_rendezvous', 'manque', 'fichiers'
        ]
        localized_fields = ['poids', 'taille']
        widgets = {
            'date': PickDateWidget,
            'themes': ThemeSelectMultiple,
            # Using TextInput as long as Android 6 has to be supported
            # (which doesn't show comma/point on virtual keyboard)
            # 'poids': forms.NumberInput(attrs={'inputformat': 'decimal'}),
            # 'taille':  forms.NumberInput(attrs={'inputformat': 'decimal'}),
            'poids': forms.TextInput,
            'taille':  forms.TextInput,
        }
        field_classes = {
            'duree': HMDurationField,
            'lieu': LieuField,
        }
        labels = {'date': 'Date', 'lieu': 'Lieu/Mode'}

    def __init__(self, user=None, famille=None, themes_abordes=None, **kwargs):
        if not kwargs['instance']:
            kwargs['initial']['date'] = date.today()
            kwargs['initial']['lieu'] = famille.region_id
        elif kwargs['instance'].mode_consult != 'pres':
            kwargs['initial']['lieu'] = kwargs['instance'].mode_consult
        super().__init__(**kwargs)
        if 'ipe' not in user.groupes and not user.is_superuser:
            self.fields['type_consult'].choices = [
                ch for ch in PrestationCIPE.TYPE_CONSULT_CHOICES if ch[0] == 'hors_stats'
            ]
        self.fields['mode_consult'].required = False
        self.fields['themes'].queryset = self.fields['themes'].queryset.order_by('nom')
        self.fields['themes'].widget._themes_abordes = themes_abordes

    def clean_date(self):
        date_ = self.cleaned_data['date']
        if date_ > date.today():
            raise forms.ValidationError("Vous ne pouvez pas saisir de prestations dans le futur.")
        return date_

    def clean(self):
        cleaned_data = super().clean()
        type_consult = cleaned_data.get('type_consult')
        if type_consult in ['cons_crn', 'cons_som'] and not cleaned_data.get('lieu'):
            self.add_error('lieu', forms.ValidationError(
                "Le lieu est obligatoire pour les consultations Croix-Rouge"
            ))
        elif (
            type_consult in ['cons_dom', 'prest_fam', 'res_fam', 'hors_stats'] and
            cleaned_data.get('lieu')
        ):
            self.add_error('lieu', forms.ValidationError(
                "Le lieu ne doit pas être indiqué pour le type de consultation choisi."
            ))
        lieu = cleaned_data.get('lieu')
        if lieu in ('tel', 'email'):
            cleaned_data['mode_consult'] = lieu
            cleaned_data['lieu'] = None
        else:
            cleaned_data['mode_consult'] = 'pres'
        if type_consult == 'cons_dom':
            if not cleaned_data['a_domicile_prest']:
                self.add_error(
                    'a_domicile_prest',
                    forms.ValidationError("Vous devez saisir la prestation à domicile")
                )
        if type_consult == 'res_fam' and (cleaned_data['poids'] or cleaned_data['taille']):
            raise forms.ValidationError(
                "Les champs poids et taille ne doivent pas être remplis pour les prestations de type «Réseau familial»"
            )
        return cleaned_data

    def save(self, **kwargs):
        if self.cleaned_data['type_consult'] == 'res_fam':
            self.instance.enfant = None
        return super().save(**kwargs)


class PrestationGenForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = PrestationGen
        exclude = ['auteur']
        widgets = {
            'date': PickDateWidget,
            'participants': BSCheckboxSelectMultiple,
        }
        field_classes = {
            'duree': HMDurationField,
        }
        labels = {'lieu': 'Centre'}

    def __init__(self, *args, user=None, **kwargs):
        if not kwargs['instance']:
            kwargs['initial']['participants'] = [user]
        super().__init__(*args, **kwargs)
        self.fields['type_prest'].choices = [
            c for c in self.fields['type_prest'].choices if c[0] != 'patio'
        ]
        self.fields['participants'].queryset = Utilisateur.intervenants_ipe()

    def clean_date(self):
        date_prestation = self.cleaned_data['date']
        if date_prestation > date.today():
            raise forms.ValidationError("La saisie anticipée est impossible !")
        return date_prestation


class ThemeDocForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = ThemeDoc
        fields = '__all__'
        widgets = {'themes': BSCheckboxSelectMultiple}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['themes'].queryset = self.fields['themes'].queryset.order_by('nom')


class ThemeDocFilterForm(forms.Form):
    theme = forms.ModelChoiceField(queryset=ThemeSante.objects.all().order_by('nom'), required=False)
    langue = forms.ChoiceField(choices=(('', '--------'),) + ThemeDoc.LANG_CHOICES, required=False)

    def filter(self, docs):
        if self.cleaned_data['theme']:
            docs = docs.filter(themes=self.cleaned_data['theme'])
        if self.cleaned_data['langue']:
            docs = docs.filter(langue=self.cleaned_data['langue'])
        return docs


class GeneralDocForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = GeneralDoc
        fields = '__all__'
        widgets = {
            'categorie': forms.Select(attrs={'call': 'form-select'})
        }


class GeneralDocFormAddCateg(GeneralDocForm):
    class Meta(GeneralDocForm.Meta):
        field_classes = {
            'categorie': ModelChoiceFieldWithNew,
        }
        widgets = {}


class PersonEmailWidget(forms.CheckboxSelectMultiple):
    """Disable person option if person has no email"""
    option_template_name = 'widgets/input_option.html'

    def create_option(self, name, value, *args, **kwarg):
        context = super().create_option(name, value, *args, **kwarg)
        if value and not value.instance.email:
            context['attrs']['disabled'] = True
        return context


class PersonEmailChoiceField(forms.ModelMultipleChoiceField):
    widget = PersonEmailWidget
    default_error_messages = {
        'required': "Vous devez choisir au moins une personne",
    }

    def label_from_instance(self, obj):
        return f"{str(obj)} <{obj.email}>"


class SendByEmailForm(forms.Form):
    recips = PersonEmailChoiceField(required=False, queryset=None)
    pediatre = forms.ChoiceField(choices=(), widget=forms.CheckboxSelectMultiple, required=False)
    recip_other = forms.CharField(max_length=250, required=False)

    def __init__(self, enfant=None, **kwargs):
        super().__init__(**kwargs)
        self.fields['recips'].queryset = Personne.objects.filter(pk__in=[
            pers.pk for pers in (enfant.famille.parents() + list(enfant.famille.autres_parents()))
        ])
        pediatre = enfant.suivienfant.pediatre
        if pediatre and pediatre.email:
            self.fields['pediatre'].choices = (
                (pediatre.email, f'{pediatre.nom_prenom} <{pediatre.email}> (Pédiatre)'),
            )
        else:
            del self.fields['pediatre']

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('recips') and not cleaned_data.get('recip_other'):
            raise forms.ValidationError("Choisissez au moins une adresse pour l’envoi")
        return cleaned_data

    def recipients(self):
        re.split('[,; ]', self.cleaned_data['recip_other'])
        return (
            [pers.email for pers in self.cleaned_data['recips']] +
            [item for item in re.split('[,; ]', self.cleaned_data['recip_other']) if '@' in item] +
            ([self.cleaned_data['pediatre']] if self.cleaned_data.get('pediatre') else [])
        )
