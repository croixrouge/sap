from datetime import date, timedelta

from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone

from common.fields import ChoiceArrayField
from croixrouge.file_array_field import ArrayFileField
from croixrouge.models import Contact, Famille, Personne, Region, Role, Utilisateur
from croixrouge.utils import is_valid_for_sms, random_string_generator

contact_intervals = {
    "9": (274, 374),
    "18": (545, 645),
    "36": (1095, 1195),
}


class FamilleCIPEManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            typ__contains=['cipe']
        ).prefetch_related(models.Prefetch('membres', queryset=Personne.objects.select_related('role')))

    def actives(self):
        return self.get_queryset().filter(archived_at__isnull=True)

    def inactives(self):
        return self.get_queryset().filter(archived_at__isnull=False)

    def create(self, **kwargs):
        kwargs['typ'] = ['cipe']
        return super().create(**kwargs)


class FamilleCIPE(Famille):
    class Meta:
        proxy = True
        permissions = {
            ("can_archive", "Archiver les dossiers CIPE"),
        }

    default_typ = 'cipe'
    objects = FamilleCIPEManager()

    @property
    def prestations(self):
        return self.prestationcipe_set

    @property
    def add_person_url(self):
        return reverse('cipe:personne-add', args=[self.pk])

    def redirect_after_personne_creation(self, personne):
        if personne.role.nom == 'Enfant suivi':
            return reverse('cipe:enfant-perinatal', args=[self.pk, personne.pk])
        return super().redirect_after_personne_creation(personne)

    @property
    def edit_url(self):
        return reverse('cipe:famille-edit', args=[self.pk])

    @property
    def suivi_url(self):
        return reverse('cipe:famille-edit', args=[self.pk])

    @property
    def print_coords_url(self):
        return reverse('cipe:print-coord-famille', args=[self.pk])

    def anonymiser(self):
        self.nom = random_string_generator()
        self.rue = ''
        self.telephone = ''
        self.remarques = ''
        self.archived_at = timezone.now()
        self.save()
        for personne in self.membres.all().select_related('role'):
            if personne.role.nom in Role.NON_EDITABLE:
                personne.anonymiser()
            else:
                personne.delete()

    def can_be_archived(self, user):
        if self.archived_at:
            return False
        return user.has_perm('cipe.can_archive')

    def get_mobile_num(self):
        for num in [self.telephone] + [par.telephone for par in self.parents() if par.telephone]:
            if is_valid_for_sms(num):
                return num

    def get_email(self):
        return next((par.email for par in self.parents() if par.email), None)


class _FamilleCIPE:
    def __init__(self, famille):
        self.famille = famille

    def access_ok(self, user):
        return user.has_perm('cipe.change_famillecipe') or user.has_role(['IPE'])

    def can_view(self, user):
        return (
            user.has_perm('cipe.view_famillecipe') and
            (not self.famille.sap or user.has_role(['IPE']))
        )

    def can_edit(self, user):
        return self.can_view(user) and user.has_perm('cipe.change_famillecipe')

    def can_be_deleted(self, user):
        return self.famille.archived_at is not None and user.has_perm('cipe.delete_famillecipe')


Famille.typ_register['cipe'] = _FamilleCIPE


class SuiviEnfant(models.Model):
    MOTIF_CHOICES = (
        ('routine', 'Suivi de routine'),
        ('primip', 'Suivi primipare'),
        ('premat', 'Suivi prématuré'),
        ('psychosoc', 'Contexte psycho-social particulier'),
        ('integr', 'Soutien à l’intégration'),
    )
    ALLAITEMENT_CHOICES = (
        ('oui', 'Oui'),
        ('non', 'Non'),
        ('mix', 'Mixte'),
    )
    PRESCRIPTEUR_CHOICES = (
        ('F', 'Fratrie déjà suivie'),
        ('M', 'Maternité'),
        ('P', 'Pédiatre'),
        ('S', 'SFIS'),
        ('O', 'OPE'),
        ('A', 'ASA'),
        ('I', 'SPE'),
        ('X', 'SOF'),
        ('E', 'Entourage'),
        ('C', 'Courrier Croix-Rouge'),
        ('D', 'Divers'),
    )

    personne = models.OneToOneField(Personne, on_delete=models.CASCADE)
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=10, choices=MOTIF_CHOICES),
        verbose_name="Motif de la demande", blank=True, null=True
    )
    prescripteur = models.CharField("Prescripteur", max_length=1, choices=PRESCRIPTEUR_CHOICES, blank=True)
    pediatre = models.ForeignKey(
        Contact, null=True, blank=True, verbose_name="Pédiatre", on_delete=models.SET_NULL
    )
    # Données périnatales
    lieu_acc = models.CharField("Lieu d’accouchement", max_length=50, blank=True)
    poids_naiss = models.DecimalField("Poids à la naissance", max_digits=4, decimal_places=3, null=True, blank=True)
    taille = models.DecimalField("Taille", max_digits=4, decimal_places=1, null=True, blank=True)
    poids_sortie = models.DecimalField("Poids de sortie", max_digits=4, decimal_places=3, null=True, blank=True)
    accouchement = models.TextField("Accouchement", blank=True)
    particularites = models.TextField("Particularités", blank=True)
    gestite = models.PositiveSmallIntegerField("Gestité", null=True, blank=True)
    parite = models.PositiveSmallIntegerField("Parité", null=True, blank=True)
    age_gest = models.PositiveSmallIntegerField("Âge gestationnel", null=True, blank=True)
    allaitement = models.CharField("Allaitement", max_length=3, choices=ALLAITEMENT_CHOICES, blank=True)
    lait_subst = models.CharField("Lait de substitution", max_length=80, blank=True)
    traitement = models.TextField("Traitement", blank=True)
    allergies = models.TextField("Allergies de l’enfant", blank=True)

    def __str__(self):
        return f"Données de suivi pour {self.personne}"

    @property
    def naissance_mult(self):
        """Renvoie le nombre de naissances dans cette famille à la même date que self."""
        if not self.personne.date_naissance:
            return 1
        return self.personne.famille.membres.filter(
            role__nom__startswith='Enfant', date_naissance=self.personne.date_naissance
        ).count()

    @property
    def is_premat(self):
        if not self.age_gest:
            return False
        return self.age_gest < 37 * 7

    def get_motif_demande_display(self):
        dic = dict(self.MOTIF_CHOICES)
        return '; '.join([dic[value] for value in self.motif_demande]) if self.motif_demande else ''

    def age_gest_display(self):
        """Âge gestationnel, stocké en jours, affiché en semaines + jours."""
        if not self.age_gest:
            return ''
        sems = self.age_gest // 7
        days = self.age_gest % 7
        return f"{sems}%s" % (f" + {days}" if days > 0 else "")

    def age_premat(self, date_=None):
        if not self.is_premat or not self.personne.date_naissance:
            return ''
        if date_ is None:
            date_ = date.today()
        if (date_ - self.personne.date_naissance).days > 730:
            # Après 2 ans, on ne corrige plus l'âge
            return ''
        return self.personne.age_str(date_=date_ - timedelta(days=40 * 7 - self.age_gest))

    def anonymiser(self):
        # Les prestations CIPE sont attachées à l'enfant (modèle Personne) et non à SuiviEnfant
        self.lieu_acc = ''
        self.poids_naiss = None
        self.taille = None
        self.poids_sortie = None
        self.accouchement = ''
        self.particularites = ''
        self.gestite = None
        self.parite = None
        self.age_gest = None
        self.allaitement = ''
        self.lait_subst = ''
        self.traitement = ''
        self.allergies = ''
        self.save()


class ThemeSante(models.Model):
    nom = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Thème de santé"
        verbose_name_plural = "Thèmes de santé"

    def __str__(self):
        return self.nom


class ThemeDoc(models.Model):
    LANG_CHOICES = (
        ('sq', 'Albanais'),
        ('de', 'Allemand'),
        ('en', 'Anglais'),
        ('ar', 'Arabe'),
        ('es', 'Espagnol'),
        ('fa', 'Farsi'),
        ('fr', 'Français'),
        ('it', 'Italien'),
        ('pt', 'Portugais'),
        ('ru', 'Russe'),
        ('hr', 'Serbo-croate'),
        ('so', 'Somali'),
        ('ta', 'Tamoul'),
        ('th', 'Thaï'),
        ('ti', 'Tigrigna'),
        ('tr', 'Turc'),
    )
    fichier = models.FileField("Document", upload_to='cipe_doc')
    titre = models.CharField(max_length=150)
    langue = models.CharField("Langue", max_length=5, choices=LANG_CHOICES, blank=True)
    themes = models.ManyToManyField(ThemeSante, blank=True, related_name='documents', verbose_name="Thèmes")

    def __str__(self):
        return f"Document «{self.titre}» ({self.get_langue_display()})"


class CheckEditableMixin:
    # nbre de jours maximum après fin d'un mois où il est encore possible de
    # saisir des prestations du mois précédent
    max_jour = 10

    def check_editable(self):
        """Contrôle si la prestation est modifiable en fonction de la date de prestation."""
        max_days_ago = date.today() - timedelta(days=self.max_jour)
        first_of_month = date(max_days_ago.year, max_days_ago.month, 1)
        return self.date >= first_of_month


class PrestationCIPE(CheckEditableMixin, models.Model):
    TYPE_CONSULT_CHOICES = (
        ('cons_crn', "Consultation"),
        ('cons_som', "Consultation sommeil"),
        ('cons_dom', "Consultation à domicile"),
        ('prest_fam', "Prestation familiale"),
        ('res_fam', "Réseau familial"),
        ('hors_stats', "Autre (hors statistiques)"),
    )
    MODE_CONSULT_CHOICES = (
        ('pres', "En présentiel"),
        ('tel', "Par téléphone"),
        ('email', "Par courriel"),
    )
    A_DOMICILE_CHOICES = (
        ('psycho', "suivi psychosocial"),
        ('nombr', "famille nombreuse"),
        ('handi', "handicap du parent"),
        ('handi-enf', "handicap de l’enfant"),
        ('fatigue', "fatigue maternelle"),
        ('eloign', "éloignement"),
        ('centre_req', "centre de requérants"),
    )
    auteur = models.ForeignKey(Utilisateur, null=True, on_delete=models.PROTECT, verbose_name='Auteur')
    date = models.DateField("Date de la consultation")
    date_saisie = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    type_consult = models.CharField(
        "Type de prestation", max_length=12, choices=TYPE_CONSULT_CHOICES, default='cons_crn'
    )
    mode_consult = models.CharField(
        "Mode de prestation", max_length=5, choices=MODE_CONSULT_CHOICES, default="pres"
    )
    lieu = models.ForeignKey(
        Region, on_delete=models.PROTECT, verbose_name="Lieu", null=True, blank=True,
        limit_choices_to={'secteurs__contains': ['cipe']}
    )
    a_domicile_prest = models.CharField(
        "Prestation à domicile", max_length=10, choices=A_DOMICILE_CHOICES, blank=True
    )
    # Supprimer autre_lieu ?
    autre_lieu = models.BooleanField(default=False)
    duree = models.DurationField("Durée", blank=True, null=True)
    famille = models.ForeignKey(FamilleCIPE, on_delete=models.CASCADE, verbose_name='Famille')
    enfant = models.ForeignKey(Personne, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Enfant')
    poids = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, verbose_name='Poids [kg]')
    taille = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True, verbose_name='Taille [cm]')
    faits_courants = models.TextField('Faits courants', blank=True)
    faits_marquants = models.TextField('Faits marquants', blank=True)
    themes = models.ManyToManyField(ThemeSante, blank=True, verbose_name="Thèmes de santé abordés")
    sans_rendezvous = models.BooleanField('Sans rendez-vous', default=False)
    manque = models.BooleanField('Rendez-vous manqué', default=False)
    fichiers = ArrayFileField(models.FileField(upload_to='prestations'), verbose_name='Fichiers/images', null=True,
                              blank=True)

    typ = 'famille'

    def __str__(self):
        if self.enfant:
            return f'Consultation pour {self.enfant} du {self.date}'
        return f'Consultation pour famille {self.famille} du {self.date}'

    @property
    def lieu_or_domicile(self):
        if self.type_consult == 'cons_dom':
            return 'À domicile'
        return {'tel': 'Par téléphone', 'email': 'Par courriel'}.get(
            self.mode_consult, self.lieu or ''
        )

    @transaction.atomic
    def anonymiser(self):
        self.poids = None
        self.taille = None
        self.faits_courants = ''
        self.faits_marquants = ''
        fichiers_a_supp = [] + self.fichiers  # copier la liste avant d'effacer l'enregistrement en DB
        self.fichiers = []
        self.save()
        self.themes.clear()

        # Supprimer les fichiers dans le dossier media
        for fichier in fichiers_a_supp:
            fichier.delete()

    def can_edit(self, user):
        return user.is_superuser or (user == self.auteur and self.check_editable())

    def can_delete(self, user):
        return user == self.auteur and timezone.now() - self.date_saisie < timedelta(minutes=30)

    def edit_url(self):
        return reverse('cipe:prestation-edit', args=[self.famille.pk, self.pk])


class PrestationGen(CheckEditableMixin, models.Model):
    TYPE_CHOICES = (
        ('patio', 'Rencontres parents-enfants'),  # Seulement pour archives
        ('recif', 'Animation Récif'),
        ('lecture', 'Animation lecture'),
        ('sommeil', 'Atelier sommeil'),
        ('autre', 'Autre animation'),
        ('colloque', 'Colloque'),
        ('superv', 'Supervision'),
        ('interv', 'Intervision'),
        ('fcont', 'Formation continue'),
        ('seance', 'Séance externe'),
    )
    typ = 'general'

    class Meta:
        verbose_name = "Prestation générale"
        verbose_name_plural = "Prestations générales"

    auteur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT, verbose_name='Auteur')
    date = models.DateField("Date de la prestation")
    duree = models.DurationField("Durée")
    lieu = models.ForeignKey(
        Region, on_delete=models.PROTECT, verbose_name="Lieu", null=True, blank=True,
        limit_choices_to={'secteurs__contains': ['cipe']}
    )
    participants = models.ManyToManyField(Utilisateur, related_name='prestations_cipe', verbose_name='Participantes')
    nb_adultes = models.PositiveSmallIntegerField("Nbre d’adultes", blank=True, null=True)
    nb_enfants = models.PositiveSmallIntegerField("Nbre d’enfants", blank=True, null=True)
    nb_tel = models.PositiveSmallIntegerField("Nbre de téléphones", blank=True, null=True)
    nb_sms = models.PositiveSmallIntegerField("Nbre de SMS", blank=True, null=True)
    nb_email = models.PositiveSmallIntegerField("Nbre de courriels", blank=True, null=True)
    nb_courr = models.PositiveSmallIntegerField("Nbre de courriers parents", blank=True, null=True)
    type_prest = models.CharField("Type de prestation", max_length=10, choices=TYPE_CHOICES, blank=True)
    remarques = models.TextField("Remarques", blank=True)

    def __str__(self):
        return f"Prestation générale du {self.date} par {self.auteur}"

    def can_edit(self, user):
        return user.is_superuser or (user == self.auteur and self.check_editable())

    def can_delete(self, user):
        return user == self.auteur and self.check_editable()

    def edit_url(self):
        return reverse('cipe:prestation-gen-edit', args=[self.pk])


class PsychoItem(models.Model):
    AGE_CHOICES = (
        (30, '1 mois'),
        (91, '3 mois'),
        (183, '6 mois'),
        (274, '9 mois'),
        (365, '12 mois'),
    )
    descriptif = models.CharField(max_length=200, unique=True)
    anomalie = models.CharField(max_length=200, blank=True)
    age_ref = models.PositiveSmallIntegerField(choices=AGE_CHOICES)
    order = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.descriptif


class PsychoEval(models.Model):
    ACQUISITION_CHOICES = (
        ('acq', 'Acquis'),
        ('part', 'Partiellement acquis'),
        ('nacq', 'Non acquis'),
    )
    suivi = models.ForeignKey(SuiviEnfant, on_delete=models.CASCADE, verbose_name="Suivi")
    item = models.ForeignKey(PsychoItem, on_delete=models.PROTECT)
    date = models.DateField("Date")
    auteur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT, verbose_name='Auteur')
    acquisition = models.CharField(max_length=4, choices=ACQUISITION_CHOICES, blank=False)
    remarque = models.TextField("Remarques", blank=True)

    def __str__(self):
        return f"Évaluation psychomotrice «{self.item.descriptif}» pour {self.suivi.personne}"

    def can_edit(self, user):
        return self.date == date.today() and user == self.auteur


class GeneralDocCateg(models.Model):
    nom = models.CharField(max_length=100)

    def __str__(self):
        return self.nom


class GeneralDoc(models.Model):
    fichier = models.FileField("Document", upload_to='cipe_doc')
    titre = models.CharField(max_length=200)
    categorie = models.ForeignKey(
        GeneralDocCateg, on_delete=models.PROTECT, related_name='documents', verbose_name="Catégorie"
    )

    def __str__(self):
        return f"Document «{self.titre}» ({self.categorie})"


@receiver(post_save, sender=Personne)
def enfant_possede_suivi(sender, instance, **kwargs):
    if 'cipe' in instance.famille.typ and instance.role.nom == 'Enfant suivi':
        try:
            instance.suivienfant
        except SuiviEnfant.DoesNotExist:
            SuiviEnfant.objects.create(personne=instance)
