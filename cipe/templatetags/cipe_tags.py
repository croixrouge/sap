from datetime import date

from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from cipe.models import PrestationCIPE
from cipe.models import contact_intervals as ci

register = template.Library()


@register.filter
def alerte_consult(enfant):
    pas_de_prest_recente = not enfant.derniere_prest or (date.today() - enfant.derniere_prest).days > 60
    if pas_de_prest_recente:
        age_jours = enfant.age_jours()
        if age_jours:
            if ci["9"][0] < age_jours < ci["9"][1]:
                return "À contacter pour contrôle 9 mois"
            if ci["18"][0] < age_jours < ci["18"][1]:
                return "À contacter pour contrôle 18 mois"
            if ci["36"][0] < age_jours < ci["36"][1]:
                return "À contacter pour contrôle 36 mois"


@register.filter
def contenu_prest(prest):
    if isinstance(prest, PrestationCIPE):
        res = ''
        if prest.faits_marquants:
            res += format_html('<div class="marquants">{}</div>\n', prest.faits_marquants)
        if prest.faits_courants:
            res += format_html('<div class="courants">{}</div>\n', prest.faits_courants)
    else:
        res = []
        if prest.type_prest:
            res.append(prest.get_type_prest_display())
        if prest.nb_adultes:
            res.append(f'{prest.nb_adultes} participant-e-s')
        if prest.nb_tel:
            res.append(f'{prest.nb_tel} téléphone-s')
        if prest.nb_sms:
            res.append(f'{prest.nb_sms} SMS')
        if prest.nb_email:
            res.append(f'{prest.nb_email} courriel-s')
        res = ', '.join(res)
        if prest.remarques:
            res += format_html('<div class="remarques">{}</div>\n', prest.remarques)
    return mark_safe(res)
