from django.contrib import admin

from croixrouge.admin import FamilleAdminBase

from . import models


@admin.register(models.FamilleCIPE)
class FamilleCIPEAdmin(FamilleAdminBase):
    def get_queryset(self, request):
        return models.FamilleCIPE.objects.filter(typ=['cipe'])


@admin.register(models.SuiviEnfant)
class SuiviEnfantAdmin(admin.ModelAdmin):
    search_fields = ['personne__nom', 'personne__prenom']


@admin.register(models.PrestationCIPE)
class PrestationCIPEAdmin(admin.ModelAdmin):
    list_display = ['auteur', 'date', 'famille']
    ordering = ['-date']


@admin.register(models.PrestationGen)
class PrestationGenAdmin(admin.ModelAdmin):
    list_display = ['auteur', 'date', 'duree', 'type_prest']
    ordering = ['-date']


@admin.register(models.PsychoEval)
class PsychoEvalAdmin(admin.ModelAdmin):
    list_display = ['item', 'get_enfant', 'acquisition', 'date']
    list_select_related = ['suivi']

    def get_enfant(self, obj):
        return obj.suivi.personne
    get_enfant.short_description = 'Enfant'


@admin.register(models.PsychoItem)
class PsychoItemAdmin(admin.ModelAdmin):
    list_display = ['descriptif', 'age_ref', 'order']
    search_fields = ['descriptif']


@admin.register(models.ThemeDoc)
class ThemeDocAdmin(admin.ModelAdmin):
    list_display = ['titre', 'langue', 'fichier']
    search_fields = ['titre']
    list_filter = ['langue']


@admin.register(models.ThemeSante)
class ThemeSanteAdmin(admin.ModelAdmin):
    pass


@admin.register(models.GeneralDocCateg)
class GeneralDocCategAdmin(admin.ModelAdmin):
    list_display = ["nom"]
    ordering = ["nom"]


@admin.register(models.GeneralDoc)
class GeneralDocAdmin(admin.ModelAdmin):
    list_display = ['titre', 'categorie', 'fichier']
    search_fields = ['titre']
    list_filter = ['categorie']
