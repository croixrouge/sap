import unicodedata
from collections import namedtuple
from datetime import date, timedelta

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Avg, Case, Count, F, FilteredRelation, Min, OuterRef, Q, Subquery, Sum, Value, When
from django.db.models.functions import TruncMonth
from django.utils.dateformat import format as django_format
from django.views.generic import FormView, TemplateView
from django_countries.fields import Country
from openpyxl.utils import get_column_letter

from cipe.models import PrestationCIPE, PrestationGen, SuiviEnfant, ThemeSante
from croixrouge.forms import DateYearForm
from croixrouge.models import Personne, Region
from croixrouge.stat_utils import ExportStatistique, Month, StatsMixin
from croixrouge.utils import continent_from_country_code


class StatsBase(StatsMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'cipe.add_famillecipe'


class StatistiquesFamillesView(StatsBase):
    template_name = 'cipe/stats-familles.html'
    stat_items = {
        'num_01': 'Enfants de 0 à 1 an',
        'num_12': 'Enfants de 1 à 2 ans',
        'num_23': 'Enfants de 2 à 3 ans',
        'num_34': 'Enfants de 3 à 4 ans',
        'num_4more': 'Enfants de plus de 4 ans',
        'num_noage': 'Enfants sans âge indiqué',
        'num_garcons': 'Garçons',
        'num_filles': 'Filles',
        'total': 'Totaux',
    }

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        # Nbre de familles suivies
        fam_query = PrestationCIPE.objects.annotate(
            month=TruncMonth('date'),
        ).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(Q(type_consult='hors_stats') | Q(manque=True))
        fam_query_month = fam_query.values('month').annotate(
            total=Count('famille', distinct=True),
            besoins_part=Count('famille', filter=Q(famille__besoins_part=True), distinct=True),
        )
        fam_query_total = fam_query.aggregate(
            total=Count('famille', distinct=True),
            besoins_part=Count('famille', filter=Q(famille__besoins_part=True), distinct=True),
        )

        # Enfants suivis par tranche d'âge
        enfants_query = PrestationCIPE.objects.annotate(
            month=TruncMonth('date'), age_jours=F('date') - F('enfant__date_naissance')
        ).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(
            Q(type_consult__in=['hors_stats', 'res_fam']) | Q(manque=True)
        ).values('month').annotate(
            num_01=Count('enfant', filter=Q(age_jours__lt=timedelta(days=365)), distinct=True),
            num_12=Count('enfant',
                         filter=Q(age_jours__gte=timedelta(days=365), age_jours__lt=timedelta(days=720)),
                         distinct=True),
            num_23=Count('enfant',
                         filter=Q(age_jours__gte=timedelta(days=720), age_jours__lt=timedelta(days=1085)),
                         distinct=True),
            num_34=Count('enfant',
                         filter=Q(age_jours__gte=timedelta(days=1085), age_jours__lt=timedelta(days=1440)),
                         distinct=True),
            num_4more=Count('enfant', filter=Q(age_jours__gte=timedelta(days=1440)), distinct=True),
            num_noage=Count('enfant', filter=Q(age_jours=None), distinct=True),
            num_garcons=Count('enfant', filter=Q(enfant__genre='M'), distinct=True),
            num_filles=Count('enfant', filter=Q(enfant__genre='F'), distinct=True),
            total=Count('enfant', distinct=True),
        )
        enfants = {'totals': {key: 0 for key in cls.stat_items.keys()}}
        enfants.update(cls.init_counters(cls.stat_items.keys(), months))
        for line in enfants_query:
            month_key = Month.from_date(line['month'])
            for key in cls.stat_items.keys():
                enfants[key][month_key] = line[key]

        # Nouvelle requête pour les totaux, car le sectionnement par mois fausse les totaux
        # Prise en compte uniquement de la consultation la plus récente (#282)
        last_prestation_per_child = PrestationCIPE.objects.exclude(
            Q(type_consult='hors_stats') | Q(manque=True)
        ).filter(
            date__lte=date_end, date__gte=date_start, enfant=OuterRef('enfant')
        ).order_by('-date')[:1]
        enfants_query_totals = PrestationCIPE.objects.filter(
            id__in=Subquery(last_prestation_per_child.values('id'))
        ).annotate(
            age_jours=F('date') - F('enfant__date_naissance')
        ).aggregate(
            num_01=Count('enfant', filter=Q(age_jours__lt=timedelta(days=365)), distinct=True),
            num_12=Count('enfant', filter=Q(age_jours__gte=timedelta(days=365), age_jours__lt=timedelta(days=720)),
                         distinct=True),
            num_23=Count('enfant', filter=Q(age_jours__gte=timedelta(days=720), age_jours__lt=timedelta(days=1085)),
                         distinct=True),
            num_34=Count('enfant', filter=Q(age_jours__gte=timedelta(days=1085), age_jours__lt=timedelta(days=1440)),
                         distinct=True),
            num_4more=Count('enfant', filter=Q(age_jours__gte=timedelta(days=1440)), distinct=True),
            num_noage=Count('enfant', filter=Q(age_jours=None), distinct=True),
            num_garcons=Count('enfant', filter=Q(enfant__genre='M'), distinct=True),
            num_filles=Count('enfant', filter=Q(enfant__genre='F'), distinct=True),
            total=Count('enfant', distinct=True),
        )
        for key in cls.stat_items.keys():
            enfants['totals'][key] = enfants_query_totals[key]

        # L'âge moyen est utilisé dans StatistiquesASCPEView
        enfants['totals']['age_moy'] = PrestationCIPE.objects.annotate(
            age_jours=F('date') - F('enfant__date_naissance')
        ).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(
            Q(type_consult__in=['hors_stats', 'res_fam']) | Q(manque=True)
        ).aggregate(
            moy=Avg('age_jours')
        )['moy']

        # Nombre de consultations par tranche d'âge
        consult_query = PrestationCIPE.objects.annotate(
            month=TruncMonth('date'), age_jours=F('date') - F('enfant__date_naissance')
        ).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(
            Q(type_consult__in=['hors_stats', 'prest_fam', 'res_fam']) | Q(manque=True)
        ).values('month').annotate(
            num_01=Count('pk', filter=Q(age_jours__lt=timedelta(days=365))),
            num_12=Count('pk', filter=Q(age_jours__gte=timedelta(days=365), age_jours__lt=timedelta(days=720))),
            num_23=Count('pk', filter=Q(age_jours__gte=timedelta(days=720), age_jours__lt=timedelta(days=1085))),
            num_34=Count('pk', filter=Q(age_jours__gte=timedelta(days=1085), age_jours__lt=timedelta(days=1440))),
            num_4more=Count('pk', filter=Q(age_jours__gte=timedelta(days=1440))),
            num_noage=Count('pk', filter=Q(age_jours=None)),
            num_garcons=Count('pk', filter=Q(enfant__genre='M')),
            num_filles=Count('pk', filter=Q(enfant__genre='F')),
            total=Count('pk'),
        )
        consults = {'totals': {key: 0 for key in cls.stat_items.keys()}}
        consults.update(cls.init_counters(cls.stat_items.keys(), months))
        for line in consult_query:
            month_key = Month.from_date(line['month'])
            for key in cls.stat_items.keys():
                consults[key][month_key] = line[key]
                consults['totals'][key] += line[key]
        return {
            'nb_fam_tot': fam_query_total['total'],
            'nb_fam_besoins': fam_query_total['besoins_part'],
            'nb_fam_month': {
                Month.from_date(line['month']): {'total': line['total'], 'besoins': line['besoins_part']}
                for line in fam_query_month
            },
            'stat_items': cls.stat_items,
            'enfants': enfants,
            'consults': consults,
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Familles et enfants'] + [str(month) for month in months] + ['Total']
        yield (
            ['Nombre de familles suivies'] +
            [context['nb_fam_month'].get(month, {}).get('total', 0) for month in months] +
            [context['nb_fam_tot']]
        )
        yield (
            ['Familles à besoins particulier'] +
            [context['nb_fam_month'].get(month, {}).get('besoins', 0) for month in months] +
            [context['nb_fam_besoins']]
        )
        for key, label in self.stat_items.items():
            yield ([label] + [context['enfants'][key][month] for month in months] +
                   [context['enfants']['totals'].get(key, 0)])
        yield ['BOLD', 'Nombre de consultations']
        for key, label in self.stat_items.items():
            yield ([label] + [context['consults'][key][month] for month in months] +
                   [context['consults']['totals'].get(key, 0)])


class StatistiquesParIntervView(StatsBase):
    template_name = 'cipe/stats-interv.html'

    def get_stats(self, months):
        def prest_stats(model, filtre=None):
            base_query = model.objects.annotate(month=TruncMonth('date')).filter(
                date__lte=self.date_end, date__gte=self.date_start
            )
            if filtre:
                base_query = base_query.filter(filtre)
            author_field = 'participants' if model == PrestationGen else 'auteur'
            authors = base_query.values(
                f'{author_field}__id', f'{author_field}__nom', f'{author_field}__prenom'
            ).distinct()
            interv_query = base_query.values('month', author_field).annotate(
                nb=Count('id'),
                duree=Sum('duree'),
            )
            interv_stats = {aut[f'{author_field}__id']: {
                **{month: {
                    'nb':  '-' if month.is_future() else 0,
                    'duree': '-' if month.is_future() else timedelta(0)
                } for month in months},
                'total': {'nb': 0, 'duree': timedelta(0)},
                'nom': f"{aut[f'{author_field}__nom']} {aut[f'{author_field}__prenom']}",
            } for aut in authors}
            for line in interv_query:
                month = Month.from_date(line['month'])
                interv_stats[line[author_field]][month] = {
                    'nb': line['nb'],
                    'duree': line['duree'] or timedelta(0),
                }
                interv_stats[line[author_field]]['total']['nb'] += line['nb']
                if line['duree']:
                    interv_stats[line[author_field]]['total']['duree'] += line['duree']
            return dict(sorted(interv_stats.items()))  # Sorted by author (pk)
        return {
            'interv_stats_consult': prest_stats(
                PrestationCIPE,
                filtre=~Q(type_consult__in=['hors_stats', 'res_fam', 'prest_fam']) & Q(manque=False)
            ),
            'interv_stats_autre': prest_stats(
                PrestationCIPE,
                filtre=Q(type_consult__in=['res_fam', 'prest_fam']) & Q(manque=False)
            ),
            'prest_gen_stats': prest_stats(PrestationGen),
        }

    def export_lines(self, context):
        months = context['months']
        Formula = namedtuple('Formula', ['value', 'number_format'])
        yield ['BOLD', 'Consultations', ''] + [str(month) for month in months] + ['Total Nbre', 'Total Durée']
        for line in context['interv_stats_consult'].values():
            yield [line['nom'], 'Nbre'] + [line[month]['nb'] for month in months] + [line['total']['nb']]
            yield ['', 'Durée'] + [line[month]['duree'] for month in months] + ['', line['total']['duree']]
        num_lines_consult = len(context['interv_stats_consult']) * 2
        tot_ltr1 = get_column_letter(3 + len(months))
        tot_ltr2 = get_column_letter(4 + len(months))
        yield ['BOLD', 'Totaux', ''] + [''] * len(months) + [
            f'=SUM({tot_ltr1}2:{tot_ltr1}{1 + num_lines_consult})',
            Formula(f'=SUM({tot_ltr2}2:{tot_ltr2}{1 + num_lines_consult})', '[h]:mm;@'),
        ]

        yield []
        yield ['BOLD', 'Prestations et réseaux familiaux']
        for line in context['interv_stats_autre'].values():
            yield [line['nom'], 'Nbre'] + [line[month]['nb'] for month in months] + [line['total']['nb']]
            yield ['', 'Durée'] + [line[month]['duree'] for month in months] + ['', line['total']['duree']]
        start_line = num_lines_consult + 5
        num_lines_autre = len(context['interv_stats_autre']) * 2
        yield ['BOLD', 'Totaux', ''] + [''] * len(months) + [
            f'=SUM({tot_ltr1}{start_line}:{tot_ltr1}{start_line + num_lines_autre - 1})',
            Formula(f'=SUM({tot_ltr2}{start_line}:{tot_ltr2}{start_line + num_lines_autre - 1})', '[h]:mm;@'),
        ]

        yield []
        yield ['BOLD', 'Prestations générales']
        for line in context['prest_gen_stats'].values():
            yield [line['nom'], 'Nbre'] + [line[month]['nb'] for month in months] + [line['total']['nb']]
            yield ['', 'Durée'] + [line[month]['duree'] for month in months] + ['', line['total']['duree']]
        start_line = num_lines_consult + num_lines_autre + 8
        num_lines_gen = len(context['prest_gen_stats']) * 2
        yield ['BOLD', 'Totaux', ''] + [''] * len(months) + [
            f'=SUM({tot_ltr1}{start_line}:{tot_ltr1}{start_line + num_lines_gen - 1})',
            Formula(f'=SUM({tot_ltr2}{start_line}:{tot_ltr2}{start_line + num_lines_gen - 1})', '[h]:mm;@'),
        ]


class StatistiquesLocalitesView(StatsBase):
    template_name = 'cipe/stats-localites.html'

    def get_stats(self, months):
        query = PrestationCIPE.objects.annotate(
            month=TruncMonth('date'),
        ).filter(
            date__lte=self.date_end, date__gte=self.date_start
        ).exclude(
            Q(type_consult='hors_stats') | Q(manque=True)
        ).values('month', 'famille__pk', 'famille__npa', 'famille__localite').distinct()
        localites = self.init_counters(['total'], months, total=set)
        for line in query:
            localite = ' '.join([line['famille__npa'], line['famille__localite']])
            if localite not in localites:
                localites.update(self.init_counters([localite], months, total=set))
            month = Month.from_date(line['month'])
            localites[localite][month] += 1
            localites[localite]['total'].add(line['famille__pk'])
            localites['total'][month] += 1
            localites['total']['total'].add(line['famille__pk'])
        # Transform total set in count
        for key in localites:
            localites[key]['total'] = len(localites[key]['total'])
        return {'localites': {key: localites[key] for key in sorted(localites.keys())}}

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Localités'] + [str(month) for month in months] + ['Total']
        for loc, stats in context['localites'].items():
            if loc == 'total':
                continue
            yield [loc] + [stats[month] for month in months] + [stats['total']]
        yield (['Totaux'] +
               [context['localites']['total'][month] for month in months] +
               [context['localites']['total']['total']])


class StatistiquesLieuxView(StatsBase):
    template_name = 'cipe/stats-lieux.html'

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        """Stats consultations par centre"""
        # a class method to be easily callable by other views
        base_query = PrestationCIPE.objects.annotate(month=TruncMonth('date')).filter(
            date__lte=date_end, date__gte=date_start, type_consult__in=['cons_crn', 'cons_som']
        )
        centres = cls.init_counters([
            reg.nom for reg in Region.objects.par_secteur('cipe').order_by('nom')
        ], months)
        query_reg = base_query.filter(lieu__isnull=False).values('month', 'lieu__nom').annotate(num_prest=Count('id'))
        totals = cls.init_counters(['total'], months)['total']
        for res in query_reg:
            month = Month.from_date(res['month'])
            centres[res['lieu__nom']][month] = res['num_prest']
            centres[res['lieu__nom']]['total'] += res['num_prest']
            totals[month] += res['num_prest']
            totals['total'] += res['num_prest']

        return {
            'centres': centres,
            'total_stats': totals,
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Nombre de consultations'] + [str(month) for month in months] + ['Total']
        for centre, stats in context['centres'].items():
            yield [centre] + [stats.get(month, 0) for month in months] + [stats['total']]
        yield (['Total'] +
               [context['total_stats'].get(month, 0) for month in months] +
               [context['total_stats']['total']])


class StatistiquesTypeView(StatsBase):
    template_name = 'cipe/stats-type.html'

    stat_items = {
        'num_prest': 'Total',
        'num_rzv': 'Sur rendez-vous',
        'num_no_rzv': 'Sans rendez-vous',
        'manque': 'Rendez-vous manqués',
        'num_centre': 'Dans un centre',
        'par_tel': 'Par téléphone',
        'par_courriel': 'Par courriel',
        'sommeil': 'dont Consultations sommeil',
        'domicile': 'À domicile',
        'reseau_famille': 'Réseau familial',
        'prest_famille': 'Prestation familiale',
    }

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        dom_prest_keys = list(dict(PrestationCIPE.A_DOMICILE_CHOICES).keys())
        annots = {
            'num_prest': Count('id'),
            'num_rzv': Count(
                'pk', filter=(
                    ~Q(type_consult__in=['prest_fam', 'res_fam']) &
                    Q(manque=False) & Q(sans_rendezvous=False)
                )
            ),
            'num_no_rzv': Count(
                'pk', filter=(
                    ~Q(type_consult__in=['prest_fam', 'res_fam']) &
                    Q(manque=False) & Q(sans_rendezvous=True)
                )
            ),
            'manque': Count('pk', filter=Q(manque=True)),
            'num_centre': Count(
                'pk',
                filter=Q(mode_consult='pres') & Q(type_consult__in=['cons_crn', 'cons_som'])
            ),
            # Autre lieu avec téléphone suite échange avec resp. CIPE
            'par_tel': Count('pk', filter=Q(mode_consult='tel') | Q(autre_lieu=True)),
            'par_courriel': Count('pk', filter=Q(mode_consult='email')),
            'sommeil': Count('pk', filter=Q(type_consult='cons_som')),
            'domicile': Count('pk', filter=Q(type_consult='cons_dom')),
            'reseau_famille':  Count('pk', filter=Q(type_consult='res_fam')),
            'prest_famille': Count('pk', filter=Q(type_consult='prest_fam')),
        }
        annots.update({
            key: Count('pk', filter=Q(a_domicile_prest=key))
            for key in dom_prest_keys
        })

        query = PrestationCIPE.objects.annotate(month=TruncMonth('date')).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(
            type_consult='hors_stats'
        ).values('month').annotate(**annots)
        all_keys = list(cls.stat_items.keys()) + dom_prest_keys
        stats = cls.init_counters(all_keys, months)
        for res in query:
            month = Month.from_date(res['month'])
            for key in annots.keys():
                stats[key][month] = res[key]
                stats[key]['total'] += res[key]
        return {
            'stat_items': cls.stat_items,
            'prest_dict': dict(PrestationCIPE.A_DOMICILE_CHOICES),
            'stats': stats,
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Types de consultations'] + [str(month) for month in months] + ['Total']
        for key, label in self.stat_items.items():
            if key == 'num_prest':
                continue
            yield [label] + [context['stats'][key][month] for month in months] + [context['stats'][key]['total']]
            if key == 'domicile':
                for key, label in dict(PrestationCIPE.A_DOMICILE_CHOICES).items():
                    yield ([f' - {label}'] +
                           [context['stats'][key][month] for month in months] +
                           [context['stats'][key]['total']])
        yield (
            [self.stat_items['num_prest']] +
            [context['stats']['num_prest'][month] for month in months] +
            [context['stats']['num_prest']['total']]
        )


class StatistiquesNouvellesView(StatsBase):
    template_name = 'cipe/stats-nouvelles.html'

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        # Nouvelles inscriptions
        query_nouv = SuiviEnfant.objects.annotate(
            prem_prest=TruncMonth(Min(
                'personne__prestationcipe__date',
                filter=(
                    ~Q(personne__prestationcipe__type_consult='hors_stats') &
                    Q(personne__prestationcipe__manque=False)
                )
            ))
        ).filter(
            prem_prest__gte=date_start, prem_prest__lte=date_end
        ).values('prem_prest', 'prescripteur', 'motif_demande')
        prescr_dict = dict(SuiviEnfant.PRESCRIPTEUR_CHOICES)
        motif_dict = dict(SuiviEnfant.MOTIF_CHOICES)
        stats_nouv = cls.init_counters(['nouv'], months)['nouv']
        stats_prescr = cls.init_counters(prescr_dict.values(), months)
        stats_motifs = cls.init_counters(motif_dict.values(), months)
        for res in query_nouv:
            month = Month.from_date(res['prem_prest'])
            stats_nouv[month] += 1
            stats_nouv['total'] += 1
            if res['prescripteur']:
                stats_prescr[prescr_dict[res['prescripteur']]][month] += 1
                stats_prescr[prescr_dict[res['prescripteur']]]['total'] += 1
            if res['motif_demande']:
                for motif in res['motif_demande']:
                    stats_motifs[motif_dict[motif]][month] += 1
                    stats_motifs[motif_dict[motif]]['total'] += 1
        return {
            'nouvelles': stats_nouv,
            'nouvelles_prescr': stats_prescr,
            'nouvelles_motifs': stats_motifs,
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Nouvelles inscriptions'] + [str(month) for month in months] + ['Total']
        new_stats = context['nouvelles']
        yield ['Total'] + [new_stats.get(month, 0) for month in months] + [new_stats['total']]
        for prescr, stats in context['nouvelles_prescr'].items():
            yield [prescr] + [stats.get(month, 0) for month in months] + [stats['total']]
        yield ['BOLD', 'Motifs des demandes']
        for motif, stats in context['nouvelles_motifs'].items():
            yield [motif] + [stats.get(month, 0) for month in months] + [stats['total']]


class StatistiquesThemesView(StatsBase):
    template_name = 'cipe/stats-themes.html'

    def get_stats(self, months):
        query = PrestationCIPE.themes.through.objects.annotate(month=TruncMonth('prestationcipe__date')).filter(
            prestationcipe__date__lte=self.date_end,
            prestationcipe__date__gte=self.date_start
        ).exclude(
            Q(prestationcipe__type_consult='hors_stats') | Q(prestationcipe__manque=True)
        ).values('month', 'themesante__nom').annotate(nb=Count('id'))

        themes = list(ThemeSante.objects.values_list('nom', flat=True).order_by('nom'))
        stats = self.init_counters(themes, months)
        for line in query:
            month = Month.from_date(line['month'])
            stats[line['themesante__nom']][month] = line['nb']
            stats[line['themesante__nom']]['total'] += line['nb']
        return {
            'theme_stats': stats,
        }

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Thèmes de santé'] + [str(month) for month in months] + ['Total']
        for nom, stats in context['theme_stats'].items():
            yield [nom] + [stats.get(month, 0) for month in months] + [context['theme_stats'][nom]['total']]


class StatistiquesNationaliteView(StatsBase):
    template_name = 'cipe/stats-nationalite.html'

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        query = Personne.objects.annotate(
            month=TruncMonth('famille__prestationcipe__date'),
            prestations=FilteredRelation(
                'famille__prestationcipe',
                condition=(
                    ~Q(famille__prestationcipe__type_consult='hors_stats') &
                    Q(famille__prestationcipe__manque=False)
                )
            )
        ).filter(
            role__nom='Mère',
            famille__prestationcipe__date__lte=date_end,
            famille__prestationcipe__date__gte=date_start
        ).distinct().values('pk', 'month', 'pays_origine')

        stats = {}
        for line in query:
            month = Month.from_date(line['month'])
            if month not in months:
                continue
            nat = line['pays_origine'] or 'Non défini'
            if nat not in stats:
                stats.update(cls.init_counters([nat], months, total=set))
            stats[nat][month] += 1
            stats[nat]['total'].add(line['pk'])

        pays_tries = sorted(
            stats.keys(),
            key=lambda code: unicodedata.normalize('NFKD', Country(code=code).name) if code != 'Non défini' else ''
        )
        return {
            'stats': dict({
                Country(code=code_pays) if code_pays != 'Non défini' else '': stats[code_pays]
                for code_pays in pays_tries
            }),
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Nationalité de la maman'] + [str(month) for month in months] + ['Total']
        for pays, data in context['stats'].items():
            yield ([pays.name if pays else 'Non défini'] +
                   [data.get(month, 0) for month in months] +
                   [len(data['total'])])


class StatistiquesDureeView(StatsBase):
    template_name = 'cipe/stats-duree.html'

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        query = PrestationCIPE.objects.annotate(
            month=TruncMonth('date'),
            typ=Case(
                When(type_consult='cons_dom', then=Value('dom')),
                When(mode_consult='tel', then=Value('tel')),
                default=Value('centre')
            ),
        ).filter(
            date__lte=date_end, date__gte=date_start
        ).exclude(
            Q(type_consult__in=['hors_stats', 'prest_fam']) | Q(manque=True)
        ).values('month', 'typ').annotate(
            duree_courte=Count('pk', filter=Q(duree__lte=timedelta(minutes=30))),
            duree_moy=Count('pk', filter=Q(duree__gt=timedelta(minutes=30), duree__lte=timedelta(minutes=60))),
            duree_longue=Count('pk', filter=Q(duree__gt=timedelta(minutes=60))),
        )
        stats = {
            'dom': cls.init_counters(['1-30 minutes', '31-60 minutes', '>60 minutes'], months),
            'tel': cls.init_counters(['1-30 minutes', '31-60 minutes', '>60 minutes'], months),
            'centre': cls.init_counters(['1-30 minutes', '31-60 minutes', '>60 minutes'], months),
        }
        for line in query:
            month = Month.from_date(line['month'])
            stats[line['typ']]['1-30 minutes'][month] = line['duree_courte']
            stats[line['typ']]['1-30 minutes']['total'] += line['duree_courte']
            stats[line['typ']]['31-60 minutes'][month] = line['duree_moy']
            stats[line['typ']]['31-60 minutes']['total'] += line['duree_moy']
            stats[line['typ']]['>60 minutes'][month] = line['duree_longue']
            stats[line['typ']]['>60 minutes']['total'] += line['duree_longue']
        return {
            'stats': stats,
            'labels': {'dom': 'À domicile', 'tel': 'Par téléphone', 'centre': 'Dans un centre'},
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Durée des consultations'] + [str(month) for month in months] + ['Total']
        for typ, stats in context['stats'].items():
            yield ['BOLD', context['labels'][typ]]
            for key in ('1-30 minutes', '31-60 minutes', '>60 minutes'):
                yield [key] + [stats[key].get(month, 0) for month in months] + [stats[key]['total']]


class StatistiquesGenView(StatsBase):
    template_name = 'cipe/stats-gen.html'
    stat_items = {
        'num_tel': 'Nbre de téléphones',
        'num_sms': 'Nbre de SMS',
        'num_email': 'Nbre de courriels',
        'num_courr': 'Nbre de courriers parents',
        'num_patio': 'Nbre de rencontres parents-enfants',
        'part_patio': 'Participant·e·s de rencontres parents-enfants',
        'duree_patio': 'Durée des rencontres parents-enfants',
        'num_recif': 'Nbre d’animations Récif',
        'part_recif': 'Participant·e·s d’animations Récif',
        'duree_recif': 'Durée des animations Récif',
        'num_lecture': 'Nbre d’animations lecture',
        'part_lecture': 'Participant·e·s d’animations lecture',
        'duree_lecture': 'Durée des animations lecture',
        'num_sommeil': 'Nbre d’ateliers sommeil',
        'part_sommeil': 'Participant·e·s d’ateliers sommeil',
        'duree_sommeil': 'Durée des ateliers sommeil',
        'num_autre': 'Nbre d’autres animations',
        'part_autre': 'Participant·e·s d’autres animations',
        'duree_autre': 'Durée des autres animations',
        'num_colloque': 'Nbre de colloques',
        'part_colloque': 'Participant·e·s de colloques',
        'duree_colloque': 'Durée de colloques',
        'num_superv': 'Nbre de supervisions',
        'part_superv': 'Participant·e·s de supervision',
        'duree_superv': 'Durée de supervision',
        'num_interv': 'Nbre d’intervisions',
        'part_interv': 'Participant·e·s d’intervisions',
        'duree_interv': 'Durée d’intervisions',
        'num_fcont': 'Nbre de formations continues',
        'part_fcont': 'Participant·e·s de formations continues',
        'duree_fcont': 'Durée des formations continues',
        'num_seance': 'Nbre de séances externes',
        'part_seance': 'Participant·e·s de séances externes',
        'duree_seance': 'Durée des séances externes',
    }

    @classmethod
    def _get_stats(cls, date_start, date_end, months):
        simple_prest_keys = ('tel', 'sms', 'email', 'courr')
        complex_prest_keys = (
            'patio', 'recif', 'lecture', 'sommeil', 'autre', 'colloque', 'superv',
            'interv', 'fcont', 'seance'
        )
        prest_with_enfants = ('patio', 'lecture', 'autre')
        annots = {
            f'num_{prest}': Sum(f'nb_{prest}') for prest in simple_prest_keys
        }
        for prest in complex_prest_keys:
            annots.update({
                f'num_{prest}': Count('pk', filter=Q(type_prest=prest)),
                f'part_{prest}': Sum('nb_adultes', filter=Q(type_prest=prest)),
                f'duree_{prest}': Sum('duree', filter=Q(type_prest=prest)),
            })
            if prest in prest_with_enfants:
                annots[f'part_{prest}_enf'] = Sum('nb_enfants', filter=Q(type_prest=prest))
        query = PrestationGen.objects.annotate(month=TruncMonth('date')).filter(
            date__lte=date_end, date__gte=date_start
        ).values('month').annotate(**annots)
        stat_keys = list(cls.stat_items.keys()) + [f'part_{prest}_enf' for prest in prest_with_enfants]
        total_counters = {}
        for key in stat_keys:
            total_counters[key] = timedelta() if key.startswith('duree') else 0
        stats_by_month = {
            month: {k: '-' for k in stat_keys} if month.is_future() else {}
            for month in months
        }
        for line in query:
            month = Month.from_date(line['month'])
            stats_by_month[month] = line
            for key in stat_keys:
                total_counters[key] += line[key] or (timedelta() if key.startswith('duree') else 0)
        return {
            'num_keys': [f'num_{prest}' for prest in simple_prest_keys + complex_prest_keys],
            'part_keys': [f'part_{prest}' for prest in complex_prest_keys],
            'part_keys_enf': {f'part_{prest}': f'part_{prest}_enf' for prest in prest_with_enfants},
            'duree_keys': [f'duree_{prest}' for prest in complex_prest_keys],
            'stat_items': cls.stat_items,
            'stats': stats_by_month,
            'totals': total_counters,
        }

    def get_stats(self, months):
        return self._get_stats(self.date_start, self.date_end, months)

    def export_as_openxml(self, context):
        export = ExportStatistique(col_widths=[50], sheet_title='Prest. générales')
        export.fill_data(self.export_lines(context))
        # Exporter une seconde feuille avec les dates et remarques des séaces externes (#418)
        export.add_sheet('Séances externes')
        col_widths = [12, 60]
        export.write_line(['Date', 'Remarques'], bold=True, col_widths=col_widths)
        query = PrestationGen.objects.filter(
            type_prest='seance', date__lte=self.date_end, date__gte=self.date_start
        ).order_by('date')
        for seance in query:
            export.write_line(
                [seance.date.strftime("%d.%m.%Y"), seance.remarques or '-'],
                col_widths=col_widths
            )
        return export.get_http_response('StatistiquesGen.xlsx')

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Nombre de prestations générales'] + [str(month) for month in months] + ['Total']
        for key, label in self.stat_items.items():
            if key.startswith('num_'):
                yield ([label] +
                       [context['stats'][month].get(key, 0) or 0 for month in months] +
                       [context['totals'][key]])

        yield ['BOLD', 'Nombre de participants aux prestations générales']
        for key, label in self.stat_items.items():
            if key.startswith('part_'):
                enfants_key = f'{key}_enf'
                has_stats_enfants = enfants_key in context['totals']
                yield ([label + (' - Parents' if has_stats_enfants else '')] +
                       [context['stats'][month].get(key, 0) or 0 for month in months] +
                       [context['totals'][key]])
                if has_stats_enfants:
                    yield ([f'{label} - Enfants'] +
                           [context['stats'][month].get(enfants_key, 0) or 0 for month in months] +
                           [context['totals'][enfants_key]])

        yield ['BOLD', 'Durée des prestations générales']
        for key, label in self.stat_items.items():
            if key.startswith('duree_'):
                yield ([label] +
                       [context['stats'][month].get(key, timedelta(0)) or timedelta(0) for month in months] +
                       [context['totals'][key]])


class StatistiquesExportView(PermissionRequiredMixin, FormView):
    permission_required = 'cipe.add_famillecipe'
    template_name = 'cipe/stats-export.html'
    form_class = DateYearForm


class StatistiquesSanteNEView(PermissionRequiredMixin, FormView):
    permission_required = 'cipe.add_famillecipe'
    form_class = DateYearForm

    def export_lines(self, year):
        def month_up(month_num):
            return django_format(date(year, month_num, 1), 'F').title()

        months = [Month(year, month) for month in range(1, 13)]
        stats = StatistiquesLieuxView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        yield ['BOLD', '1. Nombres de consultations par antenne']
        yield ['BOLD', 'Mois'] + list(stats['centres'].keys()) + ['Total général']
        for month in months:
            yield [month_up(month.month)] + [
                stats['centres'][centre].get(month, 0) for centre in stats['centres'].keys()
            ] + [stats['total_stats'].get(month, 0)]
        yield ['BOLD', 'Total'] + [
            stats['centres'][centre]['total'] for centre in stats['centres']
        ] + [stats['total_stats']['total']]

        yield []
        yield []
        stats = StatistiquesTypeView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        yield ['BOLD', '2. Nombres de consultations par type']
        yield ['BOLD', 'Mois', 'Centre de consultation', 'Par téléphone', 'À domicile', 'Total général']
        for month in months:
            line = [stats['stats'][typ][month] for typ in ['num_centre', 'par_tel', 'domicile']]
            yield [month_up(month.month)] + line + [sum([num for num in line if num != '-'])]
        line = [stats['stats'].get(typ, {'total': 0})['total'] for typ in ['num_centre', 'par_tel', 'domicile']]
        yield ['BOLD', 'Total'] + line + [sum([num for num in line if num != '-'])]

        yield []
        yield []
        yield ['BOLD', '3. Consultations à domicile par motif']
        yield ['BOLD', 'Motif', 'Nb consultations']
        for domtyp in stats['prest_dict']:
            label = stats['prest_dict'][domtyp]
            yield [label[0].upper() + label[1:], stats['stats'][domtyp]['total']]
        yield ['Total', stats['stats']['domicile']['total']]

        yield []
        yield []
        yield ['BOLD', '4. Nouvelles inscriptions (enfants) par mois']
        yield ['BOLD', 'Mois', 'Nombre']
        stats = StatistiquesNouvellesView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        for month in months:
            yield [month_up(month.month), stats['nouvelles'].get(month, 0)]
        yield ['BOLD', 'Total', stats['nouvelles']['total']]

        yield []
        yield []
        yield ['BOLD', '5. Nombre de conseils par courriel et SMS']
        yield ['BOLD', 'Mois', 'Courriels', 'SMS']
        stats = StatistiquesGenView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        for month in months:
            yield [
                month_up(month.month),
                stats['stats'].get(month, {}).get('num_email', 0),
                stats['stats'].get(month, {}).get('num_sms', 0),
            ]
        yield ['BOLD', 'Total', stats['totals']['num_email'], stats['totals']['num_sms']]

        yield []
        yield []
        yield ['BOLD', '6. Animations lecture et ateliers pour parents']
        yield ['BOLD', 'Mois', 'Animations lecture', 'Nb enfants', 'Rencontres parents-enfants', 'Nb parents']
        for month in months:
            yield [
                month_up(month.month),
                stats['stats'].get(month, {}).get('num_lecture', 0),
                stats['stats'].get(month, {}).get('part_lecture', 0) or 0,
                stats['stats'].get(month, {}).get('num_patio', 0),
                stats['stats'].get(month, {}).get('part_patio', 0) or 0,
            ]
        yield ['BOLD', 'Total'] + [
            stats['totals'][key] for key in ['num_lecture', 'part_lecture', 'num_patio', 'part_patio']
        ]

        yield []
        yield []
        yield ['BOLD', '7. Ateliers à l’Espace mamans de Récif']
        yield ['BOLD', 'Mois', 'Nb animations', 'Nb mamans']
        for month in months:
            yield [
                month_up(month.month),
                stats['stats'].get(month, {}).get('num_recif', 0),
                stats['stats'].get(month, {}).get('part_recif', 0) or 0,
            ]
        yield ['BOLD', 'Total'] + [
            stats['totals'][key] for key in ['num_recif', 'part_recif']
        ]

        yield []
        yield []
        yield ['BOLD', '8. Autres ateliers']
        yield ['BOLD', 'Mois', 'Nb animations', 'Nb participant·es']
        for month in months:
            yield [
                month_up(month.month),
                stats['stats'].get(month, {}).get('num_autre', 0),
                stats['stats'].get(month, {}).get('part_autre', 0) or 0,
            ]
        yield ['BOLD', 'Total'] + [
            stats['totals'][key] for key in ['num_autre', 'part_autre']
        ]

    def form_valid(self, form):
        export = ExportStatistique(sheet_title="Statistiques IPE")
        export.fill_data(self.export_lines(int(form.cleaned_data['year'])))
        return export.get_http_response(f"StatistiquesSanteNE{form.cleaned_data['year']}.xlsx")


class StatistiquesASCPEView(PermissionRequiredMixin, FormView):
    permission_required = 'cipe.add_famillecipe'
    form_class = DateYearForm
    # note openpyxl does not support rich text inside cells
    stat_items = {
        'nb_habs': ("Nombre d'habitants dans la zone de consultation", "nombre d'habitants"),
        'nb_naiss': ("Nombre de naissances par an dans la zone de consultation", "nombre de naissances"),
        'age_moy': ("Âge moyen de tous les enfants conseillés", "en semaines"),
        'enf_cons_0_1': ("Nombre d'enfants conseillés au cours de leur première année de vie", "nombre d'enfants"),
        'total_consult': ("Nombre de consultations par an (total)", "total des consultations"),
        'total_fam': ("Nombre de familles conseillées (total)", "nombre de familles"),
        'total_enf': ("Nombre d'enfants conseillés (total)", "nombre d'enfants"),
        'age_prem_contact': ("Âge de l'enfant lors du premier contact (uniquement si le premier contact "
                             "est explicitement indiqué)", "en semaines"),
        'nb_consult_01': ("Nombre de consultations pour les enfants de 0 à 1 an ", "nombre de consultations"),
        'nb_consult_12': ("Nombre de consultations pour les enfants de 1 à 2 ans", "nombre de consultations"),
        'nb_consult_23': ("Nombre de consultations pour les enfants de 2 à 3 ans", "nombre de consultations"),
        'nb_consult_34': ("Nombre de consultations pour les enfants de 3 à 4 ans", "nombre de consultations"),
        'nb_consult_4+': ("Nombre de consultations pour les enfants de plus de 4 ans", "nombre de consultations"),
        'nb_consult_m': ("Nombre de consultations pour des garçons", "nombre de consultations"),
        'nb_consult_f': ("Nombre de consultations pour des filles", "nombre de consultations"),
        'nb_consult_parents': ("Nombre de consultations avec les parents de l'enfant (les deux parents sont présents)",
                               "nombre de consultations"),
        'nb_consult_mere': ("Nombre de consultations pour la mère de l'enfant (seulement la mère présente)",
                            "nombre de consultations"),
        'nb_consult_pere': ("Nombre de consultations pour le père de l'enfant (seulement le père présent)",
                            "nombre de consultations"),
        'nb_consult_centre': ("Nombre de consultations au centre de consultation (avec ou sans rendez-vous)",
                              "nombre de consultations"),
        'nb_consult_sansrdv': ("Nombre de consultations sans rendez-vous", "nombre de consultations"),
        'nb_consult_rdv': ("Nombre de consultations avec notification préalable", "nombre de consultations"),
        'nb_visites': ("Nombre de visites à domicile", "nombre de consultations"),
        'nb_consult_tel': ("Nombre de consultations téléphoniques", "nombre de consultations"),
        'nb_consult_email': ("Nombre de consultations par e-mail", "nombre de consultations"),
        'nb_consult_chat': ("Nombre de consultations par chat", "nombre de consultations"),
        'nb_consult_video': ("Nombre de consultations par vidéoconférence / face-time", "nombre de consultations"),
        'nb_consult_groupe': ("Nombre de consultations de groupe", "nombre de consultations"),
        'prem_cont_sans_consult': ("Nombre de premiers contacts sans consultation", "nombre de contacts"),
        'nb_cont_sans_consult': ("Nombre de contacts sans consultation (par exemple pour fixer un rendez-vous)",
                                 "nombre de contacts"),
        'nb_consult_1_30': ("Nombre de consultations 1 - 30 minutes (courtes)", "nombre de consultations"),
        'nb_consult_31_60': ("Nombre de consultations 31 - 60 minutes (longues)", "nombre de consultations"),
        'nb_consult_60+': ("Nombre de consultations de plus de 60 minutes (extra longues)", "nombre de consultations"),
        'prc_consult_sante': ("Pourcentage de consultations concernant le thème de la santé",
                              "nombre de consultations"),
        'prc_consult_dev': ("Pourcentage de consultations concernant le thème du développement",
                            "nombre de consultations"),
        'prc_consult_educ': ("Pourcentage de consultations concernant le thème de l'éducation",
                             "nombre de consultations"),
        'prc_consult_nutr': ("Pourcentage de consultations concernant le thème de la nutrition",
                             "nombre de consultations"),
        'prc_consult_allait': ("Pourcentage de consultations concernant le thème de l'allaitement maternel",
                               "nombre de consultations"),
        'prc_consult_somm': ("Pourcentage de consultations concernant le thème du sommeil", "nombre de consultations"),
        'prc_consult_peser': ("Pourcentage de consultations concernant le thème du peser et mesurer",
                              "nombre de consultations"),
        'prc_consult_psycho': ("Pourcentage de consultations concernant des questions psychosociales",
                               "nombre de consultations"),
        'nb_consult_interp': ("Nombre de consultations avec un interprète ou un médiateur interculturel",
                              "nombre de consultations"),
        'langue_mere_fr': ("La langue de communication avec la mère est français.", "nombre de mères"),
        'prop_conseil': ("Proportion d'enfants conseillés au cours de la 1ère année de vie par rapport "
                         "à l'ensemble des naissances de la zone", "Proportion d'enfants (en %)"),
        'prop_conseil_ch': ("Proportion de mères conseillées ayant la nationalité CH", "Proportion de mères (en %)"),
        'prop_conseil_ue': ("Proportion de mères conseillées ayant la nationalité d'un pays de l'UE",
                            "Proportion de mères (en %)"),
        'prop_conseil_af': ("Proportion de mères conseillées ayant la nationalité d'un pays d'Afrique",
                            "Proportion de mères (en %)"),
        'prop_conseil_as': ("Proportion de mères conseillées ayant la nationalité d'un pays de'Asie",
                            "Proportion de mères (en %)"),
        'prop_conseil_na': ("Proportion de mères conseillées ayant la nationalité d'un pays de l'Amérique du Nord",
                            "Proportion de mères (en %)"),
        'prop_conseil_sa': ("Proportion de mères conseillées ayant la nationalité d'un pays de l'Amérique du Sud",
                            "Proportion de mères (en %)"),
        'prop_conseil_oc': ("Proportion de mères conseillées ayant la nationalité de l'Australie / Nouvelle-Zélande",
                            "Proportion de mères (en %)"),
    }

    def export_lines(self, year):
        months = [Month(year, month) for month in range(1, 13)]

        fam_stats = StatistiquesFamillesView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        stats = {
            'enf_cons_0_1': fam_stats['enfants']['totals']['num_01'],
            'total_consult':  fam_stats['consults']['totals']['total'],
            'total_fam': fam_stats['nb_fam_tot'],
            'total_enf': fam_stats['enfants']['totals']['total'],
            'nb_consult_01': fam_stats['consults']['totals']['num_01'],
            'nb_consult_12': fam_stats['consults']['totals']['num_12'],
            'nb_consult_23': fam_stats['consults']['totals']['num_23'],
            'nb_consult_34': fam_stats['consults']['totals']['num_34'],
            'nb_consult_4+': fam_stats['consults']['totals']['num_4more'],
            'nb_consult_m': fam_stats['consults']['totals']['num_garcons'],
            'nb_consult_f': fam_stats['consults']['totals']['num_filles'],
        }
        if fam_stats['enfants']['totals']['age_moy']:
            stats['age_moy'] = round(fam_stats['enfants']['totals']['age_moy'].days / 7, 2)
        else:
            stats['age_moy'] = 0
        duree_stats = StatistiquesDureeView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        stats.update({
            'nb_consult_1_30': duree_stats['stats'].get('1-30 minutes', {'total': 0})['total'],
            'nb_consult_31_60': duree_stats['stats'].get('31-60 minutes', {'total': 0})['total'],
            'nb_consult_60+': duree_stats['stats'].get('>60 minutes', {'total': 0})['total'],
        })
        gen_stats = StatistiquesGenView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        stats.update({
            'nb_consult_email': gen_stats['totals']['num_email'],
            'nb_consult_tel': gen_stats['totals']['num_tel'],
            'nb_consult_groupe': gen_stats['totals']['num_patio'],
            'nb_cont_sans_consult': gen_stats['totals']['num_tel'],  # idem nb_consult_tel ?
        })
        typ_stats = StatistiquesTypeView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        stats.update({
            'nb_consult_centre': typ_stats['stats']['num_centre']['total'],
            'nb_visites': typ_stats['stats']['domicile']['total'],
            'nb_consult_rdv': typ_stats['stats']['num_rzv']['total'],
            'nb_consult_sansrdv': typ_stats['stats']['num_no_rzv']['total'],
        })
        nat_stats = StatistiquesNationaliteView._get_stats(date(year, 1, 1), date(year, 12, 31), months)
        continents = {'CH': 0, 'EU': 0, 'AS': 0, 'AF': 0, 'NA': 0, 'SA': 0, 'OC': 0}
        for country, data in nat_stats['stats'].items():
            if country == '':
                continue
            elif country.code == 'CH':
                continents['CH'] += len(data['total'])
            else:
                cont = continent_from_country_code(country.code)
                if cont:
                    continents[cont] += len(data['total'])
        total = sum(continents.values())
        continents = {key: (num / total * 100) if total else 0 for key, num in continents.items()}
        stats.update({
            'prop_conseil_ch': continents['CH'],
            'prop_conseil_ue': continents['EU'],
            'prop_conseil_as': continents['AS'],
            'prop_conseil_af': continents['AF'],
            'prop_conseil_na': continents['NA'],
            'prop_conseil_sa': continents['SA'],
            'prop_conseil_oc': continents['OC'],
        })
        # TODO: Match ThemeSante to:
        '''
        stats.update({
            'prc_consult_sante': ,
            'prc_consult_dev': ,
            'prc_consult_educ': ,
            'prc_consult_nutr': ,
            'prc_consult_allait': ,
            'prc_consult_somm': ,
            'prc_consult_peser': ,
            'prc_consult_psycho': ,
        })
        '''

        yield ['BOLD', 'Description de l’indicateur', 'Unité à spécifier', f'Données pour l’année {year}']
        for key, data in self.stat_items.items():
            yield [data[0], data[1], stats.get(key, '')]

    def form_valid(self, form):
        export = ExportStatistique(col_widths=[85, 28, 30])
        export.fill_data(self.export_lines(int(form.cleaned_data['year'])))
        return export.get_http_response(f"StatistiquesASCPE{form.cleaned_data['year']}.xlsx")
