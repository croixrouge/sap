from django.contrib import admin

from croixrouge.admin import FamilleAdminBase

from .models import FamilleAMF, Formation, PrestationAMF, SuiviAMF


@admin.register(FamilleAMF)
class FamilleAMFAdmin(FamilleAdminBase):
    def get_queryset(self, request):
        return FamilleAMF.objects.filter(typ=["amf"])


@admin.register(SuiviAMF)
class SuiviAMFAdmin(admin.ModelAdmin):
    list_display = ["famille", "date_demande_spaj", "autorisation"]
    list_filter = ["autorisation"]
    ordering = ["famille__nom"]
    search_fields = ["famille__nom"]


@admin.register(PrestationAMF)
class PrestationAMFAdmin(admin.ModelAdmin):
    list_display = ["lib_prestation", "date_prestation", "duree", "auteur"]
    search_fields = ["famille__nom", "texte"]


@admin.register(Formation)
class FormationAdmin(admin.ModelAdmin):
    list_display = ["nom", "date", "heure_debut"]
    search_fields = ["nom"]
    date_hierarchy = "date"
