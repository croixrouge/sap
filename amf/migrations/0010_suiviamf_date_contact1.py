from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0009_remove_formation_jauge'),
    ]

    operations = [
        migrations.AddField(
            model_name='suiviamf',
            name='date_contact1',
            field=models.DateField(blank=True, null=True, verbose_name='Date premier contact'),
        ),
    ]
