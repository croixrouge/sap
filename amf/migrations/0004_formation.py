from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0003_alter_suiviamf_autorisation'),
    ]

    operations = [
        migrations.CreateModel(
            name='Formation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, verbose_name='Nom')),
                ('date', models.DateField(verbose_name='Date')),
                ('heure', models.TimeField(blank=True, null=True, verbose_name='Heure')),
                ('jauge', models.PositiveSmallIntegerField(verbose_name='Jauge')),
                ('participants', models.ManyToManyField(blank=True, related_name='formations', to='amf.familleamf', verbose_name='Familles participantes')),
            ],
            options={
                'ordering': ['date', 'heure'],
            },
        ),
    ]
