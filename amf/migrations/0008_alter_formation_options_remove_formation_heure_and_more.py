from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0007_alter_familleamf_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='formation',
            options={'ordering': ['date', 'heure_debut', 'heure_fin', 'nom']},
        ),
        migrations.RemoveField(
            model_name='formation',
            name='heure',
        ),
        migrations.AddField(
            model_name='formation',
            name='description',
            field=models.CharField(blank=True, verbose_name='Courte description'),
        ),
        migrations.AddField(
            model_name='formation',
            name='heure_debut',
            field=models.TimeField(blank=True, null=True, verbose_name='Heure de début'),
        ),
        migrations.AddField(
            model_name='formation',
            name='heure_fin',
            field=models.TimeField(blank=True, null=True, verbose_name='Heure de fin'),
        ),
        migrations.AddField(
            model_name='formation',
            name='information_delais_inscription',
            field=models.CharField(blank=True, verbose_name="Information sur le délai d'inscription"),
        ),
        migrations.AddField(
            model_name='formation',
            name='information_formateurs',
            field=models.CharField(blank=True, verbose_name='Information sur le(s) formateur(s)'),
        ),
        migrations.AddField(
            model_name='formation',
            name='lieu',
            field=models.CharField(blank=True),
        ),
    ]
