from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0006_suiviamf_enfants_souhaites_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='familleamf',
            options={'permissions': {('view_amf_stats', 'Voir les statistiques AMF')}},
        ),
    ]
