from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0005_suiviamf_referent_famille'),
    ]

    operations = [
        migrations.AddField(
            model_name='suiviamf',
            name='enfants_souhaites',
            field=models.CharField(blank=True, max_length=100, verbose_name='Enfants souhaités'),
        ),
        migrations.AddField(
            model_name='suiviamf',
            name='places_accueil_dispo',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name="Places d'accueil disponibles"),
        ),
    ]
