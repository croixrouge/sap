from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0004_formation'),
    ]

    operations = [
        migrations.AddField(
            model_name='suiviamf',
            name='referent_famille',
            field=models.CharField(blank=True, max_length=50, verbose_name='Référent de la famille (CE)'),
        ),
    ]
