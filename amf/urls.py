from django.urls import path

from . import views, views_stats

urlpatterns = [
    # Famille
    path('famille/list/', views.FamilleListView.as_view(), name='amf-famille-list'),
    path('famille/suivis-termines/', views.FamilleListView.as_view(mode='termines'),
         name='amf-famille-suivis-termines'),
    path('famille/add/', views.FamilleCreateView.as_view(), name='amf-famille-add'),
    path('famille/<int:pk>/edit/', views.FamilleUpdateView.as_view(), name='amf-famille-edit'),
    path('famille/<int:pk>/suivi/', views.AMFSuiviView.as_view(), name='amf-famille-suivi'),

    # Journal
    path('famille/<int:pk>/prestation/list/', views.PrestationListView.as_view(), name='amf-journal-list'),
    path('famille/<int:pk>/prestation/add/', views.PrestationCreateView.as_view(), name='amf-prestation-famille-add'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/edit/', views.PrestationUpdateView.as_view(),
         name='amf-prestation-edit'),
    path('famille/<int:pk>/prestation/<int:obj_pk>/delete/', views.PrestationDeleteView.as_view(),
         name='amf-prestation-delete'),

    path('formation/list/', views.FormationListView.as_view(), name="amf-formation-list"),
    path('formation/add/', views.FormationCreateView.as_view(), name="amf-formation-add"),
    path('formation/<int:pk>/edit/', views.FormationUpdateView.as_view(), name="amf-formation-edit"),
    path('formation/<int:pk>/inscription/', views.FormationInscriptionView.as_view(),
         name="amf-formation-inscription"),
    path('famille/<int:pk>/inscription/autocomplete/', views.ParticipantsFormationAutocomplete.as_view(),
         name='amf-inscription-autocomplete'),

    path('stats/familles/', views_stats.StatistiquesFamillesView.as_view(), name='amf-stats-familles'),

    path('importation-spaj/', views.ImportationSPAJView.as_view(), name='amf-importation'),
]
