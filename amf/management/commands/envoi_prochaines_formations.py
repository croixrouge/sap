from datetime import date

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.utils.dateformat import format as django_format

from common.utils import maintenant
from croixrouge.models import Personne, Role

from ...models import FamilleAMF, Formation


class Command(BaseCommand):
    help = "Envoie les formations à venir par email"

    def add_arguments(self, parser):
        parser.add_argument("--envoi-reel", action="store_true", help="Si omis, envoie à des emails tests")

    @staticmethod
    def get_1_mois_suivant(val=None):
        if not val:
            val = maintenant().date()
        return date(val.year, val.month, 1) + relativedelta(months=1)

    def handle(self, *args, **options):
        mode_test = not options["envoi_reel"] if "envoi_reel" in options else False
        if mode_test and not settings.ENVOI_EMAIL_TEST:
            raise ImproperlyConfigured("Valeur manquante dans les settings: ENVOI_EMAIL_TEST")

        date_prochain_mois_1 = self.get_1_mois_suivant()
        date_prochain_mois_2 = self.get_1_mois_suivant(date_prochain_mois_1)
        formations = Formation.objects.filter(
            date__gte=date_prochain_mois_1,
            date__lt=date_prochain_mois_2,
        )
        if not formations:
            self.stdout.write("Pas de formation à proposer")
            return

        emails = list(set(  # "set" évite les doublons
            p.email for p in Personne.objects.filter(
                famille__in=FamilleAMF.objects.filter(suiviamf__date_fin_accueil__isnull=True),
                role=Role.objects.get_parent_famille_accueil(),
            ).distinct() if p.email
        )) if not mode_test else settings.ENVOI_EMAIL_TEST

        context = {
            "formations": formations,
            "titre": f"Rappel des formations - {django_format(date_prochain_mois_1, 'F Y').capitalize()}",
            "email": settings.AMF_EMAIL,
            "telephone": settings.AMF_TELEPHONE,
            "mois": django_format(date_prochain_mois_1, 'F'),
        }
        content_txt = render_to_string("amf/emails/prochaines_formations.txt", context=context)
        content_html = render_to_string("amf/emails/prochaines_formations.html", context=context)
        sent = 0
        for email in emails:
            msg = EmailMultiAlternatives(
                context["titre"],
                content_txt,
                settings.AMF_EMAIL,
                [email],
            )
            msg.attach_alternative(content_html, "text/html")
            try:
                msg.send()
            except OSError:
                pass
            else:
                sent += 1
        self.stdout.write(
            self.style.SUCCESS(f"{sent}/{len(emails)} envoyés")
        )
