from datetime import timedelta

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Count, DateField, F, Q, Value
from django.db.models.functions import TruncMonth
from django.views.generic import TemplateView

from amf import choices as amf_choices
from amf.models import PrestationAMF, SuiviAMF
from croixrouge.models import Personne, Role
from croixrouge.stat_utils import Month, StatsMixin, permute_niveaux_dict


class StatsBase(StatsMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'amf.view_amf_stats'


class StatistiquesFamillesView(StatsBase):
    template_name = 'amf/stats-familles.html'


    @staticmethod
    def _get_enfants_suivis(date_debut, date_fin, date_fin_inclusive=False):
        query = Personne.objects.filter(
            Q(role=Role.objects.get_enfant_suivi()) &
            (
                Q(famille__suiviamf__date_fin_accueil__isnull=True) |
                Q(famille__suiviamf__date_fin_accueil__gte=date_debut)
            ) &
            Q(amf_debut_de_placement__isnull=False) &
            (
                Q(amf_fin_de_placement__isnull=True) |
                Q(amf_fin_de_placement__gte=date_debut)
            )
        )
        if date_fin_inclusive:
            return query.filter(amf_debut_de_placement__lte=date_fin)
        return query.filter(amf_debut_de_placement__lt=date_fin)


    @staticmethod
    def _get_suivis_autorises(date_debut, date_fin, date_fin_inclusive=True):
        query = SuiviAMF.objects.filter(
            Q(autorisation=amf_choices.AutorisationChoices.AUTORISEE) &
            (
                Q(date_fin_accueil__isnull=True) |
                Q(date_fin_accueil__gte=date_debut)
            )
        )
        if date_fin_inclusive:
            return query.filter(date_autorisation__lte=date_fin)
        return query.filter(date_autorisation__lt=date_fin)

    @classmethod
    def _get_suivis_autorises_sans_enfant(cls, date_debut, date_fin, date_fin_inclusive=True):
        suivis_autorises = cls._get_suivis_autorises(date_debut, date_fin, date_fin_inclusive)
        enfants_suivis = cls._get_enfants_suivis(date_debut, date_fin, date_fin_inclusive)
        return suivis_autorises.exclude(famille__in=enfants_suivis.values_list('famille', flat=True))

    @classmethod
    def get_stats_enfants_suivis(cls, date_debut, date_fin, date_fin_inclusive=False):
        """
        Retourne un dictionnaire
        """
        aggr = {}
        aggr["total"] = Count("id", distinct=True)
        aggr["0-4_ans"] = Count(
            "id",
            filter=Q(age_jours__lt=timedelta(days=1826)),  # 1826 = 5 ans
            distinct=True,
        )
        aggr["5-12_ans"] = Count(
            "id",
            filter=Q(
                age_jours__gte=timedelta(days=1826),  # 1826 = 5 ans
                age_jours__lt=timedelta(days=4748),  # 4748 = 13 ans
            ),
            distinct=True,
        )
        aggr["13_ans_et_plus"] = Count(
            "id",
            filter=Q(age_jours__gte=timedelta(days=4748)),  # 4748 = 13 ans
            distinct=True,
        )
        aggr["filles"] = Count("id", filter=Q(genre="F"), distinct=True)
        aggr["garçons"] = Count("id", filter=Q(genre="M"), distinct=True)
        for type_acc in amf_choices.TypeAccueilChoices:
            type_acc_format = f'familles_{type_acc.replace(" ", "_")}'
            aggr[type_acc_format] = Count("id", filter=Q(amf_type_accueil=type_acc), distinct=True)

        stats = cls._get_enfants_suivis(
            date_debut,
            date_fin,
            date_fin_inclusive
        ).annotate(
            age_jours=Value(date_debut, output_field=DateField()) - F('date_naissance'),
        ).aggregate(**aggr)
        stats = {key.replace('_', ' '): value for key, value in stats.items()}
        # Ajouter un libellé complet
        label = "Enfants en famille d'accueil"
        return {
            label if key == "total" else f"{label} - {key}": value
            for key, value in stats.items()
        }

    def get_stats(self, months):
        data = {}

        prestations_query = PrestationAMF.objects.annotate(
            month=TruncMonth('date_prestation'),
        ).filter(
            date_prestation__gte=self.date_start,
            date_prestation__lte=self.date_end,
        ).exclude(
            lib_prestation__code="amf03",  # Information
        )

        # Statistique nombres de visites
        data["Visites dans les familles"] = {
            Month.from_date(line['month']): line['tot']
            for line in prestations_query.values('month').annotate(tot=Count("id", distinct=True))
        }
        data["Visites dans les familles"]["total"] = prestations_query.aggregate(tot=Count("id", distinct=True))["tot"]

        # Statistique nombres de familles visitées
        data["Familles visitées"] = {
            Month.from_date(line['month']): line['tot']
            for line in prestations_query.values('month').annotate(tot=Count('famille', distinct=True))
        }
        data["Familles visitées"]["total"] = prestations_query.aggregate(
            tot=Count('famille', distinct=True)
        )["tot"]

        # Statistique SuiviAMF.date_contact1
        nb_contact1_query = SuiviAMF.objects.annotate(
            month=TruncMonth('date_contact1'),
        ).filter(
            date_contact1__gte=self.date_start,
            date_contact1__lte=self.date_end,
        )
        data["1ers contacts"] = {
            Month.from_date(line['month']): line['tot']
            for line in nb_contact1_query.values("month").annotate(tot=Count("id", distinct=True))
        }
        data["1ers contacts"]["total"] = nb_contact1_query.aggregate(tot=Count("id"))["tot"]

        # Statistique SuiviAMF.date_entretien1
        nb_entretien1_query = SuiviAMF.objects.annotate(
            month=TruncMonth('date_entretien1'),
        ).filter(
            date_entretien1__gte=self.date_start,
            date_entretien1__lte=self.date_end,
        )
        data["1ers entretiens"] = {
            Month.from_date(line['month']): line['tot']
            for line in nb_entretien1_query.values("month").annotate(tot=Count("id"))
        }
        data["1ers entretiens"]["total"] = nb_entretien1_query.aggregate(tot=Count("id"))["tot"]

        # Statistique SuiviAMF.demande_spaj
        nb_demande_spaj_query = SuiviAMF.objects.annotate(
            month=TruncMonth('date_demande_spaj'),
        ).filter(
            date_demande_spaj__gte=self.date_start,
            date_demande_spaj__lte=self.date_end,
        )
        data["Demandes SPAJ"] = {
            Month.from_date(line['month']): line['tot']
            for line in nb_demande_spaj_query.values("month").annotate(tot=Count("id"))
        }
        data["Demandes SPAJ"]["total"] = nb_demande_spaj_query.aggregate(tot=Count("id"))["tot"]

        # Statistique familles avec enfants suivis
        data["Familles d'accueil accueillant un/des enfant(s)"] = {
            "total": self._get_enfants_suivis(
                self.date_start,
                self.date_end,
                date_fin_inclusive=True,
            ).aggregate(tot=Count("famille", distinct=True))["tot"]
        }
        for month in months:
            data["Familles d'accueil accueillant un/des enfant(s)"][month] = (
                self._get_enfants_suivis(
                    month.get_first_of_the_month(),
                    month.get_next().get_first_of_the_month(),
                    date_fin_inclusive=False,
                ).aggregate(tot=Count("famille", distinct=True))["tot"]
            )

        # Statistique familles en attente d'accueil (statut autorisé, sans enfant suivi)
        data["Familles en attente d'accueillir un enfant"] = {
            "total": self._get_suivis_autorises_sans_enfant(
                self.date_start,
                self.date_end,
                date_fin_inclusive=True,
            ).count()
        }
        for month in months:
            data["Familles en attente d'accueillir un enfant"][month] = (
                self._get_suivis_autorises_sans_enfant(
                    month.get_first_of_the_month(),
                    month.get_next().get_first_of_the_month(),
                    date_fin_inclusive=False,
                ).count()
            )

        # Statistiques nombre d'enfants suivis
        enfants_suivis = {
            "total": self.get_stats_enfants_suivis(
                self.date_start,
                self.date_end,
                date_fin_inclusive=True,
            )
        }
        for month in months:
            enfants_suivis[month] = self.get_stats_enfants_suivis(
                month.get_first_of_the_month(),
                month.get_next().get_first_of_the_month(),
                date_fin_inclusive=False,
            )
        data.update(permute_niveaux_dict(enfants_suivis))

        return {"stats": data}

    def export_lines(self, context):
        months = context['months']

        yield ['BOLD', 'Statistiques AMF'] + [str(month) for month in months] + ['Total']
        for label, line in context["stats"].items():
            yield (
                [label] +
                [line.get(month, 0) for month in months] +
                [line.get("total", 0)]
            )
