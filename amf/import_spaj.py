import logging
import re
from datetime import datetime
from itertools import zip_longest

from django.db import transaction
from django.db.models import Q

from croixrouge.models import Contact, Personne, Role

from . import choices as amf_choices
from .models import FamilleAMF

__all__ = ["importer_ligne_spaj", "PlusieursFamillesTrouvees"]


logger = logging.getLogger('django')


class PlusieursFamillesTrouvees(Exception):
    pass


@transaction.atomic
def importer_ligne_spaj(data):
    """ Retourne un message d'information sur l'action faite """
    famille, famille_created = importer_donnees_famille(data)
    importer_donnees_suivi(famille.suiviamf, data)
    if data["Nom de l'enfant"] or data["Prénom de l'enfant"]:
        enfant, enfant_created = importer_donnees_enfant(famille, data)
        importer_ope(enfant, data)
    else:
        enfant, enfant_created = None, False
    importer_remarques(famille, enfant, data)

    prefix_ligne = f"[ligne {data['_num_ligne']}]"
    if famille_created and enfant_created:
        return (
            f"{prefix_ligne} Création d'une nouvelle famille «{famille.nom}» et création "
            f"d'un nouvel enfant accueilli «{enfant.prenom} {enfant.nom}»"
        )
    elif famille_created:
        return f"{prefix_ligne} Création d'une nouvelle famille «{famille.nom}»"
    elif enfant_created:
        return (
            f"{prefix_ligne} Création d'un nouvel enfant accueilli «{enfant.prenom} {enfant.nom}» "
            f"pour la famille {famille.nom}"
        )
    else:
        return f"{prefix_ligne} La mise à jour de la famille «{famille.nom}» s'est bien passée"


def importer_donnees_famille(data):
    npa, localite = data["NPA Localité"].split(maxsplit=1)
    telephones = [
        tel.strip() for tel in data["Téléphone de la famille"].split('\n')
    ] if data["Téléphone de la famille"] else [""]
    emails = [
        e.strip() for e in data["Courriel de la famille"].split('\n')
    ] if data["Courriel de la famille"] else [""]

    noms_prenoms = get_noms_prenoms(data["Famille d'accueil"], data["Prénoms de la famille"])
    nom_famille = noms_prenoms[0][0]
    try:
        famille = FamilleAMF.objects.get_familles_actives().get(
            nom__icontains=nom_famille,
            npa=npa
        )
    except FamilleAMF.DoesNotExist:
        created = True
        famille = FamilleAMF.objects.create_famille(
            nom=nom_famille,
            npa=npa,
            localite=localite,
            rue=data["Adresse de la famille"],
            telephone=telephones[0] if telephones else "",
        )
        for nom, prenom in noms_prenoms:
            email = emails.pop(0) if len(emails) > 1 else emails[0]
            telephone = telephones.pop(0) if len(telephones) > 1 else telephones[0]
            Personne.objects.create(
                famille=famille,
                role=Role.objects.get_parent_famille_accueil(),
                nom=nom,
                prenom=prenom,
                npa=famille.npa,
                localite=famille.localite,
                rue=famille.rue,
                telephone=telephone,
                email=email,
            )
    except FamilleAMF.MultipleObjectsReturned:
        nom_famille = nom_famille
        num_ligne = data["_num_ligne"]
        raise PlusieursFamillesTrouvees(
            f'Il y a plusieurs familles du nom de "{nom_famille}" dans le système (numéro postal {npa}).\n'
            f"Les données relatives à la ligne {num_ligne} n'ont pas pu être importées."
        )
    else:
        created = False
        famille.localite = localite
        famille.rue = data["Adresse de la famille"]
        if famille.telephone not in telephones:
            famille.telephone = telephones[0] if telephones else ""
        famille.save()
    return famille, created


def importer_donnees_suivi(suivi, data):
    suivi.autorisation = amf_choices.AutorisationChoices.get_key_by_label(
        data["Statut de la famille"],
        insensitive=True,
    )
    suivi.referent_famille = data["CE"] or ""
    suivi.places_accueil_dispo = data["Places d'accueil disponibles"]
    suivi.enfants_souhaites = data["Enfants souhaités"] or ""
    suivi.save()


def importer_donnees_enfant(famille, data):
    # Note: pas plus d'un enfant par ligne possible

    def clean_date(val):
        return val.date() if isinstance(val, datetime) else None

    date_naissance = clean_date(data["Année de naissance"])
    debut_de_placement = clean_date(data["Début de placement"])
    fin_de_placement = clean_date(data["Fin de placement"])
    type_accueil = data["Désignation"] if data["Désignation"] in amf_choices.TypeAccueilChoices else ""
    if not data["Provenance"]:
        provenance = ""
        provenance_details = ""
    elif data["Provenance"] in amf_choices.ProvenanceChoices:
        provenance = data["Provenance"]
        provenance_details = ""
    else:
        provenance = amf_choices.ProvenanceChoices.AUTRE.value
        provenance_details = data["Provenance"]

    try:
        enfant = Personne.objects.get(
            famille=famille,
            role__nom__in=['Enfant suivi', 'Enfant non-suivi'],
            nom__icontains=data["Nom de l'enfant"],
            prenom__icontains=data["Prénom de l'enfant"],
        )
    except Personne.DoesNotExist:
        created = True
        enfant = Personne.objects.create(
            famille=famille,
            role=Role.objects.get_enfant_suivi(),
            nom=data["Nom de l'enfant"],
            prenom=data["Prénom de l'enfant"],
            npa=famille.npa,
            localite=famille.localite,
            rue=famille.rue,
            date_naissance=date_naissance,
            amf_debut_de_placement=debut_de_placement,
            amf_fin_de_placement=fin_de_placement,
            amf_type_accueil=type_accueil,
            amf_provenance=provenance,
            amf_provenance_details=provenance_details,
        )
    else:
        created = False
        enfant.date_naissance = date_naissance
        enfant.amf_debut_de_placement = debut_de_placement
        enfant.amf_fin_de_placement = fin_de_placement
        if type_accueil:
            enfant.amf_type_accueil = type_accueil
        if provenance:
            enfant.amf_provenance = provenance
        if provenance_details:
            enfant.amf_provenance_details = provenance_details
        enfant.save()
    return enfant, created


def importer_remarques(famille, enfant, data):
    """
    Il y a un champ "Remarques" qui peut être en lien avec la famille ou avec l'enfant.
    Il a été définit que les remarques pour une famille sans enfant accueilli seront attribuées à l'objet famille,
    les remarques pour une famille avec un enfant accueilli seront attribuées à l'objet enfant.
    """
    remarques = data["Remarques"].strip() if isinstance(data["Remarques"], str) else ""
    if not remarques:
        return
    if enfant:
        if remarques not in enfant.remarque_privee:
            enfant.remarque_privee += f"\n{remarques}" if enfant.remarque_privee else remarques
            enfant.save()
    else:
        if remarques not in famille.remarques:
            famille.remarques += f"\n{remarques}" if famille.remarques else remarques
            famille.save()


def importer_ope(enfant, data):
    def _clean_nom(nom):
        if nom == "-":
            return ""
        return nom.rstrip(".")

    noms = (
        enlever_titres(data["AS OPE"].strip()) if isinstance(data["AS OPE"], str) else ""
    ).split(maxsplit=1)
    # Nom & prénom ne sont pas toujours dans le même ordre
    nom_1 = _clean_nom(noms.pop(0) if noms else "")
    nom_2 = _clean_nom(noms[0] if noms else "")
    if not nom_1:
        return
    if nom_2:
        filters = (
            Q(nom__icontains=nom_1) & Q(prenom__icontains=nom_2) |
            Q(nom__icontains=nom_2) & Q(prenom__icontains=nom_1)
        )
    else:
        filters = Q(nom__iexact=nom_1)
    role_ope, __ = Role.objects.get_or_create(
        nom="OPE",
        famille=False,
        intervenant=False,
        editeur=False
    )
    contact = role_ope.contacts.filter(filters).first()
    if not contact:
        contact = Contact.objects.create(
            nom=nom_1,
            prenom=nom_2,
        )
        contact.roles.add(role_ope)
    enfant.reseaux.add(contact)


def get_liste_noms(noms_str):
    noms_str = enlever_titres(noms_str)
    return [nom.strip(",") for nom in re.split(r"\s+et\s+|\n", noms_str)]


def get_noms_prenoms(noms, prenoms):
    """
    Reçoit une chaîne de caractères qui peut être 1 ou plusieurs noms et
    une deuxième chaîne de caractères qui peut être 1 ou plusieurs prenoms.
    Retourne une liste de tuples [(nom, prenom), ...]
    """
    noms = get_liste_noms(noms)
    prenoms = get_liste_noms(prenoms)
    nom_par_defaut = noms[-1]
    # zip les noms/prenoms en remplissant les valeurs manquantes par None
    noms_prenoms = zip_longest(noms, prenoms)
    # Remplacer les noms vide par nom_par_defaut et les prenoms vide par ""
    return [
        (nom if nom is not None else nom_par_defaut, prenom if prenom is not None else "")
        for nom, prenom in noms_prenoms
    ]


def enlever_titres(noms):
    # Supprime "M.", "Mme." ou "Mlle." (avec ou sans point)
    return re.sub(r"\b(M(\.|me|lle)?\.?)\s+", "", noms, flags=re.IGNORECASE)
