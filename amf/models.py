from django.db import models
from django.utils.dateformat import format as django_format

from common.utils import maintenant
from croixrouge.models import Famille, Personne, PrestationBase
from croixrouge.utils import format_d_m_Y

from . import choices as amf_choices


class FamilleAMFManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            suiviamf__isnull=False
        ).prefetch_related(
            models.Prefetch('membres', queryset=Personne.objects.select_related('role'))
        )

    def get_familles_actives(self):
        return self.get_queryset().select_related(
            'suiviamf'
        ).exclude(
            suiviamf__date_fin_accueil__lt=maintenant().date()
        )

    def get_familles_terminees(self):
        return self.get_queryset().select_related(
            'suiviamf'
        ).filter(
            suiviamf__date_fin_accueil__lt=maintenant().date()
        )

    def create_famille(self, **kwargs):
        kwargs.pop('_state', None)
        kwargs['typ'] = ['amf']
        famille = self.create(**kwargs)
        SuiviAMF.objects.create(
            famille=famille,
        )
        return famille


class FamilleAMF(Famille):
    class Meta:
        proxy = True
        permissions = {
            ("view_amf_stats", "Voir les statistiques AMF"),
        }

    default_typ = 'amf'
    objects = FamilleAMFManager()
    suivi_name = 'suiviamf'

    @property
    def prestations(self):
        return self.prestations_amf

    def print_coords_url(self):
        return None


class SuiviAMF(models.Model):
    famille = models.OneToOneField(FamilleAMF, on_delete=models.CASCADE)
    date_contact1 = models.DateField("Date premier contact", blank=True, null=True)
    date_entretien1 = models.DateField("Date premier entretien", blank=True, null=True)
    date_demande_spaj = models.DateField("Date dépôt de demande au SPAJ", blank=True, null=True)
    autorisation = models.CharField(
        "Statut d’autorisation",
        max_length=10,
        choices=amf_choices.AutorisationChoices,
        blank=True
    )
    date_autorisation = models.DateField("Date d’autorisation", blank=True, null=True)
    date_fin_accueil = models.DateField("Date de fin d’accueil", blank=True, null=True)
    referent_famille = models.CharField("Référent de la famille (CE)", max_length=50, blank=True)
    places_accueil_dispo = models.PositiveSmallIntegerField("Places d'accueil disponibles", blank=True, null=True)
    enfants_souhaites = models.CharField("Enfants souhaités", max_length=100, blank=True)


class PrestationAMF(PrestationBase):
    CURRENT_APP = "amf"

    famille = models.ForeignKey(
        FamilleAMF, related_name='prestations_amf', null=True, blank=True,
        on_delete=models.SET_NULL, verbose_name="Famille"
    )


class _FamilleAMF:
    def __init__(self, famille):
        self.famille = famille
        self.famille.__class__ = FamilleAMF

    def access_ok(self, user):
        return self.can_edit(user)

    def can_view(self, user):
        return user.has_perm('amf.view_familleamf')

    def can_edit(self, user):
        if user.has_perm('amf.change_familleamf') or user.is_responsable:
            return True
        return False

Famille.typ_register['amf'] = _FamilleAMF

class Formation(models.Model):
    nom = models.CharField("Nom", max_length=50)
    description = models.CharField("Courte description", blank=True)
    date = models.DateField("Date")
    heure_debut = models.TimeField("Heure de début", blank=True, null=True)
    heure_fin = models.TimeField("Heure de fin", blank=True, null=True)
    information_formateurs = models.CharField("Information sur le(s) formateur(s)", blank=True)
    lieu = models.CharField(blank=True)
    information_delais_inscription = models.CharField("Information sur le délai d'inscription", blank=True)
    participants = models.ManyToManyField(
        FamilleAMF, verbose_name="Familles participantes", related_name="formations", blank=True
    )

    class Meta:
        ordering = ['date', 'heure_debut', 'heure_fin', 'nom']

    def __str__(self):
        if self.date:
            date_formation = format_d_m_Y(self.date)
            if self.horaires is not None:
                return f"{self.nom} - {date_formation} {self.horaires}"
            return f"{self.nom} - {date_formation}"
        return "Nouvelle formation"

    @property
    def participants_potentiels(self):
        """
        A priori, ce queryset contient uniquement les familles actives.
        Cependant, le queryset peut contenir également les familles dont le suivi
        est clos mais ayant participé à la formation.
        """
        return (FamilleAMF.objects.get_familles_actives() | self.participants.all()).distinct().order_by("nom")

    @property
    def horaires(self):
        if not self.heure_debut:
            return ""
        debut = f"{self.heure_debut.hour:02d}:{self.heure_debut.minute:02d}"
        if self.heure_fin:
            fin = f"{self.heure_fin.hour:02d}:{self.heure_fin.minute:02d}"
            return f"{debut}-{fin}"
        return debut

    @property
    def date_horaires_display(self):
        date_display = django_format(self.date, 'l j F')
        if self.horaires:
            return f"{date_display}, {self.horaires}"
        return date_display
