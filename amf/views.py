from datetime import timedelta

from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Count, F
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, FormView, ListView, UpdateView

from common.utils import maintenant
from croixrouge.forms import JournalFilterForm
from croixrouge.views import BaseSuiviView, FamilleAccessRequiredMixin, FamilleUpdateViewBase, JournalAccesMixin

from . import forms
from .import_spaj import PlusieursFamillesTrouvees, importer_ligne_spaj
from .models import FamilleAMF, Formation, PrestationAMF, SuiviAMF


class AccessRequiredMixin(FamilleAccessRequiredMixin):
    famille_class = FamilleAMF


class CheckCanEditMixin:
    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied(AccessRequiredMixin.MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)


class FamilleCreateView(CreateView):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleAMF
    form_class = forms.FamilleCreateForm
    action = 'Nouvelle famille'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('amf.add_familleamf'):
            raise PermissionDenied(AccessRequiredMixin.MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        famille = form.save()
        return HttpResponseRedirect(reverse('amf-famille-edit', args=[famille.pk]))


class FamilleUpdateView(AccessRequiredMixin, SuccessMessageMixin, FamilleUpdateViewBase):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleAMF
    famille_model = FamilleAMF
    form_class = forms.FamilleUpdateForm
    success_message = 'La famille %(nom)s a bien été modifiée.'

    def get_context_data(self, *args, **kwargs):
        return super().get_context_data(
            **kwargs,
            formations_amf=self.object.formations.all(),
        )

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}


class FamilleListView(PermissionRequiredMixin, ListView):
    template_name = 'amf/famille_list.html'
    model = FamilleAMF
    paginate_by = 20
    mode = 'normal'
    normal_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'),
        ('Statut', 'autorisation'),
        ('1er contact', 'date_contact1'),
        ('1er entretien', 'date_entretien1'),
        ('Dépôt demande SPAJ', 'date_demande_spaj'),
        ('Autorisation', 'date_autorisation'),
        ('Référent (CE)', 'referent_famille'),
        ("Places disponibles", 'places_accueil_dispo'),
        ('Souhaits', 'enfants_souhaites'),
    ]
    permission_required = 'amf.view_familleamf'
    termines_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'),
        ('Statut', 'autorisation'),
        ("Date de fin de l'accueil", 'date_fin_accueil'),
    ]

    def get(self, request, *args, **kwargs):
        request.session['current_app'] = 'amf'
        self.filter_form = forms.FamilleFilterForm(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if self.mode == 'normal':
            familles = FamilleAMF.objects.get_familles_actives()
        elif self.mode == 'termines':
            familles = FamilleAMF.objects.get_familles_terminees()

        familles = familles.annotate(
            date_contact1=F('suiviamf__date_contact1'),
            date_entretien1=F('suiviamf__date_entretien1'),
            date_demande_spaj=F('suiviamf__date_demande_spaj'),
            date_autorisation=F('suiviamf__date_autorisation'),
            date_fin_accueil=F('suiviamf__date_fin_accueil'),
            referent_famille=F('suiviamf__referent_famille'),
            places_accueil_dispo=F('suiviamf__places_accueil_dispo'),
            enfants_souhaites=F('suiviamf__enfants_souhaites'),
        )

        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cols = self.normal_cols if self.mode == 'normal' else self.termines_cols

        context.update({
            'labels': [c[0] for c in cols],
            'col_keys': [c[1] for c in cols],
            'form': self.filter_form,
        })
        return context


class AMFSuiviView(AccessRequiredMixin, JournalAccesMixin, BaseSuiviView):
    template_name = 'amf/suivi_edit.html'
    model = SuiviAMF
    famille_model = FamilleAMF
    form_class = forms.SuiviAMFForm

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}


class PrestationBaseView(AccessRequiredMixin):
    model = PrestationAMF
    famille_model = FamilleAMF
    form_class = forms.PrestationAMFForm
    template_name = 'amf/prestation_edit.html'

    def dispatch(self, *args, **kwargs):
        self.famille = get_object_or_404(self.famille_model, pk=self.kwargs['pk']) if self.kwargs.get('pk') else None
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille}

    def get_success_url(self):
        if self.famille:
            return reverse('amf-journal-list', args=[self.famille.pk])
        return reverse('amf-prestation-gen-list')


class PrestationListView(PrestationBaseView, JournalAccesMixin, ListView):
    template_name = 'amf/prestation_list.html'
    model = PrestationAMF
    paginate_by = 15
    context_object_name = 'prestations'

    def get(self, request, **kwargs):
        self.filter_form = JournalFilterForm(famille=self.famille, data=request.GET or None)
        return super().get(request, **kwargs)

    def get_queryset(self):
        if self.famille:
            prestations = self.famille.prestations.all().order_by('-date_prestation')
            if self.filter_form.is_bound and self.filter_form.is_valid():
                prestations = self.filter_form.filter(prestations)
            return prestations
        return self.request.user.prestations_amf.filter(famille=None)

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs, filter_form=self.filter_form)


class PrestationCreateView(PrestationBaseView, CreateView):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('amf.add_prestationamf'):
            raise PermissionDenied("Vous n'avez pas les droits pour ajouter une prestation.")
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def form_valid(self, form):
        if self.famille:
            form.instance.famille = self.famille
        form.instance.auteur = self.request.user
        if 'duree' not in form.cleaned_data:
            form.instance.duree = timedelta()
        return super().form_valid(form)


class PrestationUpdateView(CheckCanEditMixin, PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def delete_url(self):
        fam_id = self.famille.pk if self.famille else 0
        return reverse('amf-prestation-delete', args=[fam_id, self.object.pk])


class PrestationDeleteView(PrestationBaseView, DeleteView):
    template_name = 'croixrouge/object_confirm_delete.html'
    pk_url_kwarg = 'obj_pk'
    form_class = DeleteView.form_class


class ImportationSPAJView(PermissionRequiredMixin, FormView):
    permission_required = 'amf.add_familleamf'
    form_class = forms.ImportationSPAJForm
    template_name = "amf/importation_spaj.html"

    def form_valid(self, form):
        nb_erreurs = 0
        nb_lignes = 0
        info_messages = []
        for line in form.famille_lines():
            nb_lignes += 1
            try:
                info_messages.append(importer_ligne_spaj(line))
            except PlusieursFamillesTrouvees as e:
                messages.error(self.request, e)
                nb_erreurs += 1
        if info_messages:
            messages.info(self.request, "\n".join(info_messages))
        if nb_erreurs:
            nb_lignes_importees = nb_lignes - nb_erreurs
            messages.info(
                self.request,
                f"{nb_lignes_importees}/{nb_lignes} lignes ont été importées.\n"
                f"Veuillez trouver le détails des {nb_erreurs} erreurs ci-dessus."
            )
        else:
            messages.success(self.request, f"Le fichier a bien été importé ({nb_lignes} lignes)")
        return HttpResponseRedirect(self.request.path)


class FormationListView(PermissionRequiredMixin, ListView):
    context_object_name = 'formations'
    model = Formation
    paginate_by = 15
    permission_required = 'amf.view_formation'
    template_name = 'amf/formations_list.html'

    def get(self, request, **kwargs):
        self.formations_passees = "passées" in request.GET.keys()
        self.filter_form = forms.FormationsFilterForm(data=request.GET or None)
        return super().get(request, **kwargs)

    def get_queryset(self):
        formations = Formation.objects.annotate(nb_participants=Count("participants"))
        if self.formations_passees:
            formations = formations.filter(
                date__lt=maintenant().date()
            ).order_by(
                "-date",
                "-heure_debut",
                "-heure_fin",
                "nom",
            )
        else:
            formations = formations.filter(
                date__gte=maintenant().date(),
            ).order_by(
                "date",
                "heure_debut",
                "heure_fin",
                "nom",
            )
        if self.filter_form.is_bound and self.filter_form.is_valid():
            return self.filter_form.filter(formations)
        return formations

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            **kwargs,
            filter_form=self.filter_form,
            page="passées" if self.formations_passees else "à venir",
        )


class FormationCreateView(PermissionRequiredMixin, CreateView):
    form_class = forms.FormationEditForm
    model = Formation
    permission_required = 'amf.add_formation'
    template_name = 'amf/formation_edit.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            **kwargs,
            created=True,
            page="nouveau",
        )

    def get_success_url(self):
        return reverse('amf-formation-edit', args=[self.object.pk])


class FormationUpdateView(PermissionRequiredMixin, UpdateView):
    form_class = forms.FormationEditForm
    model = Formation
    permission_required = 'amf.change_formation'
    template_name = 'amf/formation_edit.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            **kwargs,
            created=False,
            participants=self.object.participants.all(),
            inscription_media=forms.FormationInscriptionForm(instance=self.object).media,
            page="edit",
        )

    def get_success_url(self):
        return reverse('amf-formation-edit', args=[self.object.pk])


class FormationInscriptionView(PermissionRequiredMixin, UpdateView):
    form_class = forms.FormationInscriptionForm
    model = Formation
    permission_required = 'amf.change_formation'
    template_name = 'croixrouge/form_in_popup.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        return reverse('amf-formation-edit', args=[self.object.pk])

    @property
    def titre_page(self):
        return self.object


class ParticipantsFormationAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = 'amf.view_familleamf'

    def get_queryset(self):
        formation_pk = self.request.resolver_match.kwargs["pk"]
        formation = get_object_or_404(Formation, pk=formation_pk)
        qs = formation.participants_potentiels
        if self.q:
            qs = qs.filter(nom__icontains=self.q)
        return qs
