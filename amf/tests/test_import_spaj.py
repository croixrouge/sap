from datetime import datetime
from pathlib import Path

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from croixrouge.models import Personne, Role, Utilisateur

from ..import_spaj import importer_ligne_spaj
from ..models import FamilleAMF


class ImportSpajTests(TestCase):
    LIGNE_TEMPLATE = {
        'CE': 'LDM',  # Réréfent famille
        "Famille d'accueil": 'Haddock',
        'Prénoms de la famille': 'Quentin',
        'Désignation': 'FAO',  # Type d'accueil
        'Statut de la famille': 'Recours SMI',
        'Accueil en cours': 'oui',
        'Concept': 'oui',  # N'est pas à importer
        'Adresse de la famille': 'Rue de la Clé 7',
        'NPA Localité': '2052 Fontainemelon',
        'Téléphone de la famille': '078 111 22 11',
        'Courriel de la famille': 'abc@example.org',
        "Places d'accueil disponibles": 0,
        'Enfants souhaités': '-',
        "Nom de l'enfant": 'Blanc',
        "Prénom de l'enfant":'Gérard',
        'Année de naissance': datetime(2008, 10, 1, 0, 0),
        'Début de placement': datetime(2019, 5, 28, 0, 0),
        'Fin de placement': '-',
        'Provenance': 'Famille biologique',
        'AS OPE': 'Jean Bon',
        'Remarques': '-',
        '_num_ligne': 2}

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'user_amf', 'user_amf@example.org', nom='Amf', prenom='Prénom', sigle='AP',
        )
        cls.user.user_permissions.add(Permission.objects.get(codename='add_familleamf'))

    def test_vue_importation_spaj(self):
        self.client.force_login(self.user)
        file_path = Path(__file__).parent / "test_import_spaj.xlsx"
        with file_path.open('rb') as fh:
            response = self.client.post(reverse('amf-importation'), data={'fichier': fh}, follow=True)
        self.assertContains(response, "Le fichier a bien été importé")
        self.assertEqual(FamilleAMF.objects.count(), 5)

    def test_vue_importation_spaj_entetes_manquants(self):
        self.client.force_login(self.user)
        file_path = Path(__file__).parent / "test_import_spaj_missing_heads.xlsx"
        with file_path.open('rb') as fh:
            response = self.client.post(reverse('amf-importation'), data={'fichier': fh}, follow=True)
        self.assertContains(
            response,
            "Les en-têtes suivants manquent dans le fichier: Adresse de la famille, Désignation"
        )

    def test_import_avec_enfant(self):
        importer_ligne_spaj(self.LIGNE_TEMPLATE)

        famille = FamilleAMF.objects.get()
        self.assertEqual(famille.nom, 'Haddock')

        parent_accueil = Personne.objects.get(famille=famille, role=Role.objects.get_parent_famille_accueil())
        self.assertEqual(parent_accueil.nom, 'Haddock')
        self.assertEqual(parent_accueil.prenom, 'Quentin')

        enfant = Personne.objects.get(famille=famille, role=Role.objects.get_enfant_suivi())
        self.assertEqual(enfant.nom, 'Blanc')
        self.assertEqual(enfant.prenom, 'Gérard')

    def test_import_sans_enfant(self):
        data = self.LIGNE_TEMPLATE.copy()
        data.update({
            "Nom de l'enfant": '',
            "Prénom de l'enfant":'',
            'Année de naissance': '-',
            'Début de placement': '-',
            'Fin de placement': '-',
            'Provenance': '',
            'AS OPE': '-',
        })
        importer_ligne_spaj(data)

        famille = FamilleAMF.objects.get()
        self.assertEqual(famille.nom, 'Haddock')

        parent_accueil = Personne.objects.get(famille=famille, role=Role.objects.get_parent_famille_accueil())
        self.assertEqual(parent_accueil.nom, 'Haddock')
        self.assertEqual(parent_accueil.prenom, 'Quentin')

        self.assertFalse(Personne.objects.filter(famille=famille, role=Role.objects.get_enfant_suivi()).exists())
