from datetime import date

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.urls import reverse
from freezegun import freeze_time

from croixrouge.models import Utilisateur

from ..models import FamilleAMF, PrestationAMF


class PrestationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.non_autorise = Utilisateur.objects.create_user('user', 'user@example.org', nom='Pas AMF')

        cls.autorise = Utilisateur.objects.create_user(
            'user_amf', 'user_amf@example.org', nom='Amf', prenom='Prénom', sigle='AP',
        )
        prestation_ct = ContentType.objects.get(app_label="amf", model="prestationamf")
        cls.autorise.user_permissions.add(
            Permission.objects.get(codename='view_familleamf'),
            Permission.objects.get(codename='view_prestationamf', content_type=prestation_ct),
            Permission.objects.get(codename='add_prestationamf', content_type=prestation_ct),
            Permission.objects.get(codename='change_prestationamf', content_type=prestation_ct),
            Permission.objects.get(codename='edit_prest_prev_month', content_type=prestation_ct),
        )
        cls.autorise.user_permissions.add(
            Permission.objects.get(
                codename='change_familleamf',
                content_type=ContentType.objects.get(app_label="amf", model="familleamf"),
            ),
        )

        cls.prestation = PrestationAMF.objects.create(
            famille=FamilleAMF.objects.create_famille(nom='Dupont'),
            auteur=cls.autorise,
            texte="Prestation#1",
            date_prestation=date(2024, 11, 14),
            duree='00:30',
        )

    @freeze_time("2024-11-15")
    def test_acces_list_prestations(self):
        url = reverse("amf-journal-list", args=[self.prestation.famille.pk])

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n’avez pas la permission d’accéder à cette page.",
            status_code=403,
            html=True,
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, "Prestation#1", status_code=200, html=True)
