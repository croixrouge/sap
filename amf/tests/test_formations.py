from datetime import date, time, timedelta
from unittest.mock import patch

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core import mail
from django.core.management import call_command
from django.template.loader import render_to_string
from django.test import TestCase, override_settings
from django.urls import reverse
from freezegun import freeze_time

from common.utils import maintenant
from croixrouge.models import Utilisateur

from ..management.commands.envoi_prochaines_formations import Command as EnvoiCommand
from ..models import FamilleAMF, Formation


class FormationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.non_autorise = Utilisateur.objects.create_user('user', 'user@example.org', nom='Pas AMF')

        cls.autorise = Utilisateur.objects.create_user(
            'user_amf', 'user_amf@example.org', nom='Amf', prenom='Prénom', sigle='AP',
        )
        formation_ct = ContentType.objects.get(app_label="amf", model="formation")
        cls.autorise.user_permissions.add(
            Permission.objects.get(codename='view_familleamf'),
            Permission.objects.get(codename='view_formation', content_type=formation_ct),
            Permission.objects.get(codename='add_formation', content_type=formation_ct),
            Permission.objects.get(codename='change_formation', content_type=formation_ct),
        )

        cls.formation = Formation.objects.create(
            nom="HelloWorld!",
            date=date(2024, 11, 4),
            heure_debut=time(19, 0),
            heure_fin=time(22, 0),
        )

    @freeze_time("2024-11-04")
    def test_acces_list_formations(self):
        url = reverse("amf-formation-list")

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, "HelloWorld!", status_code=200, html=True)

    def test_acces_add_formation(self):
        url = reverse("amf-formation-add")

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, "Nouvelle formation", status_code=200, html=True)
        self.assertContains(
            response,
            "Il est nécessaire d'enregistrer la nouvelle formation pour y inscrire des participants.",
            status_code=200,
            html=True,
        )

    def test_acces_edit_formation(self):
        url = reverse("amf-formation-edit", args=[self.formation.pk])

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, f"Formation - {self.formation}", status_code=200, html=True)

    def test_acces_inscription_formation(self):
        url = reverse("amf-formation-inscription", args=[self.formation.pk])

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, str(self.formation), html=True)
        self.assertContains(response, "Familles participantes")

    def test_inscription_formation(self):
        url = reverse("amf-formation-inscription", args=[self.formation.pk])
        self.client.force_login(self.autorise)
        self.assertEqual(self.formation.participants.count(), 0)
        self.client.post(url, data={
            "participants":[
                FamilleAMF.objects.create_famille(nom="Famille 1").pk,
                FamilleAMF.objects.create_famille(nom="Famille 2").pk,
            ]
        })
        self.assertEqual(self.formation.participants.count(), 2)


class InfoProchainesFormationsTest(TestCase):
    @freeze_time("2024-11-15")
    def test_envoi_prochaines_formations(self):
        Formation.objects.create(
            nom="HelloWorld!",
            date=EnvoiCommand.get_1_mois_suivant(maintenant().date()),
        )
        call_command('envoi_prochaines_formations')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Rappel des formations - Décembre 2024")


class NotificationsSMSTest(TestCase):

    @override_settings(ENVOI_SMS_NUMEROS_TEST=["079 000 00 00"])
    @patch("croixrouge.utils.send_sms")
    def test_notification(self, mocked):
        date_formation = (maintenant() + timedelta(days=2)).date()
        formation = Formation.objects.create(
            nom="HelloWorld!",
            date=date_formation,
        )
        call_command('envoi_notification_sms')
        mocked.assert_called_once_with(
            render_to_string("amf/sms/notification_sms.txt", context={"formation": formation}),
            "079 000 00 00",
            (
                "Erreur d’envoi de la notification SMS pour « 079 000 00 00 » concernant "
                f"la formation « {formation} ({formation.id}) »."
            ),
        )
