from datetime import date

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.urls import reverse
from django.utils.functional import cached_property

from amf import choices as amf_choices
from amf.models import FamilleAMF, PrestationAMF
from common.utils import maintenant
from croixrouge.models import LibellePrestation, Personne, Role, Utilisateur
from croixrouge.stat_utils import Month


class StatsAMFTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.non_autorise = Utilisateur.objects.create_user('user', 'user@example.org', nom='Pas AMF')

        cls.autorise = Utilisateur.objects.create_user(
            'user_amf', 'user_amf@example.org', nom='Amf', prenom='Prénom', sigle='AP',
        )
        famille_ct = ContentType.objects.get(app_label="amf", model="familleamf")
        cls.autorise.user_permissions.add(
            Permission.objects.get(codename='view_amf_stats', content_type=famille_ct),
        )
        cls.stats_url = reverse("amf-stats-familles")

    @cached_property
    def libelle_prestation_rencontre(self):
        return LibellePrestation.objects.get_or_create(
            code="amf01", nom="Rencontre avec la famille", unite="amf"
        )[0]

    @cached_property
    def libelle_prestation_supervision(self):
        return LibellePrestation.objects.get_or_create(
            code="amf02", nom="Supervision", unite="amf"
        )[0]

    @cached_property
    def libelle_prestation_info(self):
        return LibellePrestation.objects.get_or_create(
            code="amf03", nom="Information", unite="amf"
        )[0]

    def creer_enfant_suivi(self, **kwargs):
        defaults = {
            "role": Role.objects.get_enfant_suivi(),
            "famille": None,  # doit être créée en base plus loin
            "nom": "Toto",
            "prenom": "Tutu",
        }
        for key, value in defaults.items():
            kwargs.setdefault(key, value)
        if not kwargs["famille"]:
            kwargs["famille"] = FamilleAMF.objects.create_famille(nom="Famille test")
        return Personne.objects.create_personne(**kwargs)

    def creer_prestation(self, **kwargs):
        defaults = {
            "famille": None,  # doit être créée en base plus loin
            "auteur": self.autorise,
            "texte": "Ceci est une prestation",
            "date_prestation": maintenant().date(),
            "duree": '00:30',
            "lib_prestation": self.libelle_prestation_rencontre,
        }
        for key, value in defaults.items():
            kwargs.setdefault(key, value)
        if not kwargs["famille"]:
            kwargs["famille"] = FamilleAMF.objects.create_famille(nom="Famille test")
        return PrestationAMF.objects.create(**kwargs)

    def test_acces_stats(self):
        # Accès sans login
        response = self.client.get(self.stats_url)
        self.assertRedirects(response, f"{reverse('login')}?next={self.stats_url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(self.stats_url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(self.stats_url)
        self.assertContains(response, "Statistiques AMF - Famille", status_code=200)

    def test_nombre_visites(self):
        # Créer deux prestations pour une même famille sur janvier/février
        prest = self.creer_prestation(date_prestation=date(2025, 1, 1))
        self.creer_prestation(famille=prest.famille, date_prestation=date(2025, 2, 1))

        # Créer une prestation pour une autre famille sur février
        self.creer_prestation(date_prestation=date(2025, 2, 1))

        # Créer une prestation "information" qui ne doit pas être comptabilisée
        self.creer_prestation(date_prestation=date(2025, 1, 1), lib_prestation=self.libelle_prestation_info)

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=2&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Visites dans les familles"],
            {
                Month(2025, 1): 1,
                Month(2025, 2): 2,
                "total": 3,
            },
        )

    def test_familles_visitees(self):
        # Créer deux prestations pour une même famille sur janvier/février
        prest = self.creer_prestation(date_prestation=date(2025, 1, 1))
        self.creer_prestation(famille=prest.famille, date_prestation=date(2025, 2, 1))

        # Créer une prestation pour une autre famille sur février
        self.creer_prestation(date_prestation=date(2025, 2, 1))

        # Créer une prestation "information" qui ne doit pas être comptabilisée
        self.creer_prestation(date_prestation=date(2025, 1, 1), lib_prestation=self.libelle_prestation_info)

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=2&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Familles visitées"],
            {
                Month(2025, 1): 1,
                Month(2025, 2): 2,
                "total": 2,
            },
        )

    def test_nb_contact1(self):
        FamilleAMF.objects.create_famille(nom="Famille sans date_contact1")
        suivi = FamilleAMF.objects.create_famille(nom="Famille avec date_contact1").suivi
        suivi.date_contact1 = date(2025, 1, 1)
        suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=2&end_year=2025")

        self.assertEqual(
            response.context["stats"]["1ers contacts"],
            {
                Month(2025, 1): 1,
                "total": 1,
            },
        )

    def test_nb_entretien1(self):
        FamilleAMF.objects.create_famille(nom="Famille sans date_entretien1")
        suivi = FamilleAMF.objects.create_famille(nom="Famille avec date_entretien1").suivi
        suivi.date_entretien1 = date(2025, 1, 1)
        suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=2&end_year=2025")

        self.assertEqual(
            response.context["stats"]["1ers entretiens"],
            {
                Month(2025, 1): 1,
                "total": 1,
            },
        )

    def test_nb_demande_spaj(self):
        FamilleAMF.objects.create_famille(nom="Famille sans date_demande_spaj")
        suivi = FamilleAMF.objects.create_famille(nom="Famille avec date_demande_spaj").suivi
        suivi.date_demande_spaj = date(2025, 1, 1)
        suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=2&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Demandes SPAJ"],
            {
                Month(2025, 1): 1,
                "total": 1,
            },
        )

    def test_nb_familles_avec_enfant_suivi(self):
        # Fam A - enfant suivi depuis le mois de janvier, stoppé en février
        fam_a = self.creer_enfant_suivi(
            amf_debut_de_placement=date(2025, 1, 1),
            amf_fin_de_placement=date(2025, 2, 1),
            nom="1",
        ).famille
        # Fam A - enfant suivi depuis le mois de février, stoppé en mars
        self.creer_enfant_suivi(
            famille=fam_a,
            amf_debut_de_placement=date(2025, 2, 1),
            amf_fin_de_placement=date(2025, 3, 1),
            nom="2",
        )

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=12&start_year=2024&end_month=4&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Familles d'accueil accueillant un/des enfant(s)"],
            {
                "total": 1,
                Month(2024, 12): 0,
                Month(2025, 1): 1,
                Month(2025, 2): 1,
                Month(2025, 3): 1,
                Month(2025, 4): 0,
            },
        )
    def test_nb_familles_autorisees(self):
        # On test ici via "nb_familles_en_attente" mais on ne fait pas de famille
        # avec enfant suivi (toutes les familles sont en attente et seront comptabilisées)

        # Fam A - auotirsee en janvier, fin d'accueil en mars
        fam_a = FamilleAMF.objects.create_famille(nom="Fam A")
        fam_a.suivi.date_autorisation = date(2025, 1, 1)
        fam_a.suivi.autorisation = amf_choices.AutorisationChoices.AUTORISEE.value
        fam_a.suivi.date_fin_accueil = date(2025, 3, 1)
        fam_a.suivi.save()

        # Fam B - auotirsee en février, fin d'accueil en février
        fam_b = FamilleAMF.objects.create_famille(nom="Fam B")
        fam_b.suivi.date_autorisation = date(2025, 2, 1)
        fam_b.suivi.autorisation = amf_choices.AutorisationChoices.AUTORISEE.value
        fam_b.suivi.date_fin_accueil = date(2025, 2, 25)
        fam_b.suivi.save()

        # Fam C - auotirsee en mars, pas de fin d'accueil
        fam_c = FamilleAMF.objects.create_famille(nom="Fam C")
        fam_c.suivi.date_autorisation = date(2025, 3, 1)
        fam_c.suivi.autorisation = amf_choices.AutorisationChoices.AUTORISEE.value
        fam_c.suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=12&start_year=2024&end_month=4&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Familles en attente d'accueillir un enfant"],
            {
                "total": 3,
                Month(2024, 12): 0,
                Month(2025, 1): 1,
                Month(2025, 2): 2,
                Month(2025, 3): 2,
                Month(2025, 4): 1,
            },
        )

    def test_nb_familles_autorisees_sans_enfant(self):
        # On test ici la comptabilisation des familles en fonction de l'accueil des enfants
        # Les familles ici sont toutes autorisées depuis l'année précédente
        # (logique de comptabilisation des familles autorisées dans `test_nb_familles_autorisees`)

        # Fam A - début de placement en janvier, fin en février
        fam_a = self.creer_enfant_suivi(
            amf_debut_de_placement=date(2025, 1, 1),
            amf_fin_de_placement=date(2025, 2, 1),
        ).famille
        fam_a.suivi.date_autorisation = date(2024, 1, 1)
        fam_a.suivi.autorisation = amf_choices.AutorisationChoices.AUTORISEE.value
        fam_a.suivi.save()

        # Fam B - début de placement en février, pas de fin de placement
        fam_b = self.creer_enfant_suivi(
            amf_debut_de_placement=date(2025, 2, 1),
        ).famille
        fam_b.suivi.date_autorisation = date(2024, 1, 1)
        fam_b.suivi.autorisation = amf_choices.AutorisationChoices.AUTORISEE.value
        fam_b.suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=12&start_year=2024&end_month=4&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Familles en attente d'accueillir un enfant"],
            {
                "total": 0,
                Month(2024, 12): 2,
                Month(2025, 1): 1,
                Month(2025, 2): 0,
                Month(2025, 3): 1,
                Month(2025, 4): 1,
            },
        )

    def test_nb_enfants_suivis(self):
        # enfant suivi depuis le mois de janvier, stoppé en février
        self.creer_enfant_suivi(
            amf_debut_de_placement=date(2025, 1, 1),
            amf_fin_de_placement=date(2025, 2, 1), nom="1",
        )
        # enfant suivi depuis le mois de février
        self.creer_enfant_suivi(amf_debut_de_placement=date(2025, 2, 1), nom="2")

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=12&start_year=2024&end_month=3&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Enfants en famille d'accueil"],
            {
                "total": 2,
                Month(2024, 12): 0,
                Month(2025, 1): 1,
                Month(2025, 2): 2,
                Month(2025, 3): 1,
            },
        )

    def test_nb_enfants_suivis_lorsque_suivi_famille_termine(self):
        """
        Les enfants sans "amf_fin_de_placement" pour un suivi dont la
        date_fin_accueil est passée ne sont pas comptés
        """
        # enfant suivi depuis le mois de janvier, suivi famille stoppé en février
        suivi = self.creer_enfant_suivi(
            amf_debut_de_placement=date(2025, 1, 1),
        ).famille.suivi
        suivi.date_fin_accueil = date(2025, 2, 1)
        suivi.save()

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=12&start_year=2024&end_month=3&end_year=2025")

        self.assertEqual(
            response.context["stats"]["Enfants en famille d'accueil"],
            {
                "total": 1,
                Month(2024, 12): 0,
                Month(2025, 1): 1,
                Month(2025, 2): 1,
                Month(2025, 3): 0,
            },
        )

    def test_nb_enfants_suivis_par_age(self):
        # un enfant du mois de janvier 2020 (4 ans en janvier 2025, 5 ans en février)
        self.creer_enfant_suivi(
            date_naissance=date(2020, 1, 15),
            amf_debut_de_placement=date(2024, 6, 1),
        )
        # un enfant du mois de février 2012 (12 ans en février 2025, 13 ans en mars)
        self.creer_enfant_suivi(
            date_naissance=date(2012, 2, 15),
            amf_debut_de_placement=date(2024, 6, 1),
        )

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=3&end_year=2025")

        stats_0_4_ans = response.context["stats"]["Enfants en famille d'accueil - 0-4 ans"]
        stats_5_12_ans = response.context["stats"]["Enfants en famille d'accueil - 5-12 ans"]
        stats_13_ans = response.context["stats"]["Enfants en famille d'accueil - 13 ans et plus"]

        self.assertEqual(stats_0_4_ans[Month(2025, 1)], 1)
        self.assertEqual(stats_5_12_ans[Month(2025, 1)], 1)
        self.assertEqual(stats_13_ans[Month(2025, 1)], 0)

        self.assertEqual(stats_0_4_ans[Month(2025, 2)], 0)
        self.assertEqual(stats_5_12_ans[Month(2025, 2)], 2)
        self.assertEqual(stats_13_ans[Month(2025, 2)], 0)

        self.assertEqual(stats_0_4_ans[Month(2025, 3)], 0)
        self.assertEqual(stats_5_12_ans[Month(2025, 3)], 1)
        self.assertEqual(stats_13_ans[Month(2025, 3)], 1)

        self.assertEqual(stats_0_4_ans["total"], 1)
        self.assertEqual(stats_5_12_ans["total"], 1)
        self.assertEqual(stats_13_ans["total"], 0)

    def test_nb_enfants_suivis_par_genre(self):
        # un enfant fille placée au mois de janvier, fin de placement au mois de février
        self.creer_enfant_suivi(
            genre="F",
            amf_debut_de_placement=date(2025, 1, 1),
            amf_fin_de_placement=date(2025, 2, 1),
        )
        # un garçon placé au mois de février
        self.creer_enfant_suivi(
            genre="M",
            amf_debut_de_placement=date(2025, 2, 1),
        )

        self.client.force_login(self.autorise)
        response = self.client.get(f"{self.stats_url}?start_month=1&start_year=2025&end_month=3&end_year=2025")

        stats_filles = response.context["stats"]["Enfants en famille d'accueil - filles"]
        stats_garcons = response.context["stats"]["Enfants en famille d'accueil - garçons"]
        self.assertEqual(stats_filles[Month(2025, 1)], 1)
        self.assertEqual(stats_garcons[Month(2025, 1)], 0)
        self.assertEqual(stats_filles[Month(2025, 2)], 1)
        self.assertEqual(stats_garcons[Month(2025, 2)], 1)
        self.assertEqual(stats_filles[Month(2025, 3)], 0)
        self.assertEqual(stats_garcons[Month(2025, 3)], 1)
        self.assertEqual(stats_filles["total"], 1)
        self.assertEqual(stats_garcons["total"], 1)
