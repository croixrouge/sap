from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from croixrouge.models import Utilisateur

from ..models import FamilleAMF


class AMFTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.non_autorise = Utilisateur.objects.create_user('user', 'user@example.org', nom='Pas AMF')

        cls.autorise = Utilisateur.objects.create_user(
            'user_amf', 'user_amf@example.org', nom='Amf', prenom='Prénom', sigle='AP',
        )
        cls.autorise.user_permissions.add(
            Permission.objects.get(codename='view_familleamf'),
            Permission.objects.get(codename='add_familleamf'),
        )

    def test_acces_list_famille(self):
        url = reverse("amf-famille-list")
        FamilleAMF.objects.create_famille(nom="Dupont")

        # Accès sans login
        response = self.client.get(url)
        self.assertRedirects(response, f"{reverse('login')}?next={url}")

        # Accès personne non autorisée
        self.client.force_login(self.non_autorise)
        response = self.client.get(url)
        self.assertContains(
            response,
            "Vous n'avez pas la permission d'accéder au contenu de cette page.",
            status_code=403,
            html=True
        )

        # Accès personne autorisée
        self.client.force_login(self.autorise)
        response = self.client.get(url)
        self.assertContains(response, "Dupont", status_code=200, html=True)
