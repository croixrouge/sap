from django.db import models


class AutorisationChoices(models.TextChoices):
    EN_COURS = "en_cours", "En cours"
    AUTORISEE = "autorisee", "Autorisée"
    RECOURS_SMI = "rec_smi", "Recours SMI"
    CESSATION = "cessation", "Cessation"

    @classmethod
    def get_key_by_label(cls, label, insensitive=False):
        if insensitive:
            label = label.lower()
        for key, value in cls.choices:
            if insensitive:
                value = value.lower()
            if value == label:
                return key
        return ""


class TypeAccueilChoices(models.TextChoices):
    FAO = "FAO", "Famille d’accueil par opportunité"
    RELAIS_FA = "Relais FA", "Relais d’une famille d’accueil"
    INTRA = "Intra", "Intrafamilial"
    INTRA_RELAIS = "Intra relais", "Relais intrafamilial"
    RELAIS_IES = "Relais IES", "Relais d’une institution d’éducation spécialisée"
    FA = "FA", "Famille d’accueil"
    SMI = "SMI", "Dossier transmis par le Service des Migrations"
    RELAIS_BIOLOGIQUE = "Relais biologique", "Relais d’une famille biologique"


class ProvenanceChoices(models.TextChoices):
    FAMILLE_BIOLOGIQUE = "Famille biologique", "Famille biologique"
    IES = "IES", "Institution d’Education Spécialisée"
    AUTRE_FA = "Autre FA", "Autre famille d’accueil"
    CENTRE_COUVET = "Centre Couvet", "Centre Couvet"
    AUTRE = "Autre", "Autre"
