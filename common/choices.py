

AUTORITE_PARENTALE_CHOICES = (
    ('conjointe', 'Conjointe'),
    ('pere', 'Père'),
    ('mere', 'Mère'),
    ('tutelle', 'Tutelle')
)


DEMARCHE_CHOICES = (
    ('volontaire', 'Volontaire'),
    ('contrainte', 'Contrainte'),
    ('post_placement', 'Post placement'),
    ('non_placement', 'Eviter placement')
)


MANDATS_OPE_CHOICES = (
    ('volontaire', 'Mandat volontaire'),
    ('curatelle', 'Curatelle 308'),
    ('referent', 'Référent'),
    ('enquete', 'Enquête'),
    ('tutelle', 'Curatelle de portée générale'),
)


MOTIF_DEMANDE_CHOICES = (
    ('accompagnement', 'Accompagnement psycho-éducatif'),
    ('integration', 'Aide à l’intégration'),
    ('demande', 'Elaboration d’une demande (contrainte)'),
    ('crise', 'Travail sur la crise'),
    ('post-placement', 'Post-placement'),
    ('pre-placement', 'Pré-placement'),
    ('violence', 'Violence / maltraitances'),
)


MOTIFS_FIN_SUIVI_CHOICES = (
    ('desengagement', 'Désengagement'),
    ('evol_positive', 'Autonomie familiale'),
    ('relai_amb', 'Relai vers ambulatoire'),
    ('relai', 'Relai vers autre service'),
    ('placement', 'Placement'),
    ('non_aboutie', 'Demande non aboutie'),
    ('non_dispo', 'Pas de disponibilités/place'),
    ('erreur', 'Erreur de saisie'),
    ('autres', 'Autres'),  # Obsolète (#435)
)


PROVENANCE_DESTINATION_CHOICES = (
    ('famille', 'Famille'),
    ('ies-ne', 'IES-NE'),
    ('ies-hc', 'IES-HC'),
    ('aemo', 'SAEMO'),
    ('fah', "Famille d'accueil"),
    ('refug', "Centre d’accueil réfugiés"),
    ('hopital', "Hôpital"),
    ('autre', 'Autre'),  # Obsolète (#435)
)


SERVICE_ORIENTEUR_CHOICES = (
    ('famille', 'Famille'),
    ('ope', 'OPE'),
    ('spe', 'SPE'),
    ('cnpea', 'CNPea'),
    ('ecole', 'École'),
    ('res_prim', 'Réseau primaire'),
    ('res_sec', 'Réseau secondaire'),
    ('pediatre', 'Pédiatre'),
    ('autre', 'Autre'),
)


TYPE_GARDE_CHOICES = (
    ('partage', 'garde partagée'),
    ('droit', 'droit de garde'),
    ('visite', 'droit de visite'),
)

STATUT_FINANCIER_CHOICES = (
    ('ai', 'AI PC'),
    ('gsr', 'GSR'),
    ('osas', 'OSAS'),
    ('revenu', 'Revenu'),
    ('chomage', 'Chômage'),
)


STATUT_MARITAL_CHOICES = (
    ('celibat', 'Célibataire'),
    ('mariage', 'Marié'),
    ('pacs', 'PACS'),
    ('concubin', 'Concubin'),
    ('veuf', 'Veuf'),
    ('separe', 'Séparé'),
    ('divorce', 'Divorcé')
)

UNITE_CROIXROUGE_CHOICES = (
    ('amf', 'AMF'),
    ('cipe', 'IPE'),
    ('sof', 'SOF'),
    ('spe', 'SPE'),
)
